{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Dummy unit to make sure all classes which are
  not directly referenced from the game code are serialized
  It's not necessary to reference all the classes here,
  only those that are not referenced from anywhere
  (and no need to remove them if they get referenced :) }
unit GameLoadSerializedClasses;

{$INCLUDE compilerconfig.inc}

interface

implementation
uses
  { Actions }
  GameActionAlarm,
  GameActionAlarmFlee,
  GameActionAlwaysFlee,
  GameActionCatchAndHold,
  GameActionDancingLights,
  GameActionDreamCatcher,
  GameActionGlueBeetle,
  GameActionKnockback,
  GameActionManacler,
  GameActionMoveGhost,
  GameActionRamTarget,
  GameActionShootTarget,
  GameActionSpawnMinions,
  GameActionSpawnTrapsAround,
  GameActionTrapMarkTarget,
  GameActionTrapSpawnMonsters,
  GameActionTrapSpawnVacuumStealers,
  GameActionPlayerHeld,
  GameActionPlayerStunned,
  GameActionPlayerUnconscious,
  GameActionPlayerZeroG,
  { AIs }
  GameAiNone,
  GameAiBasic,
  GameAiMimic,
  GameAiTrap,
  GameAiTrapper,
  GameAiVacuumCleaner,
  { Marks }
  GameMarkHurtTarget,
  GameMarkBindTarget,
  GameMarkBindTargetTrap,
  GameMarkBreakClothes,
  GameMarkCatchAndHold,
  GameMarkDamageInventory,
  GameMarkUndressAndTeleportTarget,
  GameMarkDice,
  GameMarkDisrobeTarget,
  GameMarkEquipSelf,
  GameMarkGrabStraitjacket,
  GameMarkTickleTarget,
  GameMarkPunishClothes,
  GameMarkPullClothes,
  GameMarkRibbonQuarrel,
  GameMarkManacles,
  GameMarkMedic,
  GameMarkMummify,
  GameMarkUndressAndTeleportTargetVacuumCleaner,
  GameMarkAnestheticTarget,
  GameMarkPinchBlindfold,
  GameMarkPushTarget,
  GameMarkStealFromTarget,
  GameMarkStealFromTargetPlus,
  GameMarkStealToChest,
  GameMarkVibrator,
  GameMarkVibratorAttack,
  GameMarkWhip,
  GameMarkZeroG,
  GameProjectileBug,
  GameProjectileBugSimple,
  GameProjectileGlue,
  GameProjectileGlueGhost,
  GameProjectileHurt,
  GameProjectilePantyThief,
  GameProjectileStun,
  GameProjectileWeb,
  GameProjectileZeroG,
  { Effects & enchantements }
  GameEnchantmentBlindfold,
  GameEnchantmentDamageBonus,
  GameEnchantmentDamageMultiplier,
  GameEnchantmentDrainHealthPerSecond,
  GameEnchantmentDrainHealthFractionPerSecond,
  GameEnchantmentDrainStaminaPerSecond,
  GameEnchantmentDrainStaminaFractionPerSecond,
  GameEnchantmentDrainWillpowerPerSecond,
  GameEnchantmentDrainWillpowerFractionPerSecond,
  GameEnchantmentDeepSleep,
  GameEnchantmentDisorientation,
  GameEnchantmentHealMaxStaminaPerSecond,
  GameEnchantmentHealMaxHealthPerSecond,
  GameEnchantmentHealMaxWillPerSecond,
  GameEnchantmentHearingBonus,
  GameEnchantmentNudeTouchWillpowerDamageMultiplier,
  GameEnchantmentStripWillpowerDamageMultiplier,
  GameEnchantmentWalkNudeWillpowerDamageMultiplier,
  GameEnchantmentPlaySound,
  GameEnchantmentResistMultiplier,
  GameEnchantmentRegenerateDurability,
  GameEnchantmentRegenerateMaxDurability,
  GameEnchantmentRollCostMultiplier,
  GameEnchantmentRestQualityBonus,
  GameEnchantmentSpeedMultiplier,
  GameEnchantmentStealthDamageBonus,
  GameEnchantmentStealthDamageMultiplier,
  GameEnchantmentThrowMarkAtParent,
  GameEnchantmentVisionBonus,
  GameEnchantmentXRayVision,
  { Vinculopedia }
  GameVinculopediaTab,
  GameVinculopediaPage,
  GameVinculopediaPageMonster,
  GameVinculopediaPageItem,
  GameVinculopediaEntryHeader,
  GameVinculopediaEntryImageText,
  GameVinculopediaEntryItemStats,
  GameVinculopediaEntryMonsterStats,
  GameVinculopediaEntryText,
  GameVinculopediaEntryUrl;

end.

