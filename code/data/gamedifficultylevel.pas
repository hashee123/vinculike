{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameDifficultyLevel;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, Generics.Collections,
  GameValidated;

{ TODO: Some of those may (should) go into skills, maybe remain as multipliers on top of those }
type
  TDifficultyLevel = class(TObject, IValidated)
  public
    Name: String;
    MonsterAttackDelayMultiplier: Single; // not used yet!
    MonstersNumberMultiplier: Single;
    MaxPrisonersPerLevel: Integer;
    ItemsNumberMultiplier: Single;
    FirstPatrolSpawnTime: Single;
    EnemyPatrolSpawnTime: Single;
    BrokenItemWillpowerDamage: Single;
    StripTopWillpowerFractionDamage: Single;
    StripBottomWillpowerFractionDamage: Single;
    HitNakedWillpowerDamage: Single;
    LowHealthWillpowerDamage: Single;
    StatsDegradationSpeed: Single;
    BondageStruggleStaminaPerSecond: Single;
    BondageStruggleStaminaToProgressRatio: Single;
    FootwearDamageMultiplier: Single;
    function MaxPrisonersAtLevel(const ALevel: Integer): Integer;
    procedure Validate;
  end;
  TDifficultyLevelsList = specialize TObjectList<TDifficultyLevel>;

var
  Difficulty: TDifficultyLevel;

  DefaultDifficultyLevel: Integer;
  DifficultyLevels: TDifficultyLevelsList;

procedure SetDifficultyLevel(const DL: Integer);
procedure LoadDifficultyLevels;
implementation
uses
  DOM,
  CastleConfig, CastleXMLUtils,
  GameLog;

type
  EDifficultyLevelValidationFailed = class(Exception);

procedure SetDifficultyLevel(const DL: Integer);
begin
  UserConfig.SetValue('difficulty_level', DL);
  Difficulty := DifficultyLevels[DL];
  DebugLog('Setting Difficulty level to %s', [Difficulty.Name]);
end;

procedure LoadDifficultyLevels;
var
  DL: TDifficultyLevel;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  DifficultyLevels := TDifficultyLevelsList.Create(true);

  DefaultDifficultyLevel := -1;
  Doc := URLReadXML('castle-data:/difficultylevels.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('DifficultyLevel');
    try
      while Iterator.GetNext do
      begin
        DL := TDifficultyLevel.Create;
        DL.Name := Iterator.Current.AttributeString('Name');
        DL.MonsterAttackDelayMultiplier := Iterator.Current.AttributeFloat('MonsterAttackDelayMultiplier');
        DL.MonstersNumberMultiplier := Iterator.Current.AttributeFloat('MonstersNumberMultiplier');
        DL.ItemsNumberMultiplier := Iterator.Current.AttributeFloat('ItemsNumberMultiplier');
        DL.FirstPatrolSpawnTime := Iterator.Current.AttributeFloat('FirstPatrolSpawnTime');
        DL.EnemyPatrolSpawnTime := Iterator.Current.AttributeFloat('EnemyPatrolSpawnTime');
        DL.BrokenItemWillpowerDamage := Iterator.Current.AttributeFloat('BrokenItemWillpowerDamage');
        DL.StripTopWillpowerFractionDamage := Iterator.Current.AttributeFloat('StripTopWillpowerFractionDamage');
        DL.StripBottomWillpowerFractionDamage := Iterator.Current.AttributeFloat('StripBottomWillpowerFractionDamage');
        DL.HitNakedWillpowerDamage := Iterator.Current.AttributeFloat('HitNakedWillpowerDamage');
        DL.LowHealthWillpowerDamage := Iterator.Current.AttributeFloat('LowHealthWillpowerDamage');
        DL.StatsDegradationSpeed := Iterator.Current.AttributeFloat('StatsDegradationSpeed');
        DL.BondageStruggleStaminaPerSecond := Iterator.Current.AttributeFloat('BondageStruggleStaminaPerSecond');
        DL.BondageStruggleStaminaToProgressRatio := Iterator.Current.AttributeFloat('BondageStruggleStaminaToProgressRatio');
        DL.MaxPrisonersPerLevel := Iterator.Current.AttributeInteger('MaxPrisonersPerLevel');
        DL.FootwearDamageMultiplier := Iterator.Current.AttributeSingle('FootwearDamageMultiplier');
        DL.Validate;
        if Iterator.Current.AttributeBoolean('Default') then
        begin
          if DefaultDifficultyLevel >= 0 then
            DebugWarning('Difficulty levels can contain only one default difficulty level, but %s and %s are marked as default', [DL.Name, DifficultyLevels[DefaultDifficultyLevel]]);
          DefaultDifficultyLevel := DifficultyLevels.Add(DL);
        end else
          DifficultyLevels.Add(DL);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;

  SetDifficultyLevel(UserConfig.GetInteger('difficulty_level', DefaultDifficultyLevel));
end;

function TDifficultyLevel.MaxPrisonersAtLevel(const ALevel: Integer): Integer;
begin
  Result := ALevel div 5;
  if Result > MaxPrisonersPerLevel then
    Result := MaxPrisonersPerLevel;
end;

procedure TDifficultyLevel.Validate;
begin
  if Name = '' then
    raise EDifficultyLevelValidationFailed.Create('Difficulty level has empty name');
  if MonsterAttackDelayMultiplier <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('MonsterAttackDelayMultiplier = %n <= 0', [MonsterAttackDelayMultiplier]);
  if MonstersNumberMultiplier <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('MonstersNumberMultiplier = %n <= 0', [MonstersNumberMultiplier]);
  if ItemsNumberMultiplier <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('ItemsNumberMultiplier = %n <= 0', [ItemsNumberMultiplier]);
  if FirstPatrolSpawnTime <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('FirstPatrolSpawnTime = %n <= 0', [FirstPatrolSpawnTime]);
  if EnemyPatrolSpawnTime <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('EnemyPatrolSpawnTime = %n <= 0', [EnemyPatrolSpawnTime]);
  if BrokenItemWillpowerDamage <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('BrokenItemWillpowerDamage = %n <= 0', [BrokenItemWillpowerDamage]);
  if StripTopWillpowerFractionDamage <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('StripTopWillpowerFractionDamage = %n <= 0', [StripTopWillpowerFractionDamage]);
  if StripBottomWillpowerFractionDamage <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('StripBottomWillpowerFractionDamage = %n <= 0', [StripBottomWillpowerFractionDamage]);
  if HitNakedWillpowerDamage <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('HitNakedWillpowerDamage = %n <= 0', [HitNakedWillpowerDamage]);
  if LowHealthWillpowerDamage <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('LowHealthWillpowerDamage = %n <= 0', [EnemyPatrolSpawnTime]);
  if StatsDegradationSpeed <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('StatsDegradationSpeed = %n <= 0', [StatsDegradationSpeed]);
  if BondageStruggleStaminaPerSecond <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('BondageStruggleStaminaPerSecond = %n <= 0', [BondageStruggleStaminaPerSecond]);
  if BondageStruggleStaminaToProgressRatio <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('BondageStruggleStaminaToProgressRatio = %n <= 0', [BondageStruggleStaminaToProgressRatio]);
end;

finalization
  FreeAndNil(DifficultyLevels);
end.

