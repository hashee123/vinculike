{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Initialize game window, load data and set up UI (views) 
  everything starts here }
unit GameInitialize;

{$INCLUDE compilerconfig.inc}

interface

{ Apply some game-wise settings, such as Fullscreen mode, global music volume, etc. }
procedure ApplySettings;
implementation

uses SysUtils, Classes,
  CastleLog, CastleConfig, CastleApplicationProperties, CastleWindow,
  CastleSoundEngine, CastleGlUtils
  {$region 'Castle Initialization Uses'}
  // The content here may be automatically updated by CGE editor.
  , GameViewGame
  , GameViewContentWarning
  , GameViewMainMenu
  , GameViewOptions
  , GameViewConfirmResetProgress
  , GameViewConfirmDeleteSave
  , GameViewConfirmNewRun
  , GameViewCredits
  , GameViewEndGame
  , GameViewVinculopedia
  {$endregion 'Castle Initialization Uses'},
  GameLoadSerializedClasses, // Makes sure all serializable classes are serialized properly
  GameCachedImages, GameItemsDatabase, GameFonts, GameMonstersDatabase, GameSounds,
  GameColors, GameTranslation, GameTileset, GameStats, GameGarbageCollector,
  GameWindow, GameVinculopedia, GameDifficultyLevel, GameBlueprintsDatabase;


procedure ApplySettings;
begin
  Window.FullScreen := UserConfig.GetValue('fullscreen', true);
  SoundEngine.Volume := UserConfig.GetFloat('master_volume', DefaultVolume);
  SoundEngine.LoopingChannel[0].Volume := UserConfig.GetFloat('music_volume', 1.0);
  ApplicationProperties.LimitFPS := UserConfig.GetInteger('limit_fps', 60);
end;

procedure ApplicationInitialize;
begin
  { load settings }
  Window.Container.LoadSettings('castle-data:/CastleSettings.xml');
  UserConfig.Load;
  ApplySettings;

  InitGarbageCollector;

  { load game data }
  InitializeSounds;
  InitializeColors;
  InitStats;
  LoadDifficultyLevels;
  LoadImages;
  InitializeTheme; // maybe temporarily
  LoadTilesets;
  LoadItemsData;
  LoadGeneticBlueprints; // after items/bodyparts
  LoadMonstersData;
  LoadFonts;
  LoadTranslation;
  InitVinculopedia; // after all data - to validate against it

  { initialize game viewes (UI states) }
  {$region 'Castle State Creation'}
  // The content here may be automatically updated by CGE editor.
  ViewGame := TViewGame.Create(Application);
  ViewContentWarning := TViewContentWarning.Create(Application);
  ViewMainMenu := TViewMainMenu.Create(Application);
  ViewOptions := TViewOptions.Create(Application);
  ViewConfirmResetProgress := TViewConfirmResetProgress.Create(Application);
  ViewConfirmDeleteSave := TViewConfirmDeleteSave.Create(Application);
  ViewConfirmNewRun := TViewConfirmNewRun.Create(Application);
  ViewCredits := TViewCredits.Create(Application);
  ViewEndGame := TViewEndGame.Create(Application);
  ViewVinculopedia := TViewVinculopedia.Create(Application);
  {$endregion 'Castle State Creation'}

  { Set default view (the first screen shown to the Player) }
  if UserConfig.GetValue('ask_content_warning', true) or UserConfig.GetValue('always_ask_content_warning', false) then
    Window.Container.View := ViewContentWarning
  else
    Window.Container.View := ViewMainMenu;
end;

procedure ApplicationFinalize;
begin
  FreeFonts;
end;

initialization
  WriteLnLog('----------------------------------------------------');
  WriteLnLog(ApplicationProperties.Caption + ' ' + ApplicationProperties.Version);
  WriteLnLog('Copyright (C) 2022-2024 Yevhen Loza');
  WriteLnLog('This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.');
  WriteLnLog('This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.');
  WriteLnLog('You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.');
  WriteLnLog('----------------------------------------------------');

  LogGLInformationVerbose := true;

  Application.OnInitialize := @ApplicationInitialize;

  InitializeWindow;

  Application.MainWindow := Window;

finalization
  ApplicationFinalize;
end.
