{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{------------------------- Compiler settings -------------------------------}

{$SMARTLINK ON} // Enable smart-linking

{$MODE ObjFPC} // FreePascal style code
{$H+} // AnsiStrings
{$J-} // non-writeable constants
{$COPERATORS ON} // allow += style operators, I like them
{$GOTO OFF} // Disallow goto and label
{$EXTENDEDSYNTAX ON} // Allow discarding function result
{$INTERFACES CORBA}
{$MINFPCONSTPREC 32} // By default constants are Single precision, see https://www.freepascal.org/docs-html/prog/progsu52.html#x59-580001.2.52 and https://forum.lazarus.freepascal.org/index.php?topic=64889

{$WARN 2005 off : Comment level $1 found}

{$IFDEF ANDROID} {$DEFINE Mobile} {$ENDIF}
{$IFDEF CASTLE_IOS} {$DEFINE Mobile} {$ENDIF}
{$IFDEF Mobile} {$DEFINE GLIndexesShort} {$DEFINE OpenGLES} {$ENDIF}

{$MACRO ON}

{---------------------------- Game specific --------------------------------}

{$IFDEF DEBUG}
{$DEFINE SafeActorTypecast}
{$ENDIF}

{$DEFINE ValidateData}

