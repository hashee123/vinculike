{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameGarbageCollector;

{$INCLUDE compilerconfig.inc}

interface
uses
  Generics.Collections;

type
  TGarbageCollectObject = class(TObject)
  private // note: not strict-private, accessible to TGarbageCollector but not to anyone else
    QueuedForFreeing: Boolean;
  public
    destructor Destroy; override;
  end;
  TGarbageCollectObjectList = specialize TObjectList<TGarbageCollectObject>;

type
  TGarbageCollector = class(TObject)
  private
    FreeingQueue: TGarbageCollectObjectList;
  public
    { Queue class for freeing and set it to nil }
    procedure AddAndNil(var AClass);
    procedure FreeNowAndNil(var AClass);
    procedure Remove(const AClass: TGarbageCollectObject);
    procedure Clear;
    constructor Create; //override;
    destructor Destroy; override;
  end;

var
  GarbageCollector: TGarbageCollector;

procedure InitGarbageCollector;
implementation
uses
  SysUtils,
  GameLog;

{ TGarbageCollectObject }

destructor TGarbageCollectObject.Destroy;
begin
  if QueuedForFreeing then
  begin
    DebugWarning('%s was queued for freeing but is freed manually!', [Self.ClassName]);
    GarbageCollector.Remove(Self);
  end;
  inherited Destroy;
end;

{ TGarbageCollector }

procedure TGarbageCollector.AddAndNil(var AClass);
var
  Temp: TObject;
begin
  { Mimicking FreeAndNil behavior here }
  Temp := TObject(AClass);
  Pointer(AClass) := nil;
  if Temp is TGarbageCollectObject then
  begin
    TGarbageCollectObject(Temp).QueuedForFreeing := true;
    FreeingQueue.Add(TGarbageCollectObject(Temp));
  end;
end;

procedure TGarbageCollector.FreeNowAndNil(var AClass);
var
  Temp: TObject;
begin
  { Mimicking FreeAndNil behavior here }
  Temp := TObject(AClass);
  Pointer(AClass) := nil;
  if Temp is TGarbageCollectObject then
  begin
    if TGarbageCollectObject(Temp).QueuedForFreeing then
    begin
      TGarbageCollectObject(Temp).QueuedForFreeing := false;
      FreeingQueue.Remove(TGarbageCollectObject(Temp));
    end else
      Temp.Free;
  end;
end;

procedure TGarbageCollector.Remove(const AClass: TGarbageCollectObject);
begin
  AClass.QueuedForFreeing := false;
  FreeingQueue.Remove(AClass)
end;

procedure TGarbageCollector.Clear;
var
  I: Integer;
begin
  for I := 0 to Pred(FreeingQueue.Count) do
    FreeingQueue[I].QueuedForFreeing := false; // to avoid call-back
  FreeingQueue.Clear;
end;

constructor TGarbageCollector.Create;
begin
  inherited; // parent is non-virtual and empty
  FreeingQueue := TGarbageCollectObjectList.Create(true);
end;

destructor TGarbageCollector.Destroy;
begin
  FreeAndNil(FreeingQueue);
  inherited Destroy;
end;

procedure InitGarbageCollector;
begin
  GarbageCollector := TGarbageCollector.Create;
end;

finalization
  FreeAndNil(GarbageCollector);
end.

