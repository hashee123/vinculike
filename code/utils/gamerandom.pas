{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameRandom;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  CastleRandom;

var
  Rnd: TCastleRandom;

function GetRandomFromSeed64(const Seed: UInt64): Single;
function GetRandomFromSeed64(const Seed: UInt64; const N: UInt64): UInt64;
implementation

function CycleSeed(const Seed: UInt64): UInt64; inline;
var
  c64: UInt64;
  procedure XorShiftCycle; inline;
  begin
    c64 := c64 xor (c64 shl 12);
    c64 := c64 xor (c64 shr 25);
    c64 := c64 xor (c64 shl 27);
  end;
begin
  c64 := Seed;
  XorShiftCycle;
  XorShiftCycle;
  XorShiftCycle;
  XorShiftCycle;
  XorShiftCycle;
  Exit(C64);
end;

function GetRandomFromSeed64(const Seed: UInt64): Single;
begin
  Exit(Single(CycleSeed(Seed)) / UInt64.MaxValue);
end;

function GetRandomFromSeed64(const Seed: UInt64; const N: UInt64): UInt64;
begin
  Exit(CycleSeed(Seed) mod N);
end;

initialization
  Rnd := TCastleRandom.Create;

finalization
  FreeAndNil(Rnd);
end.

