{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaEntryItemStats;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Classes, DOM,
  CastleUiControls,
  GameUnlockableEntry, GameItemData;

type
  TVinculopediaEntryItemStats = class(TUnlockableEntry)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    ItemData: TItemData;
    procedure InsertEntry(AOwner: TCastleUserInterface); override;
  end;

implementation
uses
  CastleXmlUtils, CastleControls,
  GameSerializableData,
  GameColors, GameFonts, GameItemsDatabase;

procedure TVinculopediaEntryItemStats.Validate;
begin
  inherited;
end;

procedure TVinculopediaEntryItemStats.Read(const Element: TDOMElement);
var
  AName: String;
begin
  inherited;
  AName := Element.AttributeString('Item');
  if ItemsDataDictionary.ContainsKey(AName) then
    ItemData := ItemsDataDictionary[AName] as TItemData
  else
    raise Exception.CreateFmt('Item data "%s" was not found in items', [AName]);
end;

procedure TVinculopediaEntryItemStats.InsertEntry(AOwner: TCastleUserInterface);
var
  C: TCastleUserInterface;
  HorizontalGroup: TCastleHorizontalGroup;
  VerticalGroup: TCastleVerticalGroup;
  I: Integer;

  procedure InsertStat(const Caption: String; const Value: Single; const Percent: Boolean);
  var
    Vert: TCastleVerticalGroup;
    L: TCastleLabel;
  begin
    Vert := TCastleVerticalGroup.Create(HorizontalGroup);
    Vert.Alignment := hpMiddle;
    Vert.Spacing := 10;
    HorizontalGroup.InsertFront(Vert);

    L := TCastleLabel.Create(Vert); // TODO: Image instead of name
    L.Caption := Caption;
    L.Color := ColorDefault;
    L.CustomFont := FontSoniano50;
    Vert.InsertFront(L);
    L := TCastleLabel.Create(Vert);
    if Percent then
      L.Caption := Round(100 * Value).ToString + '%'
    else
      L.Caption := Format('%.1n', [Value]);
    L.Color := ColorDefault;
    L.CustomFont := FontSoniano50;
    Vert.InsertFront(L);
  end;

  procedure InsertEnchantment(const Caption: String);
  var
    L: TCastleLabel;
  begin
    L := TCastleLabel.Create(VerticalGroup);
    L.Caption := Caption;
    L.Color := ColorDefault;
    L.CustomFont := FontSoniano50;
    VerticalGroup.InsertFront(L);
  end;

begin
  // TODO: Read from design
  C := TCastleUserInterface.Create(AOwner);
  //C.Width := 1300;
  C.AutoSizeToChildren := true; // I'd really make a better use of AutoSizeHeight here, but it's not available?
  AOwner.InsertFront(C);

  VerticalGroup := TCastleVerticalGroup.Create(C);
  VerticalGroup.Spacing := 20;
  C.InsertFront(VerticalGroup);

  HorizontalGroup := TCastleHorizontalGroup.Create(VerticalGroup);
  HorizontalGroup.Spacing := 20;
  VerticalGroup.InsertFront(HorizontalGroup);

  if not ItemData.Indestructible then
    InsertStat('HP', ItemData.Durability, false);
  InsertStat('NOISE', ItemData.Noise, false);
  InsertStat('DEF', ItemData.Protection, true);

  if ItemData.Damage > 0 then
    InsertEnchantment(Format('Damage: %.1n', [ItemData.Damage]));

  if ItemData.WardrobeMalfunctionChance > 0 then
    InsertEnchantment(Format('Wardrobe Malfunction Chance: %.1n%%', [ItemData.WardrobeMalfunctionChance * 100]));

  if ItemData.ProtectsFeet then
    InsertEnchantment('Protects feet while walking');
  if ItemData.Indestructible then
    InsertEnchantment('Indestructible');
  if not ItemData.CanBeRepaired then
    InsertEnchantment('Cannot be repaired');

  for I := 0 to Pred(ItemData.Enchantments.Count) do
    InsertEnchantment(Format(ItemData.Enchantments[I].Description, [])); // Format: to get rid of "%%" correctly
end;

initialization
  RegisterSerializableData(TVinculopediaEntryItemStats);

end.

