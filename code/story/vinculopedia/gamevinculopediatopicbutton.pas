{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaTopicButton;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  CastleMobileButton,
  GameVinculopediaPageAbstract;

type
  TVinculopediaTopicButton = class(TCastleMobileButton)
  public
    Selected: Boolean;
    Page: TVinculopediaPageAbstract;
    procedure Setup;
  end;

implementation
uses
  GameFonts, GameThemedButton;

procedure TVinculopediaTopicButton.Setup;
var
  ButtonImageUrlBase: String;
begin
  if Selected then
    SetTheme('inventory_button_light_selected')
  else
    SetTheme('inventory_button_light');
  Caption := '';
  Width := 500 - 25; // TODO: more generic
  Height := 60;
  EnableParentDragging := true;
  CustomFont := FontBender40;
  CustomTextColorUse := true;
end;

end.

