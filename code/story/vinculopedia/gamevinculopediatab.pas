{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaTab;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM, Generics.Collections,
  GameSerializableData;

type
  TVinculopediaTab = class(TSerializableData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Id: String;
    Caption: String;
  end;
  TVinculopediaTabsList = specialize TObjectList<TVinculopediaTab>;

implementation
uses
  CastleXmlUtils;

procedure TVinculopediaTab.Validate;
begin
  //inherited; parent is abstract
  if Id = '' then
    raise EDataValidationError.Create('Id = "" in TVinculopediaTab');
  if Caption = '' then
    raise EDataValidationError.Create('Caption = "" in TVinculopediaTab');
end;

procedure TVinculopediaTab.Read(const Element: TDOMElement);
begin
  //inherited; parent is abstract
  Caption := Element.AttributeString('Caption');
  Id := Element.AttributeString('Id');
end;

initialization
  RegisterSerializableData(TVinculopediaTab);

end.

