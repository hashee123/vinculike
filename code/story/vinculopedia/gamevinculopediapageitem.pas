{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaPageItem;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Classes, DOM,
  CastleGlImages,
  GameVinculopediaPageAbstract, GameItemData;

type
  TVinculopediaPageItem = class(TVinculopediaPageAbstract)
  strict private
    ItemData: TItemData;
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  end;

var
  ItemEntries: TStringList;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameLog, GameColors, GameItemsDatabase,
  GameVinculopediaEntryHeader, GameVinculopediaEntryItemStats,
  GameVinculopediaEntryText; // temporary

procedure TVinculopediaPageItem.Validate;
begin
  inherited;
  //image
end;

procedure TVinculopediaPageItem.Read(const Element: TDOMElement);

  function ItemStatsEntry: TVinculopediaEntryItemStats;
  begin
    Result := TVinculopediaEntryItemStats.Create;
    Result.ItemData := ItemData;
    Result.UnlockStat := '';
    Result.UnlockValue := 0;
  end;

begin
  inherited;
  if ItemEntries.IndexOf(Caption) >= 0 then
    DebugWarning('Duplicate Vinculopedia item entry: %s', [Caption])
  else
    ItemEntries.Add(Caption);
  if ItemsDataDictionary.ContainsKey(Caption) then
    ItemData := ItemsDataDictionary[Caption] as TItemData
  else
    raise EDataValidationError.CreateFmt('Item data %s was not found', [Caption]);
  Caption := ItemData.DisplayName;
  UnlockStat := 'equipped_' + ItemData.Id;
  UnlockValue := 1;
  if ItemData.IsBondage then
    TopicTextColor := ColorVinculopediaBondage
  else
    TopicTextColor := ColorVinculopediaTopic;
  Image := ItemData.MapImageDrawable;
  Entries.Insert(0, ItemStatsEntry);
  Entries.Insert(0, NewEntryHeader(ItemData.DisplayName, '', 0));
end;

initialization
  RegisterSerializableData(TVinculopediaPageItem);
  ItemEntries := TStringList.Create;
finalization
  // Just to make sure if something goes wrong during loading. It should be freed by Vinculopedia after validation
  FreeAndNil(ItemEntries);
end.

