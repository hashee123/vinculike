{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopedia;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Classes, DOM,
  CastleTimeUtils,
  GameSerializableData,
  GameVinculopediaPageAbstract, GameVinculopediaTab;

type
  TVinculopedia = class(TObject)
  strict private
    FLastSoundTime: TTimerResult;
    procedure Validate;
    procedure Read;
  public
    Tabs: TVinculopediaTabsList;
    Pages: TVinculopediaPagesList;
    procedure CheckUnlock(const AStat: String; const AStatValue: Integer);
  public
    constructor Create; //override
    destructor Destroy; override;
  end;

var
  Vinculopedia: TVinculopedia;

procedure InitVinculopedia;
implementation
uses
  CastleXmlUtils,
  GameSounds, GameLog,
  GameItemsDatabase, GameItemData, GameMonstersDatabase,
  GameVinculopediaPageItem, GameVinculopediaPageMonster;

procedure InitVinculopedia;
begin
  Vinculopedia := TVinculopedia.Create;
end;

procedure TVinculopedia.Read;
var
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  Doc := URLReadXML('castle-data:/story/vinculopedia.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('Tab');
    try
      while Iterator.GetNext do
      begin
        Tabs.Add(TVinculopediaTab.ReadClass(Iterator.Current) as TVinculopediaTab);
      end;
    finally FreeAndNil(Iterator) end;

    Iterator := Doc.DocumentElement.ChildrenIterator('Page');
    try
      while Iterator.GetNext do
      begin
        Pages.Add(TVinculopediaPageAbstract.ReadClass(Iterator.Current) as TVinculopediaPageAbstract);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;

  Validate;
end;

procedure TVinculopedia.CheckUnlock(const AStat: String;
  const AStatValue: Integer);
var
  VP: TVinculopediaPageAbstract;
  Unlocked: Boolean;
  Updated: Boolean;
begin
  Unlocked := false; // we do this complication because we need to prioritize "unlocked" if "updated and unlocked" regardless of which order they come
  Updated := false;
  for VP in Pages do
  begin
    case VP.CheckUnlock(AStat, AStatValue) of
      ukUnlocked: Unlocked := true;
      ukUpdated: Updated := true;
      ukNone: ;
    end;
  end;
  if (FLastSoundTime.ElapsedTime > 0.5) then
  begin
    if Unlocked then
    begin
      FLastSoundTime := Timer;
      Sound('page_unlocked');
    end else
    if Updated then
    begin
      FLastSoundTime := Timer;
      Sound('page_updateed');
    end;
  end;
end;

procedure TVinculopedia.Validate;
var
  Page: TVinculopediaPageAbstract;
  S: String;
  I: SizeInt;

  procedure ValidateTabIdExists(const ATabId: String);
  var
    Tab: TVinculopediaTab;
  begin
    for Tab in Tabs do
      if Tab.Id = ATabId then
        Exit;
    raise EDataValidationError.CreateFmt('Tab "%s" was not found in Vinculopedia tabs', [ATabId]);
  end;

begin
  if Tabs.Count = 0 then
    raise EDataValidationError.Create('Vinculopedia must have at least one tab');
  if Pages.Count = 0 then
    raise EDataValidationError.Create('Vinculopedia must have at least one page');

  for Page in Pages do
    ValidateTabIdExists(Page.Tab);

  for S in MonstersDataDictionary.Keys do
  begin
    I := MonstersEntries.IndexOf(S);
    if I < 0 then
      DebugWarning('Monster entry not found in Vinculopedia: %s', [S])
    else
      MonstersEntries.Delete(I);
  end;
  if MonstersEntries.Count > 0 then // This can happen if there are duplicate entries, and shouldn't happen otherwise
    DebugWarning('Excess Vinculopedia entries: %d', [MonstersEntries.Count]);
  FreeAndNil(MonstersEntries);

  for S in ItemsDataDictionary.Keys do
  if ItemsDataDictionary[S] is TItemData then
  begin
    I := ItemEntries.IndexOf(S);
    if I < 0 then
      DebugWarning('Item entry not found in Vinculopedia: %s', [S])
    else
      ItemEntries.Delete(I);
  end;
  if ItemEntries.Count > 0 then // This can happen if there are duplicate entries, and shouldn't happen otherwise
    DebugWarning('Excess Vinculopedia entries: %d', [ItemEntries.Count]);
  FreeAndNil(ItemEntries);
end;

constructor TVinculopedia.Create;
begin
  inherited; //parent is empty
  FLastSoundTime := Timer;
  Tabs := TVinculopediaTabsList.Create(true);;
  Pages := TVinculopediaPagesList.Create(true);
  Read;
end;

destructor TVinculopedia.Destroy;
begin
  FreeAndNil(Pages);
  FreeAndNil(Tabs);
  inherited Destroy;
end;

finalization
  FreeAndNil(Vinculopedia);
end.

