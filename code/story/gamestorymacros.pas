{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameStoryMacros;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils,
  CastleGlImages;

function HeaderMacros(const Header: String): String;
function ImageHeaderMacros(const ImageHeader: String): TDrawableImage; // maybe this is not good for animated images, but for now let's go this way
implementation
uses
  GameMonsterData, GameMonstersDatabase,
  GameItemsDatabase, GameItemData, GameCachedImages;

function HeaderMacros(const Header: String): String;
var
  AName: String;
begin
  if Copy(Header, 1, Length('%monster:')) = '%monster:' then
  begin
    AName := Copy(Header, Length('%monster:') + 1, Length(Header));
    if MonstersDataDictionary.ContainsKey(AName) then
      Exit(MonstersDataDictionary[AName].DisplayName)
    else
      raise Exception.CreateFmt('Monster entry not found: %s', [AName]);
  end else
  if Copy(Header, 1, Length('%item:')) = '%item:' then
  begin
    AName := Copy(Header, Length('%item:') + 1, Length(Header));
    if ItemsDataDictionary.ContainsKey(AName) then
      Exit((ItemsDataDictionary[AName] as TItemData).DisplayName)
    else
      raise Exception.CreateFmt('Item entry not found: %s', [AName]);
  end;
  Exit(Header);
end;

function ImageHeaderMacros(const ImageHeader: String): TDrawableImage;
var
  AName: String;
begin
  if Copy(ImageHeader, 1, Length('%monster:')) = '%monster:' then
  begin
    AName := Copy(ImageHeader, Length('%monster:') + 1, Length(ImageHeader));
    if MonstersDataDictionary.ContainsKey(AName) then
      Exit((MonstersDataDictionary[AName] as TMonsterData).Image)
    else
      raise Exception.CreateFmt('Monster entry not found: %s', [AName]);
  end else
  if Copy(ImageHeader, 1, Length('%item:')) = '%item:' then
  begin
    AName := Copy(ImageHeader, Length('%item:') + 1, Length(ImageHeader));
    if ItemsDataDictionary.ContainsKey(AName) then
      Exit((ItemsDataDictionary[AName] as TItemData).MapImageDrawable)
    else
      raise Exception.CreateFmt('Item entry not found: %s', [AName]);
  end;
  Exit(LoadDrawable('castle-data:/' + ImageHeader));
end;

initialization

end.

