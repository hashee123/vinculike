{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActorData;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils,
  DOM, Generics.Collections,
  GameSerializableData, GameActionMoveAbstract;

type
  EActorDataValidationError = class(Exception);

type
  TActorData = class(TSerializableData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Id: String;
    DisplayName: String;
    MaxHealth: Single;
    Size: Byte;
    MovementSpeed: Single;
    RollSpeed: Single;
    RollRange: Single;
    MoveAction: TActionMoveClass;
  public
    destructor Destroy; override;
  end;
  TActorDataList = specialize TObjectList<TActorData>;
  TActorDataDictionary = specialize TObjectDictionary<String, TActorData>;

implementation
uses
  CastleXMLUtils,
  GameMapTypes,
  GameActionMoveGhost, GameActionMove;

procedure TActorData.Validate;
begin
  if Id = '' then
    raise EActorDataValidationError.CreateFmt('Id = "" in %s', [ClassName]);
  if DisplayName = '' then
    raise EActorDataValidationError.Create('DisplayName = "" : ' + Id);
  if MaxHealth <= 1 then
    raise EActorDataValidationError.Create('MaxHealth <= 1 : ' + Id);
  if Size < 1 then
    raise EActorDataValidationError.Create('Size < 1 : ' + Id);
  if Size > PredMaxColliderSize + 1 then
    raise EActorDataValidationError.Create('Size > PredMaxColliderSize + 1 : ' + Id);
  if MovementSpeed < 0 then
    raise EActorDataValidationError.Create('MovementSpeed < 0 : ' + Id);
  if RollSpeed < 0 then
    raise EActorDataValidationError.Create('RollSpeed < 0 : ' + Id);
  if RollRange < 0 then
    raise EActorDataValidationError.Create('RollRange < 0 : ' + Id);
end;

procedure TActorData.Read(const Element: TDOMElement);
begin
  Id := Element.AttributeString('Id');
  DisplayName := Element.AttributeString('Name');
  MaxHealth := Element.AttributeSingle('Health');
  Size := Element.AttributeInteger('Size');
  MovementSpeed := Element.AttributeSingle('Speed');
  RollSpeed := Element.AttributeSingleDef('RollSpeed', 1); // default values should not necessarily make sense, but must allow validation
  RollRange := Element.AttributeSingleDef('RollRange', 1);

  // Ok, I'm confused, let's ignore this thing for now:
  //MoveAction := SerializableDataByName(Element.AttributeString('MoveAction')) as TActionMoveAbstractData;
  case Element.AttributeString('MoveAction') of
    'TActionMove': MoveAction := TActionMove;
    'TActionMoveGhost': MoveAction := TActionMoveGhost;
    else
      raise EActorDataValidationError.Create('MoveAction="' + Element.AttributeString('MoveAction') + '" unexpected : ' + Id);
  end;
end;

destructor TActorData.Destroy;
begin
  inherited Destroy;
end;

end.

