{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameColorTags;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes, DOM, Generics.Collections,
  CastleColors;

type
  TColorTagsList = TStringList;
  TColorsList = specialize TList<TCastleColor>;
  TColorTagsDictionary = specialize TObjectDictionary<String, TColorsList>;
  TColorDictionary = specialize TDictionary<string, TCastleColor>;

function GetColorTags(const Element: TDOMElement): TColorTagsDictionary;
implementation
uses
  SysUtils,
  CastleStringUtils, CastleXmlUtils;

function GetColorTags(const Element: TDOMElement): TColorTagsDictionary;
var
  ColorTagsStringList, ColorsStringList: TCastleStringList;
  S, Color, ColorTag: String;
  ColorsList: TColorsList;
begin
  S := Element.AttributeStringDef('ColorTags', 'none');
  if S <> 'none' then
  begin
    Result := TColorTagsDictionary.Create([doOwnsValues]); // TODO: maybe sometimes makes sense to keep it nil, if only none is present
    ColorTagsStringList := CreateTokens(S, [',']);
    for ColorTag in ColorTagsStringList do
    begin
      S := Element.AttributeString(ColorTag);
      ColorsStringList := CreateTokens(S, [',']);
      ColorsList := TColorsList.Create;
      for Color in ColorsStringList do
        ColorsList.Add(HexToColor(Color));
      if ColorsList.Count = 0 then
        raise Exception.CreateFmt('ColorsList.Count = 0 for ColorTag = %s', [ColorTag]);
      Result.Add(ColorTag, ColorsList);
      FreeAndNil(ColorsStringList);
    end;
    FreeAndNil(ColorTagsStringList);
  end else
    Exit(nil);
end;


end.

