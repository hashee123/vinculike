{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameInventoryItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections, DOM,
  GameSerializableObject,
  GameItemDataAbstract, GameItemData, GameAbstractItem, GameEnchantmentAbstract,
  GameColorTags;

type
  { An item that can be used by the player character (equipped or consumed, etc.)
    Note "TInventoryItem", not just "TItem" - to make sure it won't conflict
    with some FPC/CGE internal stuff }
  TInventoryItem = class(TAbstractItem)
  public
    Broken: Boolean;
    Durability: Single;
    MaxDurability: Single;
    Enchantments: TEnchantmentsList;
    Color: TColorDictionary;
    { SkillPreserveMax corresponds to how efficiently Max value is preserved
      SkillRepairCurrent corresponds to how efficiently the item current value
      will be increased }
    procedure Fix(const SkillPreserveMax, SkillRepairCurrent: Single);
    function ItemData: TItemData;
    procedure Update(const AParentActor: TObject; const SecondsPassed: Single); //virtual - for now no children
    { Stops this item's internal stuff }
    procedure Stop;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    class function NewItem(const AItemData: TItemDataAbstract): TInventoryItem;
    constructor Create(const NotLoading: Boolean = true); override;
    destructor Destroy; override;
  end;
  TInventoryItemsList = specialize TObjectList<TInventoryItem>;

implementation
uses
  SysUtils, Math,
  CastleXmlUtils, CastleColors,
  GameRandom, GameSounds;

procedure TInventoryItem.Fix(const SkillPreserveMax, SkillRepairCurrent: Single);
begin
  //A2+0,5 * B2 * ((B2-A2)/B2)^1,1
  MaxDurability := Durability + SkillPreserveMax * MaxDurability * Power((MaxDurability - Durability) / MaxDurability, 1.1);
  Durability := Durability + SkillRepairCurrent * (MaxDurability - Durability);
  if Broken then
    Broken := false;
  Sound(ItemData.SoundRepair);
end;

function TInventoryItem.ItemData: TItemData;
begin
  Exit(Data as TItemData);
end;

procedure TInventoryItem.Update(const AParentActor: TObject;
  const SecondsPassed: Single);
var
  E: TEnchantmentAbstract;
begin
  for E in Enchantments do
    E.Update(AParentActor, Self, SecondsPassed);
end;

procedure TInventoryItem.Stop;
var
  E: TEnchantmentAbstract;
begin
  for E in Enchantments do
    E.Stop;
end;

procedure TInventoryItem.Save(const Element: TDOMElement);
var
  ColorTag: String;
begin
  inherited Save(Element);
  Element.AttributeSet('Broken', Broken);
  Element.AttributeSet('Durability', Durability);
  Element.AttributeSet('MaxDurability', MaxDurability);
  if ItemData.ColorTags <> nil then
    for ColorTag in ItemData.ColorTags.Keys do
      Element.AttributeColorSet('ColorTag_' + ColorTag, Color[ColorTag]);
  {$WARNING 'Save-load enchantments. Critical TODO'}
end;

procedure TInventoryItem.Load(const Element: TDOMElement);
var
  ColorTag: String;
  E: TEnchantmentAbstract;
begin
  inherited;
  Broken := Element.AttributeBoolean('Broken');
  Durability := Element.AttributeSingle('Durability');
  MaxDurability := Element.AttributeSingle('MaxDurability');
  if ItemData.ColorTags <> nil then
    for ColorTag in ItemData.ColorTags.Keys do
      Color.Add(ColorTag, Element.AttributeColorDef('ColorTag_' + ColorTag, ItemData.ColorTags[ColorTag][Rnd.Random(ItemData.ColorTags[ColorTag].Count)]));
  {$WARNING 'Save-load enchantments. Critical TODO'}
  Enchantments := TEnchantmentsList.Create(true);
  for E in ItemData.Enchantments do
    Enchantments.Add(E.Clone);
end;

class function TInventoryItem.NewItem(
  const AItemData: TItemDataAbstract): TInventoryItem;
var
  E: TEnchantmentAbstract;
  ColorTag: String;
begin
  Result := TInventoryItem.Create(true);
  if not (AItemData is TItemData) then
    raise Exception.CreateFmt('Got item data: %s, expected: TItemData', [AItemData.ClassName]);
  Result.Data := AItemData;
  Result.MaxDurability := 1 + Sqrt(Rnd.Random) * (Result.ItemData.Durability - 1);
  Result.Durability := 1 + Sqrt(Rnd.Random) * (Result.MaxDurability - 1);

  Result.Enchantments := TEnchantmentsList.Create(true);
  for E in Result.ItemData.Enchantments do
    Result.Enchantments.Add(E.Clone);

  if Result.ItemData.ColorTags <> nil then
    for ColorTag in Result.ItemData.ColorTags.Keys do
      Result.Color.Add(ColorTag, Result.ItemData.ColorTags[ColorTag][Rnd.Random(Result.ItemData.ColorTags[ColorTag].Count)]);
end;

constructor TInventoryItem.Create(const NotLoading: Boolean);
begin
  inherited;
  Color := TColorDictionary.Create;
  Color.Add('none', White);
end;

destructor TInventoryItem.Destroy;
begin
  FreeAndNil(Color);
  FreeAndNil(Enchantments);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TInventoryItem);
end.

