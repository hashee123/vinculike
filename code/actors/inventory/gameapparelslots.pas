{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameApparelSlots;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, Generics.Collections;

type
  { Possible apparel (body/equipment) slots of the item. Prefixes:
    asXxxx - apparel slots (generic)
    bsXxxx - body slots
    esXxxx - equipment slots (player/monsters can interact with those)
    vsXxxx - virtual slot (equipment slot that is not shown in the inventory but can be used for some logic or to attach images for items that need to be more fragmented)
    NOTE: this order = render order }

  {
    vsBack,

    bsTail,
    bsHairBack,
    bsTorso,
    bsBelly,
    bsBreasts,
    bsNipples,
    bsLegs,
    vsLegsSplit,
    bsNethers,
    bsPubic,

    bsTorsoTattoo, // maybe: upperTorso+lowerTorso and some tattoos take two slots [or even more]?
    bsBreastsTattoo,
    bsLegsTattoo,

    esBottomUnder,     // 1
    esTopUnder,        // 2
    esBottomOver,      // 3
    esTopOver,         // 4

    esFeet,            // 5
    esAnkles,          // 6

    esCollar,          // 7
    esAmulet,          // 8

    //face
    bsMouth,
    esMouth,           // 9
    bsNose,
    bsPupilLeft,
    bsPupilRight,
    bsFaceTattoo,
    bsEyesShade,
    bsEyes, // special!!!
    bsEyebrows,
    esEyes, //esGlasses// 10
    bsHairMid,
    bsEars,
    bsAntlers,
    bsHairFront,
    esHat,             // 11

    bsArms,
    bsArmsTattoo,

    esShoulders,       // 12
    vsArms,

    esHands, //esWeapon// 13
    esRing,            // 14
    bsHandsOver,
    vsHandsOver,

    esWrists,           // 15

    esLeash            // 16
  }


  TApparelSlot = (

    //asNone,

    bsTail,
    bsTailTip,

    bsTorsoUnder,
    bsLegs,
    bsLegsClaws,
    bsTorsoOver,
    bsBreasts,
    bsNipples,
    bsNethers,
    bsPubic,
    bsBodyTattoo,

    esBottomUnder,
    esTopUnder,
    esBottomOver,
    esTopOver,

    bsHands,
    bsNails,

    esTopOverOver,
    esFeet,
    esAmulet,

    bsMouth,
    bsNose,
    bsHair,
    bsEars,

    esNeck, // TODO: change to esCollar + under bsHairFront

    esWeapon,

    bsEyes,
    bsEyebrows,
    esGlasses
  );

type
  TApparelSlotsSet = Set of TApparelSlot;
  TApparelSlotsList = specialize TList<TApparelSlot>;

type
  EItemSlotConversionError = class(Exception);

var
  UiApparelSlots: TApparelSlotsList;

function ApparelSlotToStr(const AEquipSlot: TApparelSlot): String;
function StrToApparelSlot(const AString: String): TApparelSlot;
function ApparelSlotsStringToSlotsSet(const AString: String): TApparelSlotsSet;
implementation
uses
  TypInfo,
  CastleStringUtils;

function ApparelSlotToStr(const AEquipSlot: TApparelSlot): String;
begin
  Result := GetEnumName(TypeInfo(TApparelSlot), Ord(AEquipSlot));
end;
function StrToApparelSlot(const AString: String): TApparelSlot;
var
  E: TApparelSlot;
begin
  // TODO: Optimize
  for E in TApparelSlot do
    if ApparelSlotToStr(E) = AString then
      Exit(E);
  raise EItemSlotConversionError.CreateFmt('Cannot find EquipSlot: "%s"', [AString]);
end;

function ApparelSlotsStringToSlotsSet(const AString: String): TApparelSlotsSet;
var
  SlotStringList: TCastleStringList;
  S: String;
begin
  SlotStringList := CreateTokens(AString, [',']);
  Result := [];
  for S in SlotStringList do
    Result := Result + [StrToApparelSlot(S)];
  FreeAndNil(SlotStringList);
end;

initialization
  UiApparelSlots := TApparelSlotsList.Create;
  UiApparelSlots.Add(esFeet);
  UiApparelSlots.Add(esBottomOver);
  UiApparelSlots.Add(esBottomUnder);
  UiApparelSlots.Add(esTopUnder);
  UiApparelSlots.Add(esTopOver);
  UiApparelSlots.Add(esTopOverOver);
  UiApparelSlots.Add(esAmulet);
  UiApparelSlots.Add(esNeck);
  UiApparelSlots.Add(esGlasses);
  UiApparelSlots.Add(esWeapon);
finalization
  FreeAndNil(UiApparelSlots);
end.

