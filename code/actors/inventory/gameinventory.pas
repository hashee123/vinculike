{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameInventory;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameApparelSlots, GameItemData, GameAbstractItem, GameInventoryItem,
  GamePaperDollSprite, GameEnchantmentAbstract, GameColorTags;

const
  WeaponDamageOnHit = Single(2.0);
  BondageDamageOnHit = Single(0.5); // I really don't like having 2 different coefficients, but we need to balance stamina-durability-damage
  StealthDamageMultiplier = 5;  // min player damage = 5 * 0.73 = 3.6 [3.6*5 = 18.1; 5*5 = 25; all weapons will insta-kill any enemy]

type
  TEquipItemResult = (eiOk, eiNoHands, eiNoLegs, eiSlotsUnavailable, eiSlotRestrained);

type
  TInventory = class(TObject)
  strict private
    WasTopCovered, WasBottomCovered: Boolean;
    procedure StartForcedCoveredChange;
    procedure EndForcedCoveredChange;
    procedure UpdateCoveredStatus;
  public
    TopCovered, BottomCovered: Integer;
    TopCoveredRemovable, BottomCoveredRemovable: Integer;
  strict private
    FEnchantmentsMultiplierCache: TEnchantmentCache;
    FEnchantmentsAdditiveCache: TEnchantmentCache;
    FUpdateInventory: Boolean;
    function GetEquipped(const ApparelSlot: TApparelSlot): TInventoryItem;
    procedure SetEquipped(const ApparelSlot: TApparelSlot; const EquipItem: TInventoryItem);
    function EnchantmentRequirementsMet(const Enchantment: TEnchantmentAbstract): Boolean;
    procedure InvalidateInventory;
    procedure RegenerateEnchantmentsCache;
  public
    Apparel: array [TApparelSlot] of TAbstractItem;
    BodyColor: TColorDictionary;
    Censored: Boolean;
    CensoredInScreenshots: Boolean;
    property Equipped [ApparelSlot: TApparelSlot]: TInventoryItem read GetEquipped write SetEquipped;
    function GetRandomClothesSlot: TApparelSlot;
    function GetRandomClothesSlotEquippedItem: TApparelSlot;
    function GetRandomClothesSlotEquippedItemOrBondage: TApparelSlot;
    { Clothes and weapons }
    function GetRandomEquipmentSlot: TApparelSlot;
    function GetRandomEquipmentSlotEquippedItem: TApparelSlot;
    function GetRandomEquipmentSlotEquippedItemOrBondage: TApparelSlot;
    // TODO: move those to UpdateCoveredStatus
    function EquippedSlotsRemovable: Integer;
    function EquippedClothesRemovable: Integer;
    function EquippedClothesAndBondage: Integer;
    // .
    function TotalIdleNoise: Single;
    function GetItemNoiseSafe(const Slot: TApparelSlot): Single;
    procedure DamageItem(const Slot: TApparelSlot; const Damage: Single);
    procedure DamageItem(const AItem: TInventoryItem; const Damage: Single);
    { Gets damage of the Player item or unarmed }
    function GetDamage: Single;
    function GetStealthDamage: Single;
    procedure DamageWeapon;
    procedure PlayAttackSound;
    { Will either damage footwear and report false or return true (to hurt the parent) }
    function HurtFeet(const Damage: Single): Boolean;
    function PartiallyOrFullyNude: Boolean;
    function Nude: Boolean;
    procedure WardrobeMalfunction;
    function CanEquipItem(const AItem: TInventoryItem): TEquipItemResult;
    procedure EquipItem(const AItem: TInventoryItem);
    procedure EquipApparel(const AItem: TAbstractItem);
    function UnequipAndReturn(const AEquipSlot: TApparelSlot; const Forced: Boolean): TInventoryItem;
    procedure UnequipAndDrop(const AEquipSlot: TApparelSlot; const Forced: Boolean);
    procedure UnequipAndDisintegrate(const AEquipSlot: TApparelSlot; const Forced: Boolean);
    { TODO: item is equipped but passed as InventoryItem instance,
      that's because it comes from DamageItem
      but it doesn't make sense in this specific case }
    procedure DisintegrateEquippedItem(const AItem: TInventoryItem; const Forced: Boolean);
    procedure DisintegrateOrDrop(const AEquipSlot: TApparelSlot; const Forced: Boolean);
    function DisintegrateOrReturn(const AEquipSlot: TApparelSlot; const Forced: Boolean): TInventoryItem;
    procedure DestroyEquippedItemSafe(const AEquipSlot: TApparelSlot);
    procedure UnequipAndDisintegrateEverything;
  public
    function CanUseHands: Boolean; //override; todo?
    function HearingBonusSqr: Single;
    function VisionBonus: Single;
    function XRayVision: Boolean;
    function Blindfolded: Boolean;
    function FindEffectAdditive(const EffectClass: TEnchantmentClass): Single;
    function FindEffectMultiplier(const EffectClass: TEnchantmentClass): Single;
  public
    procedure MaximizeDurability(const AEquipSlot: TApparelSlot);
    procedure ReinforceItem(const AEquipSlot: TApparelSlot);
    function ShouldMaximizeDurability(const AEquipSlot: TApparelSlot): Boolean;
    function ShouldMaximizeDurability(const AItemName: String): Boolean;
    function HasItem(const AOtherItemData: TItemData): Boolean;
    function HasItem(const AOtherItemName: String): Boolean;
  public
    PaperDollSprite: TPaperDollSprite;
    procedure Update(const SecondsPassed: Single);
  public
    procedure Save(const Element: TDOMElement);
    procedure Load(const Element: TDOMElement);
  public
    { Parent Actor for this Inventory
      Cannot be properly typed because of cyclic reference }
    Parent: TObject;
    constructor Create; // override;
    { Must be called after constructor because needs
      a properly assigned Parent and initialized Parent.Inventory }
    procedure CreatePaperDoll;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils, CastleColors,
  GameViewGame, GameRandom, GameLog, GameTranslation, GameSounds, GameColors,
  GameMap, GameMapItem, GameActor, GamePlayerCharacter, GameStats,
  GameItemsDatabase, GameBodyPart, GameDifficultyLevel,
  GameEnchantmentHearingBonus, GameEnchantmentXRayVision, GameEnchantmentVisionBonus,
  GameEnchantmentDamageBonus, GameEnchantmentDamageMultiplier,
  GameEnchantmentDisorientation, GameEnchantmentBlindfold, GameEnchantmentStripWillpowerDamageMultiplier,
  GameEnchantmentStealthDamageBonus, GameEnchantmentStealthDamageMultiplier;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$ENDIF}

procedure TInventory.StartForcedCoveredChange;
begin
  WasTopCovered := TopCovered > 0;
  WasBottomCovered := BottomCovered > 0;
end;

procedure TInventory.EndForcedCoveredChange;
var
  SomethingExposed: Boolean;
  WillpowerDisrobingDamage: Single;
begin
  SomethingExposed := false;
  if WasTopCovered and (TopCovered = 0) and WasBottomCovered and (BottomCovered = 0) then
  begin
    ShowLog('%s is buck naked', [ParentActor.Data.DisplayName], ColorLogNaked);
    WillpowerDisrobingDamage := ParentPlayer.Will * FindEffectMultiplier(TEnchantmentStripWillpowerDamageMultiplier) *
      (Difficulty.StripTopWillpowerFractionDamage + Difficulty.StripBottomWillpowerFractionDamage *
      (1 - Difficulty.StripTopWillpowerFractionDamage)); // Otherwise it'll result in higher fraction if otherwise those would have been applied sequentially which gives a noticeable disadvantage to items that cover both top and bottom
    SomethingExposed := true;
  end else
  if WasTopCovered and (TopCovered = 0) then
  begin
    ShowLog(GetTranslation('PlayerLostTop'), [ParentActor.Data.DisplayName], ColorLogNaked);
    WillpowerDisrobingDamage := ParentPlayer.Will * Difficulty.StripTopWillpowerFractionDamage * FindEffectMultiplier(TEnchantmentStripWillpowerDamageMultiplier);
    SomethingExposed := true;
  end else
  if WasBottomCovered and (BottomCovered = 0) then
  begin
    ShowLog(GetTranslation('PlayerLostBottom'), [ParentActor.Data.DisplayName], ColorLogNaked);
    WillpowerDisrobingDamage := ParentPlayer.Will * Difficulty.StripBottomWillpowerFractionDamage * FindEffectMultiplier(TEnchantmentStripWillpowerDamageMultiplier);
    SomethingExposed := true;
  end;
  if SomethingExposed then
  begin
    ParentPlayer.DegradeWill(WillpowerDisrobingDamage / 2); // note, we don't degrade full damage
    ParentPlayer.Will -= WillpowerDisrobingDamage / 2;
    if WillpowerDisrobingDamage > ParentPlayer.Will * 0.01 then
      ShowLog('%s lost %.1n willpower', [ParentActor.Data.DisplayName, WillpowerDisrobingDamage], ColorLogWillDamage);
    if FindEffectMultiplier(TEnchantmentStripWillpowerDamageMultiplier) > 0.5 then
      ParentPlayer.PlaySurpriseSound;
  end;
end;

procedure TInventory.UpdateCoveredStatus;
var
  E: TApparelSlot;
begin
  TopCovered := 0;
  BottomCovered := 0;
  TopCoveredRemovable := 0;
  BottomCoveredRemovable := 0;
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Equipped[E] <> nil) and (E = Equipped[E].Data.MainSlot) then
      begin
        if Equipped[E].ItemData.CoversTop then
        begin
          Inc(TopCovered);
          if not Equipped[E].ItemData.IsBondage then
            Inc(TopCoveredRemovable);
        end;
        if Equipped[E].ItemData.CoversBottom then
        begin
          Inc(BottomCovered);
          if not Equipped[E].ItemData.IsBondage then
            Inc(BottomCoveredRemovable);
        end;
      end;
end;

function TInventory.GetEquipped(const ApparelSlot: TApparelSlot): TInventoryItem;
begin
  Exit(Apparel[ApparelSlot] as TInventoryItem);
end;

procedure TInventory.SetEquipped(const ApparelSlot: TApparelSlot;
  const EquipItem: TInventoryItem);
begin
  Apparel[ApparelSlot] := EquipItem;
  InvalidateInventory;
end;

function TInventory.GetRandomClothesSlot: TApparelSlot;
begin
  Result := ParentPlayer.Blueprint.ClothesSlotsList[Rnd.Random(ParentPlayer.Blueprint.ClothesSlotsList.Count)];
end;

function TInventory.GetRandomClothesSlotEquippedItem: TApparelSlot;
begin
  repeat
    Result := GetRandomClothesSlot;
  until (Equipped[Result] <> nil) and not Equipped[Result].ItemData.IsBondage; // warning, no checks! It can freeze if all are nil
end;

function TInventory.GetRandomClothesSlotEquippedItemOrBondage: TApparelSlot;
begin
  repeat
    Result := GetRandomClothesSlot;
  until Equipped[Result] <> nil; // warning, no checks! It can freeze if all are nil
end;

function TInventory.GetRandomEquipmentSlot: TApparelSlot;
begin
  Result := ParentPlayer.Blueprint.EquipmentSlotsList[Rnd.Random(ParentPlayer.Blueprint.EquipmentSlotsList.Count)];
end;

function TInventory.GetRandomEquipmentSlotEquippedItem: TApparelSlot;
begin
  repeat
    Result := GetRandomEquipmentSlot;
  until (Equipped[Result] <> nil) and (not Equipped[Result].ItemData.IsBondage); // warning, no checks! It can freeze if all are nil
end;

function TInventory.GetRandomEquipmentSlotEquippedItemOrBondage: TApparelSlot;
begin
  repeat
    Result := GetRandomEquipmentSlot;
  until Equipped[Result] <> nil; // warning, no checks! It can freeze if all are nil
end;

function TInventory.EquippedSlotsRemovable: Integer;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Equipped[E] <> nil) and not Equipped[E].ItemData.IsBondage then
      Inc(Result);
end;

function TInventory.EquippedClothesRemovable: Integer;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in ParentPlayer.Blueprint.ClothesSlots do
    if (Equipped[E] <> nil) and not Equipped[E].ItemData.IsBondage then
      Inc(Result);
end;

function TInventory.EquippedClothesAndBondage: Integer;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in ParentPlayer.Blueprint.ClothesSlots do
    if Equipped[E] <> nil then
      Inc(Result);
end;

function TInventory.TotalIdleNoise: Single;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Equipped[E] <> nil) and (Equipped[E].Data.MainSlot = E) then
      Result += Equipped[E].ItemData.Noise;
end;

function TInventory.GetItemNoiseSafe(const Slot: TApparelSlot): Single;
begin
  if Equipped[Slot] <> nil then
    Exit(Equipped[Slot].ItemData.Noise)
  else
    Exit(0); // while this is an erroneous situation, we don't show log here - we properly workaround it
end;

procedure TInventory.DamageItem(const Slot: TApparelSlot;
  const Damage: Single);
begin
  DamageItem(Equipped[Slot], Damage);
end;

procedure TInventory.DamageItem(const AItem: TInventoryItem;
  const Damage: Single);
begin
  if AItem.ItemData.Indestructible then
    Exit;

  AItem.Durability -= Damage;
  // item breaks
  if AItem.ItemData.IsBondage then
  begin
    if AItem.Durability <= 0 then
    begin
      if Parent = ViewGame.CurrentCharacter then
      begin
        ViewGame.ShakeCharacter;
      end;
      Sound(AItem.ItemData.SoundDisintegrate);
      DisintegrateEquippedItem(AItem, true);
    end;
  end else
  if Rnd.Random > (3 * AItem.Durability / AItem.MaxDurability) then
  begin
    ViewGame.ShakeCharacter;

    if (Rnd.Random < AItem.Durability / ItemDisintegrationThreshold) and AItem.ItemData.CanBeRepaired then
    begin
      if Parent = ViewGame.CurrentCharacter then
        ShowLog(GetTranslation('ItemBreaksLog'), [AItem.Data.DisplayName], ColorLogItemBreak);
      AItem.Broken := true;
      Sound(AItem.ItemData.SoundBreak);
      UnequipAndDrop(AItem.Data.MainSlot, true);
    end else
    begin
      Sound(AItem.ItemData.SoundDisintegrate);
      DisintegrateEquippedItem(AItem, true);
    end;

    ParentPlayer.HitWill(Difficulty.BrokenItemWillpowerDamage);
  end else
  // a chance to slip off
  if (Rnd.Random < AItem.ItemData.WardrobeMalfunctionChance) then
  begin
    if Parent = ViewGame.CurrentCharacter then
      ShowLog(GetTranslation('ItemWardrobeMalfunctionLog'), [AItem.Data.DisplayName], ColorLogItemBreak);
    Sound(AItem.ItemData.SoundUnequip);
    UnequipAndDrop(AItem.Data.MainSlot, true);
  end;
end;

function TInventory.GetDamage: Single;
begin
  if (Equipped[esWeapon] <> nil) and not Equipped[esWeapon].ItemData.IsBondage then
    Result := Equipped[esWeapon].ItemData.Damage
  else
  if (Equipped[esWeapon] <> nil) {and Equipped[esWeapon].Data.IsBondage -- obvious} then
  begin
    if Equipped[esWeapon].ItemData.Damage > 0 then
      Result := Equipped[esWeapon].ItemData.Damage
    else
      Result := ParentPlayer.Blueprint.PunchDamage; // Set damage to -1 if character can use unarmed damage with this bondage item
  end else
    Result := ParentPlayer.Blueprint.PunchDamage;
  Result *= FindEffectMultiplier(TEnchantmentDamageMultiplier);
  Result += FindEffectAdditive(TEnchantmentDamageBonus);
end;

function TInventory.GetStealthDamage: Single;
begin
  Exit(FindEffectAdditive(TEnchantmentStealthDamageBonus) + GetDamage * StealthDamageMultiplier * FindEffectMultiplier(TEnchantmentStealthDamageMultiplier));
end;

procedure TInventory.WardrobeMalfunction;
var
  E: TApparelSlot;
begin
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Equipped[E] <> nil) and (Equipped[E].Data.MainSlot = E) and (Rnd.Random < Equipped[E].ItemData.WardrobeMalfunctionChance) then
    begin
      if Parent = ViewGame.CurrentCharacter then
        ShowLog(GetTranslation('ItemWardrobeMalfunctionLog'), [Equipped[E].Data.DisplayName], ColorLogItemBreak);
      Sound(Equipped[E].ItemData.SoundUnequip);
      UnequipAndDrop(E, true);
    end;
end;

function TInventory.CanEquipItem(const AItem: TInventoryItem): TEquipItemResult;
var
  E: TApparelSlot;
begin
  for E in AItem.Data.EquipSlots do
    if not (E in ParentPlayer.Blueprint.EquipmentSlots) then
      Exit(eiSlotsUnavailable); // without specifying which exactly

  // TODO: more generic handling of equip/bondage
  for E in AItem.Data.EquipSlots do
    if (Apparel[E] <> nil) and Equipped[E].ItemData.IsBondage then
      Exit(eiSlotRestrained); // without specifying which exactly

  //if AItem.ItemData.RequiresArms and (not ParentPlayer.Blueprint.HasArms) then
  //  Exit(eiNoHands);

  if AItem.ItemData.RequiresLegs and (not ParentPlayer.Blueprint.HasLegs) then
    Exit(eiNoLegs);

  Exit(eiOk);
end;

procedure TInventory.EquipItem(const AItem: TInventoryItem);
var
  E: TApparelSlot;
begin
  for E in AItem.Data.EquipSlots do
    UnequipAndDrop(E, false); // TODO: fail, maybe crash: make sure all slots are empty before equipping anything
  for E in AItem.Data.EquipSlots do
    Equipped[E] := AItem; // the item takes all slots
  UpdateCoveredStatus; // WARNING: Do not optimize, next actions may require this
  LocalStats.IncStat('equipped_' + AItem.Data.Id);
end;

procedure TInventory.EquipApparel(const AItem: TAbstractItem);
var
  E: TApparelSlot;
begin
  if AItem is TInventoryItem then
    EquipItem(TInventoryItem(AItem))
  else
    for E in AItem.Data.EquipSlots do
      Apparel[E] := AItem;
  InvalidateInventory;
end;

procedure TInventory.UnequipAndDrop(const AEquipSlot: TApparelSlot;
  const Forced: Boolean);
var
  AItem: TInventoryItem;
begin
  if Apparel[AEquipSlot] = nil then
    Exit; // TODO: This is not needed, make call safe on logic level
  AItem := UnequipAndReturn(AEquipSlot, Forced);
  TMapItem.DropItem(ParentPlayer.LastTileX + ParentPlayer.PredSize div 2, ParentPlayer.LastTileY + ParentPlayer.PredSize div 2, AItem, false);
end;

procedure TInventory.UnequipAndDisintegrate(const AEquipSlot: TApparelSlot;
  const Forced: Boolean);
var
  AItem: TInventoryItem;
begin
  if Apparel[AEquipSlot] = nil then
    Exit; // TODO: This is not needed, make call safe on logic level
  AItem := UnequipAndReturn(AEquipSlot, Forced);
  FreeAndNil(AItem);
end;

procedure TInventory.DisintegrateEquippedItem(const AItem: TInventoryItem;
  const Forced: Boolean);
var
  I: Integer;
  DisintegratedItem: TInventoryItem;
begin
  if AItem.ItemData.DisintegratesInto.Count = 0 then
  begin
    if Forced and (Parent = ViewGame.CurrentCharacter) then
      ShowLog(GetTranslation('ItemDisintegratesLog'), [AItem.Data.DisplayName], ColorLogItemDisintegrated);
    UnequipAndDisintegrate(AItem.Data.MainSlot, Forced);
  end else
  begin
    if AItem.ItemData.DisintegrationReequip then
    begin
      if Forced and (Parent = ViewGame.CurrentCharacter) then
        ShowLog('%s breaks', [AItem.Data.DisplayName], ColorLogItemDisintegrated);
      if Forced then
        StartForcedCoveredChange;
      DisintegratedItem := UnequipAndReturn(AItem.Data.MainSlot, false); // forced=false because we'll check if something got uncovered after reequipping remains
      for I := 0 to Pred(AItem.ItemData.DisintegratesInto.Count) do
        EquipItem(TInventoryItem.NewItem(ItemsDataDictionary[AItem.ItemData.DisintegratesInto[I]] as TItemData));
        // TODO set durability based on parent item https://gitlab.com/EugeneLoza/vinculike/-/issues/1278
      FreeAndNil(DisintegratedItem);
      if Forced then
        EndForcedCoveredChange; // note: both UnequipAndDisintegrate and EquipItem will ask for UpdateCoveredStatus, no need to call it manually
    end else
    begin
      if Forced and (Parent = ViewGame.CurrentCharacter) then
        ShowLog(GetTranslation('ItemDisintegratesLog'), [AItem.Data.DisplayName], ColorLogItemDisintegrated);
      DisintegratedItem := UnequipAndReturn(AItem.Data.MainSlot, Forced);
      for I := 0 to Pred(AItem.ItemData.DisintegratesInto.Count) do
        TMapItem.DropItem(ParentPlayer.LastTileX + ParentPlayer.PredSize div 2, ParentPlayer.LastTileY + ParentPlayer.PredSize div 2, TInventoryItem.NewItem(ItemsDataDictionary[AItem.ItemData.DisintegratesInto[I]] as TItemData), false);
        // TODO set durability based on parent item https://gitlab.com/EugeneLoza/vinculike/-/issues/1278
      FreeAndNil(DisintegratedItem);
    end;
  end;
end;

procedure TInventory.DisintegrateOrDrop(const AEquipSlot: TApparelSlot;
  const Forced: Boolean);
begin
  if Apparel[AEquipSlot] = nil then
    Exit;
  if Equipped[AEquipSlot].ItemData.UnequipDisintegrates then
    UnequipAndDisintegrate(AEquipSlot, Forced)
  else
    UnequipAndDrop(AEquipSlot, Forced);
end;

function TInventory.DisintegrateOrReturn(const AEquipSlot: TApparelSlot;
  const Forced: Boolean): TInventoryItem;
begin
  if Apparel[AEquipSlot] = nil then
    Exit(nil);
  if Equipped[AEquipSlot].ItemData.UnequipDisintegrates then
  begin
    UnequipAndDisintegrate(AEquipSlot, Forced);
    Exit(nil);
  end else
    Exit(UnequipAndReturn(AEquipSlot, Forced));
end;

procedure TInventory.DestroyEquippedItemSafe(const AEquipSlot: TApparelSlot);
var
  AItem: TAbstractItem;
  E: TApparelSlot;
begin
  AItem := Apparel[AEquipSlot];
  if (AItem <> nil) then
  begin
    for E in AItem.Data.EquipSlots do
      if Apparel[E] = AItem then
        Apparel[E] := nil;
    FreeAndNil(AItem);
    InvalidateInventory;
  end;
end;

procedure TInventory.UnequipAndDisintegrateEverything;
var
  E: TApparelSlot;
begin
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    UnequipAndDisintegrate(E, false);
end;

function TInventory.UnequipAndReturn(const AEquipSlot: TApparelSlot;
  const Forced: Boolean): TInventoryItem;
var
  E: TApparelSlot;
begin
  if Forced then
    StartForcedCoveredChange;

  Result := Equipped[AEquipSlot];
  for E in Result.Data.EquipSlots do
    if Apparel[E] = Result then
      Apparel[E] := nil;
  Result.Stop;
  InvalidateInventory;

  UpdateCoveredStatus;
  if Forced then
    EndForcedCoveredChange;
end;

procedure TInventory.DamageWeapon;
begin
  inherited;
  if (Equipped[esWeapon] <> nil) and not Equipped[esWeapon].ItemData.IsBondage then // TODO: IsBondage
    DamageItem(Equipped[esWeapon], WeaponDamageOnHit)
  else
  if (Equipped[esWeapon] <> nil) then
  begin
    // TODO: more generic
    if Equipped[esWeapon].ItemData.Damage > 0 then
      DamageItem(Equipped[esWeapon], BondageDamageOnHit)
  end else
  //Hit(1)
    ;
end;

procedure TInventory.PlayAttackSound;
begin
  if Equipped[esWeapon] <> nil then
    Sound(Equipped[esWeapon].ItemData.SoundAttack)
  else
    Sound('hit_punch');
end;

function TInventory.HurtFeet(const Damage: Single): Boolean;
var
  E: TApparelSlot;
begin
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Equipped[E] <> nil) and Equipped[E].ItemData.ProtectsFeet then // warning if there are multiple items protecting feet, it'll pick the first one
    begin
      Equipped[E].Durability -= Damage * Difficulty.FootwearDamageMultiplier;
      if Equipped[E].Durability < 0 then
      begin
        if Rnd.Random < Equipped[E].MaxDurability / ItemDisintegrationThreshold then
        begin
          if Parent = ViewGame.CurrentCharacter then
          begin
            ViewGame.ShakeCharacter;
            ShowLog('%s wear off', [Equipped[E].Data.DisplayName], ColorLogItemDisintegrated);
          end;
          Sound(Equipped[E].ItemData.SoundBreak);
          Equipped[E].Durability := 0.01; // just avoid it going < 0
          Equipped[E].Broken := true;
          DisintegrateOrDrop(E, false);
        end else
        begin
          if Parent = ViewGame.CurrentCharacter then
          begin
            ViewGame.ShakeCharacter;
            ShowLog('%s completely wear off', [Equipped[E].Data.DisplayName], ColorLogItemDisintegrated);
          end;
          Sound(Equipped[E].ItemData.SoundDisintegrate);
          UnequipAndDisintegrate(E, false);
        end;
      end;
      Exit(false);
    end;
  Exit(true);
end;

function TInventory.PartiallyOrFullyNude: Boolean;
begin
  Result := (TopCovered = 0) or (BottomCovered = 0);
end;

function TInventory.Nude: Boolean;
begin
  Result := (TopCovered = 0) and (BottomCovered = 0);
end;

function TInventory.CanUseHands: Boolean;
begin
  // TODO: more generic, will hurt when poses will be introduced
  if Equipped[esWeapon] <> nil then
    Exit(not Equipped[esWeapon].ItemData.HandsUnavailable)
  else
    Exit(true);
end;

function TInventory.HearingBonusSqr: Single;
begin
  // TODO: This is an expensive call and we call it on every monster every frame! Optimize!
  Exit(Sqr(FindEffectAdditive(TEnchantmentHearingBonus)));
end;

function TInventory.VisionBonus: Single;
begin
  // TODO: This is an expensive call and we call it on every monster every frame! Optimize!
  Exit(FindEffectAdditive(TEnchantmentVisionBonus));
end;

function TInventory.XRayVision: Boolean;
begin
  // TODO: This is an expensive call and we call it on every monster every frame! Optimize!
  Exit(FindEffectAdditive(TEnchantmentXRayVision) > 0);
end;

function TInventory.Blindfolded: Boolean;
begin
  Exit(FindEffectAdditive(TEnchantmentBlindfold) > 0);
end;

function TInventory.EnchantmentRequirementsMet(const Enchantment: TEnchantmentAbstract): Boolean;
begin
  Exit(
    (not Enchantment.RequiresNudeTop or (TopCovered = 0)) and
    (not Enchantment.RequiresDressedTop or (TopCovered > 0)) and
    (not Enchantment.RequiresNudeBottom or (BottomCovered = 0)) and
    (not Enchantment.RequiresDressedBottom or (BottomCovered > 0)) and
    (not Enchantment.RequiresUnarmed or ((Equipped[esWeapon] = nil) or (Equipped[esWeapon].ItemData.Damage <= 0))) and
    (ParentPlayer.Health / ParentPlayer.PlayerCharacterData.MaxHealth <= Enchantment.RequiresHealthBelow)
  );
end;

procedure TInventory.InvalidateInventory;
begin
  FUpdateInventory := true;
  // we need to call it twice? Because otherwise Inventory buttons can get updated for obsolete state of the inventory (and SIGSEGV)
  // It shouldn't cause any issues, just sets a flag to update the inventory
  ViewGame.InvalidateInventory(Parent);
end;

procedure TInventory.RegenerateEnchantmentsCache;
var
  A: TApparelSlot;
  E: TEnchantmentAbstract;
begin
  FEnchantmentsAdditiveCache.Clear;
  FEnchantmentsMultiplierCache.Clear;
  for A in ParentPlayer.Blueprint.EquipmentSlots do
    if (Equipped[A] <> nil) and (Equipped[A].Data.MainSlot = A) then
      for E in Equipped[A].Enchantments do
        if EnchantmentRequirementsMet(E) then
          if FEnchantmentsMultiplierCache.ContainsKey(E.EnchantmentClass) then
          begin
            FEnchantmentsMultiplierCache[E.EnchantmentClass] := FEnchantmentsMultiplierCache[E.EnchantmentClass] * E.Strength;
            FEnchantmentsAdditiveCache[E.EnchantmentClass] := FEnchantmentsAdditiveCache[E.EnchantmentClass] + E.Strength;
          end else
          begin
            FEnchantmentsMultiplierCache.Add(E.EnchantmentClass, E.Strength);
            FEnchantmentsAdditiveCache.Add(E.EnchantmentClass, E.Strength);
          end;
end;

function TInventory.FindEffectAdditive(const EffectClass: TEnchantmentClass): Single;
begin
  if FEnchantmentsAdditiveCache.ContainsKey(EffectClass) then
    Exit(FEnchantmentsAdditiveCache[EffectClass])
  else
    Exit(0.0);
end;

function TInventory.FindEffectMultiplier(const EffectClass: TEnchantmentClass): Single;
begin
  if FEnchantmentsMultiplierCache.ContainsKey(EffectClass) then
    Exit(FEnchantmentsMultiplierCache[EffectClass])
  else
    Exit(1.0);
end;

procedure TInventory.MaximizeDurability(const AEquipSlot: TApparelSlot);
begin
  Equipped[AEquipSlot].MaxDurability := Equipped[AEquipSlot].ItemData.Durability;
  Equipped[AEquipSlot].Durability := Equipped[AEquipSlot].MaxDurability;
end;

procedure TInventory.ReinforceItem(const AEquipSlot: TApparelSlot);
begin
  Equipped[AEquipSlot].MaxDurability := (Equipped[AEquipSlot].MaxDurability + Equipped[AEquipSlot].ItemData.Durability) / 2;
  Equipped[AEquipSlot].Durability := (Equipped[AEquipSlot].Durability + Equipped[AEquipSlot].MaxDurability) / 2;
end;

function TInventory.ShouldMaximizeDurability(const AEquipSlot: TApparelSlot): Boolean;
begin
  Exit((Equipped[AEquipSlot] <> nil) and (Equipped[AEquipSlot].Durability < Equipped[AEquipSlot].ItemData.Durability / 2));
end;

function TInventory.ShouldMaximizeDurability(const AItemName: String): Boolean;
var
  AItem: TItemData;
begin
  AItem := ItemsDataDictionary[AItemName] as TItemData;
  if HasItem(AItem) then
    Exit(ShouldMaximizeDurability(AItem.MainSlot))
  else
    Exit(false);
end;

function TInventory.HasItem(const AOtherItemData: TItemData): Boolean;
begin
  Exit((Equipped[AOtherItemData.MainSlot] <> nil) and (Equipped[AOtherItemData.MainSlot].Data = AOtherItemData));
end;

function TInventory.HasItem(const AOtherItemName: String): Boolean;
begin
  //Exit(HasItem(ItemsDataDictionary[AOtherItemName]));
  Exit((Equipped[ItemsDataDictionary[AOtherItemName].MainSlot] <> nil) and (Equipped[ItemsDataDictionary[AOtherItemName].MainSlot].Data.Id = AOtherItemName));
end;

procedure TInventory.Update(const SecondsPassed: Single);
var
  Disorient: Integer;
  E: TApparelSlot;
begin
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Equipped[E] <> nil) and (Equipped[E].Data.MainSlot = E) then
      Equipped[E].Update(Parent, SecondsPassed);

  if FUpdateInventory then
  begin
    FUpdateInventory := false;
    UpdateCoveredStatus; // As we always do this in Equip/Unequip calls, it's redundant here, but better safe than sorry
    // Next one must account for proper covered status
    RegenerateEnchantmentsCache;
    //ViewGame.InvalidateInventory(Parent); - we do that in InvalidateInventory and it may be already updated at this point
    //ViewGame.InvalidatePosition(Parent); // in case some vision-related enchantments changed
    Map.FScheduleUpdateVisible := true;
  end;

  if Map.FScheduleUpdateVisible then // TEMPORARY: TODO
  begin
    Disorient := Round(FindEffectAdditive(TEnchantmentDisorientation));
    if Disorient > 0 then
      Map.ForgetVisible(Disorient);
  end;
end;

procedure TInventory.Save(const Element: TDOMElement);
var
  E: TApparelSlot;
  ColorTag: String;
begin
  for E in TApparelSlot do
    if (Apparel[E] <> nil) and (Apparel[E].Data.MainSlot = E) then
      Apparel[E].Save(Element.CreateChild(ApparelSlotToStr(E)));
  for ColorTag in ParentPlayer.Blueprint.ColorTags.Keys do
    Element.AttributeColorSet('ColorTag_' + ColorTag, BodyColor[ColorTag]);
  Element.AttributeSet('Censored', Censored);
  Element.AttributeSet('CensoredInScreenshots', CensoredInScreenshots);
end;

procedure TInventory.Load(const Element: TDOMElement);
var
  E: TApparelSlot;
  ColorTag: String;
begin
  for E in TApparelSlot do
    if Element.Child(ApparelSlotToStr(E), false) <> nil then
    begin
      try
        EquipApparel(TAbstractItem.LoadClass(Element.Child(ApparelSlotToStr(E), true)) as TAbstractItem);
      except
        on Ex: Exception do
        begin
          try
            if Element.Child(ApparelSlotToStr(E), false) = nil then
              ShowError('%s:"%s". Cannot load player "%s" inventory slot %s. Record not found.', [Ex.ClassName, Ex.Message, ParentPlayer.Data.DisplayName, ApparelSlotToStr(E)])
            else
              ShowError('%s:"%s". Failed to load item %s in player "%s" inventory.', [Ex.ClassName, Ex.Message, Element.Child(ApparelSlotToStr(E), true).AttributeStringDef('Data.Id', 'N/A'), ParentPlayer.Data.DisplayName]);
          except
            ShowError('Exception while trying to report exception in TInventory.Load', []);
          end;
          Apparel[E] := nil; // just to make sure
        end;
      end;
    end;
  for E in ParentPlayer.Blueprint.BodySlots do
    if (Apparel[E] = nil) or not (Apparel[E] is TBodyPart) then
    begin
      ShowError('Failed to load body slot %s for %s. Applying random body slot.', [ApparelSlotToStr(E), ParentPlayer.Data.DisplayName]);
      EquipApparel(TBodyPart.NewBodyPart(GetRandomItemForSlot(E)));
    end;

  BodyColor.Add('none', White);
  for ColorTag in ParentPlayer.Blueprint.ColorTags.Keys do
    BodyColor.Add(ColorTag, Element.AttributeColorDef('ColorTag_' + ColorTag, ParentPlayer.Blueprint.ColorTags[ColorTag][Rnd.Random(ParentPlayer.Blueprint.ColorTags[ColorTag].Count)]));

  Censored := Element.AttributeBooleanDef('Censored', false);
  CensoredInScreenshots := Element.AttributeBooleanDef('CensoredInScreenshots', false);

  InvalidateInventory;
end;

constructor TInventory.Create;
begin
  inherited; //Parent is non-virtual and empty
  FEnchantmentsMultiplierCache := TEnchantmentCache.Create;
  FEnchantmentsAdditiveCache := TEnchantmentCache.Create;
  BodyColor := TColorDictionary.Create;
end;

procedure TInventory.CreatePaperDoll;
begin
  PaperDollSprite := TPaperDollSprite.Create;
  PaperDollSprite.Parent := Parent;
  // TODO: something more meaningful? This one will just blink an empty sprite for 1 frame.
  PaperDollSprite.UpdateToInventory; // just to avoid image = nil // TODO - optimize
end;

destructor TInventory.Destroy;
var
  E, E2: TApparelSlot;
begin
  FreeAndNil(BodyColor);
  FreeAndNil(PaperDollSprite);
  for E in TApparelSlot do
  begin
    for E2 in TApparelSlot do
      if (E2 <> E) and (Apparel[E] = Apparel[E2]) then
        Apparel[E2] := nil;
    FreeAndNil(Apparel[E]);
  end;
  FreeAndNil(FEnchantmentsMultiplierCache);
  FreeAndNil(FEnchantmentsAdditiveCache);
  inherited Destroy;
end;

end.

