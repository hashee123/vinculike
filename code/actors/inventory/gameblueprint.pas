{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameBlueprint;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM, Generics.Collections,
  GameSerializableData, GameApparelSlots, GameColorTags;

type
  { Blueprints of player characters (aka species+gender)
    WARNING: this concept seems quite flawed at the core
    We already have ActorData that defines static information for the actor
    However, as we use a "hack" to make ActorData individual for Player Characters
    we don't actually use ActorData in the way it was intended to be used
    (and it was intended to be same thing as Blueprint)
    So what we do here is we "workaround" around the "workaround"
    Which is never a good thing to do.
    TODO: Merge those two concepts.
    Make one part of the actor data "variable" (individual for actors)
    And another part of the actor data "static" (blueprints) }
  TBlueprint = class(TSerializableData)
  strict private const // move to apparel slots?
    AllTopSlots = [esTopOverOver, esTopOver, esTopUnder, esAmulet, esNeck, esGlasses];
    AllBottomSlots = [esFeet, esBottomUnder, esBottomOver];
  strict private
    FClothesSlots: TApparelSlotsSet;
    FEquipmentSlots: TApparelSlotsSet;
    FBodySlots: TApparelSlotsSet;
    FTopSlots: TApparelSlotsSet;
    FBottomSlots: TApparelSlotsSet;
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Id: String;
    DisplayName: String;
    PunchDamage: Single;
    //BiteDamage
    //KickDamage
    HasArms: Boolean;
    HasLegs: Boolean;
    BodyNoise: Single;
    WalkFeetDamageMultiplier: Single;
  public
    EquipmentSlotsList: TApparelSlotsList;
    ClothesSlotsList: TApparelSlotsList;
    ColorTags: TColorTagsDictionary;
    { Tested: property read field - identical speed to reading the field directly,
      Getter - 6x slower than property/field (in release mode, same in debug)
      Note: inlined Getter has the same speed, so can be used too }
    property ClothesSlots: TApparelSlotsSet read FClothesSlots;
    property EquipmentSlots: TApparelSlotsSet read FEquipmentSlots;
    property TopSlots: TApparelSlotsSet read FTopSlots;
    property BottomSlots: TApparelSlotsSet read FBottomSlots;
    property BodySlots: TApparelSlotsSet read FBodySlots;
  public
    function EquipSlotsToHumanReadableString(const AEquipSlots: TApparelSlotsSet): String;
    function EquipmentSlotToHumanReadableString(const AEquipSlot: TApparelSlot): String;
    function BodySlotToHumanReadableString(const AEquipSlot: TApparelSlot): String;
    procedure RandomBody(const TargetPlayer: TObject);
  public
    destructor Destroy; override;
  end;
  TBlueprintsList = specialize TObjectList<TBlueprint>;
  TBlueprintsDictionary = specialize TObjectDictionary<String, TBlueprint>;

implementation
uses
  CastleXmlUtils, CastleColors,
  GamePlayerCharacter, GameBodyPart,
  GameItemsDatabase, GameRandom;

type
  EBlueprintValidationError = class(Exception);

procedure TBlueprint.Validate;
begin
  if Id = '' then
    raise EBlueprintValidationError.CreateFmt('Id = "" in %s', [ClassName]);
  if DisplayName = '' then
    raise EBlueprintValidationError.CreateFmt('DisplayName = "" [%s:%s]', [ClassName, Id]);
  if PunchDamage <= 0 then
    raise EBlueprintValidationError.CreateFmt('PunchDamage <= 0 [%s:%s]', [ClassName, Id]);
  if BodyNoise <= 0 then
    raise EBlueprintValidationError.CreateFmt('BodyNoise <= 0 [%s:%s]', [ClassName, Id]);
  if WalkFeetDamageMultiplier < 0 then
    raise EBlueprintValidationError.CreateFmt('WalkFeetDamageMultiplier < 0 [%s:%s]', [ClassName, Id]);
  if FBodySlots = [] then
    raise EBlueprintValidationError.CreateFmt('FBodySlots = [] : [%s:%s]', [Id, ClassName]);
  if FClothesSlots = [] then
    raise EBlueprintValidationError.CreateFmt('FClothesSlots = [] : [%s:%s]', [Id, ClassName]);
  if FEquipmentSlots = [] then
    raise EBlueprintValidationError.CreateFmt('FEquipmentSlots = [] : [%s:%s]', [Id, ClassName]);
  if FTopSlots = [] then
    raise EBlueprintValidationError.CreateFmt('FTopSlots = [] : [%s:%s]', [Id, ClassName]);
  if FBottomSlots = [] then
    raise EBlueprintValidationError.CreateFmt('FBottomSlots = [] : [%s:%s]', [Id, ClassName]);
end;

procedure TBlueprint.Read(const Element: TDOMElement);
var
  E: TApparelSlot;
begin
  Id := Element.AttributeString('Id');
  DisplayName := Element.AttributeString('DisplayName');
  PunchDamage := Element.AttributeFloat('PunchDamage');
  BodyNoise := Element.AttributeFloat('BodyNoise');
  WalkFeetDamageMultiplier := Element.AttributeFloat('WalkFeetDamageMultiplier');
  HasArms := Element.AttributeBoolean('HasArms');
  HasLegs := Element.AttributeBoolean('HasLegs');
  FBodySlots := ApparelSlotsStringToSlotsSet(Element.AttributeString('BodySlots'));
  FClothesSlots := ApparelSlotsStringToSlotsSet(Element.AttributeString('ClothesSlots'));
  // Post-processing
  FEquipmentSlots := FClothesSlots;
  if HasArms then
    FEquipmentSlots += [esWeapon];
  FTopSlots := FClothesSlots * AllTopSlots;
  FBottomSlots := FClothesSlots * AllBottomSlots;

  ColorTags := GetColorTags(Element);

  ClothesSlotsList := TApparelSlotsList.Create;
  for E in ClothesSlots do
    ClothesSlotsList.Add(E);
  EquipmentSlotsList := TApparelSlotsList.Create;
  for E in EquipmentSlots do
    EquipmentSlotsList.Add(E);
end;

procedure TBlueprint.RandomBody(const TargetPlayer: TObject);
var
  E: TApparelSlot;
  P: TPlayerCharacter;
  ColorTag: String;
begin
  P := TargetPlayer as TPlayerCharacter;
  for E in BodySlots do
  begin
    // TODO: this is not good now as bodyparts occupy more than one slot?
    P.Inventory.DestroyEquippedItemSafe(E); // if there was something here (e.g. we generate a random body over the previous one)
    P.Inventory.EquipApparel(TBodyPart.NewBodyPart(GetRandomItemForSlot(E)));
  end;
  P.Inventory.BodyColor.Clear;
  P.Inventory.BodyColor.Add('none', White);
  for ColorTag in ColorTags.Keys do
    P.Inventory.BodyColor.Add(ColorTag, ColorTags[ColorTag][Rnd.Random(ColorTags[ColorTag].Count)]);
  P.Inventory.Censored := false;
end;

destructor TBlueprint.Destroy;
begin
  FreeAndNil(EquipmentSlotsList);
  FreeAndNil(ClothesSlotsList);
  FreeAndNil(ColorTags);
  inherited Destroy;
end;

function TBlueprint.EquipSlotsToHumanReadableString(const AEquipSlots: TApparelSlotsSet): String;
var
  WSlots: Integer;
  TSlots: Integer;
  BSlots: Integer;
  E: TApparelSlot;
begin
  WSlots := 0;
  TSlots := 0;
  BSlots := 0;
  for E in AEquipSlots do
    if E = esWeapon then
      Inc(WSlots)
    else
    if E in TopSlots then
      Inc(TSlots)
    else
    if E in BottomSlots then
      Inc(BSlots);
  if WSlots + TSlots + BSlots = 1 then
    for E in AEquipSlots do
      Result := EquipmentSlotToHumanReadableString(E) // this is really weird looking cat, TODO: fix properly
  else
  if WSlots + TSlots = 0 then
    Result := 'lower body'
  else
  if WSlots + BSlots = 0 then
    Result := 'upper body'
  else
    Result := 'body';
end;

function TBlueprint.EquipmentSlotToHumanReadableString(const AEquipSlot: TApparelSlot): String;
begin
  case AEquipSlot of
    esWeapon: Result := 'hands';
    esTopOver: Result := 'arms and chest';
    esTopOverOver: Result := 'shoulders';
    esTopUnder: Result := 'breasts';
    esBottomOver: Result := 'legs';
    esBottomUnder: Result := 'waist and groin';
    esNeck, esAmulet: Result := 'neck';
    esFeet: Result := 'feet';
    esGlasses: Result := 'nose';
    else
      raise EItemSlotConversionError.Create('Cannot convert EquipSlot to human readable string: ' + ApparelSlotToStr(AEquipSlot));
  end;
end;

function TBlueprint.BodySlotToHumanReadableString(const AEquipSlot: TApparelSlot): String;
begin
  case AEquipSlot of
    bsTail: Result := 'Tail';
    bsTailTip: Result := 'Tail tip';
    bsTorsoUnder: Result := 'Torso';
    bsLegs: Result := 'Feet';
    bsLegsClaws: Result := 'Claws';
    bsTorsoOver: Result := 'Belly';
    bsBreasts: Result := 'Breasts';
    bsNipples: Result := 'Nipples';
    bsNethers: Result := 'Nethers';
    bsPubic: Result := 'Pubic';
    bsBodyTattoo: Result := 'Body Tattoo';
    bsEyes: Result := 'Eyes';
    bsEyebrows: Result := 'Eyebrows';
    bsMouth: Result := 'Mouth';
    bsNose: Result := 'Nose';
    bsHair: Result := 'Hair';
    bsEars: Result := 'Ears';
    bsHands: Result := 'Arms';
    bsNails: Result := 'Nails';
    else
      raise EItemSlotConversionError.Create('Cannot convert EquipSlot to human readable string: ' + ApparelSlotToStr(AEquipSlot));
  end;
end;

initialization
  RegisterSerializableData(TBlueprint);
end.

