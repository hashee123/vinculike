{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameItemData;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, Generics.Collections, DOM,
  CastleImages, CastleGlImages, CastleRectangles, CastleStringUtils,
  GameApparelSlots, GameItemDataAbstract, GameEnchantmentAbstract,
  GameColorTags;

const
  // Below this the item has a growing chance to disintegrate when breaking
  ItemDisintegrationThreshold = 10;

type
  TItemData = class(TItemDataAbstract)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    MapImageRect: TFloatRectangle;
    MapImage: TRgbAlphaImage;
    MapImageDrawable: TDrawableImage; // for now only for Vinculopedia?
    CoversTop, CoversBottom: Boolean;
    NumberOfSlots: Byte;
    Damage: Single;
    Protection: Single;
    Durability: Single;
    CanBeRepaired: Boolean;
    Indestructible: Boolean; //TODO: Temporary
    WardrobeMalfunctionChance: Single;
    Noise: Single;
    SpawnFrequency: Single;
    StartSpawningAtDepth: Integer;
    IsBondage: Boolean; // TODO: Temporary?
    UnequipDisintegrates: Boolean;
    DisintegratesInto: TCastleStringList; // TODO: direct references
    { if item disintegrates into something,
      then should those objects be reequipped back (true) or drop to the ground (false)? }
    DisintegrationReequip: Boolean;
    ProtectsFeet: Boolean; // TODO: Temporary?
    HandsUnavailable: Boolean; // TODO: Temporary?
    { If the item can be worn only by characters with ... legs }
    RequiresLegs: Boolean;
    { No need to currently: this is obvious from available equip slots, e.g. esWeapon/esShoulders/esAnkles;
      while esBottomUnder/esBottomOver don't technically specify if legs are split or not
      However this might change in future if some problems arise }
    //RequiresArms: Boolean;
    SoundAttack: String;
    SoundEquip: String;
    SoundUnequip: String;
    SoundBreak: String;
    SoundDisintegrate: String;
    SoundRepair: String;
    Enchantments: TEnchantmentsList;
    ColorTags: TColorTagsDictionary;
  public
    destructor Destroy; override;
  end;
  TItemsDataList = specialize TObjectList<TItemData>;
  TItemsDataDictionary = specialize TObjectDictionary<String, TItemData>;

implementation
uses
  TypInfo,
  CastleXMLUtils, CastleColors,
  GameCachedImages, GameSerializableData;

procedure TItemData.Validate;
begin
  inherited;
  if MapImage = nil then
    raise EItemDataValidationError.Create('MapImage = nil : ' + Id);
  if MapImageDrawable = nil then
    raise EItemDataValidationError.Create('MapImageDrawable = nil : ' + Id);
  if NumberOfSlots <= 0 then
    raise EItemDataValidationError.Create('NumberOfSlots <= 0 : ' + Id);
  {if (Damage <= 0) and not IsBondage then
    raise EItemDataValidationError.Create('Damage <= 0 : ' + Id);}
  if Durability <= 1 then
    raise EItemDataValidationError.Create('Durability <= 1 : ' + Id);
  if SpawnFrequency < 0 then
    raise EItemDataValidationError.Create('SpawnFrequency < 0 : ' + Id);
  if (WardrobeMalfunctionChance < 0) or (WardrobeMalfunctionChance > 1) then
    raise EItemDataValidationError.Create('(WardrobeMalfunctionChance < 0) or (WardrobeMalfunctionChance > 1) : ' + Id);
  if IsWeapon and (SoundAttack = '') then // TODO: weapons as a different class?
    raise EItemDataValidationError.Create('Weapon has no attack sound : ' + Id);
  if SoundEquip = '' then
    raise EItemDataValidationError.Create('SoundEquip missing : ' + Id);
  if SoundUnequip = '' then
    raise EItemDataValidationError.Create('SoundUnequip missing : ' + Id);
  if SoundBreak = '' then
    raise EItemDataValidationError.Create('SoundBreak missing : ' + Id);
  if SoundDisintegrate = '' then
    raise EItemDataValidationError.Create('SoundDisintegrate missing : ' + Id);
  if SoundRepair = '' then
    raise EItemDataValidationError.Create('SoundRepair missing : ' + Id);
  if DisintegrationReequip and (DisintegratesInto.Count = 0) then
    raise EItemDataValidationError.Create('DisintegrationReequip and (DisintegratesInto.Count = 0) : ' + Id);
  // TODO: validate that if reequip and disintegration fragments do not collide in their equipment slots (disintegration fragments should perfectly fit inside parent otherwise they'll break something else
end;

procedure TItemData.Read(const Element: TDOMElement);
var
  E: TApparelSlot;
  Iterator: TXMLElementIterator;
  Enchantment: TEnchantmentAbstract;
begin
  inherited;
  CoversTop := Element.AttributeBoolean('CoversTop');
  CoversBottom := Element.AttributeBoolean('CoversBottom');
  MapImage := LoadRgba('castle-data:/' + Element.AttributeString('MapImageUrl'));
  MapImageDrawable := LoadDrawable('castle-data:/' + Element.AttributeString('MapImageUrl'));
  Damage := Element.AttributeFloatDef('Damage', 0);
  Protection := Element.AttributeFloatDef('Protection', 0);
  Durability := Element.AttributeFloat('Durability');
  CanBeRepaired := Element.AttributeBooleanDef('CanBeRepaired', true);
  Indestructible := Element.AttributeBooleanDef('Indestructible', false);
  WardrobeMalfunctionChance := Element.AttributeFloatDef('WardrobeMalfunctionChance', 0);
  Noise := Element.AttributeFloat('Noise');
  SpawnFrequency := Element.AttributeFloatDef('SpawnFrequency', 1.0);
  StartSpawningAtDepth := Element.AttributeInteger('StartSpawningAtDepth');
  IsBondage := Element.AttributeBooleanDef('IsBondage', false);
  UnequipDisintegrates := Element.AttributeBooleanDef('UnequipDisintegrates', false);
  DisintegratesInto := CreateTokens(Element.AttributeStringDef('DisintegratesInto', ''), [',']);
  DisintegrationReequip := Element.AttributeBooleanDef('DisintegrationReequip', false);
  ProtectsFeet := Element.AttributeBooleanDef('ProtectsFeet', false);
  HandsUnavailable := Element.AttributeBooleanDef('HandsUnavailable', false);
  RequiresLegs := Element.AttributeBooleanDef('RequiresLegs', false);

  SoundAttack := Element.AttributeStringDef('SoundAttack', '');
  SoundEquip := Element.AttributeStringDef('SoundEquip', '');
  SoundUnequip := Element.AttributeStringDef('SoundUnequip', '');
  SoundBreak := Element.AttributeStringDef('SoundBreak', '');
  SoundDisintegrate := Element.AttributeStringDef('SoundDisintegrate', '');
  SoundRepair := Element.AttributeStringDef('SoundRepair', '');

  ColorTags := GetColorTags(Element);

  Enchantments := TEnchantmentsList.Create(true);
  Iterator := Element.ChildrenIterator('Enchantment');
  try
    while Iterator.GetNext do
    begin
      Enchantment := TEnchantmentAbstract.ReadClass(Iterator.Current) as TEnchantmentAbstract;
      Enchantments.Add(Enchantment);
    end;
  finally FreeAndNil(Iterator) end;

  NumberOfSlots := 0;
  for E in EquipSlots do
    Inc(NumberOfSlots);
end;

destructor TItemData.Destroy;
begin
  FreeAndNil(ColorTags);
  FreeAndNil(Enchantments);
  FreeAndNil(DisintegratesInto);
  inherited Destroy;
end;

initialization
  RegisterSerializableData(TItemData);
end.

