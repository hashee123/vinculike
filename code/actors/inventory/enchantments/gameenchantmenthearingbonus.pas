{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentHearingBonus;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  TEnchantmentHearingBonus = class(TEnchantmentAbstract)
  public
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData;

function TEnchantmentHearingBonus.Description: String;
begin
  Result := Format('Increases hearing range by %.0n' + RequirementDescription, [Strength]);
end;

initialization
  RegisterSerializableData(TEnchantmentHearingBonus);

end.

