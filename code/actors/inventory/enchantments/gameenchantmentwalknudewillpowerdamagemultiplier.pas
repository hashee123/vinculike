{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentWalkNudeWillpowerDamageMultiplier;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  TEnchantmentWalkNudeWillpowerDamageMultiplier = class(TEnchantmentAbstract)
  public
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData;

function TEnchantmentWalkNudeWillpowerDamageMultiplier.Description: String;
begin
  if Strength > 1 then
    Result := Format('Increases damage from walking nude by %d%%%%' + RequirementDescription, [Round(100 * (Strength - 1))])
  else
    Result := Format('Decreases damage from walking nude by %d%%%%' + RequirementDescription, [Round(100 * (1 - Strength))]);
end;

initialization
  RegisterSerializableData(TEnchantmentWalkNudeWillpowerDamageMultiplier);

end.

