{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM, Generics.Collections,
  GameEffectAbstract;

type
  TEnchantmentClass = class of TEnchantmentAbstract;
  TEnchantmentAbstract = class(TEffectAbstract)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    function CloneInternal: TEnchantmentAbstract; virtual;
  public
    RequiresNudeTop: Boolean;
    RequiresDressedTop: Boolean;
    RequiresNudeBottom: Boolean;
    RequiresDressedBottom: Boolean;
    RequiresUnarmed: Boolean;
    RequiresHealthBelow: Single;
    function RequirementDescription: String;
    function Clone: TEnchantmentAbstract;
    function EnchantmentClass: TEnchantmentClass;
    {$WARNING 'Use Enchantment.Parent, Enchantment.Item; see https://gitlab.com/EugeneLoza/vinculike/-/issues/1241'}
    procedure Update(const AParentActor: TObject; const AParentItem: TObject; const SecondsPassed: Single); virtual;
    procedure Stop; virtual;
  end;
  TEnchantmentsList = specialize TObjectList<TEnchantmentAbstract>;
  TEnchantmentCache = specialize TDictionary<TEnchantmentClass, Single>;

implementation
uses
  CastleXmlUtils;

procedure TEnchantmentAbstract.Validate;
begin
  inherited Validate;
  if RequiresNudeTop and RequiresDressedTop then
    raise Exception.Create('RequiresNudeTop and RequiresDressedTop');
  if RequiresNudeBottom and RequiresDressedBottom then
    raise Exception.Create('RequiresNudeBottom and RequiresDressedBottom');
  if RequiresHealthBelow <= 0 then
    raise Exception.Create('RequiresHealthBelow <= 0');
end;

procedure TEnchantmentAbstract.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  RequiresNudeTop := Element.AttributeBooleanDef('RequiresNudeTop', false);
  RequiresDressedTop := Element.AttributeBooleanDef('RequiresDressedTop', false);
  RequiresNudeBottom := Element.AttributeBooleanDef('RequiresNudeBottom', false);
  RequiresDressedBottom := Element.AttributeBooleanDef('RequiresDressedBottom', false);
  RequiresUnarmed := Element.AttributeBooleanDef('RequiresUnarmed', false);
  RequiresHealthBelow := Element.AttributeSingleDef('RequiresHealthBelow', 1.0);
end;

function TEnchantmentAbstract.RequirementDescription: String;
begin
  if RequiresNudeTop and RequiresNudeBottom then
    Result := ' when nude'
  else
  if RequiresDressedTop and RequiresDressedBottom then
    Result := ' when properly dressed'
  else
  if RequiresNudeTop and RequiresDressedBottom then
    Result := ' when barechested but bottom covered'
  else
  if RequiresNudeBottom and RequiresDressedTop then
    Result := ' when bottom not covered but breasts are'
  else
  if RequiresNudeTop then
    Result := ' when barechested'
  else
  if RequiresNudeBottom then
    Result := ' when bottom not covered'
  else
  if RequiresDressedTop then
    Result := ' when breasts covered'
  else
  if RequiresDressedBottom then
    Result := ' when bottom covered'
  else
    Result := '';

  if RequiresUnarmed then
  begin
    if Result = '' then
      Result += ' when '
    else
      Result += ' and ';
    Result += 'unarmed';
  end;

  if RequiresHealthBelow < 1.0 then
  begin
    if Result = '' then
      Result += ' when '
    else
      Result += ' and ';
    Result += 'health is below ' + Round(100 * RequiresHealthBelow).ToString() + '%%%%';
  end;
end;

function TEnchantmentAbstract.CloneInternal: TEnchantmentAbstract;
begin
  Result := ClassType.Create as TEnchantmentAbstract;
  TEffectAbstract(Result).Strength := Strength;
  TEnchantmentAbstract(Result).RequiresNudeTop := RequiresNudeTop;
  TEnchantmentAbstract(Result).RequiresDressedTop := RequiresDressedTop;
  TEnchantmentAbstract(Result).RequiresNudeBottom := RequiresNudeBottom;
  TEnchantmentAbstract(Result).RequiresDressedBottom := RequiresDressedBottom;
  TEnchantmentAbstract(Result).RequiresUnarmed := RequiresUnarmed;
  TEnchantmentAbstract(Result).RequiresHealthBelow := RequiresHealthBelow;
end;

function TEnchantmentAbstract.Clone: TEnchantmentAbstract;
begin
  Result := CloneInternal;
  {$IFDEF ValidateData}
  Result.Validate;
  {$ENDIF}
end;

function TEnchantmentAbstract.EnchantmentClass: TEnchantmentClass;
begin
  Exit(TEnchantmentClass(ClassType));
end;

procedure TEnchantmentAbstract.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
begin
  // do nothing
end;

procedure TEnchantmentAbstract.Stop;
begin
  // do nothing;
end;

end.

