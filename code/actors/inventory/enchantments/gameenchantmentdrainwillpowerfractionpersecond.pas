{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentDrainWillpowerFractionPerSecond;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  TEnchantmentDrainWillpowerFractionPerSecond = class(TEnchantmentAbstract)
  public
    function Description: String; override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
  end;

implementation
uses
  GameSerializableData,
  GamePlayerCharacter;

function TEnchantmentDrainWillpowerFractionPerSecond.Description: String;
begin
  // TODO: Validate Strength < 1.0 ; Strength > 0.0
  Result := Format('Drains %.1n%%%% of current willpower per second' + RequirementDescription, [100 * Strength]);
end;

procedure TEnchantmentDrainWillpowerFractionPerSecond.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  (AParentActor as TPlayerCharacter).DegradeWill((AParentActor as TPlayerCharacter).Will * Strength * SecondsPassed);
end;

initialization
  RegisterSerializableData(TEnchantmentDrainWillpowerFractionPerSecond);

end.

