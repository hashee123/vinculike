{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentPlaySound;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  CastleSoundEngine,
  GameEnchantmentAbstract;

type
  TEnchantmentPlaySound = class(TEnchantmentAbstract)
  strict private
    SoundName: String;
    SoundPlaying: TCastlePlayingSound;
  protected
    function CloneInternal: TEnchantmentAbstract; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    function Description: String; override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
    procedure Stop; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameSounds;

function TEnchantmentPlaySound.CloneInternal: TEnchantmentAbstract;
begin
  Result := inherited CloneInternal;
  (Result as TEnchantmentPlaySound).SoundName := SoundName;
end;

procedure TEnchantmentPlaySound.Validate;
begin
  inherited Validate;
  if not SoundExists(SoundName) then
    raise Exception.CreateFmt('Unable to find sound %s in %s', [SoundName, ClassName]);
end;

procedure TEnchantmentPlaySound.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  SoundName := Element.AttributeString('SoundName');
end;

function TEnchantmentPlaySound.Description: String;
begin
  Result := Format('Plays an annoying sound' + RequirementDescription, []);
end;

procedure TEnchantmentPlaySound.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  if (SoundPlaying = nil) then
  begin
    SoundPlaying := TCastlePlayingSound.Create(nil);
    SoundPlaying.Sound := GetSoundByName(SoundName);
    SoundPlaying.Loop := true;
    SoundPlaying.Volume := Strength;
  end;
  if not SoundPlaying.Playing then
  begin
    SoundEngine.Play(SoundPlaying);
  end;
end;

procedure TEnchantmentPlaySound.Stop;
begin
  inherited Stop;
  if SoundPlaying <> nil then
    SoundPlaying.Stop;
end;

destructor TEnchantmentPlaySound.Destroy;
begin
  if SoundPlaying <> nil then
  begin
    SoundPlaying.Stop;
    FreeAndNil(SoundPlaying);
  end;
  inherited Destroy;
end;

initialization
  RegisterSerializableData(TEnchantmentPlaySound);

end.

