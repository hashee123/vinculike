{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentDisorientation;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  TEnchantmentDisorientation = class(TEnchantmentAbstract)
  public
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData;

function TEnchantmentDisorientation.Description: String;
begin
  Result := Format('Disorients the character by %.0n' + RequirementDescription, [Strength]);
end;

initialization
  RegisterSerializableData(TEnchantmentDisorientation);

end.

