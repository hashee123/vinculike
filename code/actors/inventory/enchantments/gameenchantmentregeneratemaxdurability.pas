{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentRegenerateMaxDurability;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  TEnchantmentRegenerateMaxDurability = class(TEnchantmentAbstract)
  public
    function Description: String; override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
  end;

implementation
uses
  GameSerializableData,
  GameInventoryItem, GamePlayerCharacter;

function TEnchantmentRegenerateMaxDurability.Description: String;
begin
  if Strength > 0 then
    Result := Format('Regenerates %.1n max durability per second' + RequirementDescription, [Strength])
  else
    Result := Format('Degrades %.1n max durability per second' + RequirementDescription, [Strength])
end;

procedure TEnchantmentRegenerateMaxDurability.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
var
  AItem: TInventoryItem;
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  AItem := AParentItem as TInventoryItem;
  AItem.MaxDurability += SecondsPassed * Strength;
  if AItem.MaxDurability > AItem.ItemData.Durability then
    AItem.MaxDurability := AItem.ItemData.Durability;
  if AItem.Durability > AItem.MaxDurability then
    AItem.Durability := AItem.MaxDurability;
  if AItem.MaxDurability < 0 then
    (AParentActor as TPlayerCharacter).Inventory.DamageItem(AItem, 1.0);
end;

initialization
  RegisterSerializableData(TEnchantmentRegenerateMaxDurability);

end.

