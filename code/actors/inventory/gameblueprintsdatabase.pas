{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameBlueprintsDatabase;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  GameBlueprint;

var
  BlueprintsList: TBlueprintsList;
  BlueprintsDictionary: TBlueprintsDictionary;

procedure LoadGeneticBlueprints;
implementation
uses
  CastleXmlUtils;

procedure LoadGeneticBlueprints;
var
  Blueprint: TBlueprint;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  BlueprintsList := TBlueprintsList.Create(true);
  BlueprintsDictionary := TBlueprintsDictionary.Create([]);

  Doc := URLReadXML('castle-data:/character/blueprints.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('Blueprint');
    try
      while Iterator.GetNext do
      begin
        Blueprint := TBlueprint.ReadClass(Iterator.Current) as TBlueprint;
        BlueprintsList.Add(Blueprint);
        BlueprintsDictionary.Add(Blueprint.Id, Blueprint);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

finalization
  FreeAndNil(BlueprintsList);
  FreeAndNil(BlueprintsDictionary);
end.

