{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePaperDoll;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleUiControls, CastleGlImages,
  GameApparelSlots;

type
  TPaperDoll = class(TCastleUserInterface)
  strict private
    RenderImage: TDrawableImage;
  public
    Nude: Boolean;
    procedure UpdateToInventory;
    procedure Render; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleConfig, CastleRectangles, CastleImages, CastleVectors, CastleColors,
  GameViewGame, GameItemDataAbstract, GameLog, GameViewAbstract;

procedure TPaperDoll.UpdateToInventory;
var
  E: TApparelSlot;
  Image: TRgbAlphaImage;
  Hidden: TApparelSlotsSet;
  AImage: TApparelImage;
begin
  FreeAndNil(RenderImage);
  Image := TRgbAlphaImage.Create(
    ViewGame.CurrentCharacter.Inventory.Apparel[bsTorsoUnder].Data.Images[ViewGame.CurrentCharacter.GetShapePose][bsTorsoUnder].Image.Width, // TODO: More generic
    ViewGame.CurrentCharacter.Inventory.Apparel[bsTorsoUnder].Data.Images[ViewGame.CurrentCharacter.GetShapePose][bsTorsoUnder].Image.Height);
  Image.Clear(Vector4Byte(0, 0, 0, 0));
  RenderImage := TDrawableImage.Create(Image, true, true);

  RenderImage.RenderToImageBegin;

  Hidden := [];
  if UserConfig.GetValue('censored', false) or ViewGame.CurrentCharacter.Inventory.Censored or (ScreenshotNow and (UserConfig.GetValue('censored_screenshots', false) or ViewGame.CurrentCharacter.Inventory.CensoredInScreenshots)) then // TODO: Inventory.HiddenSlots
    Hidden += [bsNipples, bsNethers, bsPubic];
  if Nude then
    Hidden += ViewGame.CurrentCharacter.Blueprint.EquipmentSlots
  else
    for E in TApparelSlot do
      if (ViewGame.CurrentCharacter.Inventory.Apparel[E] <> nil) then
        Hidden += ViewGame.CurrentCharacter.Inventory.Apparel[E].Data.HidesSlots;

  // TODO: maybe optimize (ViewGame.CurrentCharacter.Inventory.Apparel[E] <> nil) and ViewGame.CurrentCharacter.Inventory.Apparel[E].Data.RenderSlots.ContainsKey(E) -> Data.RenderSlotsSet
  for E in TApparelSlot do
    if not (E in Hidden) and (ViewGame.CurrentCharacter.Inventory.Apparel[E] <> nil) and ViewGame.CurrentCharacter.Inventory.Apparel[E].Data.Images[ViewGame.CurrentCharacter.GetShapePose].ContainsKey(E) then
    begin
      AImage := ViewGame.CurrentCharacter.Inventory.Apparel[E].Data.Images[ViewGame.CurrentCharacter.GetShapePose][E];
      if E in ViewGame.CurrentCharacter.Blueprint.BodySlots then
      begin
        // TODO: optimize
        if ViewGame.CurrentCharacter.Inventory.BodyColor.ContainsKey(AImage.ColorTag) then
          AImage.Image.Color := ViewGame.CurrentCharacter.Inventory.BodyColor[AImage.ColorTag]
        else
        begin
          ShowError('ColorTag %s not found in body', [AImage.ColorTag]);
          AImage.Image.Color := White;
        end;
      end else
        AImage.Image.Color := ViewGame.CurrentCharacter.Inventory.Equipped[E].Color[AImage.ColorTag];

      AImage.Image.DrawUnscaled;
    end;

  RenderImage.RenderToImageEnd;
  //RenderImage.RenderToImageFree - doesn't seem to be needed
end;

procedure TPaperDoll.Render;
var
  E: TApparelSlot;
  R: TFloatRectangle;
  W: Single;
begin
  inherited Render;
  // TODO: cache in Resize?
  W := Single(RenderRect.Height) * Single(RenderImage.Width) / Single(RenderImage.Height);
  R := FloatRectangle(RenderRect.Left + (RenderRect.Width - W) / 2, RenderRect.Bottom, W, RenderRect.Height);
  for E in TApparelSlot do
    RenderImage.Draw(R);
end;

constructor TPaperDoll.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FullSize := true; // TODO: Maybe temporary
  Nude := false;
end;

destructor TPaperDoll.Destroy;
begin
  FreeAndNil(RenderImage);
  inherited Destroy;
end;


end.

