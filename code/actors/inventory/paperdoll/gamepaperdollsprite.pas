{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePaperDollSprite;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  CastleGlImages,
  GameApparelSlots;

type
  TPaperDollSprite = class(TObject)
  public
    Image: TDrawableImage;
    Parent: TObject;
    procedure UpdateToInventory;
    destructor Destroy; override;
  end;

implementation
uses
  CastleImages, CastleConfig, CastleVectors, CastleColors,
  GamePlayerCharacter, GameLog,
  GameViewGame, GameCachedImages, GameItemDataAbstract, GameViewAbstract;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentInventory:=(Parent as TPlayerCharacter).Inventory}
{$ELSE}
{$DEFINE ParentInventory:=TPlayerCharacter(Parent).Inventory}
{$ENDIF}

procedure TPaperDollSprite.UpdateToInventory;
var
  E: TApparelSlot;
  Img: TRgbAlphaImage;
  Hidden: TApparelSlotsSet;
  AImage: TApparelImage;
begin
  FreeAndNil(Image);
  Img := TRgbAlphaImage.Create(99, 99);
  Img.Clear(Vector4Byte(0, 0, 0, 0));
  Image := TDrawableImage.Create(Img, true, true);
  Image.Alpha := acBlending; // autodetect fails on image with no features, force alpha blending mode

  Hidden := [];
  if UserConfig.GetValue('censored', false) or ParentInventory.Censored or (ScreenshotNow and (UserConfig.GetValue('censored_screenshots', false) or ParentInventory.CensoredInScreenshots)) then // TODO: Inventory.HiddenSlots
    Hidden += [bsNipples, bsNethers, bsPubic];
  for E in TApparelSlot do
    if (ParentInventory.Apparel[E] <> nil) then
      Hidden += ParentInventory.Apparel[E].Data.HidesSlots;

  Image.RenderToImageBegin;

  if Parent = ViewGame.CurrentCharacter then
    SelectedCharacterHighlight.DrawUnscaled;

  // TODO: maybe optimize (ParentInventory.Apparel[E] <> nil) and (ParentInventory.Apparel[E].Data.RenderSlots.ContainsKey(E)) -> Data.RenderSlotsSet
  for E in TApparelSlot do
    if not (E in Hidden) and (ParentInventory.Apparel[E] <> nil) and (ParentInventory.Apparel[E].Data.Images[ViewGame.CurrentCharacter.GetShapePose].ContainsKey(E)) then
    begin
      AImage := ParentInventory.Apparel[E].Data.Images[ViewGame.CurrentCharacter.GetShapePose][E];
      {$WARNING Not ViewGame.CurrentCharacter.GetShapePose but ParentPlayer.GetShapePose OR move GetShapePose to Inventory - which also makes sense}
      if AImage.Sprite <> nil then
      begin
        if AImage.UseAverageColor then
          AImage.Sprite.Color := AImage.AverageColor
        else
        if E in (Parent as TPlayerCharacter).Blueprint.BodySlots then
        begin
          // TODO: optimize
          if ParentInventory.BodyColor.ContainsKey(AImage.ColorTag) then
            AImage.Sprite.Color := ParentInventory.BodyColor[AImage.ColorTag]
          else
          begin
            ShowError('ColorTag %s not found in body', [AImage.ColorTag]);
            AImage.Sprite.Color := White;
          end;
        end else
          AImage.Sprite.Color := ParentInventory.Equipped[E].Color[AImage.ColorTag];

        AImage.Sprite.DrawUnscaled;
      end;
    end;

  Image.RenderToImageEnd;
  //Image.RenderToImageFree - doesn't seem to be needed
end;

destructor TPaperDollSprite.Destroy;
begin
  FreeAndNil(Image);
  inherited Destroy;
end;


end.

