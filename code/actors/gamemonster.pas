{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMonster;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, Generics.Collections, DOM,
  GameSerializableObject,
  GameActor, GameActorData, GamePlayerCharacter, GameInventoryItem, GameMonsterData,
  GameAiAbstract;

type
  TMonster = class(TActor)
  strict private
    LifeTime: Single;
  protected
    procedure Die; override;
    procedure DropLootAndResetToIdle;
    procedure SetData(const AData: TActorData); override;
  public
    Loot: TInventoryItemsList;
    Ai: TAiAbstract; // TODO: move to TActor? As player will also use Ai and chests/traps should eventually also be separate from monsters
    function CurrentTarget: TPlayerCharacter;
    function IsVisible: Boolean; override;
    function IsHearable: Boolean;
    procedure Update(const SecondsPassed: Single); override;
    function Unsuspecting: Boolean; override;
    function Immobilized: Boolean; override;
    function Aggressive: Boolean;
    function GetNoise: Single; override;
    function VisibilityRange: Single;
    function MonsterData: TMonsterData; inline;
    procedure Hit(const Damage: Single); override;
    procedure Reset; override;
  public const Signature = 'monster'; //deprecated;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    constructor Create(const NotLoading: Boolean = true); override;
    destructor Destroy; override;
  end;
  TMonstersList = specialize TObjectList<TMonster>;

implementation
uses
  Math,
  CastleXmlUtils,
  GameViewGame, GameMap, GameMapTypes, GameRandom, GameTranslation,
  GameStats, GameMonstersDatabase,
  GameLog, GameColors, GameMapItem, GameParticle,
  GameActionIdle, GameActionMoveAbstract;

function TMonster.CurrentTarget: TPlayerCharacter;
begin
  Exit(ViewGame.CurrentCharacter);
end;

procedure TMonster.Die;
begin
  LocalStats.IncStat('killed_' + Data.Id);
  if MonsterData.Chest then
  begin
    if Loot.Count > 0 then
      ShowLog(GetTranslation('ChestDiesLog'), [FData.DisplayName], ColorLogMonsterDies)
    else
      ShowLog(GetTranslation('ChestEmptyDiesLog'), [FData.DisplayName], ColorLogMonsterDies);
  end else
  begin
    GlobalStats.IncStat('killed_monsters');
    ShowLog(GetTranslation('MonsterDiesLog'), [FData.DisplayName], ColorLogMonsterDies);
  end;
  DropLootAndResetToIdle;
end;

procedure TMonster.DropLootAndResetToIdle;
begin
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
  Loot.OwnsObjects := false; // and we don't seem to need to set it back
  TMapItem.DropItems(LastTileX + PredSize div 2, LastTileY + PredSize div 2, Loot);
  if Loot.Count > 0 then
  begin
    ShowError('LootCount > 0', []);
    Loot.OwnsObjects := true;
    Loot.Clear;
  end;
end;

procedure TMonster.SetData(const AData: TActorData);
begin
  inherited SetData(AData);
  if Ai <> nil then // this can happen if we "reroll" monster kind in map.generate monsters (due to frequency). Maybe it's not a good idea, but for now let's just avoid memory leak
  begin
    //ShowError('AI <> nil in TMonster.SetData. Was %s, will be %s', [Ai.ClassName, MonsterData.AiData.Ai.ClassName]);
    FreeAndNil(Ai);
  end;
  Ai := MonsterData.AiData.Ai.Create;
  Ai.Parent := Self;
  Ai.Data := MonsterData.AiData;
end;

function TMonster.IsVisible: Boolean;
begin
  if MonsterData.Chest or MonsterData.Trap then
    Exit(inherited)
  else
    Exit(VisibleGrade >= DirectlyVisible);
end;

function TMonster.IsHearable: Boolean;
var
  P: TPlayerCharacter;
begin
  if MonsterData.Chest or MonsterData.Trap then
    Exit(false)
  else
  if not Unsuspecting then // TODO: maybe cannot hear attacking monsters always
    Exit(true)
  else
    for P in Map.CharactersOnThisLevel do
      if P.CanAct and (DistanceToSqr(P) < MonsterData.NoisinessSqr + P.Inventory.HearingBonusSqr) then
        Exit(true);
  Exit(false);
end;

function TMonster.VisibilityRange: Single;
begin
  Result := CurrentTarget.GetNoise + MonsterData.VisionBonus;
  if (CurrentTarget.Inventory.BottomCovered > 0) and (Result < MonsterData.VisionBottomClothed) then
    Result := MonsterData.VisionBottomClothed;
  if (CurrentTarget.Inventory.BottomCovered = 0) and (Result < MonsterData.VisionBottomNaked) then
    Result := MonsterData.VisionBottomNaked;
  if (CurrentTarget.Inventory.TopCovered > 0) and (Result < MonsterData.VisionTopClothed) then
    Result := MonsterData.VisionTopClothed;
  if (CurrentTarget.Inventory.TopCovered = 0) and (Result < MonsterData.VisionTopNaked) then
    Result := MonsterData.VisionTopNaked;
  Result := CurrentTarget.HalfSize + HalfSize + Result;
end;

procedure TMonster.Update(const SecondsPassed: Single);
begin
  try
    Ai.Update(SecondsPassed);
  except
    on E: Exception do
    begin
      try
        ShowError('%s:"%s" when updating %s for %s', [E.ClassName, E.Message, Ai.ClassName, Data.Id]);
        FreeAndNil(Ai); // it may also throw an exception in some weirdest case, e.g. if AI is a dangling pointer or has one inside
      except
        ShowError('Exception while trying to report exception in TMonster.Update', []);
      end;
      Ai := MonsterData.AiData.Ai.Create;
      Ai.Parent := Self;
      Ai.Data := MonsterData.AiData;
    end;
  end;
  inherited;
  if LifeTime < MaxLifeTime then
  begin
    LifeTime -= SecondsPassed;
    if LifeTime < 0 then
    begin
      ShowLog('%s despawns', [Data.DisplayName], ColorLogCancel);
      Health := -1;
      DropLootAndResetToIdle;
    end;
  end;
end;

function TMonster.Unsuspecting: Boolean;
begin
  //parent is abstract
  Result := (CurrentAction is TActionIdle) or (CurrentAction is TActionMoveAbstract); // Temporary: TODO
end;

function TMonster.Immobilized: Boolean;
begin
  Exit(false);
end;

function TMonster.Aggressive: Boolean;
begin
  Exit((not MonsterData.Chest and not MonsterData.Trap and not MonsterData.VacuumCleaner) or not Unsuspecting);
end;

function TMonster.GetNoise: Single;
begin
  Exit(MonsterData.NoisinessSqr); // In Player we use non-sqr?
end;

function TMonster.MonsterData: TMonsterData; inline;
begin
  Exit({$IFDEF SafeActorTypecast}FData as TMonsterData{$ELSE}TMonsterData(FData){$ENDIF});
end;

procedure TMonster.Hit(const Damage: Single);
begin
  inherited Hit(Damage);
  NewParticle(CenterX, CenterY, Round(Damage).ToString, ColorParticleMonsterHurt);
end;

procedure TMonster.Reset;
begin
  inherited Reset;
  if MonsterData.LifeTime < MaxLifeTime then
    LifeTime := MonsterData.LifeTime / 2 + Sqrt(Rnd.Random) * MonsterData.LifeTime /2
  else
    LifeTime := MaxLifeTime+1; //+1 just to make sure no floating-point errors occur
end;

procedure TMonster.Save(const Element: TDOMElement);
var
  II: TInventoryItem;
begin
  inherited Save(Element);
  Ai.Save(Element.CreateChild('Ai'));
  for II in Loot do
    II.Save(Element.CreateChild('LootItem'));
  Element.AttributeSet('LifeTime', LifeTime);
end;

procedure TMonster.Load(const Element: TDOMElement);
var
  Iterator: TXMLElementIterator;
  AiElement: TDOMElement;
begin
  inherited;
  AiElement := Element.ChildElement('Ai', false);
  Data := MonstersDataDictionary[Element.AttributeString('Data.Id')]; // this also calls Ai := MonsterData.AiData.Ai.Create; TODO: optimize
  if AiElement <> nil then
  begin
    FreeAndNil(Ai);
    Ai := TAiAbstract.LoadClass(AiElement) as TAiAbstract;
    Ai.Parent := Self;
    Ai.Data := MonsterData.AiData;
  end else
    ShowError('Failed to load AI for %s', [Data.Id]);

  SetSize(FData.Size);
  try
    Iterator := Element.ChildrenIterator('LootItem');
    while Iterator.GetNext do
      Loot.Add(TInventoryItem.LoadClass(Iterator.Current) as TInventoryItem);
    FreeAndNil(Iterator);
  except
    on E: Exception do
    begin
      Loot.Clear; // Note that we fail to load *all* the loot, not filtering by a single item, maybe we should TODO
      ShowError('%s:"%s". Failed to load loot for %s.', [E.ClassName, E.Message, FData.Id]);
    end;
  end;

  LifeTime := Element.AttributeSingleDef('LifeTime', MaxLifeTime+1); // TODO: not def
end;

constructor TMonster.Create(const NotLoading: Boolean = true);
begin
  inherited Create(NotLoading);
  Loot := TInventoryItemsList.Create(true);
end;

destructor TMonster.Destroy;
begin
  FreeAndNil(Loot);
  FreeAndNil(Ai);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMonster);
end.

