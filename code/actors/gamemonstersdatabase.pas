{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMonstersDatabase;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  GameActorData, GameMonsterData;

var
  MonstersData: TActorDataList;
  ChestsData: TActorDataList;
  MimicsData: TActorDataList;
  TrapsData: TActorDataList;
  VacuumCleaner: TMonsterData; // TODO: Only one for now!
  MonstersDataDictionary: TActorDataDictionary;

procedure LoadMonstersData;
implementation
uses
  CastleXmlUtils;

procedure LoadMonstersData;
var
  AMonsterData: TMonsterData;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  MonstersData := TActorDataList.Create(true);
  ChestsData := TActorDataList.Create(true);
  MimicsData := TActorDataList.Create(true);
  TrapsData := TActorDataList.Create(true);
  MonstersDataDictionary := TActorDataDictionary.Create([]);

  Doc := URLReadXML('castle-data:/monsters/monsters.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('MonsterData');
    try
      while Iterator.GetNext do
      begin
        AMonsterData := TMonsterData.ReadClass(Iterator.Current) as TMonsterData;
        if AMonsterData.VacuumCleaner then
          VacuumCleaner := AMonsterData
        else
        if AMonsterData.Trap then
          TrapsData.Add(AMonsterData)
        else
        if AMonsterData.Mimic then // WARNING: Mimics are also chests, so we shouldn't add them to ChestsData TODO
          MimicsData.Add(AMonsterData)
        else
        if AMonsterData.Chest then
          ChestsData.Add(AMonsterDAta)
        else
          MonstersData.Add(AMonsterData);
        MonstersDataDictionary.Add(AMonsterData.Id, AMonsterData);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

finalization
  FreeAndNil(MonstersData);
  FreeAndNil(ChestsData);
  FreeAndNil(TrapsData);
  FreeAndNil(VacuumCleaner);
  FreeAndNil(MimicsData);
  FreeAndNil(MonstersDataDictionary);
end.

