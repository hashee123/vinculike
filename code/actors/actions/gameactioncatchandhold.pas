{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionCatchAndHold;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  CastleVectors,
  GameActionAbstract, GameActionOnTarget, GameMarkAbstract, GameApparelSlots,
  GameUnlockableEntry;

type
  TActionCatchAndHold = class(TActionOnTarget)
  private const
    AttackSlots = [esBottomOver, esBottomUnder, esTopOver, esTopUnder, esTopOverOver];
  strict private
    Phase: Single;
    Mark: TMarkAbstract;
    procedure MarkFinished;
    function HeldByMe: Boolean; inline;
  public
    function CanStop: Boolean; override;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Update(const SecondsPassed: Single); override;
    destructor Destroy; override;
  end;

  TActionCatchAndHoldData = class(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    StruggleSound: String;
    ReleasedSound: String;
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameActor, GamePlayerCharacter, GameActionIdle, GameActionPlayerHeld, GameActionPlayerUnconscious,
  GameMonster, GameMap,
  GameViewGame, GameLog, GameColors, GameStats, GameSounds, GameRandom, GameDifficultyLevel,
  GameParticle,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ActionData:=(Data as TActionCatchAndHoldData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ActionData:=TActionCatchAndHoldData(Data)}
{$ENDIF}

procedure TActionCatchAndHold.MarkFinished;
begin
  Mark := nil;
end;

function TActionCatchAndHold.HeldByMe: Boolean;
begin
  Exit((TargetPlayer.CurrentAction is TActionPlayerHeld) and (TActionPlayerHeld(TargetPlayer.CurrentAction).HeldBy = ParentActor.ReferenceId));
end;

function TActionCatchAndHold.CanStop: Boolean;
begin
  Exit(Mark = nil);
end;

function TActionCatchAndHold.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionCatchAndHold.NoiseAddition: Single;
begin
  Exit(0.0);
end;

procedure TActionCatchAndHold.Update(const SecondsPassed: Single);

  procedure ThrowMark;
  begin
    LocalStats.IncStat(Data.ClassName);

    if ActionData.MarkData.Independent then
      ShowError('Mark %s must NOT be independent to be used with %s', [ActionData.MarkData.ClassName, ClassName]);
    Mark := ActionData.MarkData.Mark.Create as TMarkTargetAbstract;
    TMarkTargetAbstract(Mark).Target := TargetPlayer;
    Mark.Parent := ParentActor;
    Mark.Data := ActionData.MarkData;
    Mark.SetSize(Mark.Data.Size);
    Mark.X := TargetPlayer.X;
    Mark.Y := TargetPlayer.Y;
    Mark.CenterX := TargetPlayer.CenterX;
    Mark.CenterY := TargetPlayer.CenterY;
    Mark.LastTileX := TargetPlayer.LastTileX;
    Mark.LastTileY := TargetPlayer.LastTileY;
    Mark.LastTile := TargetPlayer.LastTile;
    Mark.OnFinished := @MarkFinished;
    Map.MarksList.Add(Mark);
  end;

var
  E: TApparelSlot;
begin
  inherited;

  if not TargetPlayer.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  if TargetPlayer.Unsuspecting then
  begin
    LocalStats.IncStat(Data.ClassName + '_sleeping');
    if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
    begin
      ShowLog('%s feels as if she is groped all over her body', [TargetPlayer.Data.DisplayName], ColorLogItemSteal);
      E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem;
      Sound(TargetPlayer.Inventory.Equipped[E].ItemData.SoundUnequip);
      TargetPlayer.Inventory.UnequipAndDrop(E, false);
    end else
      ShowLog('%s has a weird dream about swimming nude', [TargetPlayer.Data.DisplayName], ColorLogItemSteal);
    ParentMonster.Ai.AiFlee := true;
    Exit;
  end;

  if not (TargetPlayer.CurrentAction is TActionPlayerHeld) then
  begin
    Phase := 0;
    if Mark = nil then
      ThrowMark
    //else - nothing
  end else
  begin // target is immobilized
    Phase += SecondsPassed;
    if Phase >= ActionData.WarmUpTime * Difficulty.MonsterAttackDelayMultiplier then
    begin
      if HeldByMe and (Rnd.Random < 0.1) then
      begin
        ShowLog('%s catches the moment and breaks free of %s''s embrace', [TargetPlayer.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogInventory);
        TargetPlayer.CurrentAction := TActionIdle.NewAction(TargetPlayer);
        TargetPlayer.CurrentAction.Start;
        TargetPlayer.HitStamina(12);
        Sound(ActionData.ReleasedSound);
        Exit;
      end;
      Phase := 0;
      repeat
        E := TargetPlayer.Inventory.GetRandomClothesSlot;
      until E in AttackSlots;
      if TargetPlayer.Inventory.Apparel[E] = nil then
      begin
        if HeldByMe then
        begin
          if Rnd.Random < 0.5 then
          begin
            ShowLog('%s finally manages to break free from %s''s grasp', [TargetPlayer.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogInventory);
            TargetPlayer.CurrentAction := TActionIdle.NewAction(TargetPlayer);
            TargetPlayer.CurrentAction.Start;
            Sound(ActionData.ReleasedSound);
            TargetPlayer.HitStamina(12);
          end else
          begin
            NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'HOLD', ColorParticlePlayerStunned);
            ShowLog('%s manages to free her %s only to get subdued again', [TargetPlayer.Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E)], ColorLogBondage);
            TargetPlayer.CurrentAction := TActionPlayerHeld.NewAction(TargetPlayer, 2.0);
            TActionPlayerHeld(TargetPlayer.CurrentAction).HeldBy := ParentActor.ReferenceId;
            TargetPlayer.CurrentAction.Start;
            Sound(ActionData.StruggleSound);
            TargetPlayer.HitStamina(12 + TargetPlayer.MaxStamina * 0.01);
          end;
        end else
        begin
          LocalStats.IncStat(Data.ClassName + '_group');
          ShowLog('%s eagerly helps to keep %s suppressed', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogStaminaDamage);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'HOLD', ColorParticlePlayerStunned);
          Sound(ActionData.StruggleSound);
          TargetPlayer.HitStamina(25 + TargetPlayer.MaxStamina * 0.03);
        end;
      end else
      begin
        if TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
        begin
          if HeldByMe then
          begin
            ShowLog('%s firmly holds %s by %s and her attempt at escape fails', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Apparel[E].Data.DisplayName], ColorLogBondage);
            NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'HOLD', ColorParticlePlayerStunned);
            TargetPlayer.CurrentAction := TActionPlayerHeld.NewAction(TargetPlayer, 2.0);
            TActionPlayerHeld(TargetPlayer.CurrentAction).HeldBy := ParentActor.ReferenceId;
            TargetPlayer.CurrentAction.Start;
            Sound(ActionData.StruggleSound);
            TargetPlayer.HitStamina(12 + TargetPlayer.MaxStamina * 0.01);
          end else
          begin
            LocalStats.IncStat(Data.ClassName + '_group');
            ShowLog('%s takes advantage of %s''s %s and makes sure she is held tight', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Apparel[E].Data.DisplayName], ColorLogStaminaDamage);
            NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'HOLD', ColorParticlePlayerStunned);
            Sound(ActionData.StruggleSound);
            TargetPlayer.HitStamina(25 + TargetPlayer.MaxStamina * 0.03);
          end;
        end else
        begin
          if HeldByMe then
          begin
            if Rnd.Random < 0.5 then
            begin
              ShowLog('While %s struggles in vain, %s removes her %s', [TargetPlayer.Data.DisplayName, ParentActor.Data.DisplayName, TargetPlayer.Inventory.Apparel[E].Data.DisplayName], ColorLogBondage);
              NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STRIP', ColorParticlePlayerDisrobed);
              TargetPlayer.CurrentAction := TActionPlayerHeld.NewAction(TargetPlayer, 2.0);
              TActionPlayerHeld(TargetPlayer.CurrentAction).HeldBy := ParentActor.ReferenceId;
              TargetPlayer.CurrentAction.Start;
              Sound(ActionData.StruggleSound);
              TargetPlayer.HitStamina(12);
            end else
            begin
              ShowLog('%s manages to wriggle out of tight embrace, leaving %s in %s''s clutch', [TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Apparel[E].Data.DisplayName, ParentActor.Data.DisplayName], ColorLogInventory);
              NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STRIP', ColorParticlePlayerDisrobed);
              TargetPlayer.CurrentAction := TActionIdle.NewAction(TargetPlayer);
              TargetPlayer.CurrentAction.Start;
              Sound(ActionData.ReleasedSound);
              TargetPlayer.HitStamina(12);
            end;
          end else
          begin
            LocalStats.IncStat(Data.ClassName + '_group');
            ShowLog('%s strips %s from %s who can''t resist', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Apparel[E].Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondage);
            NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STRIP', ColorParticlePlayerDisrobed);
          end;
          Sound(TargetPlayer.Inventory.Equipped[E].ItemData.SoundUnequip);
          TargetPlayer.Inventory.UnequipAndDrop(E, true);
        end;
      end;
      if (TargetPlayer.CurrentAction is TActionPlayerHeld) and (TargetPlayer.Stamina < -200) then
      begin
        LocalStats.IncStat(Data.ClassName + '_suffocate');
        if Rnd.Random < -TargetPlayer.Stamina/1000 then
        begin
          ShowLog('%s suffocates %s and she blacks out too tired to struggle any longer', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogStaminaDamage);
          TargetPlayer.CurrentAction := TActionPlayerUnconscious.NewAction(TargetPlayer);
          TargetPlayer.CurrentAction.Start;
          ViewGame.UnPauseGame;
          ParentMonster.Ai.AiFlee := true;
        end else
        begin
          ShowLog('%s squeezes %s too tight preventing her from catching breath', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogStaminaDamage);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'HOLD', ColorParticlePlayerStunned);
          TargetPlayer.HitStamina(20);
        end;
      end;
    end;
  end;
end;

destructor TActionCatchAndHold.Destroy;
begin
  if HeldByMe and not ParentActor.CanAct then
  begin
    ShowLog('%s is released from the grasp that held her', [TargetPlayer.Data.DisplayName], ColorLogInventory);
    TargetPlayer.CurrentAction := TActionIdle.NewAction(TargetPlayer);
    TargetPlayer.CurrentAction.Start;
  end;
  if Mark <> nil then
  begin
    Mark.OnFinished := nil;
    Mark.EndAction;
  end;
  inherited Destroy;
end;

{ TActionCatchAndHoldData -------------------------------}

procedure TActionCatchAndHoldData.Validate;
begin
  if not SoundExists(StruggleSound) then
    raise EDataValidationError.CreateFmt('Invalid StruggleSound = "%s" in %s', [StruggleSound, Self.ClassName]);
  if not SoundExists(ReleasedSound) then
    raise EDataValidationError.CreateFmt('Invalid ReleasedSound = "%s" in %s', [ReleasedSound, Self.ClassName]);
  inherited Validate;
end;

procedure TActionCatchAndHoldData.Read(const Element: TDOMElement);
begin
  StruggleSound := Element.AttributeString('StruggleSound');
  ReleasedSound := Element.AttributeString('ReleasedSound');
  inherited Read(Element);
end;

function TActionCatchAndHoldData.Description: TEntriesList;
var
  ActionEntries: TEntriesList;
begin
  Result := inherited Description;
  ActionEntries := MarkData.Description;
  Result.AddRange(ActionEntries);
  FreeAndNil(ActionEntries);

  Result.Add(
    NewEntryText(
      Format('The heroine gets a chance to escape, affected by if she is wearing clothes or restraints. Nude heroine has 60%% chance of escaping per struggle round. Fully restrained - only 10%%', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('Multiple monsters will help each other to keep the character under control, draining her strength and undressing her without giving her a chance to escape.', []),
    Classname + '_group', 1));
  Result.Add(
    NewEntryText(
      Format('If character''s stamina falls below "-200" the monster will try to suffocate her and if succeeds will leave her alone unconscious.', []),
    Classname + '_suffocate', 1));
  Result.Add(
    NewEntryText(
      Format('If discovers the heroine sleeping, will unequip one equipped item and leave her alone.', []),
    Classname + '_sleeping', 1));
end;

function TActionCatchAndHoldData.Action: TActionClass;
begin
  Exit(TActionCatchAndHold);
end;

initialization
  RegisterSimpleSerializableObject(TActionCatchAndHold);
  RegisterSerializableData(TActionCatchAndHoldData);

end.

