{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionRamTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameActionAbstract, GameActionOnTarget, GameActionRoll,
  GameUnlockableEntry;

type
  TActionRamTarget = class(TActionOnTarget)
  strict private
    Phase: Single;
    PreHit: Boolean;
    SubAction: TActionRoll;
    procedure FinishRoll;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    destructor Destroy; override;
  end;

  TActionRamTargetData = class(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    RollWarmUp: Single;
    StaminaDamage: Single;
    HealthDamage: Single;
    NudeStaminaDamage: Single;
    NudeHealthDamage: Single;
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleVectors, CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData, GameGarbageCollector,
  GameActor, GamePlayerCharacter, GameActionKnockback, GameApparelSlots,
  GameViewGame, GameLog, GameColors, GameRandom, GameStats, GameSounds, GameDifficultyLevel,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ActionData:=(Data as TActionRamTargetData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ActionData:=TActionRamTargetData(Data)}
{$ENDIF}

procedure TActionRamTarget.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('PreHit', PreHit);
  if SubAction <> nil then // TODO: It most likely will crash Loading --- but can happen as soon as action started but didn't receive Update yet.
    SubAction.Save(Element.CreateChild('SubAction'));
end;

procedure TActionRamTarget.Load(const Element: TDOMElement);
var
  SubActionElement: TDOMElement;
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
  PreHit := Element.AttributeBoolean('PreHit');
  SubActionElement := Element.ChildElement('SubAction', false);
  if SubActionElement <> nil then
  begin
    SubAction := TActionAbstract.LoadClass(SubActionElement) as TActionRoll;
    SubAction.OnActionFinished := @FinishRoll;
  end else
    SubAction := nil;
end;

function TActionRamTarget.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionRamTarget.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionRamTarget.CanStop: Boolean;
begin
  Exit(Phase < ActionData.RollWarmUp * Difficulty.MonsterAttackDelayMultiplier / 10); // "/10" because otherwise it won't be as dumb as it should be
end;

procedure TActionRamTarget.FinishRoll;
begin
  GarbageCollector.AddAndNil(SubAction);
  Phase := 0;
  PreHit := true;
end;

procedure TActionRamTarget.Update(const SecondsPassed: Single);
var
  EscapeVector: TVector2;
  E: TApparelSlot;
begin
  inherited;

  if not TargetPlayer.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if (SubAction = nil) and (Phase >= ActionData.RollWarmUp * Difficulty.MonsterAttackDelayMultiplier) then
  begin
    SubAction := TActionRoll.NewAction(Parent) as TActionRoll;
    TActionRoll(SubAction).MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
    if (TActionRoll(SubAction).MoveVector.Length > 3)
      or TargetPlayer.Unsuspecting {hack to avoid bug that rams cannot attack sleeping target, because they are too close - it doesn't seem to work too good, but it works} then
    begin
      TActionRoll(SubAction).MoveVectorNormalized := TActionRoll(SubAction).MoveVector.Normalize;
      TActionRoll(SubAction).MoveVector := 15 * TActionRoll(SubAction).MoveVectorNormalized;
    end else
    begin
      //roll away if target is too close;
      LocalStats.IncStat(Data.ClassName + '_retreat');
      TActionRoll(SubAction).MoveVectorNormalized := TActionRoll(SubAction).MoveVector.Normalize;
      repeat
        EscapeVector := Vector2(Rnd.Random - 0.5, Rnd.Random - 0.5).Normalize;
      until TVector2.DotProduct(EscapeVector, TActionRoll(SubAction).MoveVectorNormalized) <= 0;
      TActionRoll(SubAction).MoveVectorNormalized := EscapeVector;
      TActionRoll(SubAction).MoveVector := 5 * TActionRoll(SubAction).MoveVectorNormalized;
      PreHit := false;
    end;
    SubAction.Start;
    SubAction.OnActionFinished := @FinishRoll;
  end;
  if SubAction <> nil then
  begin
    if PreHit and (ParentActor.Collides(TargetPlayer, 0) or ViewGame.BuggyLargeTimeFlowSpeed) then
    begin
      GarbageCollector.AddAndNil(SubAction);
      SubAction := TActionRoll.NewAction(Parent) as TActionRoll;
      TActionRoll(SubAction).MoveVectorNormalized := -Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY).Normalize;
      TActionRoll(SubAction).MoveVector := 4 * TActionRoll(SubAction).MoveVectorNormalized;
      SubAction.Start;
      SubAction.OnActionFinished := @FinishRoll;
      PreHit := false;

      LocalStats.IncStat(Data.ClassName);
      if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
      begin
        E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem;
        ShowLog('%s rams into %s knocking %s off her %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(TargetPlayer.Inventory.Equipped[E].Data.EquipSlots)], ColorLogItemSteal);
        TargetPlayer.Inventory.UnequipAndDrop(E, true);
        TargetPlayer.HitStamina(ActionData.StaminaDamage);
        TargetPlayer.Hit(ActionData.HealthDamage);
      end else
      begin
        //otherwise hit double power
        TargetPlayer.HitStamina(ActionData.NudeStaminaDamage);
        TargetPlayer.Hit(ActionData.NudeHealthDamage);
      end;

      // hurt player (=TMarkHurt)
      Sound(ActionData.HitSound);
      ParentActor.DamageWeapon;
      // Push target
      TargetPlayer.CurrentAction := TActionKnockback.NewAction(Target);
      TActionKnockback(TargetPlayer.CurrentAction).MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
      TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized := TActionKnockback(TargetPlayer.CurrentAction).MoveVector.Normalize;
      TActionKnockback(TargetPlayer.CurrentAction).MoveVector := 20 * TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized;
      TargetPlayer.CurrentAction.Start;
      ViewGame.ShakeMap;
    end;
    SubAction.Update(SecondsPassed);
  end;
end;

class function TActionRamTarget.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionRamTarget).Phase := 0;
  (Result as TActionRamTarget).PreHit := true;
end;

destructor TActionRamTarget.Destroy;
begin
  GarbageCollector.FreeNowAndNil(SubAction);
  inherited Destroy;
end;

{ TActionRamTargetData -------------------------------}

procedure TActionRamTargetData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]); 
  if RollWarmUp < 0 then
    raise EDataValidationError.CreateFmt('RollWarmUp must be >= 0 in %s', [ClassName]);
  if StaminaDamage < 0 then
    raise EDataValidationError.CreateFmt('StaminaDamage must be >= 0 in %s', [ClassName]);
  if HealthDamage < 0 then
    raise EDataValidationError.CreateFmt('HealthDamage must be >= 0 in %s', [ClassName]);
  if NudeStaminaDamage < 0 then
    raise EDataValidationError.CreateFmt('NudeStaminaDamage must be >= 0 in %s', [ClassName]);
  if NudeHealthDamage < 0 then
    raise EDataValidationError.CreateFmt('NudeHealthDamage must be >= 0 in %s', [ClassName]);
end;

procedure TActionRamTargetData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  RollWarmUp := Element.AttributeSingle('RollWarmUp');
  StaminaDamage := Element.AttributeSingle('StaminaDamage');
  HealthDamage := Element.AttributeSingle('HealthDamage');
  NudeStaminaDamage := Element.AttributeSingle('NudeStaminaDamage');
  NudeHealthDamage := Element.AttributeSingle('NudeHealthDamage');
end;

function TActionRamTargetData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will try to ram into the character. Unable to change direction while moving.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('Needs to build up momentum before hitting the target, therefore will instead retreat if the target is too close, hoping for a better attack position.', []),
    Classname + '_retreat', 1));
end;

function TActionRamTargetData.Action: TActionClass;
begin
  Exit(TActionRamTarget);
end;

initialization
  RegisterSimpleSerializableObject(TActionRamTarget);
  RegisterSerializableData(TActionRamTargetData);

end.

