{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameAiAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameSimpleSerializableObject, GameSerializableData,
  GameUnlockableEntry;

type
  TAiState = (asIdle, asInvestigate, asAttack, asFlee);

type
  EAiInvalidOperation = class(Exception);

type
  TAiAbstractData = class; //forward
  TAiAbstract = class abstract(TSimpleSerializableObject)
  protected const
    AlertRangeAttack = Integer(8);
    AlertRangeInvestigate = Integer(12);
  protected
    procedure DoWalkRandomly;
    procedure DoWalkRandomlyLong;
    procedure DoWalkCurious;
    procedure DoFlee;
    procedure DoChasePlayerAndAlertNearbyToChase;
    procedure DoInvestigateNoiseAndAlertNearbyToInvestigate;
    procedure DoReturnToGuard;
  public
    procedure OnHit(const ATimeout: Single); virtual; abstract;
    procedure ChasePlayer(const ATimeout: Single); virtual; abstract;
    procedure InvestigateNoise(const AX, AY: Single); virtual; abstract;
  public
    { AiMonster }
    //TODO: maybe make TAiMonsterAbstract? To separate those for to-be player AI
    Timeout: Single;
    AiState: TAiState;
    Guard, SoftGuard: Boolean;
    GuardPointX, GuardPointY: Int16;
    AiFlee: Boolean;
    { /AiMonster }

    Parent: TObject; // we can't properly typecast it due to cycle reference
    Data: TAiAbstractData;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    procedure Update(const SecondsPassed: Single); virtual; abstract;
    constructor Create; override;
  end;
  TAiClass = class of TAiAbstract;

  TAiAbstractData = class abstract(TSerializableData)
  public
    function Description: TEntriesList; virtual;
    function Ai: TAiClass; virtual; abstract;
  end;

implementation
uses
  Math, TypInfo,
  CastleXmlUtils,
  GameRandom, GameActor, GameMap, GameMapTypes,
  GameMonster, GameActionMoveAbstract;

{$IFDEF SafeActorTypecast}
//{$DEFINE AiData:=(Data as TAiTrapData)}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
//{$DEFINE AiData:=TAiTrapData(Data)}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

function AiStateToStr(const AAiState: TAiState): String;
begin
  Result := GetEnumName(TypeInfo(TAiState), Ord(AAiState));
end;
function StrToAiState(const AString: String): TAiState;
var
  A: TAiState;
begin
  // TODO: Optimize
  for A in TAiState do
    if AiStateToStr(A) = AString then
      Exit(A);
  raise Exception.CreateFmt('Cannot find TAiState: "%s"', [AString]);
end;

procedure TAiAbstract.DoWalkRandomly;
const
  IdleTravelDistance = Int16(10);
var
  TX, TY: Int16;
begin
  repeat
    TX := ParentActor.LastTileX + Rnd.Random(IdleTravelDistance * 2 + 1) - IdleTravelDistance;
    TY := ParentActor.LastTileY + Rnd.Random(IdleTravelDistance * 2 + 1) - IdleTravelDistance;
  until (TX > 0) and (TY > 0) and (TX < Map.PredSizeX) and (TY < Map.PredSizeY) and ParentActor.PassableTiles[TX + Map.SizeX * TY];
  ParentActor.MoveTo(TX, TY);
  Timeout := (ParentActor.CurrentAction as TActionMoveAbstract).RemainingTime;
end;

procedure TAiAbstract.DoWalkRandomlyLong;
var
  TX, TY: Int16;
begin
  repeat
    TX := Rnd.Random(Map.SizeX);
    TY := Rnd.Random(Map.SizeY);
  until Map.PassableTiles[ParentActor.PredSize][TX + Map.SizeX * TY];
  ParentActor.MoveTo(TX, TY);
end;

procedure TAiAbstract.DoWalkCurious;
var
  I: Integer;
  EPoint, EPointRandom: SizeInt;
begin
  EPoint := Rnd.Random(Map.Extrema.Count);
  // We select one of 10(11) extrema visited longest ago
  // This seems faster than sorting Extrema list + adds necessary chaos in behavior
  repeat
    for I := 0 to Min(10, Map.Extrema.Count div 2) do
    begin
      EPointRandom := Rnd.Random(Map.Extrema.Count);
      if (Map.Extrema[EPoint].LastChecked > Map.Extrema[EPOintRandom].LastChecked) then
        EPoint := EPOintRandom;
    end;
  until (Map.Visible[Map.Extrema[EPoint].X + Map.SizeX * Map.Extrema[EPoint].Y] = NotVisible) or (Rnd.Random < 0.5);
  ParentActor.MoveTo(Map.Extrema[EPoint].X, Map.Extrema[EPoint].Y);
  Map.Extrema[EPoint].LastChecked := Map.TimeSpentOnTheMap; // NOTE: we mark extremum as visited NOW - maybe it should be marked on arrival (switching to idle) + also consider other monsters
  Timeout := (ParentActor.CurrentAction as TActionMoveAbstract).RemainingTime;
end;

procedure TAiAbstract.DoFlee;
var
  TX, TY: Int16;
  DistMap: TDistanceMapArray;
  MaxDistance: TDistanceQuant;
begin
  AiState := asFlee;
  Guard := false;
  SoftGuard := false;
  //warning, we are using player.size here, not monster.size. It should be ok, but doesn't seem clean enough? May cause troubles (freeze) if monster.size > player.size. TODO
  DistMap := Map.DistanceMap(ParentMonster.CurrentTarget); // Warning: can still run into Player instead as Player can be on the other side: needs 2 distance maps
  MaxDistance := Map.MaxDistance(DistMap);
  repeat
    TX := Rnd.Random(Map.SizeX);
    TY := Rnd.Random(Map.SizeY);
  until Map.PassableTiles[ParentActor.PredSize][TX + Map.SizeX * TY] and (DistMap[TX + Map.SizeX * TY] > MaxDistance div 2);
  ParentActor.MoveTo(TX, TY);
end;

procedure TAiAbstract.DoChasePlayerAndAlertNearbyToChase;
var
  M: TMonster;
begin
  for M in Map.MonstersList do
    if M.CanAct and not M.MonsterData.VacuumCleaner and (Sqr(M.CenterX - ParentActor.CenterX) + Sqr(M.CenterY - ParentActor.CenterY) < Sqr(AlertRangeAttack)) then
      if (not M.Ai.Guard) or Guard or M.LineOfSight(ParentMonster.CurrentTarget) then
        M.Ai.ChasePlayer(10); // current included
end;

procedure TAiAbstract.DoInvestigateNoiseAndAlertNearbyToInvestigate;
var
  M: TMonster;
begin
  if Timeout < 10 then
    Timeout := 10;
  for M in Map.MonstersList do
    if M.CanAct and not M.MonsterData.VacuumCleaner and (Sqr(M.CenterX - ParentActor.CenterX) + Sqr(M.CenterY - ParentActor.CenterY) < Sqr(AlertRangeInvestigate)) then
      if (not M.Ai.Guard) or Guard or M.LineOfSight(ParentMonster.CurrentTarget) or ParentActor.LineOfSight(M) then
        M.Ai.InvestigateNoise(ParentMonster.CurrentTarget.CenterX, ParentMonster.CurrentTarget.CenterY);
end;

procedure TAiAbstract.DoReturnToGuard;
begin
  if (ParentActor.LastTileX <> GuardPointX) or (ParentActor.LastTileY <> GuardPointY) then
  begin
    ParentActor.MoveTo(GuardPointX, GuardPointY);
    Timeout := (ParentActor.CurrentAction as TActionMoveAbstract).RemainingTime;
  end;
end;

procedure TAiAbstract.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Timeout', Timeout);
  Element.AttributeSet('Guard', Guard);
  Element.AttributeSet('SoftGuard', SoftGuard);
  Element.AttributeSet('GuardPointX', GuardPointX);
  Element.AttributeSet('GuardPointY', GuardPointY);
  Element.AttributeSet('AiFlee', AiFlee);
  Element.AttributeSet('AiState', AiStateToStr(AiState));
end;

procedure TAiAbstract.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  Timeout := Element.AttributeSingle('Timeout');
  Guard := Element.AttributeBoolean('Guard');
  SoftGuard := Element.AttributeBoolean('SoftGuard');
  GuardPointX := Element.AttributeInteger('GuardPointX');
  GuardPointY := Element.AttributeInteger('GuardPointY');
  AiFlee := Element.AttributeBoolean('AiFlee');
  AiState := StrToAiState(Element.AttributeString('AiState'));
end;

constructor TAiAbstract.Create;
begin
  inherited Create;
  // TODO: Temporary?
  AiFlee := false;
  AiState := asIdle;
end;

function TAiAbstractData.Description: TEntriesList;
begin
  Exit(nil);
end;


initialization

end.

