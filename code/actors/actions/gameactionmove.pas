{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionMove;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  CastleVectors,
  GameSimpleSerializableObject,
  GameMapTypes,
  GameActionMoveAbstract, GameActionAbstract;

type
  TActionMove = class(TActionMoveAbstract)
  strict private
    DestinationX, DestinationY: Single; // used only to LoadGame -> AfterDeserealization
  public
    CurrentWaypoint: Integer;
    Waypoints: TWaypoints;
    MoveVector, MoveVectorNormalized: TVector2;
    MovePhase: Single;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    { Very rough approximation of remaining distance and remaining time of the route
      It's based ONLY on number of waypoints to pass and considers waypoints do diagonal movement
      I.e. this _should_ be max of the proper values
      It's used only for monster's AI and doesn't need higher accuracy for now }
    function RemainingDistance: Single; override;
    function RemainingTime: Single; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure MoveTo(const ToX, ToY: Single); override;
  public
    procedure Save(const Element: TDOMElement); override;
    procedure AfterDeserealization; override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    destructor Destroy; override;
  end;

  TActionMoveData = class(TActionMoveAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameActor, GameMonster, GameMap, GameLog;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ELSE}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ENDIF}

function TActionMove.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionMove.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionMove.RemainingDistance: Single;
begin
  Exit(1.5 * (Waypoints.Count - CurrentWaypoint));
end;

function TActionMove.RemainingTime: Single;
begin
  Exit(1.5 * (Waypoints.Count - CurrentWaypoint) / ParentActor.Data.MovementSpeed);
end;

procedure TActionMove.Update(const SecondsPassed: Single);
var
  V: TVector2;
begin
  MovePhase += SecondsPassed * ParentActor.GetSpeed;
  while MovePhase > MoveVector.Length do
  begin
    MovePhase -= MoveVector.Length;
    Inc(CurrentWaypoint);
    ParentActor.HurtFeet(MoveVector.Length * FeetDamagePerTile);
    ParentActor.MoveMeTo(Waypoints[CurrentWaypoint][0], Waypoints[CurrentWaypoint][1]);
    if not Map.CanMove(ParentActor.LastTile) then
      ShowError('Nav grid error in TActionMove.Update: %d,%d is not walkable', [ParentActor.LastTileX, ParentActor.LastTileY]);

    ParentActor.UpdateVisible;
    if CurrentWaypoint = Pred(Waypoints.Count) then // Arrived at destination
    begin
      FreeAndNil(Waypoints);
      MovePhase := -1;
      ActionFinished;
      Exit; // it may be not the last iteration of While loop
    end else
    begin
      MoveVector := Vector2(Waypoints[CurrentWaypoint + 1][0] - Waypoints[CurrentWaypoint][0], Waypoints[CurrentWaypoint + 1][1] - Waypoints[CurrentWaypoint][1]);
      MoveVectorNormalized := MoveVector.Normalize;
    end;
  end;
  V := MovePhase * MoveVectorNormalized;
  ParentActor.MoveMeToSafe(Waypoints[CurrentWaypoint][0] + V.X, Waypoints[CurrentWaypoint][1] + V.Y);
end;

class function TActionMove.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionMove).CurrentWaypoint := 0;
  (Result as TActionMove).MovePhase := 0;
end;


procedure TActionMove.MoveTo(const ToX, ToY: Single);
var
  Range: Integer;
  ToXInt, ToYInt, GotoX, GotoY: Int16;
  CurrentPassableTiles: TMoveArray;

  // TODO: This should be UI-only thing
  procedure GetNearestClick;
  var
    DX, DY: Integer;
  begin
    for DX := -Range to Range do
      for DY := -Range to Range do
        if Sqr(DX) + Sqr(DY) <= Sqr(Range) then
        begin
          GotoX := ToXInt + DX;
          GotoY := ToYInt + DY;
          if (GotoX >= 0) and (GotoX <= Map.PredSizeX) and (GotoY >= 0) and (GotoY <= Map.PredSizeY) and CurrentPassableTiles[GotoX + Map.SizeX * GotoY] then
            Exit;
        end;
  end;

begin
  if not Map.PassableTiles[ParentActor.PredSize][ParentActor.lastTile] then
    ShowError('Last tile is not walkable in TActionMove.MoveTo: %d,%d', [ParentActor.LastTileX, ParentActor.LastTileY]);

  CurrentPassableTiles := ParentActor.PassableTiles;
  ToXInt := Round(ToX); // note, not TRUNC, because we want "nearest" tile
  ToYInt := Round(ToY);
  GotoX := ToXInt;
  GotoY := ToYInt;
  Range := 0;
  while (GotoX < 0) or (GotoX > Map.PredSizeX) or (GotoY < 0) or (GotoY > Map.PredSizeY) or not CurrentPassableTiles[GotoX + Map.SizeX * GotoY] do
  begin
    GetNearestClick;
    Inc(Range);
  end;

  if (ToXInt = GotoX) and (ToYInt = GotoY) then
    Waypoints := Map.GetWaypoints(Map.GenerateDijkstra(CurrentPassableTiles, GotoX, GotoY, 1024, Parent is TMonster, ParentActor.LastTileX, ParentActor.LastTileY, true), ParentActor.X, ParentActor.Y, ToX, ToY, ParentActor.LastTileX, ParentActor.LastTileY)
  else
    Waypoints := Map.GetWaypoints(Map.GenerateDijkstra(CurrentPassableTiles, GotoX, GotoY, 1024, Parent is TMonster, ParentActor.LastTileX, ParentActor.LastTileY, true), ParentActor.X, ParentActor.Y, GotoX, GotoY, ParentActor.LastTileX, ParentActor.LastTileY);
  {if Waypoints = nil then
  begin
    // "magic" value to report "no way to reach target"
    OnFinished;
    Exit;
  end;}

  // Map.GetWaypoints is guaranteed to give at least 2 waypoints!
  // Waypoint[0] === (X,Y)
  MoveVector := Vector2(Waypoints[1][0] - ParentActor.X, Waypoints[1][1] - ParentActor.Y);
  MoveVectorNormalized := MoveVector.Normalize;
end;

procedure TActionMove.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  // To avoid painful saving everything we save... only destination.
  Element.AttributeSet('DestinationX', Waypoints[Pred(Waypoints.Count)][0]);
  Element.AttributeSet('DestinationY', Waypoints[Pred(Waypoints.Count)][1]);
end;

procedure TActionMove.AfterDeserealization;
begin
  inherited AfterDeserealization;
  MoveTo(DestinationX, DestinationY); // TODO: Maybe better save waypoints?
end;

procedure TActionMove.Load(const Element: TDOMElement);
begin
  inherited;
  DestinationX := Element.AttributeSingle('DestinationX');
  DestinationY := Element.AttributeSingle('DestinationY');
end;

destructor TActionMove.Destroy;
begin
  FreeAndNil(Waypoints);
  inherited Destroy;
end;

{ TActionMoveData -------------------------------}

function TActionMoveData.Action: TActionClass;
begin
  Exit(TActionMove);
end;

initialization
  RegisterSimpleSerializableObject(TActionMove);
  RegisterSerializableData(TActionMoveData);

end.

