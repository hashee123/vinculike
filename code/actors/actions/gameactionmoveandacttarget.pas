{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionMoveAndActTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameSimpleSerializableObject,
  GameActionAbstract, GameActionOnTarget;

type
  TActionMoveAndActTarget = class(TActionOnTarget)
  strict private
    LastTargetX, LastTargetY: Int16;
    CurrentSubAction: TActionAbstract;
    procedure MoveFinished;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function ProposedTimeSpeed: Single; override;
    procedure Update(const SecondsPassed: Single); override;
    destructor Destroy; override;
  end;

  TActionMoveAndActTargetData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData, GameGarbageCollector,
  GameActor, GameMap, GameRandom, GameLog,
  GameActionMoveAbstract, GameActionIdle, GameViewGame;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE ActionData:=(Data as TActionAbstractData)} // WARNING: for some reason we can't typecast it to proper TActionMoveAndActTargetData TODO: investigate why
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE ActionData:=TActionAbstractData(Data)}
{$ENDIF}


procedure TActionMoveAndActTarget.MoveFinished;
begin
  {$WARNING 'Severe movement bug here'}
  if ParentActor = ViewGame.CurrentCharacter then
  begin
    DebugLog('%s failed to reach the target to act.', [ParentActor.Data.Id]);
    GarbageCollector.AddAndNil(CurrentSubAction);
    CurrentSubAction := TActionIdle.NewAction(Parent);
    CurrentSubAction.Start;
  end else
  begin
    DebugLog('%s has finished moving but didn''t reach the target. Trying again.', [ParentActor.Data.Id]);
    // Let's try this way to start with - first to see how it actually affects the monsters, then just go straight into the target.
    GarbageCollector.AddAndNil(CurrentSubAction);
    CurrentSubAction := ParentActor.MoveAction.NewAction(Parent);
    TActionMoveAbstract(CurrentSubAction).MoveTo(TargetActor.CenterX - ParentActor.HalfSize, TargetActor.CenterY - ParentActor.HalfSize);
    CurrentSubAction.OnActionFinished := @MoveFinished;
    CurrentSubAction.Start;
  end;
end;

procedure TActionMoveAndActTarget.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  if CurrentSubAction <> nil then // TODO: It most likely will crash Loading --- but can happen as soon as action started but didn't receive Update yet.
    CurrentSubAction.Save(Element.CreateChild('SubAction'));
end;

procedure TActionMoveAndActTarget.Load(const Element: TDOMElement);
begin
  inherited;
  CurrentSubAction := TActionAbstract.LoadClass(Element.ChildElement('SubAction', true)) as TActionAbstract;
  raise EActionLoadNotImplemented.Create('Loading TActionMoveAndActTarget is not implemented yet as the action data is not serialized!');
end;

function TActionMoveAndActTarget.NoiseMultiplier: Single;
begin
  if CurrentSubAction <> nil then
    Exit(CurrentSubAction.NoiseMultiplier)
  else
    Exit(1.0);
end;

function TActionMoveAndActTarget.NoiseAddition: Single;
begin
  if CurrentSubAction <> nil then
    Exit(CurrentSubAction.NoiseAddition)
  else
    Exit(0.0);
end;

function TActionMoveAndActTarget.ProposedTimeSpeed: Single;
begin
  if CurrentSubAction <> nil then
    Exit(CurrentSubAction.ProposedTimeSpeed)
  else
    Exit(1.0);
end;

procedure TActionMoveAndActTarget.Update(const SecondsPassed: Single);
var
  InRange: Boolean;
  CanSee: Boolean;
  SurroundX, SurroundY: Single;
begin
  inherited;

  if not TargetActor.CanBeInteractedWith then
  begin
    ActionFinished;
    Exit;
  end;

  InRange := Sqr(TargetActor.CenterX - ParentActor.CenterX) +
             Sqr(TargetActor.CenterY - ParentActor.CenterY) <= ActionData.SqrAttackRange;
  if ActionData.TemporaryRequiresFullViewOfTarget then
    CanSee := ParentActor.LineOfSightMore(TargetActor) // raycast from "x" in the center
  else
    CanSee := ParentActor.LineOfSight(Trunc(TargetActor.CenterX), Trunc(TargetActor.CenterY)) or // simple raycast in 1 ray from center to center
      (Sqr(TargetActor.CenterX - ParentActor.CenterX) +
      Sqr(TargetActor.CenterY - ParentActor.CenterY) < Sqr(TargetActor.HalfSize + ParentActor.HalfSize + 0.1)); // TODO: not sure about that, but otherwise ghosts break
  if InRange and CanSee and not (CurrentSubAction is ActionData.Action) then     // nil or TActionMoveAbstract
  begin
    GarbageCollector.AddAndNil(CurrentSubAction);
    CurrentSubAction := ActionData.Action.NewAction(ParentActor);
    ((CurrentSubAction as ActionData.Action) as TActionOnTarget).Data := Data;
    ((CurrentSubAction as ActionData.Action) as TActionOnTarget).Target := TargetActor;
    CurrentSubAction.Start;
  end else
  if not InRange or not CanSee then
  begin
    if (CurrentSubAction = nil) or
      ((CurrentSubAction is ActionData.Action) and CurrentSubAction.CanStop) or
      ((CurrentSubAction is TActionMoveAbstract) and ((LastTargetX <> TargetActor.LastTileX) or (LastTargetY <> TargetActor.LastTileY))) then
    begin
      GarbageCollector.AddAndNil(CurrentSubAction);
      CurrentSubAction := ParentActor.MoveAction.NewAction(Parent);
      LastTargetX := TargetActor.LastTileX;
      LastTargetY := TargetActor.LastTileY;
      if ParentActor = ViewGame.CurrentCharacter then
      begin
        SurroundX := TargetActor.CenterX - ParentActor.HalfSize;
        SurroundY := TargetActor.CenterY - ParentActor.HalfSize;
      end else
      begin
        SurroundX := TargetActor.CenterX - ParentActor.HalfSize + (GetRandomFromSeed64(ParentActor.ReferenceId) - 0.5) * (ParentActor.Size + TargetActor.Size + 3);
        SurroundY := TargetActor.CenterY - ParentActor.HalfSize + (GetRandomFromSeed64(ParentActor.ReferenceId + 1) - 0.5) * (ParentActor.Size + TargetActor.Size + 3);
        if not Map.Ray(0, TargetActor.CenterX, TargetActor.CenterY, SurroundX, SurroundY) or (ParentActor.DistanceToSqr(TargetActor) < TargetActor.DistanceToSqr(SurroundX, SurroundY)) then
        begin
          SurroundX := TargetActor.CenterX - ParentActor.HalfSize;
          SurroundY := TargetActor.CenterY - ParentActor.HalfSize;
        end;
      end;
      TActionMoveAbstract(CurrentSubAction).MoveTo(SurroundX, SurroundY);
      CurrentSubAction.OnActionFinished := @MoveFinished;
      CurrentSubAction.Start;
    end;
  end;
  CurrentSubAction.Update(SecondsPassed);
end;

destructor TActionMoveAndActTarget.Destroy;
begin
  GarbageCollector.FreeNowAndNil(CurrentSubAction);
  inherited Destroy;
end;

{ TActionMoveAndActTargetData ---------------------------}

function TActionMoveAndActTargetData.Action: TActionClass;
begin
  Exit(TActionMoveAndActTarget);
end;

initialization
  RegisterSimpleSerializableObject(TActionMoveAndActTarget);
  RegisterSerializableData(TActionMoveAndActTargetData);

end.

