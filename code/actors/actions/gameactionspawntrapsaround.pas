{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionSpawnTrapsAround;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  GameActionAbstract, GameActionOnTarget,
  GameUnlockableEntry;

type
  TActionSpawnTrapsAround = class(TActionOnTarget) // TODO: not OnTarget
  strict private
    procedure Perform;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionSpawnTrapsAroundData = class(TActionOnTargetData)
  public const
    Radius = Single(9);
    NumberOfTraps = Integer(24);
  public
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GameMap, GameRandom, GameActor, GameMonster, GameActorData, GameMonsterData, GameMonstersDatabase,
  GameSounds, GameLog, GameParticle, GameColors, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ActionData:=(Data as TActionSpawnTrapsAroundData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ActionData:=TActionSpawnTrapsAroundData(Data)}
{$ENDIF}

procedure TActionSpawnTrapsAround.Perform;
var
  I: Integer;
  AMonster: TMonster;
  MonsterKind: TActorData;
  SX, SY: Int16;
  SpawnList: TActorDataList;

  function NotCollidesWithOtherTraps: Boolean;
  var
    M: TMonster;
  begin
    for M in Map.MonstersList do
      if M.MonsterData.Trap and M.CanAct and M.CollidesInt(SX, SY, 0) then
        Exit(false);
    Exit(true);
  end;

var
  Count: Integer;
begin
  SpawnList := TActorDataList.Create(false);
  for I := 0 to Pred(TrapsData.Count) do
    if (TrapsData[I] as TMonsterData).StartSpawningAtDepth <= Map.CurrentDepth then
      SpawnList.Add(TrapsData[I]);

  if SpawnList.Count = 0 then
  begin
    ShowError('Trap cannot spawn: SpawnList is empty', []);
    FreeAndNil(SpawnList);
    Exit;
  end;

  Sound('spawner'); // TODO
  MonsterKind := SpawnList[Rnd.Random(SpawnList.Count)];
  LocalStats.IncStat(Data.ClassName);
  Count := 0;
  for I := 0 to ActionData.NumberOfTraps do
  begin
    SX := Round(ParentActor.CenterX + ActionData.Radius * Cos(2 * Pi * Single(I) / Single(ActionData.NumberOfTraps)));
    SY := Round(ParentActor.CenterY + ActionData.Radius * Sin(2 * Pi * Single(I) / Single(ActionData.NumberOfTraps)));
    if (SX > 0) and (SY > 0) and (SX < Map.PredSizeX - Pred(MonsterKind.Size)) and (SY < Map.PredSizeY - Pred(MonsterKind.Size)) and
      Map.PassableTiles[Pred(MonsterKind.Size)][SX + Map.SizeX * SY] and ParentActor.LineOfSight(SX, SY) and
      NotCollidesWithOtherTraps then
    begin
      Inc(Count);
      AMonster := TMonster.Create;
      AMonster.Data := MonsterKind;
      AMonster.Reset;
      AMonster.Teleport(SX, SY);
      Map.MonstersList.Insert(0, AMonster);
    end;
  end;
  if Count = 1 then
  begin
    ShowLog('Another trap appears nearby', [], ColorLogTrap);
    NewParticle(ParentActor.CenterX, ParentActor.CenterY, 'TRAP', ColorParticleSpawn);
  end else
  if Count > 1 then
  begin
    ShowLog('A bunch of traps suddenly activate around', [], ColorLogTrap);
    NewParticle(ParentActor.CenterX, ParentActor.CenterY, 'TRAP', ColorParticleSpawn);
  end else
    ShowLog('Luckily nothing seems to have happened', [], ColorLogTrap);

  FreeAndNil(SpawnList);
end;

function TActionSpawnTrapsAround.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionSpawnTrapsAround.NoiseAddition: Single;
begin
  Exit(0.0);
end;

procedure TActionSpawnTrapsAround.Update(const SecondsPassed: Single);
begin
  if not ParentActor.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  Perform;

  ParentActor.Health := -1;
  ParentActor.ForceResetToIdle;
  inherited;
end;

{ TActionSpawnTrapsAroundData -------------------------------}

function TActionSpawnTrapsAroundData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Spans multiple traps around the central one.', []),
    Classname, 1));
end;

function TActionSpawnTrapsAroundData.Action: TActionClass;
begin
  Exit(TActionSpawnTrapsAround);
end;

initialization
  RegisterSimpleSerializableObject(TActionSpawnTrapsAround);
  RegisterSerializableData(TActionSpawnTrapsAroundData);

end.

