{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionAlarmFlee;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameActionAbstract, GameActionAlarmAbstract,
  GameUnlockableEntry;

type
  TActionAlarmFlee = class(TActionAlarmAbstract)
  strict private
    Phase: Single;
  protected
    procedure Perform; override;
  public
    function CanStop: Boolean; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionAlarmFleeData = class(TActionAlarmAbstractData)
  public
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GameViewGame, GameMonster, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE ActionData:=(Data as TActionAlarmFleeData)}
{$ELSE}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE ActionData:=TActionAlarmFleeData(Data)}
{$ENDIF}

procedure TActionAlarmFlee.Perform;
begin
  if Phase < ActionData.WarmUpTime then
    Exit;
  inherited;
  LocalStats.IncStat(Data.ClassName);
  ViewGame.WakeUp(true, false);
  ParentMonster.Ai.AiFlee := true;
end;

function TActionAlarmFlee.CanStop: Boolean;
begin
  Exit(Phase > ActionData.WarmUpTime);
end;

procedure TActionAlarmFlee.Start;
begin
  inherited Start;
  Phase := 0;
end;

procedure TActionAlarmFlee.Update(const SecondsPassed: Single);
begin
  Phase += SecondsPassed;
  inherited Update(SecondsPassed);
end;

{ TActionAlarmFleeData -------------------------------- }

function TActionAlarmFleeData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will alert all monsters within radius %.0n of the character''s current position and flee.', [Sqrt(SqrAlarmRange)]),
    Classname, 1));
end;

function TActionAlarmFleeData.Action: TActionClass;
begin
  Exit(TActionAlarmFlee);
end;

initialization
  RegisterSimpleSerializableObject(TActionAlarmFlee);
  RegisterSerializableData(TActionAlarmFleeData);

end.

