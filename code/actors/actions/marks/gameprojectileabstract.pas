{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileAbstract;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Classes, DOM,
  CastleUtils, CastleVectors,
  GameMarkAbstract, GameActor;

type
  TProjectileAbstract = class abstract(TMarkAbstract)
  protected
    DistancePassed: Single;
  public
    /// TODO: Do we even need Parent/Phase here?
    MoveVector: TVector2;
    procedure HitWall; virtual; abstract;
    procedure HitTarget(const TargetsHit: array of TActor); virtual; abstract;
    procedure Update(SecondsPassed: Single); override;
    {$WARNING Critical TODO: Save-load}
  end;

  TProjectileAbstractData = class abstract(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Speed: Single;
    Homing: Single;
    IsGhost: Boolean;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameMap, GamePositionedObject, GameMath,
  GameViewGame;

{$IFDEF SafeActorTypecast}
{$DEFINE MarkData:=(Data as TProjectileAbstractData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileAbstractData(Data)}
{$ENDIF}

procedure TProjectileAbstractData.Validate;
begin
  inherited Validate;
  if Speed <= 0 then
    raise EDataValidationError.CreateFmt('Speed <= 0 in %s', [Self.ClassName]);
  if Homing < 0 then
    raise EDataValidationError.CreateFmt('Homing < 0 in %s', [Self.ClassName]);
end;

procedure TProjectileAbstractData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Speed := Element.AttributeSingle('Speed');
  Homing := Element.AttributeSingleDef('Homing', 0);
  IsGhost := Element.AttributeBooleanDef('IsGhost', false);
end;

procedure TProjectileAbstract.Update(SecondsPassed: Single);
const
  MinDeltaSeconds = Single(0.01);
var
  RemainingSeconds: Single;
  DeltaSeconds: Single;
  NewTileX, NewTileY: Int16;
  NewX, NewY: Single;
  NearestValidTarget: TPositionedObject;
begin
  inherited;

  (* TODO: BUG
     if SecondsPassed is large enough we can "jump over a tile".
     This may be very bad for navmesh and can even lead to a crash due to navmesh error *)
  RemainingSeconds := SecondsPassed;
  while RemainingSeconds > 0 do
  begin
    if RemainingSeconds > MinDeltaSeconds then
    begin
      DeltaSeconds := MinDeltaSeconds;
      RemainingSeconds -= MinDeltaSeconds;
    end else
    begin
      DeltaSeconds := RemainingSeconds;
      RemainingSeconds := 0;
    end;
    if MarkData.Homing > 0 then
    begin
      NearestValidTarget := ViewGame.CurrentCharacter;
      MoveVector := RotatePoint2D(MoveVector, MarkData.Homing * DeltaSeconds *
        Sign(MoveVector.X * (NearestValidTarget.CenterY - CenterY) - MoveVector.Y * (NearestValidTarget.CenterX - CenterX)) // here we technically calculate cross product and see if resulting vector is pointing up or down (i.e. clockwise or counter-clockwise rotation). Obviously as both vectors are in XY plane - the cross-product will be a vector parallel to Z
      );
    end;
    NewX := X + DeltaSeconds * MarkData.Speed * MoveVector.X;
    NewY := Y + DeltaSeconds * MarkData.Speed * MoveVector.Y;
    DistancePassed += DeltaSeconds * MarkData.Speed;
    NewTileX := Trunc(NewX);
    NewTileY := Trunc(NewY);
    if DistanceToSqr(ViewGame.CurrentCharacter) < Sqr(HalfSize + ViewGame.CurrentCharacter.HalfSize) then // TODO: HalfColliderSize: Single
    begin
      HitTarget([ViewGame.CurrentCharacter]);
      EndAction;
      Exit;
    end else
    if (DistancePassed < MarkData.Range) and (Map.PassableTiles[0][NewTileX + Map.SizeX * NewTileY] or MarkData.IsGhost) and
      (NewTileX > 0) and (NewTileX < Map.PredSizeX) and (NewTileY > 0) and (NewTileY < Map.PredSizeY) then
      MoveMeTo(NewX, NewY)
    else
    begin
      HitWall;
      EndAction;
      Exit;
    end;
  end;
end;

end.

