{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkRibbonQuarrel;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameMarkAbstract,
  GameItemData,
  GameUnlockableEntry;

type
  TMarkRibbonQuarrel = class(TMarkTargetAbstract)
  strict private
    function MyRibbon: TItemData;
  protected
    procedure Perform; override;
  end;

  TMarkRibbonQuarrelData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    DressedTopWillpowerDamage: Single;
    NudeTopWillpowerDamage: Single;
    DressedBottomWillpowerDamage: Single;
    NudeBottomWillpowerDamage: Single;
    Ribbons: TItemsDataList;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  public
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameMap,
  GameViewGame, GameParticle, GameApparelSlots, GameActor, GameSounds,
  GameItemsDatabase, GameInventoryItem, GamePlayerCharacter, GameMonster, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkRibbonQuarrelData}
{$INCLUDE marktypecasts.inc}

function TMarkRibbonQuarrel.MyRibbon: TItemData;
begin
  Exit(MarkData.Ribbons[GetRandomFromSeed64(ParentMonster.ReferenceId, MarkData.Ribbons.Count)]);
end;

procedure TMarkRibbonQuarrel.Perform;

  procedure EquipRandomItem;
  var
    ItemsList: TItemsDataList;
    ItemGift: TItemData;
  begin
    ItemsList := TItemsDataList.Create(false);
    for ItemGift in ItemsDataList do
      if ItemGift.StartSpawningAtDepth <= Map.CurrentDepth + 10 then
        if not (esAmulet in ItemGift.EquipSlots) and not (esWeapon in ItemGift.EquipSlots) and (ItemGift.NumberOfSlots = 1) and (TargetPlayer.Inventory.Apparel[ItemGift.MainSlot] = nil) then
          ItemsList.Add(ItemGift);
    if ItemsList.Count > 0 then
    begin
      ItemGift := ItemsList[Rnd.Random(ItemsList.Count)];
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemGift));
      TargetPlayer.Inventory.ReinforceItem(ItemGift.MainSlot);
      ShowLog('Not gifted with any aesthetic taste %s thinks %s will match perfectly with %s', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esAmulet].Data.DisplayName, ItemGift.DisplayName], ColorDefault);
    end;
    FreeAndNil(ItemsList);
  end;

var
  ItemStolen: TInventoryItem;
  E, AttackSlot: TApparelSlot;
begin
  LocalStats.IncStat('TMarkRibbonQuarrel');

  if TargetPlayer.Inventory.HasItem(MyRibbon) then
  begin
    if TargetPlayer.Unsuspecting then
    begin
      LocalStats.IncStat('TMarkRibbonQuarrel_sleep');
      EquipRandomItem;
      ParentMonster.Ai.AiFlee := true;
    end else
    begin
      if Rnd.RandomBoolean then
      begin
        // Attack top
        if TargetPlayer.Inventory.TopCovered > 0 then
        begin
          if (TargetPlayer.Inventory.Equipped[esTopUnder] <> nil) and TargetPlayer.Inventory.Equipped[esTopUnder].ItemData.CoversTop then
            AttackSlot := esTopUnder
          else
          if (TargetPlayer.Inventory.Equipped[esTopOver] <> nil) and TargetPlayer.Inventory.Equipped[esTopOver].ItemData.CoversTop then
            AttackSlot := esTopOver
          else
            for E in TargetPlayer.Blueprint.EquipmentSlots do
              if (TargetPlayer.Inventory.Equipped[E] <> nil) and TargetPlayer.Inventory.Equipped[E].ItemData.CoversTop then
              begin
                AttackSlot := E;
                Break;
              end;
          ShowLog('%s plunges a ribbon under %s''s %s and slides it across her nipple', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[AttackSlot].Data.Displayname], ColorLogWillDamage);
          TargetPlayer.HitWill(MarkData.DressedTopWillpowerDamage);
        end else
        begin
          ShowLog('%s slaps %s''s breasts with a ribbon', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogWillDamage);
          TargetPlayer.HitWill(MarkData.NudeTopWillpowerDamage);
        end;
      end else
      begin
        // Attack bottom
        if TargetPlayer.Inventory.BottomCovered > 0 then
        begin
          if (TargetPlayer.Inventory.Equipped[esBottomUnder] <> nil) and TargetPlayer.Inventory.Equipped[esBottomUnder].ItemData.CoversBottom then
            AttackSlot := esBottomUnder
          else
          if (TargetPlayer.Inventory.Equipped[esBottomOver] <> nil) and TargetPlayer.Inventory.Equipped[esBottomOver].ItemData.CoversBottom then
            AttackSlot := esBottomOver
          else
            for E in TargetPlayer.Blueprint.EquipmentSlots do
              if (TargetPlayer.Inventory.Equipped[E] <> nil) and TargetPlayer.Inventory.Equipped[E].ItemData.CoversBottom then
              begin
                AttackSlot := E;
                Break;
              end;
          ShowLog('%s thrusts a ribbon under %s''s %s and slides it around her bottoms', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[AttackSlot].Data.Displayname], ColorLogWillDamage);
          TargetPlayer.HitWill(MarkData.DressedBottomWillpowerDamage);
        end else
        begin
          ShowLog('%s slaps %s''s bottoms with a ribbon', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogWillDamage);
          TargetPlayer.HitWill(MarkData.NudeBottomWillpowerDamage);
        end;
      end;
      Sound(MarkData.HitSound);
      ParentActor.DamageWeapon;
      ViewGame.WakeUp(true, true); // well, was awake already
    end;
  end else
  begin
    if TargetPlayer.Inventory.Equipped[esAmulet] <> nil then
    begin
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(esAmulet, true);
      if ItemStolen.ItemData.UnequipDisintegrates then
      begin
        ShowLog('Angry %s tears %s down, replacing with %s', [ParentActor.Data.DisplayName, ItemStolen.Data.DisplayName, MyRibbon.DisplayName], ColorLogBondage);
        Sound(ItemStolen.ItemData.SoundDisintegrate);
        FreeAndNil(ItemStolen);
      end else
      begin
        ShowLog('%s steals %s, replacing with %s', [ParentActor.Data.DisplayName, ItemStolen.Data.DisplayName, MyRibbon.DisplayName], ColorLogBondage);
        Sound(ItemStolen.ItemData.SoundUnequip);
        ParentMonster.Loot.Add(ItemStolen);
      end;
    end else
      ShowLog('%s quickly wraps %s around %s''s %s', [ParentActor.Data.DisplayName, MyRibbon.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(MyRibbon.EquipSlots)], ColorLogBondage);
    TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MyRibbon));
    Sound(MyRibbon.SoundEquip);
    NewParticle(TargetActor.CenterX, TargetActor.CenterY, 'BOUND', ColorParticlePlayerBound);
    ViewGame.WakeUp(true, true);
  end;
end;

{ TMarkRibbonQuarrelData ----------------------------- }

procedure TMarkRibbonQuarrelData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if DressedTopWillpowerDamage <= 0 then
    raise EDataValidationError.CreateFmt('DressedTopWillpowerDamage %n <= 0 in %s', [DressedTopWillpowerDamage, Self.ClassName]);
  if NudeTopWillpowerDamage <= 0 then
    raise EDataValidationError.CreateFmt('NudeTopWillpowerDamage %n <= 0 in %s', [NudeTopWillpowerDamage, Self.ClassName]);
  if DressedBottomWillpowerDamage <= 0 then
    raise EDataValidationError.CreateFmt('DressedBottomWillpowerDamage %n <= 0 in %s', [DressedBottomWillpowerDamage, Self.ClassName]);
  if NudeBottomWillpowerDamage <= 0 then
    raise EDataValidationError.CreateFmt('NudeBottomWillpowerDamage %n <= 0 in %s', [NudeBottomWillpowerDamage, Self.ClassName]);
end;

procedure TMarkRibbonQuarrelData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  DressedTopWillpowerDamage := Element.AttributeSingle('DressedTopWillpowerDamage');
  NudeTopWillpowerDamage := Element.AttributeSingle('NudeTopWillpowerDamage');
  DressedBottomWillpowerDamage := Element.AttributeSingle('DressedBottomWillpowerDamage');
  NudeBottomWillpowerDamage := Element.AttributeSingle('NudeBottomWillpowerDamage');
  Ribbons := TItemsDataList.Create(false);
  Ribbons.Add(ItemsDataDictionary['neckerchief_black'] as TItemData);
  Ribbons.Add(ItemsDataDictionary['neckerchief_blue'] as TItemData);
  Ribbons.Add(ItemsDataDictionary['neckerchief_brown'] as TItemData);
  Ribbons.Add(ItemsDataDictionary['neckerchief_cyan'] as TItemData);
  Ribbons.Add(ItemsDataDictionary['neckerchief_green'] as TItemData);
  Ribbons.Add(ItemsDataDictionary['neckerchief_purple'] as TItemData);
  Ribbons.Add(ItemsDataDictionary['neckerchief_red'] as TItemData);
  Ribbons.Add(ItemsDataDictionary['neckerchief_white'] as TItemData);
  Ribbons.Add(ItemsDataDictionary['neckerchief_yellow'] as TItemData);
end;

function TMarkRibbonQuarrelData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Prefers own color (one of 9) and will equip the appropriately colored neckerchief on the player character''s neck stealing or destroying anything that prevents from doing so. If the heroine is already wearing a neckerchief of the prefered color - will attack dealing high willpower damage against dressed target and lower willpower damage against a nude target.', []),
    'TMarkRibbonQuarrel', 1));
  Result.Add(
    NewEntryText(
      Format('Will reward a heroine sleeping with correct color of the neckerchief with a potentially useful item.', []),
    'TMarkRibbonQuarrel_sleep', 1));
end;

function TMarkRibbonQuarrelData.Mark: TMarkClass;
begin
  Exit(TMarkRibbonQuarrel);
end;

destructor TMarkRibbonQuarrelData.Destroy;
begin
  FreeAndNil(Ribbons);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMarkRibbonQuarrel);
  RegisterSerializableData(TMarkRibbonQuarrelData);

end.

