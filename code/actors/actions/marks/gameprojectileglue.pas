{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileGlue;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TProjectileGlue = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
  end;

  TProjectileGlueData = class(TProjectileAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public const
    NudeFailHealthDamage = 3;
    FullyGluedStaminaDamage = 10;
  public
    Damage: Single;
    HitSound: String;
  public
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  Math,
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameItemData, GameItemsDatabase, GamePlayerCharacter, GameApparelSlots, GameParticle, GameInventoryItem,
  GameViewGame, GameLog, GameColors, GameSounds,
  GameStats,
  GameActionGlueBeetle;

{$IFDEF SafeActorTypecast}
{$DEFINE MarkData:=(Data as TProjectileGlueData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileGlueData(Data)}
{$ENDIF}

procedure TProjectileGlue.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileGlue.HitTarget(const TargetsHit: array of TActor);
var
  E: TApparelSlot;
  GlueItem: TItemData;
  TargetPlayer: TPlayerCharacter;
  NextGlue: String;
  TotalDurability: Single;
begin
  TargetPlayer := TargetsHit[0] as TPlayerCharacter;
  ViewGame.WakeUp(true, true);
  LocalStats.IncStat(Data.ClassName);
  Sound(MarkData.HitSound);

  NextGlue := TActionGlueBeetle.NextGlue(TargetPlayer.Inventory);
  if NextGlue <> '*CAPTURE*' then
  begin
    GlueItem := ItemsDataDictionary[NextGlue] as TItemData;
    for E in GlueItem.EquipSlots do
      if TargetPlayer.Inventory.Equipped[E] <> nil then
        if not TActionGlueBeetle.IsGlue(TargetPlayer.Inventory, E) then // This may happen if the previous projectile glued the last slot, but the next projectile hits while the next slot is occupied by clothes
        begin
          ShowLog('%s is hit by a glue blob, but luckily it doesn''t adhese', [TargetPlayer.Data.DisplayName], ColorLogAbsorbDamage);
          TargetPlayer.HitHealth(MarkData.NudeFailHealthDamage);
          Exit;
        end;
    TotalDurability := 0;
    for E in GlueItem.EquipSlots do
      if TargetPlayer.Inventory.Equipped[E] <> nil then
      begin
        TotalDurability += TargetPlayer.Inventory.Equipped[E].Durability;
        TargetPlayer.Inventory.UnequipAndDisintegrate(E, false);
      end;
    TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(GlueItem));
    TargetPlayer.Inventory.Equipped[GlueItem.MainSlot].Durability := Max(TotalDurability * 1.1, TargetPlayer.Inventory.Equipped[GlueItem.MainSlot].Durability);
    ShowLog('Glue splashes over %s''s %s', [TargetPlayer.Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(GlueItem.EquipSlots)], ColorLogBondage);
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
  end else
  begin
    // this can happen, though normally shouldn't, glue beetle should switch to capture action here but some projectiles can still be flying
    ShowLog('%s can barely hold balance as the weight of glue increases', [TargetPlayer.Data.DisplayName], ColorLogBondage);
    TargetPlayer.Inventory.ReinforceItem(esWeapon);
    TargetPlayer.HitStamina(MarkData.FullyGluedStaminaDamage);
  end;
end;

{ TProjectileGlueData ----------------------------- }

function TProjectileGlueData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileGlueData.Validate;
begin
  inherited Validate;
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage <= 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TProjectileGlueData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Damage := Element.AttributeSingle('Damage');
  HitSound := Element.AttributeString('HitSound');
end;

function TProjectileGlueData.Description: TEntriesList;
begin
  Result := inherited Description;
  raise Exception.Create('TProjectileGlueData.Description not implemented');
end;

function TProjectileGlueData.Mark: TMarkClass;
begin
  Exit(TProjectileGlue);
end;

initialization
  RegisterSerializableObject(TProjectileGlue);
  RegisterSerializableData(TProjectileGlueData);

end.

