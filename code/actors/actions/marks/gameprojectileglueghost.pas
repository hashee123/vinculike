{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileGlueGhost;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TProjectileGlueGhost = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
  end;

  TProjectileGlueGhostData = class(TProjectileAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Damage: Single;
    HitSound: String;
  public
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  Math,
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameItemData, GameItemsDatabase, GamePlayerCharacter, GameApparelSlots, GameParticle, GameInventoryItem,
  GameViewGame, GameLog, GameColors, GameSounds,
  GameStats, GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE MarkData:=(Data as TProjectileGlueGhostData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileGlueGhostData(Data)}
{$ENDIF}

procedure TProjectileGlueGhost.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileGlueGhost.HitTarget(const TargetsHit: array of TActor);
var
  TargetPlayer: TPlayerCharacter;
  NewItem: TItemData;
begin
  TargetPlayer := TargetsHit[0] as TPlayerCharacter;
  ViewGame.WakeUp(true, true);
  LocalStats.IncStat(Data.ClassName);
  Sound(MarkData.HitSound);

  if TargetPlayer.Inventory.Nude then
  begin
    if TargetPlayer.Inventory.Apparel[esWeapon] = nil then
    begin
      NewItem := ItemsDataDictionary['ectoplasm_hands'] as TItemData;
      Sound(NewItem.SoundEquip);
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(NewItem));
      ShowLog('Some glue-like glowing goo sticks to %s''s hands', [TargetPlayer.Data.DisplayName], ColorLogBondage);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
    end else
    if not TargetPlayer.Inventory.Equipped[esWeapon].ItemData.IsBondage then
    begin
      ShowLog('%s is knocked off %s''s hands', [TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogItemSteal);
      TargetPlayer.Inventory.UnequipAndDrop(esWeapon, true);
    end else
    if TargetPlayer.Inventory.Apparel[esGlasses] = nil then
    begin
      NewItem := ItemsDataDictionary['ectoplasm_eyes'] as TItemData;
      Sound(NewItem.SoundEquip);
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(NewItem));
      ShowLog('Some glue-like glowing goo splashes over %s''s forehead', [TargetPlayer.Data.DisplayName], ColorLogBondage);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
    end else
    if not TargetPlayer.Inventory.Equipped[esGlasses].ItemData.IsBondage then
    begin
      ShowLog('%s is knocked off %s''s face', [TargetPlayer.Inventory.Equipped[esGlasses].Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogItemSteal);
      TargetPlayer.Inventory.UnequipAndDrop(esGlasses, true);
    end else
      TargetPlayer.Hit(MarkData.Damage);
  end else
    TargetPlayer.Hit(MarkData.Damage);
end;

{ TProjectileGlueGhostData ----------------------------- }

function TProjectileGlueGhostData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileGlueGhostData.Validate;
begin
  inherited Validate;
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage <= 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TProjectileGlueGhostData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Damage := Element.AttributeSingle('Damage');
  HitSound := Element.AttributeString('HitSound');
end;

function TProjectileGlueGhostData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: If the target is fully nude will disarm and knock off glasses and apply ecto glue on hands and eyes. Otherwise will do %.1n points of damage.', [Damage]),
      Classname, 1)
  );
end;

function TProjectileGlueGhostData.Mark: TMarkClass;
begin
  Exit(TProjectileGlueGhost);
end;

initialization
  RegisterSerializableObject(TProjectileGlueGhost);
  RegisterSerializableData(TProjectileGlueGhostData);

end.

