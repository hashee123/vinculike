{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkTickleTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameApparelSlots,
  GameUnlockableEntry;

type
  TMarkTickleTarget = class(TMarkTargetAbstract)
  strict private
    procedure StealOrDrop(const E: TApparelSlot; const Forced: Boolean);
    procedure InteractWithSleepingTarget;
  protected
    procedure Perform; override;
  end;

  TMarkTickleTargetData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    StaminaDamageBondage: Single;
    StaminaDamageBondageFurther: Single;
    WillpowerDamageBondageFurther: Single;
    StaminaDamageNude: Single;
    WillpowerDamageNude: Single;
    EquipMittensChance: Single;
    StripFurtherDragChance: Single;
    StripDragChance: Single;
    BondageDragChance: Single;
    NudeDragChance: Single;
    StealItems: Boolean;
    PickUpItems: Boolean;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXMLUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation, GameSounds,
  GameViewGame, GameParticle, GameItemsDatabase,
  GameMap, GameMapItem,
  GameInventoryItem, GamePlayerCharacter, GameActor, GameMonster, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE MarkData:=(Data as TMarkTickleTargetData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE MarkData:=TMarkTickleTargetData(Data)}
{$ENDIF}

procedure TMarkTickleTarget.StealOrDrop(const E: TApparelSlot;
  const Forced: Boolean);
var
  ItemStolen: TInventoryItem;
begin
  if MarkData.StealItems then
  begin
    ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(E, Forced);
    Sound(ItemStolen.ItemData.SoundUnequip);
    ParentMonster.Loot.Add(ItemStolen);
  end else
    UnequipAndDrop(E, Forced);
end;

procedure TMarkTickleTarget.InteractWithSleepingTarget;
var
  E: TApparelSlot;
  HadWeapon: Boolean;
  HadClothes: Boolean;
begin
  HadWeapon := (TargetPlayer.Inventory.Equipped[esWeapon] <> nil) and (not TargetPlayer.Inventory.Equipped[esWeapon].ItemData.IsBondage);
  HadClothes := (TargetPlayer.Inventory.BottomCoveredRemovable > 0) or (TargetPlayer.Inventory.TopCoveredRemovable > 0) or ((TargetPlayer.Inventory.Equipped[esFeet] <> nil) and not TargetPlayer.Inventory.Equipped[esFeet].ItemData.IsBondage);

  if HadWeapon and HadClothes then
    ShowLog(GetTranslation('ActorTickleStripAndDisarmSleeping'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleStrip);
  if not HadWeapon and HadClothes then
    ShowLog(GetTranslation('ActorTickleStripSleeping'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleStrip);
  if HadWeapon and not HadClothes then
    ShowLog(GetTranslation('ActorTickleDisarmSleeping'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogTickleStrip);

  for E in TargetPlayer.Blueprint.EquipmentSlots do
    if (TargetPlayer.Inventory.Equipped[E] <> nil) and not TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
      StealOrDrop(E, false); // Note: not forced strip, will not generate "disrobing" events

  LocalStats.IncStat('TMarkTickleTarget_sleeping');
  if not HadWeapon and not HadClothes then
  begin
    LocalStats.IncStat('TMarkTickleTarget_sleepingbondage');
    if TargetPlayer.Inventory.CanUseHands then
    begin
      ShowLog(GetTranslation('ActorTickleFullBondageSleeping1'), [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
      ShowLog(GetTranslation('ActorTickleFullBondageSleeping2'), [], ColorLogTickleBondage);
      TargetPlayer.Inventory.DisintegrateOrDrop(esWeapon, false); // make sure hands are not bound
      TargetPlayer.Inventory.DisintegrateOrDrop(esFeet, false); // make sure feet are not bound
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_mittens']));
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_feet_mittens']));
      Sound(TargetPlayer.Inventory.Equipped[esWeapon].ItemData.SoundEquip);
      //TODO: feet mittens!
      //TODO: special for replace bondage with mittens
    end else
    begin
      ShowLog('For a moment contemplating helpless %s sound asleep', [TargetActor.Data.DisplayName], ColorLogTickleBondage);
      ShowLog('First of all %s makes sure that %s are locked in tightly', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Inventory.ReinforceItem(esWeapon);
      Sound(TargetPlayer.Inventory.Equipped[esWeapon].ItemData.SoundRepair);
      if (TargetPlayer.Inventory.Equipped[esFeet] <> nil) and TargetPlayer.Inventory.HasItem('bondage_feet_mittens') then
      begin
        ShowLog('And checks if feet mittens are secure and bells ring loudly', [], ColorLogTickleBondage);
        TargetPlayer.Inventory.ReinforceItem(esFeet);
        // no sound
      end else
      if TargetPlayer.Inventory.Equipped[esFeet] = nil then // equip feet mittens only if feet are not already restrained
      begin
        ShowLog('And zips on a pair of new feet mittens with golden bells', [], ColorLogTickleBondage);
        TargetPlayer.Inventory.UnequipAndDisintegrate(esFeet, false); // make sure feet are not bound
        TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_feet_mittens']));
        // no sound
      end;
    end;
  end;
  if not HadWeapon and HadClothes then
  begin
    LocalStats.IncStat('TMarkTickleTarget_sleepingbondage');
    if TargetPlayer.Inventory.CanUseHands then
    begin
      if TargetPlayer.Inventory.Equipped[esWeapon] = nil then
      begin
        // hands were empty
        ShowLog(GetTranslation('ActorTickleHandsBondageSleeping1'), [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
        ShowLog(GetTranslation('ActorTickleHandsBondageSleeping2'), [], ColorLogTickleBondage);
      end else
      begin
        // here we know it was a bondage item
        ShowLog('%s is more than happy to see %s''s hands restrained', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleBondage);
        ShowLog('But bondage mittens are more appropriate for peaceful entertainment', [], ColorLogTickleBondage);
        TargetPlayer.Inventory.DisintegrateOrDrop(esWeapon, false); // make sure hands are not bound
      end;
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_mittens']));
      Sound(TargetPlayer.Inventory.Equipped[esWeapon].ItemData.SoundEquip);
    end else
    begin
      // hands were immobilized
      ShowLog('%s''s restrained hands is a very good start', [TargetActor.Data.DisplayName], ColorLogTickleBondage);
      ShowLog('%s only makes sure %s won''t accidentally slip off', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Inventory.ReinforceItem(esWeapon);
      Sound(TargetPlayer.Inventory.Equipped[esWeapon].ItemData.SoundRepair);
    end;
  end;
  if HadWeapon and not HadClothes then
  begin
    LocalStats.IncStat('TMarkTickleTarget_sleepingbondage');
    LocalStats.IncStat(Data.ClassName);
    if (TargetPlayer.Inventory.Equipped[esFeet] <> nil) and TargetPlayer.Inventory.HasItem('bondage_feet_mittens') then
    begin
      // there were feet mittens on feet
      ShowLog('It is good that %s likes her feet mittens and decided to keep them on', [TargetActor.Data.DisplayName], ColorLogTickleBondage);
      ShowLog('%s only makes sure they won''t accidentally slip off', [ParentActor.Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Inventory.ReinforceItem(esFeet);
      Sound(TargetPlayer.Inventory.Equipped[esFeet].ItemData.SoundEquip);
    end else
    begin
      // feet were nil or restrained with other restrain types
      ShowLog(GetTranslation('ActorTickleFeetBondageSleeping1'), [], ColorLogTickleBondage);
      ShowLog(GetTranslation('ActorTickleFeetBondageSleeping2'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Inventory.DisintegrateOrDrop(esFeet, false); // make sure feet are not bound
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_feet_mittens']));
      Sound(TargetPlayer.Inventory.Equipped[esFeet].ItemData.SoundEquip);
    end;
  end;

  ShowLog(GetTranslation('ActorTickleStripSleepingNext'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickle);
end;

procedure TMarkTickleTarget.Perform;
var
  E, E2: TApparelSlot;
  ChanceToDrag: Single;
  I: Integer;
  MapItem: TMapItem;
  PickUpCount: Integer;
begin
  if TargetPlayer.Unsuspecting then
    InteractWithSleepingTarget;
  // and proceed as normal

  E := TargetPlayer.Inventory.GetRandomEquipmentSlot;
  ChanceToDrag := -1;
  LocalStats.IncStat('TMarkTickleTarget');
  if (TargetPlayer.Inventory.Equipped[E] <> nil) then
  begin
    if TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
    begin
      ShowLog(GetTranslation('ActorTickleBondage'), [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E)], ColorLogTickleBondage);
      TargetPlayer.HitStamina(MarkData.StaminaDamageBondage);
      // go for another round
      repeat
        E2 := TargetPlayer.Inventory.GetRandomEquipmentSlot;
      until TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E2) <> TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E); // TODO something more useful here
      if (TargetPlayer.Inventory.Equipped[E2] <> nil) and not TargetPlayer.Inventory.Equipped[E2].ItemData.IsBondage then
      begin
        if E2 = esWeapon then
        begin
          ShowLog(GetTranslation('ActorTickleDisarmFurther'), [TargetPlayer.Inventory.Equipped[E2].Data.DisplayName], ColorLogTickleStrip);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'DISARM', ColorParticleTickleStrip);
        end else
        begin
          ShowLog(GetTranslation('ActorTickleStripFurther'), [TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E2].Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E2)], ColorLogTickleStrip);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STRIP', ColorParticleTickleStrip);
        end;
        StealOrDrop(E2, true);
        ChanceToDrag := MarkData.StripFurtherDragChance;
      end else
      begin
        if (E2 = esWeapon) and (TargetPlayer.Inventory.Equipped[E2] = nil) and ((Rnd.Random < 0.5) or TargetActor.Immobilized) then
        begin
          ShowLog(GetTranslation('ActorTickleForceBondageHandsFurther'), [TargetActor.Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E2), ParentActor.Data.DisplayName], ColorLogTickleBondage);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
          TargetPlayer.PlaySurpriseSound;
          TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_mittens']));
          Sound(TargetPlayer.Inventory.Equipped[E2].ItemData.SoundEquip);
          ChanceToDrag := 0;
        end else
        if (E2 = esFeet) and (TargetPlayer.Inventory.Equipped[E2] = nil) and ((Rnd.Random < 0.5) or TargetActor.Immobilized) then
        begin
          ShowLog(GetTranslation('ActorTickleForceBondageFeetFurther'), [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E2)], ColorLogTickleBondage);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
          TargetPlayer.PlaySurpriseSound;
          TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_feet_mittens']));
          Sound(TargetPlayer.Inventory.Equipped[E2].ItemData.SoundEquip);
          ChanceToDrag := 0;
        end else
        begin
          TargetPlayer.PlayGiggleSound;
          ShowLog(GetTranslation('ActorTickleNakedFurther'), [TargetActor.Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E2)], ColorLogTickle);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'TICKLE', ColorParticleTickle);
          TargetPlayer.HitStamina(MarkData.StaminaDamageBondageFurther);
          TargetPlayer.HitWill(MarkData.WillpowerDamageBondageFurther);
          ChanceToDrag := MarkData.BondageDragChance;
          ViewGame.WakeUp(true, true);
        end;
      end;
    end else
    begin
      if E = esWeapon then
      begin
        if MarkData.StealItems then
          ShowLog('%s goes for %s''s armpits and as she drops guard pulls %s from her hands', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E)], ColorLogTickleStrip)
        else
          ShowLog(GetTranslation('ActorTickleDisarm'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E)], ColorLogTickleStrip);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'DISARM', ColorParticleTickleStrip);
      end else
      begin
        if MarkData.StealItems then
          ShowLog('%s skillfully stels %s''s %s from her %s', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E)], ColorLogTickleStrip)
        else
          ShowLog(GetTranslation('ActorTickleStrip'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E)], ColorLogTickleStrip);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STRIP', ColorParticleTickleStrip);
      end;
      StealOrDrop(E, true);
      ChanceToDrag := MarkData.StripDragChance;
    end;
  end else
  begin // tickle nude
    TargetPlayer.PlayGiggleSound;
    ShowLog(GetTranslation('ActorTickleNaked'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E)], ColorLogTickle);
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'TICKLE', ColorParticleTickle);
    TargetPlayer.HitStamina(MarkData.StaminaDamageNude);
    TargetPlayer.HitWill(MarkData.WillpowerDamageNude);
    ChanceToDrag := MarkData.NudeDragChance;
    if (E = esAmulet) and ((Rnd.Random < Sqr(TargetPlayer.ResistMultiplier)) or (not TargetPlayer.Inventory.CanUseHands) or TargetActor.Immobilized) then
    begin
      LocalStats.IncStat('TMarkTickleTarget_bondage');
      ShowLog('As a souvenir for the fun time %s gets an amulet with loud golden bells', [TargetActor.Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['amulet_bells']));
      Sound(TargetPlayer.Inventory.Equipped[E].ItemData.SoundEquip);
    end;
    ViewGame.WakeUp(true, true);
    // if target is exhausted - try equipping bondage items
    if (TargetPlayer.Stamina <= 0) or (Rnd.Random > Sqrt(Sqrt(TargetPlayer.ResistMultiplier))) or TargetActor.Immobilized then // sqrtsqrt=16% chance for 12-20% chance at 50% stamina (sqrtsqrtsqrt=9%)
    begin
      LocalStats.IncStat('TMarkTickleTarget_bondage');
      repeat
        if Rnd.RandomBoolean then
          E2 := esWeapon
        else
          E2 := esFeet;
      until E2 <> E;
      if (E2 = esWeapon) and (TargetPlayer.Inventory.Equipped[E2] = nil) and ((Rnd.Random < MarkData.EquipMittensChance) or TargetActor.Immobilized) then
      begin
        LocalStats.IncStat('TMarkTickleTarget_bondage');
        ShowLog(GetTranslation('ActorTickleForceBondageHandsExhausted'), [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
        TargetPlayer.PlaySurpriseSound;
        TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_mittens']));
        Sound(TargetPlayer.Inventory.Equipped[E2].ItemData.SoundEquip);
        ChanceToDrag := 0;
      end else
      if (E2 = esFeet) and (TargetPlayer.Inventory.Equipped[E2] = nil) and ((Rnd.Random < MarkData.EquipMittensChance) or TargetActor.Immobilized) then
      begin
        LocalStats.IncStat('TMarkTickleTarget_bondage');
        ShowLog(GetTranslation('ActorTickleForceBondageFeetExhausted'), [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
        TargetPlayer.PlaySurpriseSound;
        TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_feet_mittens']));
        Sound(TargetPlayer.Inventory.Equipped[E2].ItemData.SoundEquip);
        ChanceToDrag := 0;
      end;
    end;
  end;
  if ChanceToDrag < 0 then
    raise Exception.Create('ChanceToDrag was not set');
  if (Rnd.Random < ChanceToDrag) and (TargetPlayer.PassableTiles[ParentActor.LastTile]) then
  begin
    LocalStats.IncStat('TMarkTickleTarget_drag');
    ShowLog(GetTranslation('ActorTickleDrag'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickle);
    //TargetPlayer.ActionFinished;
    TargetPlayer.PlayGruntSound;
    TargetPlayer.Teleport(ParentActor.LastTileX, ParentActor.LastTileY);
  end;

  if MarkData.PickUpItems then
  begin
    I := 0;
    PickUpCount := 0;
    while I < Map.MapItemsList.Count do
    begin
      if Map.MapItemsList[I].Collides(ParentActor, 1) then
      begin
        MapItem := Map.MapItemsList.ExtractIndex(I);
        ParentMonster.Loot.Add(MapItem.Item);
        MapItem.Item := nil;
        FreeAndNil(MapItem);
        Inc(PickUpCount);
      end else
        Inc(I);
    end;
    if PickUpCount = 1 then
      ShowLog('%s sees an item on the ground and picks it up', [ParentActor.Data.DisplayName], ColorLogItemSteal)
    else
    if PickUpCount > 1 then
      ShowLog('%s doesn''t endorse littering and picks up items lying around', [ParentActor.Data.DisplayName], ColorLogItemSteal);
  end;
end;

{ TMarkTickleTargetData ----------------------------- }

procedure TMarkTickleTargetData.Validate;
begin
  inherited Validate;
  if StaminaDamageBondage <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamageBondage %n <= 0 in %s', [StaminaDamageBondage, Self.ClassName]);
  if StaminaDamageBondageFurther <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamageBondageFurther %n <= 0 in %s', [StaminaDamageBondageFurther, Self.ClassName]);
  if WillpowerDamageBondageFurther <= 0 then
    raise EDataValidationError.CreateFmt('WillpowerDamageBondageFurther %n <= 0 in %s', [WillpowerDamageBondageFurther, Self.ClassName]);
  if StaminaDamageNude <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamageNude %n <= 0 in %s', [StaminaDamageNude, Self.ClassName]);
  if WillpowerDamageNude <= 0 then
    raise EDataValidationError.CreateFmt('WillpowerDamageNude %n <= 0 in %s', [WillpowerDamageNude, Self.ClassName]);
  if EquipMittensChance <= 0 then
    raise EDataValidationError.CreateFmt('EquipMittensChance %n <= 0 in %s', [EquipMittensChance, Self.ClassName]);
  if StripFurtherDragChance < 0 then
    raise EDataValidationError.CreateFmt('StripFurtherDragChance %n < 0 in %s', [StripFurtherDragChance, Self.ClassName]);
  if StripDragChance < 0 then
    raise EDataValidationError.CreateFmt('StripDragChance %n < 0 in %s', [StripDragChance, Self.ClassName]);
  if BondageDragChance < 0 then
    raise EDataValidationError.CreateFmt('BondageDragChance %n < 0 in %s', [BondageDragChance, Self.ClassName]);
  if NudeDragChance < 0 then
    raise EDataValidationError.CreateFmt('NudeDragChance %n < 0 in %s', [NudeDragChance, Self.ClassName]);
end;

procedure TMarkTickleTargetData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  StaminaDamageBondage := Element.AttributeSingle('StaminaDamageBondage');
  StaminaDamageBondageFurther := Element.AttributeSingle('StaminaDamageBondageFurther');
  WillpowerDamageBondageFurther := Element.AttributeSingle('WillpowerDamageBondageFurther');
  StaminaDamageNude := Element.AttributeSingle('StaminaDamageNude');
  WillpowerDamageNude := Element.AttributeSingle('WillpowerDamageNude');
  EquipMittensChance := Element.AttributeSingle('EquipMittensChance');
  StripFurtherDragChance := Element.AttributeSingle('StripFurtherDragChance');
  StripDragChance := Element.AttributeSingle('StripDragChance');
  BondageDragChance := Element.AttributeSingle('BondageDragChance');
  NudeDragChance := Element.AttributeSingle('NudeDragChance');
  StealItems := Element.AttributeBoolean('StealItems');
  PickUpItems := Element.AttributeBoolean('PickUpItems');
end;

function TMarkTickleTargetData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will attempt to undress the target, and if hits a nude spot will tickle it causing high stamina and minor willpower damage. Picking an inventory slot with restraints in it will increase the damage done and give a small chance to the attacker to equip restraints too.', []),
    'TMarkTickleTarget', 1));
  Result.Add(
    NewEntryText(
      Format('From time to time after a successful attack can also drag the target closer and force stop any action.', []),
    'TMarkTickleTarget_drag', 1));
  Result.Add(
    NewEntryText(
      Format('If the target is caught by other restraints or is exhausted the attacker can equip an amulet of bells (extremely noisy item), bondage mittens with golden bells (prevent target from fighting effectively and do a lot of noise) and bondage feet mittens (slow down movement significantly and also do a lot of noise)', []),
    'TMarkTickleTarget_bondage', 1));
  Result.Add(
    NewEntryText(
      Format('SLEEP ATTACK: The attacker will very happinly take advantage of a sleeping target to prepare the character for more fun before waking up. This included both disarming and undressing the target stark naked.', []),
    'TMarkTickleTarget_sleeping', 1));
  Result.Add(
    NewEntryText(
      Format('If the target was sleeping nude, the spare time will be used to apply bondage feet mittens, and if the target was sleeping unarmed - bondage hands mittens will be sure to take their righteous place. If some useful restraints were already in place the attacker will be happy to reinforce them to prevent easy escape.', []),
    'TMarkTickleTarget_sleepingbondage', 1));
end;

function TMarkTickleTargetData.Mark: TMarkClass;
begin
  Exit(TMarkTickleTarget);
end;

initialization
  RegisterSerializableObject(TMarkTickleTarget);
  RegisterSerializableData(TMarkTickleTargetData);

end.

