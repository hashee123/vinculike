{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkPullClothes;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameMarkAbstract, GameApparelSlots,
  GameUnlockableEntry;

type
  TMarkPullClothes = class(TMarkTargetAbstract)
  strict private const
    PullableClothes = [esBottomUnder, esBottomOver, esTopUnder, esTopOver];
  strict private
    procedure TeleportToTarget;
  protected
    procedure Perform; override;
    procedure Miss; override;
  end;

  TMarkPullClothesData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public const
    DragChance = 0.7;
    TeleportToTargetChanceAfterAttackHit = 0.7;
    TeleportToTargetChanceAfterAttackMiss = 0.4;
    MaxTeleportRange = 29;
  public
    HitSound: String;
    DamageDressed: Single; // warning most often will get x5 multiplier
    DamageNude: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  Math,
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation, GameSounds,
  GameViewGame, GameParticle, GameMap, GameMapTypes, GameActor,
  GamePlayerCharacter, GameMonster, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE MarkData:=(Data as TMarkPullClothesData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE MarkData:=TMarkPullClothesData(Data)}
{$ENDIF}

procedure TMarkPullClothes.Perform;
var
  E: TApparelSlot;
  DamageDone: Single;
  ItemNameCache: String;
  CanDrag: Boolean;

  function TargetHasPullableClothes: Boolean;
  var
    E: TApparelSlot;
  begin
    for E in PullableClothes do
      if TargetPlayer.Inventory.Equipped[E] <> nil then
        Exit(true);
    Exit(false);
  end;

  function GetRandomPullableSlot: TApparelSlot;
  begin
    repeat
      Result := TApparelSlot(Rnd.Random(Ord(High(TApparelSlot)) + 1));
    until (Result in PullableClothes) and (TargetPlayer.Inventory.Equipped[Result] <> nil);
  end;

begin
  LocalStats.IncStat(Data.ClassName);

  if TargetPlayer.Unsuspecting then
  begin
    LocalStats.IncStat(Data.ClassName + '_sleeping');
    ShowLog(GetTranslation('ActorClothesPullSleeping'), [TargetActor.Data.DisplayName], ColorLogItemSteal);
    ParentMonster.Ai.AiFlee := true;
    Exit; // to avoid all subsequent interactions (careful!)
  end;

  if TargetHasPullableClothes then
  begin
    E := GetRandomPullableSlot;
    ItemNameCache := TargetPlayer.Inventory.Equipped[E].Data.DisplayName; // TargetPlayer.Equipped[E] can become nil if item will be broken by attack
    Sound(MarkData.HitSound);
    CanDrag := false;
    DamageDone := Min(MarkData.DamageDressed, TargetPlayer.Inventory.Equipped[E].Durability);
    // bug: item damage log comes before "our" log, but we don't know beforehands if the item will break
    TargetPlayer.Inventory.DamageItem(E, DamageDone); // it can destroy bondage items too
    if TargetPlayer.Inventory.Equipped[E] <> nil then
    begin
      // if the item didn't break - do full damage
      DamageDone := DamageDone * 5;
      CanDrag := true;
    end;
    ShowLog(GetTranslation('ActorClothesPull'), [ParentActor.Data.DisplayName, ItemNameCache, TargetActor.Data.DisplayName, DamageDone], ColorLogItemSteal);
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, Round(DamageDone).ToString, ColorParticlePlayerHurt);
    TargetPlayer.DegradeHealth(DamageDone);
    if CanDrag then
    begin
      if (Rnd.Random < MarkData.DragChance) and (TargetPlayer.PassableTiles[ParentActor.LastTile]) then
      begin
        ShowLog(GetTranslation('ActorTickleDrag'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickle);
        //TargetPlayer.ActionFinished;
        Sound('female_grunt');
        TargetPlayer.Teleport(ParentActor.LastTileX, ParentActor.LastTileY);
      end else
      if Rnd.Random < MarkData.TeleportToTargetChanceAfterAttackHit then
        TeleportToTarget;
    end;
  end else
  begin
    TargetActor.Hit(MarkData.DamageNude);
    Sound(MarkData.HitSound);
    ParentActor.DamageWeapon;
  end;
  ViewGame.WakeUp(true, true);
end;

procedure TMarkPullClothes.Miss;
begin
  inherited Miss;
  if Rnd.Random < MarkData.TeleportToTargetChanceAfterAttackMiss then
    TeleportToTarget;
end;

procedure TMarkPullClothes.TeleportToTarget;
var
  Dist: TDistanceMapArray;
  ToX, ToY: Int16;
begin
  Dist := Map.DistanceMap(Target);
  repeat // TODO: Optimize
    ToX := Rnd.Random(Map.SizeX);
    ToY := Rnd.Random(Map.SizeY);
  until (Dist[ToX + Map.SizeX * ToY] > 5) and (Dist[ToX + Map.SizeX * ToY] <= MarkData.MaxTeleportRange) and Map.PassableTiles[ParentActor.PredSize][ToX + Map.SizeX * ToY];
  ShowLog(GetTranslation('ActorTeleportCloser'), [ParentActor.Data.DisplayName], ColorLogTickle);
  ParentActor.MoveMeTo(ToX, ToY);
end;

{ TMarkPullClothesData ----------------------------- }

procedure TMarkPullClothesData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if DamageDressed <= 0 then
    raise EDataValidationError.CreateFmt('DamageDressed %n <= 0 in %s', [DamageDressed, Self.ClassName]);
  if DamageNude <= 0 then
    raise EDataValidationError.CreateFmt('DamageNude %n <= 0 in %s', [DamageNude, Self.ClassName]);
end;

procedure TMarkPullClothesData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  DamageDressed := Element.AttributeSingle('DamageDressed');
  DamageNude := Element.AttributeSingle('DamageNude');
end;

function TMarkPullClothesData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will pull an article of clothes or restraint dealing high damage directly to character''s health. If the clothes break in the process, the damage to health will be reduced.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('Will not attack sleeping character.', []),
    Classname + '_sleeping', 1));
  Result.Add(
    NewEntryText(
      Format('Pretty harmless against a nude target.', []),
    Classname, 1));
end;

function TMarkPullClothesData.Mark: TMarkClass;
begin
  Exit(TMarkPullClothes);
end;

initialization
  RegisterSerializableObject(TMarkPullClothes);
  RegisterSerializableData(TMarkPullClothesData);

end.

