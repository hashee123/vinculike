{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkStealFromTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkStealFromTarget = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkStealFromTargetData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    Damage: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation,
  GameViewGame, GameParticle, GameApparelSlots, GameActor, GameSounds,
  GameInventoryItem, GamePlayerCharacter, GameMonster, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE MarkData:=(Data as TMarkStealFromTargetData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE MarkData:=TMarkStealFromTargetData(Data)}
{$ENDIF}

procedure TMarkStealFromTarget.Perform;
var
  E: TApparelSlot;
  ItemStolen: TInventoryItem;
  WasCoveredTop, WasCoveredBottom: Boolean;
begin
  LocalStats.IncStat('TMarkStealFromTarget');
  if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
  begin
    if (TargetPlayer.Inventory.TopCoveredRemovable > 0) and (TargetPlayer.Inventory.TopCoveredRemovable <= TargetPlayer.Inventory.BottomCoveredRemovable) then
      repeat
        E := TargetPlayer.Inventory.GetRandomClothesSlotEquippedItem;
      until TargetPlayer.Inventory.Equipped[E].ItemData.CoversTop
    else
    if TargetPlayer.Inventory.BottomCoveredRemovable > 0 then
      repeat
        E := TargetPlayer.Inventory.GetRandomClothesSlotEquippedItem;
      until TargetPlayer.Inventory.Equipped[E].ItemData.CoversBottom
    else
      E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem;

    WasCoveredTop := TargetPlayer.Inventory.TopCoveredRemovable > 0;
    WasCoveredBottom := TargetPlayer.Inventory.BottomCoveredRemovable > 0;
    if TargetPlayer.Unsuspecting then
    begin
      LocalStats.IncStat('TMarkStealFromTarget_sleeping');
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(E, false);
      Sound(ItemStolen.ItemData.SoundUnequip);
      ShowLog(GetTranslation('ActorItemStolenSleeping'), [ParentActor.Data.DisplayName, ItemStolen.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogItemSteal);
    end else
    begin
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(E, true);
      ShowLog(GetTranslation('ActorItemStolen'), [ParentActor.Data.DisplayName, ItemStolen.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogItemSteal);
      Sound(MarkData.HitSound);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STEAL', ColorParticleItemStolen);
    end;
    ParentMonster.Loot.Add(ItemStolen);

    if Rnd.Random < (4 + ParentMonster.Loot.Count) / (TargetPlayer.Inventory.EquippedSlotsRemovable + 5) then  // Warning Loot.Count needs rework when monsters will have special loot or can pick up items, this is only stolen items count
      ParentMonster.Ai.AiFlee := true;
    //TargetPlayer.Inventory.UpdateCoveredStatus; // to make sure to recalculate "new" covered status after the item was stolen; note that we may call one more in UnequipAndReturn if forced=true --- now always calculated
    if WasCoveredTop and (TargetPlayer.Inventory.TopCovered = 0) then // note, here we do not use Removable - bondage items also count
      ParentMonster.Ai.AiFlee := true;
    if WasCoveredBottom and (TargetPlayer.Inventory.BottomCovered = 0) then
      ParentMonster.Ai.AiFlee := true;
  end else
  begin
    if TargetPlayer.Unsuspecting then
      ParentMonster.Ai.AiFlee := true // if player is sleeping - let sleep; note: no log message
    else
    begin
      TargetActor.Hit(MarkData.Damage);
      Sound(MarkData.HitSound);
      ParentActor.DamageWeapon;
      ViewGame.WakeUp(true, true);
    end;
  end;
end;

{ TMarkStealFromTargetData ----------------------------- }

procedure TMarkStealFromTargetData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage %n <= 0 in %s', [Damage, Self.ClassName]);
end;

procedure TMarkStealFromTargetData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  Damage := Element.AttributeSingle('Damage');
end;

function TMarkStealFromTargetData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will try to snatch an item or few from the target and run away. Find and kill the thief to recover the stolen loot. Will prioritize clothes covering breasts, then bottoms, then everything else that can be unequipped.', []),
    'TMarkStealFromTarget', 1));
  Result.Add(
    NewEntryText(
      Format('If finds the unsuspecting target sleeping, will stealthily steal some items and retreat.', []),
    'TMarkStealFromTarget_sleeping', 1));
end;

function TMarkStealFromTargetData.Mark: TMarkClass;
begin
  Exit(TMarkStealFromTarget);
end;

initialization
  RegisterSerializableObject(TMarkStealFromTarget);
  RegisterSerializableData(TMarkStealFromTargetData);

end.

