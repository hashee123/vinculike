{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkBindTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameItemDataAbstract,
  GameUnlockableEntry;

type
  TMarkBindTarget = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  protected
    procedure InteractAllTiedUp; virtual;
  end;

  TMarkBindTargetData = class(TMarkAbstractData)
  public const
    ImmobilizedStruggleDamage = 200;
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    ClothesDamage: Single;
    BondageItemsList: TItemsDataAbstractList;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils, CastleStringUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation, GameSounds, GameApparelSlots,
  GameViewGame, GameViewEndGame, GameParticle, GameActionIdle,
  GameItemData, GameItemsDatabase, GameInventoryItem, GamePlayerCharacter, GameActor, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE MarkData:=(Data as TMarkBindTargetData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE MarkData:=TMarkBindTargetData(Data)}
{$ENDIF}

procedure TMarkBindTarget.Perform;
var
  I: Integer;
  ItemData: TItemData;
  ItemDamage: Single;
  ResistBindChance: Single;
  BondageItemsList: TItemsDataAbstractList;

  function AllTiedUp: Boolean;
  var
    E: TApparelSlot;
  begin
    for E in [esWeapon, esFeet, esBottomUnder, esBottomOver, esTopUnder, esTopOver] do
      if (TargetPlayer.Inventory.Equipped[E] = nil) or (not TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage) then
        Exit(false);
    Exit(true);
  end;

begin
  Sound(MarkData.HitSound);
  if AllTiedUp then
  begin
    InteractAllTiedUp;
    Exit;
  end;

  BondageItemsList := TItemsDataAbstractList.Create(false);
  for I := 0 to Pred(MarkData.BondageItemsList.Count) do
    if (TargetPlayer.Inventory.Equipped[MarkData.BondageItemsList[I].MainSlot] = nil) or // naked slot
      not TargetPlayer.Inventory.Equipped[MarkData.BondageItemsList[I].MainSlot].ItemData.IsBondage or // will try to undress
      (
         MarkData.BondageItemsList.Contains(TargetPlayer.Inventory.Equipped[MarkData.BondageItemsList[I].MainSlot].Data) and
         TargetPlayer.Inventory.ShouldMaximizeDurability(MarkData.BondageItemsList[I].MainSlot)
      ) // will try to reinforce
      then
        BondageItemsList.Add(MarkData.BondageItemsList[I]);

  if BondageItemsList.Count = 0 then
  begin
    ShowError('%s cannot apply any bondage to %s, all slots are locked/reinforced', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName]);
    FreeAndNil(BondageItemsList);
    Exit;
  end;

  ItemData := BondageItemsList[Rnd.Random(BondageItemsList.Count)] as TItemData;
  FreeAndNil(BondageItemsList);

  LocalStats.IncStat('TMarkBindTarget');
  if TargetPlayer.Inventory.Equipped[ItemData.MainSlot] <> nil then
  begin
    if TargetPlayer.Inventory.Equipped[ItemData.MainSlot].ItemData.IsBondage then
    begin
      TargetPlayer.Inventory.ReinforceItem(ItemData.MainSlot);
      if TargetActor.Immobilized then
        TargetPlayer.Inventory.ReinforceItem(ItemData.MainSlot); // reinforce twice if immobilized
      LocalStats.IncStat('TMarkBindTarget_reinforce');
      ShowLog(GetTranslation('ActorBondageReinforced'), [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(ItemData.EquipSlots)], ColorLogBondage);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
    end else
    begin
      ItemDamage := MarkData.ClothesDamage;
      ResistBindChance := ItemDamage / TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Durability;
      ShowLog(GetTranslation('ActorBondageDamageItem'), [TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Data.DisplayName, ItemDamage], ColorLogBondageItemSave);
      TargetPlayer.Inventory.DamageItem(ItemData.MainSlot, ItemDamage);
      LocalStats.IncStat('TMarkBindTarget_clothes');
      if TargetPlayer.Inventory.Equipped[ItemData.MainSlot] <> nil then // item resisted
      begin
        ShowLog(GetTranslation('ActorBondageItemSave'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(ItemData.EquipSlots)], ColorLogBondageItemSave);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'EVADE', ColorParticleItemSave);
      end else // item broke or wardrobe malfunction
      begin
        if Rnd.Random > ResistBindChance then
        begin
          ShowLog(GetTranslation('ActorBondageItemBreakSave'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(ItemData.EquipSlots)], ColorLogBondageItemSave);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'EVADE', ColorParticleItemSave);
        end else
        begin
          ShowLog(GetTranslation('ActorBondageItemBreakFail'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(ItemData.EquipSlots)], ColorLogBondage);
          ViewGame.ShakeCharacter;
          TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemData));
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
          TargetPlayer.PlaySurpriseSound;
        end;
      end;
    end;
  end else
  begin
    ShowLog(GetTranslation('ActorBondageApplied'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(ItemData.EquipSlots)], ColorLogBondage);
    ViewGame.ShakeCharacter;
    TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemData));
    if TargetActor.Immobilized then
    begin
      LocalStats.IncStat('TMarkBindTarget_sleeping');
      TargetPlayer.Inventory.ReinforceItem(ItemData.MainSlot); // reinforce twice if immobilized
      ShowLog('As %s cannot resist %s takes time to make sure knots are secure and tight', [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogBondage);
    end;
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
    TargetPlayer.PlaySurpriseSound;
  end;

  ViewGame.WakeUp(true, true);

  if AllTiedUp then
    ShowLog('%s is barely mobile', [TargetActor.Data.DisplayName], ColorLogBondage);
end;

procedure TMarkBindTarget.InteractAllTiedUp;
begin
  LocalStats.IncStat(ClassName + '_alltiedup');
  if (TargetPlayer.Stamina > 0) and not TargetPlayer.Immobilized then
  begin
    ShowLog('%s easily catches almost completely restrained %s with a lasso', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
    ShowLog('After a short desperate struggle she still manages to wriggle out', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
    ViewGame.ShakeCharacter;
    TargetPlayer.HitStamina(MarkData.ImmobilizedStruggleDamage);
    TargetPlayer.CurrentAction := TActionIdle.Create;
    TargetPlayer.CurrentAction.Start;
  end else
  begin
    ShowLog(GetTranslation('ActorBondageImmobilized'), [TargetActor.Data.DisplayName], ColorLogDefeat);
    TargetPlayer.GetCaptured(egTiedUp);
  end;
end;

{ TMarkBindTargetData ----------------------------- }

procedure TMarkBindTargetData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if ClothesDamage <= 0 then
    raise EDataValidationError.CreateFmt('ClothesDamage %n <= 0 in %s', [ClothesDamage, Self.ClassName]);
  if BondageItemsList.Count = 0 then
    raise EDataValidationError.CreateFmt('%s has no bondage items', [Self.ClassName]);
end;

procedure TMarkBindTargetData.Read(const Element: TDOMElement);
var
  SlotStringList: TCastleStringList;
  S: String;
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  ClothesDamage := Element.AttributeSingle('ClothesDamage');

  BondageItemsList := TItemsDataAbstractList.Create(false);
  SlotStringList := CreateTokens(Element.AttributeString('BondageItemsList'), [',']);
  for S in SlotStringList do
    if ItemsDataDictionary.ContainsKey(S) then
      BondageItemsList.Add(ItemsDataDictionary[S])
    else
      raise Exception.CreateFmt('Item "%s" not found for action %s', [S, Self.ClassName]);
  FreeAndNil(SlotStringList);
end;

function TMarkBindTargetData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will attempt to restrain the target with ropes.', []),
    'TMarkBindTarget', 1));
  Result.Add(
    NewEntryText(
      Format('Equipped items can potentially protect against such attack but will themselves get damaged by %.1n and maybe broken in the process.', [ClothesDamage]),
    'TMarkBindTarget_clothes', 1));
  Result.Add(
    NewEntryText(
      Format('If the target is already wearing rope harness it can get reinforced.', []),
    'TMarkBindTarget_reinforce', 1));
  Result.Add(
    NewEntryText(
      Format('The knots will get a much higher durability if the target is immobilized.', []),
    'TMarkBindTarget_sleeping', 1));
  Result.Add(
    NewEntryText(
      Format('If the target is all tied up in different kinds of restraints, struggling against more ropes will cause devastating stamina damage and when the target finally runs ouf ot stamina to resist, will get completely immobilized and captured.', []),
    ClassName + '_alltiedup', 1));
end;

function TMarkBindTargetData.Mark: TMarkClass;
begin
  Exit(TMarkBindTarget);
end;

destructor TMarkBindTargetData.Destroy;
begin
  FreeAndNil(BondageItemsList);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMarkBindTarget);
  RegisterSerializableData(TMarkBindTargetData);

end.

