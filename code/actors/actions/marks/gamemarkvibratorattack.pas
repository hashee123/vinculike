{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkVibratorAttack;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkVibratorAttack = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkVibratorAttackData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    HealthHealingSleepNaked: Single;
    WillHealingSleepNaked: Single;
    StaminaDamageSleepNaked: Single;
    CrotchDamageResonate: Single;
    CrotchStunDuration: Single;
    CrotchChanceToEquipVibrator: Single;
    CrotchDamageNaked: Single;
    CrotchDamageClothed: Single;
    BreastsDamageResonate: Single;
    BreastsStunDuration: Single;
    BreastsChanceToEquipVibrator: Single;
    BreastsDamageNaked: Single;
    BreastsDamageClothed: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameStats, GameSounds, GamePlayerCharacter, GameLog, GameColors, GameActor,
  GameActionPlayerStunned, GameRandom, GameMonster, GameParticle,
  GameItemData, GameItemsDatabase, GameApparelSlots, GameInventoryItem,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE MarkData:=(Data as TMarkVibratorAttackData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE MarkData:=TMarkVibratorAttackData(Data)}
{$ENDIF}

procedure TMarkVibratorAttack.Perform;
var
  VibratorBottom, VibratorTop: TItemData;
  E: TApparelSlot;
begin
  VibratorBottom := ItemsDataDictionary['vibrator_small_bottom'] as TItemData;
  VibratorTop := ItemsDataDictionary['vibrator_small_top'] as TItemData;

  // Attack sleeping target
  if TargetPlayer.Unsuspecting then
  begin
    LocalStats.IncStat(Data.ClassName + '_sleep');
    if TargetPlayer.Inventory.BottomCovered = 0 then
    begin
      ShowLog('%s has an intense erotic dream', [TargetPlayer.Data.DisplayName], ColorLogTickle);
      ShowLog('Leavingher a bit exhausted and slightly... wet... down there', [], ColorLogTickle);
      TargetPlayer.HealMaxHealth(MarkData.HealthHealingSleepNaked);
      TargetPlayer.HealMaxWill(MarkData.WillHealingSleepNaked);
      TargetPlayer.HitStamina(MarkData.StaminaDamageSleepNaked);
    end else
    if (TargetPlayer.Inventory.BottomCoveredRemovable < TargetPlayer.Inventory.BottomCovered) or ((TargetPlayer.Inventory.Equipped[esBottomUnder] <> nil) and (TargetPlayer.Inventory.Equipped[esBottomUnder].ItemData.IsBondage)) then
    begin
      ShowLog('%s has an strange erotic dream... or was it a nightmare?', [TargetPlayer.Data.DisplayName], ColorLogTickle);
      ShowLog('If she could remember it later, it would feel really weird', [], ColorLogTickle);
      // nothing else happens
    end else
    begin
      ShowLog('%s finds %s sound asleep and even the noisy monster didn''t wake her up', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTickle);
      ShowLog('She''ll get a surprise gift for her when she wakes up', [], ColorLogTickle);
      TargetPlayer.Inventory.UnequipAndDrop(esBottomUnder, false);
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(VibratorBottom));
    end;
    ParentMonster.Ai.AiFlee := true;
    Exit;
  end;

  // Attack target in catsuit
  if TargetPlayer.Inventory.HasItem('catsuit_tight_full') then
  begin
    E := ItemsDataDictionary['catsuit_tight_full'].MainSlot;
    if (Rnd.Random < 0.2) or TargetPlayer.Immobilized then
    begin
      ShowLog('%s catches the zipper on %s''s %s and breaks it', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogTickleStrip);
      TargetPlayer.Inventory.DamageItem(E, 1000);
      Exit;
    end else
      ShowLog('%s tries to catch and pull the zipper on %s''s %s but fails', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogBondageItemSave);
  end else
  if TargetPlayer.Inventory.HasItem('catsuit_tight_half') then
  begin
    E := ItemsDataDictionary['catsuit_tight_half'].MainSlot;
    if (Rnd.Random < 0.2) or TargetPlayer.Immobilized then
    begin
      ShowLog('%s catches the zipper on %s''s %s and breaks it', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogTickleStrip);
      TargetPlayer.Inventory.DamageItem(E, 1000);
      Exit;
    end else
      ShowLog('%s tries to catch and pull the zipper on %s''s %s but fails', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogBondageItemSave);
  end;
  if TargetPlayer.Inventory.HasItem('catsuit_loose_full') then
  begin
    E := ItemsDataDictionary['catsuit_loose_full'].MainSlot;
    if (Rnd.Random < 0.2) or TargetPlayer.Immobilized then
    begin
      ShowLog('%s catches the drawstring on %s''s %s and it snapps', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogTickleStrip);
      TargetPlayer.Inventory.DamageItem(E, 1000);
      Exit;
    end else
      ShowLog('%s tries to catch and pull the drawstring on %s''s %s but fails', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogBondageItemSave);
  end;

  // Attack immobilized target
  if TargetPlayer.Immobilized then
  begin
    // gets additional attack to strip the one of the covering items
    if Rnd.RandomBoolean then
    begin
      for E in TargetPlayer.Blueprint.EquipmentSlots do
        if (TargetPlayer.Inventory.Equipped[E] <> nil) and (not TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage) and TargetPlayer.Inventory.Equipped[E].ItemData.CoversTop then
        begin
          ShowLog('%s makes use of %s''s inability to resist pulls off her %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogTickleStrip);
          TargetPlayer.Inventory.UnequipAndDrop(E, true);
          break;
        end;
    end else
    begin
      for E in TargetPlayer.Blueprint.EquipmentSlots do
        if (TargetPlayer.Inventory.Equipped[E] <> nil) and (not TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage) and TargetPlayer.Inventory.Equipped[E].ItemData.CoversBottom then
        begin
          ShowLog('%s makes use of %s''s inability to resist pulls off her %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogTickleStrip);
          TargetPlayer.Inventory.UnequipAndDrop(E, true);
          break;
        end;
    end;
  end;

  // Attack normal
  LocalStats.IncStat(Data.ClassName);
  if Rnd.RandomBoolean then
  begin
    if TargetPlayer.Inventory.BottomCovered = 0 then
    begin
      if TargetPlayer.Inventory.HasItem(VibratorBottom) and not TargetPlayer.Immobilized then
      begin
        LocalStats.IncStat(Data.ClassName + '_vibrator');
        ShowLog('%s pushes a buzzing tentacle at %s''s crotch', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
        ShowLog('The vibrations resonate, blacking her out for a moment in ecstasy (%.1n s)', [MarkData.CrotchStunDuration], ColorLogStaminaExhausted);
        TargetPlayer.HitWill(MarkData.CrotchDamageResonate);
        TargetPlayer.CurrentAction := TActionPlayerStunned.NewAction(TargetPlayer, MarkData.CrotchStunDuration);
        TargetPlayer.CurrentAction.Start;
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STUN', ColorParticlePlayerStunned);
        TargetPlayer.PlayGruntSound;
        Sound(MarkData.HitSound);
      end else
      begin
        if (TargetPlayer.Inventory.Equipped[esBottomUnder] = nil) and (Rnd.Random < MarkData.CrotchChanceToEquipVibrator) then
        begin
          TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(VibratorBottom));
          ShowLog('Instead of attacking %s swiftly attaches a buzzing pea vibrator to %s''s bottom', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondage);
          ShowLog('This isn''t going to end well...', [], ColorLogBondage);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
          Sound(VibratorBottom.SoundEquip);
        end else
        begin
          ShowLog('%s presses a vibrating tentacle against %s''s nethers', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTickle);
          TargetPlayer.HitWill(MarkData.CrotchDamageNaked);
          Sound(MarkData.HitSound);
        end;
      end;
    end else
    begin
      ShowLog('%s presses against %s''s clothes without much harm', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondageItemSave);
      Sound(MarkData.HitSound);
      TargetPlayer.HitWill(MarkData.CrotchDamageClothed);
    end;
  end else
  begin
    if TargetPlayer.Inventory.TopCovered = 0 then
    begin
      if TargetPlayer.Inventory.HasItem(VibratorTop) and not TargetPlayer.Immobilized then
      begin
        LocalStats.IncStat(Data.ClassName + '_vibrator');
        ShowLog('%s pushes a buzzing tentacle between %s''s breasts', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
        ShowLog('The vibrations resonate, blacking her out for a moment in ecstasy (%.1n s)', [MarkData.BreastsStunDuration], ColorLogStaminaExhausted);
        TargetPlayer.HitWill(MarkData.BreastsDamageResonate);
        TargetPlayer.CurrentAction := TActionPlayerStunned.NewAction(TargetPlayer, MarkData.BreastsStunDuration);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STUN', ColorParticlePlayerStunned);
        TargetPlayer.CurrentAction.Start;
        TargetPlayer.PlayGruntSound;
        Sound(MarkData.HitSound);
      end else
      begin
        if (TargetPlayer.Inventory.Equipped[esTopUnder] = nil) and (Rnd.Random < MarkData.BreastsChanceToEquipVibrator) then
        begin
          TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(VibratorTop));
          ShowLog('%s skillfully glues a pair of pea vibrators to %s''s nipples', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondage);
          ShowLog('They immediately start pulsating and vibrating.', [], ColorLogBondage);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
          Sound(VibratorTop.SoundEquip);
        end else
        begin
          ShowLog('%s touches %s''s nipple and vibration passes through her chest', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTickle);
          TargetPlayer.HitWill(MarkData.BreastsDamageNaked);
          Sound(MarkData.HitSound);
        end;
      end;
    end else
    begin
      ShowLog('%s presses against %s''s clothes without much harm', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondageItemSave);
      TargetPlayer.HitWill(MarkData.BreastsDamageClothed);
      Sound(MarkData.HitSound);
    end;
  end;
end;

{ TMarkVibratorAttackData ----------------------------- }

procedure TMarkVibratorAttackData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);

  if HealthHealingSleepNaked <= 0 then
    raise EDataValidationError.CreateFmt('HealthHealingSleepNaked %n <= 0 in %s', [HealthHealingSleepNaked, Self.ClassName]);
  if WillHealingSleepNaked <= 0 then
    raise EDataValidationError.CreateFmt('WillHealingSleepNaked %n <= 0 in %s', [WillHealingSleepNaked, Self.ClassName]);
  if StaminaDamageSleepNaked <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamageSleepNaked %n <= 0 in %s', [StaminaDamageSleepNaked, Self.ClassName]);
  if CrotchDamageResonate <= 0 then
    raise EDataValidationError.CreateFmt('CrotchDamageResonate %n <= 0 in %s', [CrotchDamageResonate, Self.ClassName]);
  if CrotchStunDuration <= 0 then
    raise EDataValidationError.CreateFmt('CrotchStunDuration %n <= 0 in %s', [CrotchStunDuration, Self.ClassName]);
  if CrotchChanceToEquipVibrator <= 0 then
    raise EDataValidationError.CreateFmt('CrotchChanceToEquipVibrator %n <= 0 in %s', [CrotchChanceToEquipVibrator, Self.ClassName]);
  if CrotchDamageNaked <= 0 then
    raise EDataValidationError.CreateFmt('CrotchDamageNaked %n <= 0 in %s', [CrotchDamageNaked, Self.ClassName]);
  if CrotchDamageClothed <= 0 then
    raise EDataValidationError.CreateFmt('CrotchDamageClothed %n <= 0 in %s', [CrotchDamageClothed, Self.ClassName]);
  if BreastsDamageResonate <= 0 then
    raise EDataValidationError.CreateFmt('BreastsDamageResonate %n <= 0 in %s', [BreastsDamageResonate, Self.ClassName]);
  if BreastsStunDuration <= 0 then
    raise EDataValidationError.CreateFmt('BreastsStunDuration %n <= 0 in %s', [BreastsStunDuration, Self.ClassName]);
  if BreastsChanceToEquipVibrator <= 0 then
    raise EDataValidationError.CreateFmt('BreastsChanceToEquipVibrator %n <= 0 in %s', [BreastsChanceToEquipVibrator, Self.ClassName]);
  if BreastsDamageNaked <= 0 then
    raise EDataValidationError.CreateFmt('BreastsDamageNaked %n <= 0 in %s', [BreastsDamageNaked, Self.ClassName]);
  if BreastsDamageClothed <= 0 then
    raise EDataValidationError.CreateFmt('BreastsDamageClothed %n <= 0 in %s', [BreastsDamageClothed, Self.ClassName]);
end;

procedure TMarkVibratorAttackData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  HealthHealingSleepNaked := Element.AttributeSingle('HealthHealingSleepNaked');
  WillHealingSleepNaked := Element.AttributeSingle('WillHealingSleepNaked');
  StaminaDamageSleepNaked := Element.AttributeSingle('StaminaDamageSleepNaked');
  CrotchDamageResonate := Element.AttributeSingle('CrotchDamageResonate');
  CrotchStunDuration := Element.AttributeSingle('CrotchStunDuration');
  CrotchChanceToEquipVibrator := Element.AttributeSingle('CrotchChanceToEquipVibrator');
  CrotchDamageNaked := Element.AttributeSingle('CrotchDamageNaked');
  CrotchDamageClothed := Element.AttributeSingle('CrotchDamageClothed');
  BreastsDamageResonate := Element.AttributeSingle('BreastsDamageResonate');
  BreastsStunDuration := Element.AttributeSingle('BreastsStunDuration');
  BreastsChanceToEquipVibrator := Element.AttributeSingle('BreastsChanceToEquipVibrator');
  BreastsDamageNaked := Element.AttributeSingle('BreastsDamageNaked');
  BreastsDamageClothed := Element.AttributeSingle('BreastsDamageClothed');
end;

function TMarkVibratorAttackData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will try to press its vibrating tentacles against vulnerable sensitive bits of the heroine. If the target is dressed - most likely won''t cause any meaningful harm. However, can be catastrophically dangerous for a nude target.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If the character has bare chest or bottoms, will try to attach vibrators there. Vibrators, especially the bottom one, can cause severe willpower damage over time and a lot of other negative effects. What is even worse - the monster will resonate with the equipped vibrators and immobilize the target in ecstasy for a short time. Hopefully the heroine will have her hands free and will be able to remove those quickly, otherwise things can go pretty rough.', []),
    Classname + '_vibrator', 1));
  Result.Add(
    NewEntryText(
      Format('Will offer a small... healing for the target that sleeps with no bottom underwear. Will not interact if bottom underwear is a bondage item. And will remove bottom underwear if one was equipped and replace it with a vibrator.', []),
    Classname + '_sleep', 1));
end;

function TMarkVibratorAttackData.Mark: TMarkClass;
begin
  Exit(TMarkVibratorAttack);
end;

initialization
  RegisterSerializableObject(TMarkVibratorAttack);
  RegisterSerializableData(TMarkVibratorAttackData);

end.

