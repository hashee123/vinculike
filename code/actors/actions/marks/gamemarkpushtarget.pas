{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkPushTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TMarkPushTarget = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkPushTargetData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    Damage: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleVectors, CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GamePlayerCharacter, GameMonster, GameInventoryItem, GameSounds,
  GameActionKnockback, GameParticle,
  GameLog, GameViewGame, GameColors, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE MarkData:=(Data as TMarkPushTargetData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE MarkData:=TMarkPushTargetData(Data)}
{$ENDIF}

procedure TMarkPushTarget.Perform;
var
  ItemStolen: TInventoryItem;
begin
  // steal item
  LocalStats.IncStat(Data.ClassName);
  if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
  begin
    ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem, true);
    ParentMonster.Loot.Add(ItemStolen);
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STEAL', ColorParticleItemStolen);
    ShowLog('%s grabs %s and tosses her away, stealing %s in process', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemSteal);
  end else
  if TargetPlayer.Inventory.PartiallyOrFullyNude then
  begin
    ShowLog('%s squeezes %s touching her private bits and throws her away', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogNaked);
    TargetPlayer.HitWill(MarkData.Damage);
  end else
    ShowLog('%s catches %s by her restraints and sends her flying', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleBondage);
  TargetPlayer.HitStamina(MarkData.Damage);

  // hurt player (=TMarkHurt)
  TargetActor.Hit(MarkData.Damage);
  Sound(MarkData.HitSound);
  ParentActor.DamageWeapon;
  // Push target
  TargetPlayer.CurrentAction := TActionKnockback.NewAction(Target);
  TActionKnockback(TargetPlayer.CurrentAction).MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
  TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized := TActionKnockback(TargetPlayer.CurrentAction).MoveVector.Normalize;
  TActionKnockback(TargetPlayer.CurrentAction).MoveVector := 20 * TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized;
  TargetPlayer.CurrentAction.Start;
  ViewGame.ShakeMap;
end;

{ TMarkPushTargetData ----------------------------- }

procedure TMarkPushTargetData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage %n <= 0 in %s', [Damage, Self.ClassName]);
end;

procedure TMarkPushTargetData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  Damage := Element.AttributeSingle('Damage');
end;

function TMarkPushTargetData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will throw the target around the map, stealing one equipped item in the process and dealing regular damage.', []),
    Classname, 1));
end;

function TMarkPushTargetData.Mark: TMarkClass;
begin
  Exit(TMarkPushTarget);
end;

initialization
  RegisterSerializableObject(TMarkPushTarget);
  RegisterSerializableData(TMarkPushTargetData);

end.

