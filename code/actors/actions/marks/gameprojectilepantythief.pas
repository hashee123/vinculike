{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectilePantyThief;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract, GameActor, GameInventoryItem,
  GameUnlockableEntry;

type
  TProjectilePantyThief = class(TProjectileAbstract)
  strict private
    procedure SpawnMonster(const AItem: TInventoryItem; const ToX, ToY: Single);
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
    procedure Update(SecondsPassed: Single); override;
  end;

  TProjectilePantyThiefData = class(TProjectileAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Damage: Single;
    HitSound: String;
    TickleStaminaDamage: Single;
    TickleWillpowerDamage: Single;
    TickleStunDuration: Single;
  public
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  Math,
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GamePlayerCharacter, GameApparelSlots, GameParticle,
  GameViewGame, GameLog, GameColors, GameSounds, GameRandom,
  GameMonster, GameMonstersDatabase, GameMap, GameMapItem,
  GameActionPlayerStunned,
  GameStats, GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE MarkData:=(Data as TProjectilePantyThiefData)}
{$ELSE}
{$DEFINE MarkData:=TProjectilePantyThiefData(Data)}
{$ENDIF}

procedure TProjectilePantyThief.SpawnMonster(const AItem: TInventoryItem;
  const ToX, ToY: Single);
var
  AMonster: TMonster;
begin
  AMonster := TMonster.Create;
  if (esTopUnder in AItem.Data.EquipSlots) and (esBottomUnder in AItem.Data.EquipSlots) then
    AMonster.Data := MonstersDataDictionary['ghost_pantythief_topbottom']
  else
  if esTopUnder in AItem.Data.EquipSlots then
    AMonster.Data := MonstersDataDictionary['ghost_pantythief_top']
  else
  if esBottomUnder in AItem.Data.EquipSlots then
    AMonster.Data := MonstersDataDictionary['ghost_pantythief_bottom']
  else
  begin
    AMonster.Data := MonstersDataDictionary['ghost_pantythief_bottom'];
    ShowError('Unexpected Situation in TProjectilePantyThief', []);
  end;
  AMonster.Reset;
  AMonster.Teleport(Round(ToX), Round(ToY));
  AMonster.Ai.Guard := false;
  AMonster.Ai.AiFlee := true;
  AMonster.Loot.Add(AItem);
  Map.MonstersList.Add(AMonster);
end;

procedure TProjectilePantyThief.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectilePantyThief.HitTarget(const TargetsHit: array of TActor);
var
  E: TApparelSlot;
  TargetPlayer: TPlayerCharacter;
  BottomUnderwear: Boolean;
  BottomOverwear: Boolean;
  TopUnderwear: Boolean;
  TopOverwear: Boolean;

  procedure StealItem(const ItemSlot: TApparelSlot);
  begin
    if TargetPlayer.Unsuspecting then
    begin
      ShowLog('%s feels awkwardly cold and... comfortable', [TargetPlayer.Data.DisplayName], ColorLogItemSteal);
    end else
    begin
      ShowLog('%s suddenly phases through %s''s body and flies away', [TargetPlayer.Inventory.Equipped[ItemSlot].Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogItemSteal);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STRIP', ColorParticlePlayerBound);
    end;
    SpawnMonster(TargetPlayer.Inventory.UnequipAndReturn(ItemSlot, not TargetPlayer.Unsuspecting), TargetPlayer.CenterX, TargetPlayer.CenterY);
  end;

  procedure DoTickle;
  begin
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'TICKLE', ColorParticleTickle);
    TargetPlayer.PlayGiggleSound;
    TargetPlayer.CurrentAction := TActionPlayerStunned.NewAction(TargetPlayer, MarkData.TickleStunDuration);
    TargetPlayer.CurrentAction.Start;
    TargetPlayer.HitStamina(MarkData.TickleStaminaDamage);
    TargetPlayer.HitWill(MarkData.TickleWillpowerDamage);
    ViewGame.WakeUp(true, true);
  end;

begin
  TargetPlayer := TargetsHit[0] as TPlayerCharacter;
  LocalStats.IncStat(Data.ClassName);
  Sound(MarkData.HitSound);

  BottomUnderwear := (TargetPlayer.Inventory.Apparel[esBottomUnder] <> nil) and (not TargetPlayer.Inventory.Equipped[esBottomUnder].ItemData.IsBondage) and TargetPlayer.Inventory.Equipped[esBottomUnder].ItemData.CoversBottom;
  TopUnderwear := (TargetPlayer.Inventory.Apparel[esTopUnder] <> nil) and (not TargetPlayer.Inventory.Equipped[esTopUnder].ItemData.IsBondage) and TargetPlayer.Inventory.Equipped[esTopUnder].ItemData.CoversTop;
  BottomOverwear := false;
  TopOverwear := false;
  for E in TargetPlayer.Blueprint.ClothesSlots do
    if not (E in [esBottomUnder, esTopUnder]) then
    begin
      if (TargetPlayer.Inventory.Apparel[E] <> nil) and TargetPlayer.Inventory.Equipped[E].ItemData.CoversBottom then
        BottomOverwear := true;
      if (TargetPlayer.Inventory.Apparel[E] <> nil) and TargetPlayer.Inventory.Equipped[E].ItemData.CoversTop then
        TopOverwear := true;
    end;

  if TopUnderwear and not TopOverwear then
    StealItem(esTopUnder)
  else
  if BottomUnderwear and not BottomOverwear then
    StealItem(esBottomUnder)
  else
  begin
    if Rnd.RandomBoolean and (TargetPlayer.Inventory.BottomCovered = 0) then
    begin
      ViewGame.WakeUp(true, true);
      ShowLog('%s feels unbearable tickles on her nipples and is forced to distract (%.1ns)', [TargetPlayer.Data.DisplayName, 0.1], ColorLogTickle);
      DoTickle;
    end else
    if TargetPlayer.Inventory.TopCovered = 0 then
    begin
      ShowLog('%s feels desperately itchy on her groin and can''t help but stagger (%.1ns)', [TargetPlayer.Data.DisplayName, 0.1], ColorLogTickle);
      DoTickle;
    end else
      TargetPlayer.Hit(MarkData.Damage); // will also wake up
  end;
end;

procedure TProjectilePantyThief.Update(SecondsPassed: Single);
var
  M: TMapItem;
  PickUpItem: TMapItem;
begin
  PickUpItem := nil;

  for M in Map.MapItemsList do
    if M.CollidesInt(Self, 1) then
      if M.Item.ItemData.CoversTop or M.Item.ItemData.CoversBottom then
        if not (esTopOver in M.Item.Data.EquipSlots) and not (esBottomOver in M.Item.Data.EquipSlots) then
          if (esTopUnder in M.Item.Data.EquipSlots) or (esBottomUnder in M.Item.Data.EquipSlots) then
          begin
            PickUpItem := M;
            Break;
          end;

  if PickUpItem <> nil then
  begin
    LocalStats.IncStat(Data.ClassName + '_pickup');
    ShowLog('Suddenly %s raises from the ground and flees', [PickUpItem.Item.Data.DisplayName], ColorLogItemSteal);
    SpawnMonster(PickUpItem.Item, PickUpItem.LastTileX, PickUpItem.LastTileY);
    PickUpItem.Item := nil;
    Map.MapItemsList.Remove(PickUpItem);
    PickUpItem := nil;
    HitWall;
    EndAction;
    Exit;
  end else
    inherited; // Warning: this may be not safe, though we don't do anything special down there
end;

{ TProjectilePantyThiefData ----------------------------- }

function TProjectilePantyThiefData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectilePantyThiefData.Validate;
begin
  inherited Validate;
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage <= 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if TickleStaminaDamage < 0 then
    raise EDataValidationError.CreateFmt('TickleStaminaDamage < 0 in %s', [Self.ClassName]);
  if TickleWillpowerDamage < 0 then
    raise EDataValidationError.CreateFmt('TickleWillpowerDamage < 0 in %s', [Self.ClassName]);
  if TickleStunDuration < 0 then
    raise EDataValidationError.CreateFmt('TickleStunDuration < 0 in %s', [Self.ClassName]);
end;

procedure TProjectilePantyThiefData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Damage := Element.AttributeSingle('Damage');
  HitSound := Element.AttributeString('HitSound');
  TickleStaminaDamage := Element.AttributeSingle('TickleStaminaDamage');
  TickleWillpowerDamage := Element.AttributeSingle('TickleWillpowerDamage');
  TickleStunDuration := Element.AttributeSingle('TickleStunDuration');
end;

function TProjectilePantyThiefData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will launch several slow homing projectiles at random angles. If target is hit will attempt to steal underwear (if it covers anything and is not protected by overwear), and if succeeds - the item will try to flee the owner. If target is nude - will tickle causing %.1n stamina damage, %.1n willpower damage and stunning target for %.1n seconds. Eventually if everything else fails will hit target for %.1n damage.', [TickleStaminaDamage, TickleWillpowerDamage, TickleStunDuration, Damage]),
      Classname, 1)
  );
  Result.Add(
    NewEntryText(
      Format('While flying over unerwear items on the floor projectiles may also "pick those up" and make them flee the player character.', [TickleStaminaDamage, TickleWillpowerDamage, TickleStunDuration]),
      Classname + '_pickup', 1)
  );
end;

function TProjectilePantyThiefData.Mark: TMarkClass;
begin
  Exit(TProjectilePantyThief);
end;

initialization
  RegisterSerializableObject(TProjectilePantyThief);
  RegisterSerializableData(TProjectilePantyThiefData);

end.

