{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkUndressAndTeleportTargetVacuumCleaner;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameMarkUndressAndTeleportTarget, GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkUndressAndTeleportTargetVacuumCleaner = class(TMarkUndressAndTeleportTarget)
  protected
    procedure Perform; override;
  end;

  TMarkUndressAndTeleportTargetVacuumCleanerData = class(TMarkUndressAndTeleportTargetData)
  public
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameApparelSlots, GamePlayerCharacter, GameLog, GameColors, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE MarkData:=(Data as TMarkUndressAndTeleportTargetVacuumCleanerData)}
{$ELSE}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE MarkData:=TMarkUndressAndTeleportTargetVacuumCleanerData(Data)}
{$ENDIF}

procedure TMarkUndressAndTeleportTargetVacuumCleaner.Perform;
var
  E: TApparelSlot;
  Count: Integer;
begin
  Count := 0;
  for E in MarkData.DisrobingSlots do
    if (TargetPlayer.Inventory.Equipped[E] <> nil) and not TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
      Inc(Count);
  if Count = 0 then // no clothes to remove - inherited will not unequip anything; warning: keep synchronized
    if (TargetPlayer.Inventory.Equipped[esWeapon] <> nil) and not TargetPlayer.Inventory.Equipped[esWeapon].ItemData.IsBondage then
    begin
      LocalStats.IncStat(Data.ClassName);
      ShowLog('Without any warning %s flies off %s''s hands pulled by invisible force', [TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTeleport);
      TargetPlayer.Inventory.UnequipAndDrop(esWeapon, true);
    end;

  // Below is broken, it's a good "progression" but doesn't play well with inherited
  // TODO: but overall above is enough
  {if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
  begin
    if (TargetPlayer.Inventory.Equipped[esWeapon] <> nil) and not TargetPlayer.Inventory.Equipped[esWeapon].ItemData.IsBondage then
    begin
      LocalStats.IncStat(Data.ClassName);
      ShowLog('Without any warning %s flies off %s''s hands pulled by invisible force', [TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTeleport);
      TargetPlayer.Inventory.UnequipAndDrop(esWeapon, true);
    end else
    begin
      for E in TargetPlayer.Blueprint.EquipmentSlots do
        if (TargetPlayer.Inventory.Apparel[E] <> nil) and not TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
          TargetPlayer.Inventory.UnequipAndDrop(E, true);
      ShowLog('An invisible force drags all items off %s which aren''t attached too tight', [TargetPlayer.Data.DisplayName], ColorLogTeleport);
    end;
  end;}
  inherited;
end;

{ TMarkUndressAndTeleportTargetVacuumCleanerData ----------------------------- }

function TMarkUndressAndTeleportTargetVacuumCleanerData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('If the target doesn''t have any clothes to remove, will also disarm.', []),
    Classname, 1));
end;

function TMarkUndressAndTeleportTargetVacuumCleanerData.Mark: TMarkClass;
begin
  Exit(TMarkUndressAndTeleportTargetVacuumCleaner);
end;

initialization
  RegisterSerializableObject(TMarkUndressAndTeleportTargetVacuumCleaner);
  RegisterSerializableData(TMarkUndressAndTeleportTargetVacuumCleanerData);

end.

