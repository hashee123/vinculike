{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileWeb;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TProjectileWeb = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
  end;

  TProjectileWebData = class(TProjectileAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Damage: Single;
  public
    HitSound: String;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameItemData, GameItemsDatabase, GamePlayerCharacter, GameParticle, GameInventoryItem,
  GameViewGame, GameLog, GameColors, GameRandom, GameTranslation, GameSounds, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE MarkData:=(Data as TProjectileWebData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileWebData(Data)}
{$ENDIF}

procedure TProjectileWeb.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileWeb.HitTarget(const TargetsHit: array of TActor);
var
  I: Integer;
  ItemData: TItemData;
  ActionBondageItemsList: TItemsDataList;
  BondageItemsList: TItemsDataList;
  TargetPlayer: TPlayerCharacter;
begin
  TargetPlayer := TargetsHit[0] as TPlayerCharacter;
  ViewGame.WakeUp(true, true);

  Sound(MarkData.HitSound);

  ActionBondageItemsList := TItemsDataList.Create(false);
  ActionBondageItemsList.Add(ItemsDataDictionary['webbing_bottom_over'] as TItemData);
  ActionBondageItemsList.Add(ItemsDataDictionary['webbing_bottom_under'] as TItemData);
  ActionBondageItemsList.Add(ItemsDataDictionary['webbing_top_over'] as TItemData);
  ActionBondageItemsList.Add(ItemsDataDictionary['webbing_top_overover'] as TItemData);
  ActionBondageItemsList.Add(ItemsDataDictionary['webbing_top_under'] as TItemData);
  if ActionBondageItemsList.Count = 0 then
  begin
    ShowError('could not find bondage items to apply to %s', [TargetPlayer.Data.DisplayName]);
    Exit;
  end;

  BondageItemsList := TItemsDataList.Create(false);
  for I := 0 to Pred(ActionBondageItemsList.Count) do
    if (TargetPlayer.Inventory.Equipped[ActionBondageItemsList[I].MainSlot] = nil) or // naked slot
      not TargetPlayer.Inventory.Equipped[ActionBondageItemsList[I].MainSlot].ItemData.IsBondage or // will try to undress
      (
         ActionBondageItemsList.Contains(TargetPlayer.Inventory.Equipped[ActionBondageItemsList[I].MainSlot].ItemData)
      ) // will try to reinforce
      then
        BondageItemsList.Add(ActionBondageItemsList[I]);
  FreeAndNil(ActionBondageItemsList);

  if BondageItemsList.Count > 0 then
  begin
    ItemData := BondageItemsList[Rnd.Random(BondageItemsList.Count)];

    if TargetPlayer.Inventory.Equipped[ItemData.MainSlot] <> nil then
    begin
      if TargetPlayer.Inventory.Equipped[ItemData.MainSlot].ItemData.IsBondage then // we guarantee it's web, selected above
      begin
        LocalStats.IncStat(Data.ClassName + '_reinforce');
        TargetPlayer.Inventory.Equipped[ItemData.MainSlot].MaxDurability := TargetPlayer.Inventory.Equipped[ItemData.MainSlot].MaxDurability * 1.15 + Sqrt(Rnd.Random) * ItemData.Durability;
        TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Durability := (TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Durability + TargetPlayer.Inventory.Equipped[ItemData.MainSlot].MaxDurability) / 2;
        ShowLog('Another layer of web is wrapped around %s''s %s', [TargetPlayer.Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(ItemData.EquipSlots)], ColorLogBondage);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
      end else
      begin
        LocalStats.IncStat(Data.ClassName + '_clothes');
        ShowLog('A bolt of web hits %s for %.1n', [TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Data.DisplayName, MarkData.Damage], ColorLogAbsorbDamage);
        TargetPlayer.Inventory.DamageItem(ItemData.MainSlot, MarkData.Damage);
      end;
    end else
    begin
      LocalStats.IncStat(Data.ClassName);
      ShowLog('Web sticks to %s''s %s', [TargetPlayer.Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(ItemData.EquipSlots)], ColorLogBondage);
      ViewGame.ShakeCharacter;
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemData));
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
      TargetPlayer.PlaySurpriseSound;
    end;
  end else // if there are no slots to apply web
    TargetPlayer.Hit(MarkData.Damage);

  FreeAndNil(BondageItemsList);
end;

{ TProjectileWebData ----------------------------- }

function TProjectileWebData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileWebData.Validate;
begin
  inherited Validate;
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage <= 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TProjectileWebData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Damage := Element.AttributeSingle('Damage');
  HitSound := Element.AttributeString('HitSound');
end;

function TProjectileWebData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will try to wrap the target in web if hits.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('Multiple layers of web will add up progressively making it much harder to remove.', []),
    Classname + '_reinforce', 1));
  Result.Add(
    NewEntryText(
      Format('Web projectiles don''t do much against a well dressed target.', []),
    Classname + '_clothes', 1));
end;

function TProjectileWebData.Mark: TMarkClass;
begin
  Exit(TProjectileWeb);
end;

initialization
  RegisterSerializableObject(TProjectileWeb);
  RegisterSerializableData(TProjectileWebData);

end.

