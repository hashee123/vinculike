{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkDamageInventory;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameApparelSlots, GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkDamageInventory = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkDamageInventoryData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public const
    ItemDamagePercent = Single(0.3);
    OneMinusBodyDamagePercent = Single(0.9); // 46% for fully nude
    AttackSlots = [esTopOver, esTopUnder, esBottomOver, esBottomUnder, esWeapon, esTopOverOver];
  public
    HitSound: String;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  Math,
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameSounds,
  GameViewGame,
  GamePlayerCharacter,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE MarkData:=(Data as TMarkDamageInventoryData)}
{$ELSE}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE MarkData:=TMarkDamageInventoryData(Data)}
{$ENDIF}

procedure TMarkDamageInventory.Perform;
var
  E: TApparelSlot;
  OneMinusBodyDamage: Single;
  ItemDamage: Single;
  Count: Integer;
begin
  OneMinusBodyDamage := 1.0;
  Count := 0;
  Sound(MarkData.HitSound);
  for E in MarkData.AttackSlots do
    if TargetPlayer.Inventory.Equipped[E] = nil then
      OneMinusBodyDamage *= MarkData.OneMinusBodyDamagePercent
    else
    if TargetPlayer.Inventory.Equipped[E].ItemData.MainSlot = E then // Bondage items included!
    begin
      Inc(Count);
      ItemDamage := TargetPlayer.Inventory.Equipped[E].Durability * MarkData.ItemDamagePercent;
      ShowLog('%s is damaged by %.1n', [TargetPlayer.Inventory.Equipped[E].Data.DisplayName, ItemDamage], ColorLogAbsorbDamage);
      TargetPlayer.Inventory.DamageItem(E, ItemDamage);
    end;
  ViewGame.ShakeCharacter;
  if Count > 0 then
  begin
    if OneMinusBodyDamage < 1.0 then
    begin
      ShowLog('A dozen of sandpaper stripes slip across %s''s body damaging items and hurting her', [TargetPlayer.Data.DisplayName], ColorLogTeleport);
      TargetPlayer.HitHealth(TargetPlayer.Health * (1 - OneMinusBodyDamage));
    end else
      ShowLog('Thin belts of sandpaper slide along %s''s items', [TargetPlayer.Data.DisplayName], ColorLogTeleport);
  end else
  begin
    ShowLog('Thin bands of sandpaper wrap around %s hurting her skin as she struggles out', [TargetPlayer.Data.DisplayName], ColorLogStaminaDamage);
    TargetPlayer.HitHealth(TargetPlayer.Health * (1 - OneMinusBodyDamage));
  end;
end;

{ TMarkDamageInventoryData ----------------------------- }

procedure TMarkDamageInventoryData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TMarkDamageInventoryData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
end;

function TMarkDamageInventoryData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Damages most large equipped items for %d%% of their durability. If no item is equipped in the slot, will hurt skin instead, %d%% of health for each empty slot.', [Round(100 * ItemDamagePercent), Round(100 * (1 - OneMinusBodyDamagePercent))]),
    Classname, 1));
end;

function TMarkDamageInventoryData.Mark: TMarkClass;
begin
  Exit(TMarkDamageInventory);
end;

initialization
  RegisterSerializableObject(TMarkDamageInventory);
  RegisterSerializableData(TMarkDamageInventoryData);

end.

