{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkManacles;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkManacles = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkManaclesData = class(TMarkAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
  public
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameSounds, GameMap,
  GameViewGame, GameParticle,
  GameItemData, GameItemsDatabase, GameInventoryItem, GameMapItem,
  GamePlayerCharacter,
  GameStats, GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$ENDIF}

procedure TMarkManacles.Perform;
var
  ItemStolen: TInventoryItem;
  Wristbands: TItemData;
  Anklets: TItemData;
  EmptyWrists, EmptyAnkles: Boolean;
  UnequipWrists, UnequipAnkles: Boolean;
  TX, TY: Int16;
begin
  LocalStats.IncStat(Data.ClassName);

  Wristbands := ItemsDataDictionary['nude_wristbands'] as TItemData;
  Anklets := ItemsDataDictionary['nude_anklets'] as TItemData;
  UnequipWrists := (TargetPlayer.Inventory.Equipped[Wristbands.MainSlot] <> nil) and not TargetPlayer.Inventory.Equipped[Wristbands.MainSlot].ItemData.IsBondage;
  UnequipAnkles := (TargetPlayer.Inventory.Equipped[Anklets.MainSlot] <> nil) and not TargetPlayer.Inventory.Equipped[Anklets.MainSlot].ItemData.IsBondage;
  EmptyWrists := TargetPlayer.Inventory.Equipped[Wristbands.MainSlot] = nil;
  EmptyAnkles := TargetPlayer.Inventory.Equipped[Anklets.MainSlot] = nil;

  if not (EmptyWrists or UnequipWrists or EmptyAnkles or UnequipAnkles) then
  begin
    if TargetPlayer.Unsuspecting then
      ShowLog('%s hears some strange sound through her dream but nothing seems to happen', [TargetPlayer.Data.DisplayName], ColorLogItemSteal)
    else
      ShowLog('%s feels weird, but nothing seems to happen', [TargetPlayer.Data.DisplayName], ColorLogItemSteal);
  end else
  if TargetPlayer.Unsuspecting and TargetPlayer.Inventory.Nude then
  begin
    ShowLog('%s feels some soft tingling on her ankles and wrists but it doesn''t wake her up', [TargetPlayer.Data.DisplayName], ColorLogItemSteal)
  end else
  if UnequipWrists and (not UnequipAnkles or Rnd.RandomBoolean) and (not EmptyAnkles or Rnd.RandomBoolean) then
  begin
    ViewGame.WakeUp(true, true);
    ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(Wristbands.MainSlot, true);
    ShowLog('Before %s can react, %s disappears from her %s', [TargetPlayer.Data.DisplayName, ItemStolen.Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(ItemStolen.Data.EquipSlots)], ColorLogItemSteal);
    repeat
      TX := Rnd.Random(Map.SizeX);
      TY := Rnd.Random(Map.SizeY);
    until Map.CanMove(TX + Map.SizeX * TY);
    TMapItem.DropItem(TX, TY, ItemStolen, false);
  end else
  if UnequipAnkles and (not EmptyWrists or Rnd.RandomBoolean) then
  begin
    ViewGame.WakeUp(true, true);
    ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(Anklets.MainSlot, true);
    ShowLog('Before %s can react, %s disappears from her %s', [TargetPlayer.Data.DisplayName, ItemStolen.Data.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(ItemStolen.Data.EquipSlots)], ColorLogItemSteal);
    repeat
      TX := Rnd.Random(Map.SizeX);
      TY := Rnd.Random(Map.SizeY);
    until Map.CanMove(TX + Map.SizeX * TY);
    TMapItem.DropItem(TX, TY, ItemStolen, false);
  end else
  if EmptyWrists and (not EmptyAnkles or Rnd.RandomBoolean) then
  begin
    ViewGame.WakeUp(true, true);
    ShowLog('%s hears a soft click and sees %s locked around her %s', [TargetPlayer.Data.DisplayName, Wristbands.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(Wristbands.EquipSlots)], ColorLogBondage);
    TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(Wristbands));
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
    TargetPlayer.PlaySurpriseSound;
    Sound(Wristbands.SoundEquip);
    ViewGame.ShakeMap;
  end else
  if EmptyAnkles then
  begin
    ViewGame.WakeUp(true, true);
    ShowLog('%s hears a soft click and sees %s locked around her %s', [TargetPlayer.Data.DisplayName, Anklets.DisplayName, TargetPlayer.Blueprint.EquipSlotsToHumanReadableString(Anklets.EquipSlots)], ColorLogBondage);
    TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(Anklets));
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
    TargetPlayer.PlaySurpriseSound;
    Sound(Anklets.SoundEquip);
    ViewGame.ShakeMap;
  end else
    raise Exception.Create('Unexpected situation in TMarkManacles.Perform');
end;

{ TMarkManaclesData ----------------------------- }

function TMarkManaclesData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

function TMarkManaclesData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('Will unequip items in "wrists" (currently: shoulders) and "ankles" (currently: feet) slots and teleport them to a random spot on the map. If the slot is empty, will equip Nude Anklets or Nude Wristbands into it.', []),
    Classname, 1))
end;

function TMarkManaclesData.Mark: TMarkClass;
begin
  Exit(TMarkManacles);
end;

initialization
  RegisterSerializableObject(TMarkManacles);
  RegisterSerializableData(TMarkManaclesData);

end.

