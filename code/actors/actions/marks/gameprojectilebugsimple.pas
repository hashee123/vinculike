{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileBugSimple;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUtils,
  GameApparelSlots, GameProjectileAbstract, GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TProjectileBugSimple = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
  end;

  TProjectileBugSimpleData = class(TProjectileAbstractData)
  public const
    // TargetSlots = [esFeet, esBottomOver, esBottomUnder, esTopUnder, esTopOver, esTopOverOver]; // needs reworking the Perform a bit
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    StaminaDamage: Single;
    WillDamage: Single;
    StaminaDamageNude: Single;
    WillDamageNude: Single;
    StaminaDamageBondage: Single;
    WillDamageBondage: Single;
    HitSound: String;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameInventoryItem, GameMapItem, GamePlayerCharacter,
  GameSounds,
  GameViewGame, GameLog, GameColors, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE MarkData:=(Data as TProjectileBugSimpleData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileBugSimpleData(Data)}
{$ENDIF}

procedure TProjectileBugSimple.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileBugSimple.HitTarget(const TargetsHit: array of TActor);
var
  ItemStolen: TInventoryItem;
  E: TApparelSlot;
  TargetPlayer: TPlayerCharacter;
begin
  TargetPlayer := TargetsHit[0] as TPlayerCharacter;
  ViewGame.WakeUp(true, true);
  if TargetPlayer.Immobilized then
  begin
    LocalStats.IncStat('TProjectileBugSimple_immobilized');
    ShowLog('Bugs crawl all over immobilized %s and she can only desperately gasp in panic', [TargetPlayer.Data.DisplayName], ColorLogItemBreak);
    TargetPlayer.HitWill(MarkData.WillDamageNude * 2);
    ViewGame.ShakeMap;
  end;
  if (TargetPlayer.Inventory.EquippedClothesRemovable > 0) and TargetPlayer.Inventory.CanUseHands then
  begin
    LocalStats.IncStat('TProjectileBugSimple_removable');
    ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(TargetPlayer.Inventory.GetRandomClothesSlotEquippedItem, true);
    if ItemStolen.ItemData.CanBeRepaired then
      ItemStolen.Broken := true;
    TMapItem.DropItem(TargetPlayer.LastTileX + TargetPlayer.PredSize div 2, TargetPlayer.LastTileY + TargetPlayer.PredSize div 2, ItemStolen, false);
    ShowLog('A swarm of insects sticks to %s''s %s and she throws it down in panic', [TargetPlayer.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemBreak);
    TargetPlayer.HitStamina(MarkData.StaminaDamage);
    TargetPlayer.HitWill(MarkData.WillDamage);
  end else
  begin
    E := TargetPlayer.Inventory.GetRandomClothesSlot;
    if TargetPlayer.Inventory.Equipped[E] <> nil then // IsBondage redundant?
    begin
      LocalStats.IncStat('TProjectileBugSimple_restraints');
      ShowLog('A few beetles crawl under %s''s %s and she desperately struggles to get rid of them', [TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogStaminaDamage);
      TargetPlayer.HitWill(MarkData.WillDamageNude);
      TargetPlayer.HitStamina(MarkData.StaminaDamageNude);
      ViewGame.ShakeMap;
      TargetPlayer.ResetToIdle;
    end else
    begin
      LocalStats.IncStat('TProjectileBugSimple_naked');
      ShowLog('A dozen of bugs catch to %s''s %s and she dances like crazy to shake them off', [TargetPlayer.Data.DisplayName, TargetPlayer.Blueprint.EquipmentSlotToHumanReadableString(E)], ColorLogStaminaDamage);
      TargetPlayer.HitWill(MarkData.WillDamageBondage);
      TargetPlayer.HitStamina(MarkData.StaminaDamageBondage);
      ViewGame.ShakeMap;
      TargetPlayer.ResetToIdle;
    end;
  end;
  Sound(MarkData.HitSound);
end;

{ TProjectileBugSimpleData ----------------------------- }

function TProjectileBugSimpleData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileBugSimpleData.Validate;
begin
  inherited Validate;
  if StaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamage <= 0 in %s', [Self.ClassName]);
  if WillDamage <= 0 then
    raise EDataValidationError.CreateFmt('WillDamage <= 0 in %s', [Self.ClassName]);
  if StaminaDamageNude <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamageNude <= 0 in %s', [Self.ClassName]);
  if WillDamageNude <= 0 then
    raise EDataValidationError.CreateFmt('WillDamageNude <= 0 in %s', [Self.ClassName]);
  if StaminaDamageBondage <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamageBondage <= 0 in %s', [Self.ClassName]);
  if WillDamageBondage <= 0 then
    raise EDataValidationError.CreateFmt('WillDamageBondage <= 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TProjectileBugSimpleData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  StaminaDamage := Element.AttributeSingle('StaminaDamage');
  WillDamage := Element.AttributeSingle('WillDamage');
  StaminaDamageNude := Element.AttributeSingle('StaminaDamageNude');
  WillDamageNude := Element.AttributeSingle('WillDamageNude');
  StaminaDamageBondage := Element.AttributeSingle('StaminaDamageBondage');
  WillDamageBondage := Element.AttributeSingle('WillDamageBondage');
  HitSound := Element.AttributeString('HitSound');
end;

function TProjectileBugSimpleData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: The bugs will stick to worn article of clothes, forcing the character to drop it with panic and refuse to take it back on until all the critters are picked out of it which could take some time.', []),
    'TProjectileBugSimple_removable', 1));
  Result.Add(
    NewEntryText(
      Format('If the target is immobilized, insectophobia will deal massive willpower damage.', []),
    'TProjectileBugSimple_immobilized', 1));
  Result.Add(
    NewEntryText(
      Format('If bugs will crawl under restraints will cause willpower and stamina damage.', []),
    'TProjectileBugSimple_restraints', 1));
  Result.Add(
    NewEntryText(
      Format('If bugs start crawing over nude skin will cause high willpower and stamina damage.', []),
    'TProjectileBugSimple_naked', 1));
end;

function TProjectileBugSimpleData.Mark: TMarkClass;
begin
  Exit(TProjectileBugSimple);
end;

initialization
  RegisterSerializableObject(TProjectileBugSimple);
  RegisterSerializableData(TProjectileBugSimpleData);

end.

