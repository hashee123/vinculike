{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerRest;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameActionAbstract;

type
  TActionPlayerRest = class(TActionAbstract)
  strict private const
    { Time after which resting quality increases by 100% }
    SleepTimeScale = Single(50); // changing this line causes gameplayercharacter.pas(269,45) Error: Internal error 2010122901 unless clean up and rebuild
  strict private
    Phase: Single;
    NextQualityStep: Integer;
    NextQualityTime: Single;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function ProposedTimeSpeed: Single; override;
    function CanStop: Boolean; override;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
    destructor Destroy; override;
  end;

  TActionPlayerRestData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GamePlayerCharacter, GameLog, GameTranslation, GameColors,
  GameEnchantmentRestQualityBonus, GameEnchantmentDeepSleep;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$ENDIF}

function TActionPlayerRest.NoiseMultiplier: Single;
begin
  Exit(0.8);
end;

function TActionPlayerRest.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionPlayerRest.ProposedTimeSpeed: Single;
begin
  Exit(30.0);
end;

function TActionPlayerRest.CanStop: Boolean;
begin
  // don't use inherited
  Result := ParentPlayer.Stamina >= ParentPlayer.Inventory.FindEffectAdditive(TEnchantmentDeepSleep) * ParentPlayer.PlayerCharacterData.MaxStamina;
  if not Result then
    ShowLog('%s tries to wake up, but her limbs do not respond and eyelids refuse to open', [ParentPlayer.Data.DisplayName], ColorLogNotEnoughStamina);
end;

procedure TActionPlayerRest.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
end;

procedure TActionPlayerRest.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
end;

procedure TActionPlayerRest.Start;
begin
  inherited Start;
  ShowLog(GetTranslation('ActorStartsResting'), [ParentPlayer.Data.DisplayName], ColorLogActionStart);
  NextQualityStep := 100;
  NextQualityTime := SleepTimeScale;
end;

procedure TActionPlayerRest.Update(const SecondsPassed: Single);
begin
  //inherited;

  if (Phase < NextQualityTime) and (Phase + SecondsPassed >= NextQualityTime) then
  begin
    ShowLog('Rest quality +%d%%', [NextQualityStep], ColorLogSleep);
    Inc(NextQualityStep, 100);
    NextQualityTime := NextQualityTime + SleepTimeScale;
  end;

  Phase += SecondsPassed;
  ParentPlayer.RegenerateMaxStats(SecondsPassed, Phase / SleepTimeScale + ParentPlayer.Inventory.FindEffectAdditive(TEnchantmentRestQualityBonus));
end;

destructor TActionPlayerRest.Destroy;
begin
  if (Parent <> nil) and (ParentPlayer.Data <> nil) then
  begin
    ShowLog(GetTranslation('ActorStopsResting'), [ParentPlayer.Data.DisplayName], ColorLogCancel);
  end;
  inherited Destroy;
end;

{ TActionPlayerRestData -------------------------------}

function TActionPlayerRestData.Action: TActionClass;
begin
  Exit(TActionPlayerRest);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerRest);
  RegisterSerializableData(TActionPlayerRestData);

end.

