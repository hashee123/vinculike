{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionGlueBeetle;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  CastleVectors,
  GameActionAbstract, GameActionOnTarget, GameActionRoll, GameApparelSlots,
  GameInventory,
  GameUnlockableEntry;

type
  TActionGlueBeetle = class(TActionOnTarget)
  strict private const
    GlueNames: array [0..7] of String = ('glue_bottom', 'glue_top', 'glue_topbottom', 'glue_toptopbottom', 'glue_toptopbottombottom', 'glue_toptoptopbottombottom', 'glue_toptoptopbottombottombottom', 'glue_full');
  strict private
    OldVector: TVector2;
    Phase: Single;
    PreHit: Boolean;
    SubActionRoll: TActionRoll;
    procedure FinishRollToTarget;
    procedure FinishRollFromTarget;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    class function NextSlot(const TargetInventory: TInventory): TApparelSlot;
    class function NextGlue(const TargetInventory: TInventory): String;
    class function IsGlue(const TargetInventory: TInventory; const ASlot: TApparelSlot): Boolean;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    destructor Destroy; override;
  end;

  TActionGlueBeetleData = class(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public const
    ImmobilizedStruggleDamage = 200;
  public
    StealItemSound: String;
    ScratchDamage: Single;
    ActionWarmup: Single; //WARNING: both for shooting and dashing
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData, GameGarbageCollector,
  GameActor, GamePlayerCharacter, GameViewEndGame,
  GameInventoryItem, GameMonster, GameMapItem, GameMap, GameDifficultyLevel,
  GameViewGame, GameLog, GameColors, GameRandom, GameStats, GameSounds,
  GameProjectileGlue, GameParticle,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ActionData:=(Data as TActionGlueBeetleData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ActionData:=TActionGlueBeetleData(Data)}
{$ENDIF}

procedure TActionGlueBeetle.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('PreHit', PreHit);
  Element.AttributeSet('OldVector', OldVector);
  if SubActionRoll <> nil then // TODO: It most likely will crash Loading --- but can happen as soon as action started but didn't receive Update yet.
    SubActionRoll.Save(Element.CreateChild('SubActionRoll'));
end;

procedure TActionGlueBeetle.Load(const Element: TDOMElement);
var
  SubActionRollElement: TDOMElement;
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
  PreHit := Element.AttributeBoolean('PreHit');
  OldVector := Element.AttributeVector2('OldVector');
  SubActionRollElement := Element.ChildElement('SubActionRoll', false);
  if SubActionRollElement <> nil then
  begin
    SubActionRoll := TActionAbstract.LoadClass(SubActionRollElement) as TActionRoll;
    if PreHit then
      SubActionRoll.OnActionFinished := @FinishRollToTarget
    else
      SubActionRoll.OnActionFinished := @FinishRollFromTarget;
  end else
    SubActionRoll := nil;
end;

class function TActionGlueBeetle.IsGlue(const TargetInventory: TInventory;
  const ASlot: TApparelSlot): Boolean;
var
  I: Integer;
begin
  if TargetInventory.Equipped[ASlot] <> nil then
  begin
    for I := Low(GlueNames) to High(GlueNames) do
      if TargetInventory.Equipped[ASlot].Data.Id = GlueNames[I] then
        Exit(true);
  end;
  Exit(false);
end;

class function TActionGlueBeetle.NextGlue(const TargetInventory: TInventory): String;

  function CanGlueTop: Boolean;
  begin
    if TargetInventory.HasItem('glue_top') then
      Exit(false);
    if TargetInventory.Equipped[esTopUnder] = nil then
      Exit(true);
    Exit(not IsGlue(TargetInventory, esTopUnder));
  end;

  function CanGlueBottom: Boolean;
  begin
    if TargetInventory.HasItem('glue_bottom') then
      Exit(false);
    if TargetInventory.Equipped[esBottomUnder] = nil then
      Exit(true);
    Exit(not IsGlue(TargetInventory, esBottomUnder));
  end;

begin
  if CanGlueTop and (not CanGlueBottom or Rnd.RandomBoolean) then
    Exit('glue_top')
  else
  if CanGlueBottom then
    Exit('glue_bottom')
  else
  // the second check is redundant here!
  if TargetInventory.HasItem('glue_top') and
     TargetInventory.HasItem('glue_bottom') then
    Exit('glue_topbottom')
  else
  if TargetInventory.HasItem('glue_topbottom') then
    Exit('glue_toptopbottom')
  else
  if TargetInventory.HasItem('glue_toptopbottom') then
    Exit('glue_toptopbottombottom')
  else
  if TargetInventory.HasItem('glue_toptopbottombottom') then
    Exit('glue_toptoptopbottombottom')
  else
  if TargetInventory.HasItem('glue_toptoptopbottombottom') then
    Exit('glue_toptoptopbottombottombottom')
  else
  if TargetInventory.HasItem('glue_toptoptopbottombottombottom') then
    Exit('glue_full')
  else
  if TargetInventory.HasItem('glue_full') then
    Exit('*CAPTURE*') // "magic" string
  else
  begin
    ShowError('Unexpected glue state on the character', []);
    Exit('glue_full')
  end;
end;

class function TActionGlueBeetle.NextSlot(const TargetInventory: TInventory): TApparelSlot;
var
  E: TApparelSlot;
begin
  // if target has top/bottom covered - strip those first
  if (TargetInventory.TopCovered > 0) and ((TargetInventory.BottomCovered = 0) or Rnd.RandomBoolean) then
  begin
    // first strip clothes, then bondage items
    if TargetInventory.TopCoveredRemovable > 0 then
      repeat
        E := TargetInventory.GetRandomClothesSlotEquippedItem;
      until TargetInventory.Equipped[E].ItemData.CoversTop
    else
      repeat
        E := TargetInventory.GetRandomClothesSlotEquippedItemOrBondage;
      until TargetInventory.Equipped[E].ItemData.CoversTop;
    Exit(E);
  end else
  if TargetInventory.BottomCovered > 0 then
  begin
    // first strip clothes, then bondage items
    if TargetInventory.BottomCoveredRemovable > 0 then
      repeat
        E := TargetInventory.GetRandomClothesSlotEquippedItem;
      until TargetInventory.Equipped[E].ItemData.CoversBottom
    else
      repeat
        E := TargetInventory.GetRandomClothesSlotEquippedItemOrBondage;
      until TargetInventory.Equipped[E].ItemData.CoversBottom;
    Exit(E);
  end else
    case NextGlue(TargetInventory) of
      'glue_bottom': Exit(esBottomUnder);
      'glue_top': Exit(esTopUnder);
      'glue_topbottom': Exit(esTopOver);
      'glue_toptopbottom': Exit(esTopOver);
      'glue_toptopbottombottom': Exit(esBottomOver);
      'glue_toptoptopbottombottom': Exit(esTopOverOver);
      'glue_toptoptopbottombottombottom': Exit(esFeet);
      'glue_full': Exit(esWeapon);
      '*CAPTURE*': Exit(esTopUnder); // magic constant
      else
      begin
        ShowError('Unexpected NextGlue(TargetInventory): %s', [NextGlue(TargetInventory)]);
        Exit(esTopUnder);
      end;
    end;
end;

function TActionGlueBeetle.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionGlueBeetle.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionGlueBeetle.CanStop: Boolean;
begin
  Exit(Phase < ActionData.ActionWarmup * Difficulty.MonsterAttackDelayMultiplier / 10);
end;

procedure TActionGlueBeetle.FinishRollToTarget;
begin
  GarbageCollector.AddAndNil(SubActionRoll);
  Phase := 0;
  PreHit := true;
end;

procedure TActionGlueBeetle.FinishRollFromTarget;
begin
  GarbageCollector.AddAndNil(SubActionRoll);
  Phase := 0;
  PreHit := true;
  // drop items after rolling back
  while ParentMonster.Loot.Count > 0 do
    TMapItem.DropItem(ParentActor.LastTileX + ParentActor.PredSize div 2, ParentActor.LastTileY + ParentActor.PredSize div 2, ParentMonster.Loot.ExtractIndex(0), false);
end;

procedure TActionGlueBeetle.Update(const SecondsPassed: Single);
var
  NextInventorySlot: TApparelSlot;

  procedure StartRollToTarget;
  begin
    SubActionRoll := TActionRoll.NewAction(Parent) as TActionRoll;
    SubActionRoll.MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
    OldVector := SubActionRoll.MoveVector;
    SubActionRoll.MoveVectorNormalized := TActionRoll(SubActionRoll).MoveVector.Normalize;
    SubActionRoll.MoveVector := 15 * TActionRoll(SubActionRoll).MoveVectorNormalized;
    SubActionRoll.Start;
    SubActionRoll.OnActionFinished := @FinishRollToTarget;
  end;

  procedure HitTargetAndRollBack;
  var
    ItemStolen: TInventoryItem;
  begin
    GarbageCollector.AddAndNil(SubActionRoll);
    SubActionRoll := TActionRoll.NewAction(Parent) as TActionRoll;
    if OldVector.Length < ActionData.AttackRange / 2 then
      OldVector := OldVector.Normalize * ActionData.AttackRange / 2;
    TActionRoll(SubActionRoll).MoveVectorNormalized := -OldVector.Normalize;
    TActionRoll(SubActionRoll).MoveVector := -OldVector;
    SubActionRoll.Start;
    SubActionRoll.OnActionFinished := @FinishRollFromTarget;
    PreHit := false;

    LocalStats.IncStat(Data.ClassName);
    NextInventorySlot := NextSlot(TargetPlayer.Inventory);
    if NextGlue(TargetPlayer.Inventory) = '*CAPTURE*' then
    begin
      if TargetPlayer.Stamina > 0 then
      begin
        ShowLog('%s hugs %s and she desperately struggles to push it away', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondageItemSave);
        TargetPlayer.HitStamina(ActionData.ImmobilizedStruggleDamage);
        ViewGame.WakeUp(true, true);
      end else
      begin
        ShowLog('The flexible layer of glue around %s solidifies', [TargetPlayer.Data.DisplayName], ColorLogDefeat);
        TargetPlayer.GetCaptured(egGlued);
        LocalStats.IncStat(Data.ClassName + '_capture');
      end;
    end else
    // TODO: IsGlue should be impossible at this moment?
    if (TargetPlayer.Inventory.Equipped[NextInventorySlot] <> nil) and not IsGlue(TargetPlayer.Inventory, NextInventorySlot) then
    begin
      if TargetPlayer.Unsuspecting then
        ShowLog('%s makes a good use of %s''s lowered guard and silently removes her %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[NextInventorySlot].Data.DisplayName], ColorLogItemSteal)
      else
      if TargetPlayer.Inventory.Equipped[NextInventorySlot].ItemData.UnequipDisintegrates then
        ShowLog('%s clutches %s and effortlessly removes %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[NextInventorySlot].Data.DisplayName], ColorLogItemSteal)
      else
        ShowLog('%s aptly wraps its multiple legs around %s and steals %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[NextInventorySlot].Data.DisplayName], ColorLogItemSteal);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STRIP', ColorParticlePlayerDisrobed);
      ItemStolen := TargetPlayer.Inventory.DisintegrateOrReturn(NextInventorySlot, not TargetPlayer.Unsuspecting);
      if ItemStolen <> nil then
        ParentMonster.Loot.Add(ItemStolen);
      //TargetPlayer.Inventory.UpdateCoveredStatus; // to properly detect "nude after this action" ----- always calculated in "unequip"
      if TargetPlayer.Inventory.Nude then
        ViewGame.WakeUp(true, true);
    end else
    begin
      ShowLog('%s accidentally scratches %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogItemSteal);
      TargetPlayer.Hit(ActionData.ScratchDamage);
      ViewGame.WakeUp(true, true);
    end;
    Sound(ActionData.StealItemSound);
    ParentActor.DamageWeapon;
    ViewGame.ShakeMap;
  end;

  procedure ShootGlue;
  var
    Projectile: TProjectileGlue;
  begin
    Projectile := TProjectileGlue.Create;
    Projectile.Parent := Parent;
    Projectile.Data := ActionData.MarkData;
    Projectile.SetSize(Projectile.Data.Size);
    Projectile.MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY).Normalize;
    Projectile.MoveCenterTo(ParentActor);
    Map.MarksList.Add(Projectile);
    Phase := 0;
  end;

begin
  inherited;

  if not TargetPlayer.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  // todo variable phase
  if (SubActionRoll = nil) and (Phase >= ActionData.ActionWarmup * Difficulty.MonsterAttackDelayMultiplier) then
  begin
    NextInventorySlot := NextSlot(TargetPlayer.Inventory);
    //DebugLog('Next Slot: %s', [ApparelSlotToStr(NextInventorySlot)]);
    if (TargetPlayer.Inventory.Equipped[NextInventorySlot] = nil) and
      (NextGlue(TargetPlayer.Inventory) <> '*CAPTURE*') then
      // TODO: only if directly visible
      ShootGlue
    else
      StartRollToTarget;
  end;
  if SubActionRoll <> nil then
  begin
    if PreHit and (ParentActor.Collides(TargetPlayer, 0) or ViewGame.BuggyLargeTimeFlowSpeed) then
      HitTargetAndRollBack;
    SubActionRoll.Update(SecondsPassed);
  end;
end;

class function TActionGlueBeetle.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionGlueBeetle).Phase := 0;
  (Result as TActionGlueBeetle).PreHit := true;
end;

destructor TActionGlueBeetle.Destroy;
begin
  GarbageCollector.FreeNowAndNil(SubActionRoll);
  inherited Destroy;
end;

{ TActionGlueBeetleData -------------------------------}

procedure TActionGlueBeetleData.Validate;
begin
  inherited Validate;
  if not SoundExists(StealItemSound) then
    raise EDataValidationError.CreateFmt('Invalid StealItemSound = "%s" in %s', [StealItemSound, Self.ClassName]); 
  if ScratchDamage < 0 then
    raise EDataValidationError.CreateFmt('ScratchDamage must be >= 0 in %s', [ClassName]);
  if ActionWarmup < 0 then
    raise EDataValidationError.CreateFmt('ActionWarmup must be >= 0 in %s', [ClassName]);
end;

procedure TActionGlueBeetleData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  StealItemSound := Element.AttributeString('StealItemSound');
  ScratchDamage := Element.AttributeSingle('ScratchDamage');
  ActionWarmup := Element.AttributeSingle('ActionWarmup');
end;

function TActionGlueBeetleData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('First will try to undress the target naked by dashing to, stealing an item and retreating back and throwing it to the ground.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If the target is nude will shoot homing projectiles that will apply honey glue to the target. Every new layer of glue will restrain actions more and more. The attacker will dash in and out to steal the items if they are in the way of applying more glue.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('Finally, if the target is completely covered with glue, will capture it.', []),
    Classname+ '_capture', 1));
end;

function TActionGlueBeetleData.Action: TActionClass;
begin
  Exit(TActionGlueBeetle);
end;

initialization
  RegisterSimpleSerializableObject(TActionGlueBeetle);
  RegisterSerializableData(TActionGlueBeetleData);

end.

