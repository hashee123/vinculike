{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionMoveGhost;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  CastleVectors,
  GameSimpleSerializableObject,
  GameMapTypes,
  GameActionMoveAbstract, GameActionAbstract;

type
  TActionMoveGhost = class(TActionMoveAbstract)
  strict private
    DestinationX, DestinationY, OriginX, OriginY: Single;
  public
    MoveVector, MoveVectorNormalized: TVector2;
    MoveVectorLength, MovePhase: Single;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function RemainingDistance: Single; override;
    function RemainingTime: Single; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure MoveTo(const ToX, ToY: Single); override;
  public
    procedure Save(const Element: TDOMElement); override;
    procedure AfterDeserealization; override;
  protected
    procedure Load(const Element: TDOMElement); override;
  end;

  TActionMoveGhostData = class(TActionMoveAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameActor, GameMap;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ELSE}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ENDIF}

function TActionMoveGhost.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionMoveGhost.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionMoveGhost.RemainingDistance: Single;
begin
  Exit(MoveVectorLength);
end;

function TActionMoveGhost.RemainingTime: Single;
begin
  Exit(MoveVectorLength / ParentActor.Data.MovementSpeed);
end;

procedure TActionMoveGhost.Update(const SecondsPassed: Single);
var
  Diff: Single;
begin
  Diff := SecondsPassed * ParentActor.GetSpeed;
  // todo: make sure step no bigger than a tile?
  MovePhase += diff;
  ParentActor.HurtFeet(diff * FeetDamagePerTile);
  if MovePhase < MoveVectorLength then
  begin
    ParentActor.MoveMeTo(OriginX + MoveVectorNormalized.X * MovePhase, OriginY + MoveVectorNormalized.Y * MovePhase);
    ParentActor.UpdateVisible;
  end else
  begin
    ParentActor.MoveMeTo(DestinationX, DestinationY);
    ParentActor.UpdateVisible;
    ActionFinished;
    Exit;
  end;
end;

class function TActionMoveGhost.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionMoveGhost).MovePhase := 0;
end;


procedure TActionMoveGhost.MoveTo(const ToX, ToY: Single);
var
  Range: Integer;
  ToXInt, ToYInt, GotoX, GotoY: Int16;
  CurrentPassableTiles: TMoveArray;

  // TODO: This should be UI-only thing
  procedure GetNearestClick;
  var
    DX, DY: Integer;
  begin
    for DX := -Range to Range do
      for DY := -Range to Range do
        if Sqr(DX) + Sqr(DY) <= Sqr(Range) then
        begin
          GotoX := ToXInt + DX;
          GotoY := ToYInt + DY;
          if (GotoX >= 0) and (GotoX <= Map.PredSizeX) and (GotoY >= 0) and (GotoY <= Map.PredSizeY) and CurrentPassableTiles[GotoX + Map.SizeX * GotoY] then
          begin
            DestinationX := GotoX;
            DestinationY := GotoY;
            Exit;
          end;
        end;
  end;

begin
  DestinationX := ToX;
  DestinationY := ToY;
  OriginX := ParentActor.X;
  OriginY := ParentActor.Y;

  CurrentPassableTiles := ParentActor.PassableTiles;
  ToXInt := Round(ToX);
  ToYInt := Round(ToY);
  GotoX := ToXInt;
  GotoY := ToYInt;
  Range := 0;
  while (GotoX < 0) or (GotoX > Map.PredSizeX) or (GotoY < 0) or (GotoY > Map.PredSizeY) or not CurrentPassableTiles[GotoX + Map.SizeX * GotoY] do
  begin
    GetNearestClick;
    Inc(Range);
  end;

  MoveVector := Vector2(DestinationX - OriginX, DestinationY - OriginY);
  MoveVectorLength := MoveVector.Length;
  MoveVectorNormalized := MoveVector / MoveVectorLength;
end;

procedure TActionMoveGhost.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  // To avoid painful saving everything we save... only destination.
  Element.AttributeSet('DestinationX', DestinationX);
  Element.AttributeSet('DestinationY', DestinationY);
end;

procedure TActionMoveGhost.AfterDeserealization;
begin
  inherited AfterDeserealization;
  MoveTo(DestinationX, DestinationY); // TODO: Maybe better save waypoints?
end;

procedure TActionMoveGhost.Load(const Element: TDOMElement);
begin
  inherited;
  DestinationX := Element.AttributeSingle('DestinationX');
  DestinationY := Element.AttributeSingle('DestinationY');
end;

{ TActionMoveGhostData -------------------------------}

function TActionMoveGhostData.Action: TActionClass;
begin
  Exit(TActionMoveGhost);
end;

initialization
  RegisterSimpleSerializableObject(TActionMoveGhost);
  RegisterSerializableData(TActionMoveGhostData);

end.

