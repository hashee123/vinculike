{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionMoveWithoutPathfinding;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  CastleVectors, CastleUtils,
  GameSimpleSerializableObject,
  GameActionAbstract, GameActionMoveAbstract;

type
  TActionMoveWithoutPathfinding = class(TActionMoveAbstract)
  public
    MovePhase: Single;
    MoveVector, MoveVectorNormalized: TVector2;
    procedure MoveTo(const ToX, ToY: Single); override;
    function RemainingDistance: Single; override;
    function RemainingTime: Single; override;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
  end;

  { This is an internal action and doesn't need data
    But as some others inherit from it, maybe it'll be useful as parent }
  TActionMoveWithoutPathfindingData = class(TActionMoveAbstractData)
  public
    function Action: TActionClass; override;
  end;


implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameActor;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$ENDIF}

procedure TActionMoveWithoutPathfinding.MoveTo(const ToX, ToY: Single);
begin
  MoveVector := Vector2(ToX - ParentActor.X, ToY - ParentActor.Y);
  MoveVectorNormalized := MoveVector.Normalize;
end;

function TActionMoveWithoutPathfinding.RemainingDistance: Single;
begin
  Exit(MoveVector.Length)
end;

function TActionMoveWithoutPathfinding.RemainingTime: Single;
begin
  Exit(MoveVector.Length / ParentActor.Data.MovementSpeed);
end;

procedure TActionMoveWithoutPathfinding.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('MovePhase', MovePhase);
  Element.AttributeSet('MoveVector', MoveVector);
end;

procedure TActionMoveWithoutPathfinding.Load(const Element: TDOMElement);
begin
  inherited;
  MovePhase := Element.AttributeSingle('MovePhase');
  MoveVector := Element.AttributeVector2('MoveVector');
  MoveVectorNormalized := MoveVector.Normalize;
end;

function TActionMoveWithoutPathfinding.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionMoveWithoutPathfinding.NoiseAddition: Single;
begin
  Exit(0.0);
end;

procedure TActionMoveWithoutPathfinding.Update(const SecondsPassed: Single);
const
  MinDeltaSeconds = Single(0.01);
var
  RemainingSeconds: Single;
  DeltaSeconds: Single;
  OldTile: SizeInt;
begin
  (* TODO: BUG
     if SecondsPassed is large enough we can "jump over a tile".
     This may be very bad for navmesh and can even lead to a crash due to navmesh error *)
  RemainingSeconds := SecondsPassed;
  while RemainingSeconds > 0 do
  begin
    if RemainingSeconds > MinDeltaSeconds then
    begin
      DeltaSeconds := MinDeltaSeconds;
      RemainingSeconds -= MinDeltaSeconds;
    end else
    begin
      DeltaSeconds := RemainingSeconds;
      RemainingSeconds := 0;
    end;

    OldTile := ParentActor.LastTile;

    MovePhase += DeltaSeconds * ParentActor.GetSpeed;

    if ParentActor.MoveMeToDelta(DeltaSeconds * ParentActor.GetSpeed, MoveVectorNormalized.X, MoveVectorNormalized.Y) then
    begin
      if OldTile <> ParentActor.LastTile then
        ParentActor.UpdateVisible;
    end else
    begin
      ActionFinished;
      Exit;
    end;

    if MovePhase > MoveVector.Length then
    begin
      ActionFinished;
      Exit;
    end;
  end;
end;

class function TActionMoveWithoutPathfinding.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionMoveWithoutPathfinding).MovePhase := 0;
end;

{ TActionMoveWithoutPathfindingData ------------------------------ }

function TActionMoveWithoutPathfindingData.Action: TActionClass;
begin
  Exit(TActionMoveWithoutPathfinding);
end;

initialization
  RegisterSimpleSerializableObject(TActionMoveWithoutPathfinding);
  RegisterSerializableData(TActionMoveWithoutPathfindingData);

end.

