{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionAlwaysFlee;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameActionAbstract, GameActionOnTarget,
  GameUnlockableEntry;

type
  TActionAlwaysFlee = class(TActionOnTarget)
  public
    function CanStop: Boolean; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionAlwaysFleeData = class(TActionOnTargetData)
  public
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GameStats, GameMonster,
  GameVinculopediaEntryText;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE ActionData:=(Data as TActionAlwaysFleeData)}
{$ELSE}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE ActionData:=TActionAlwaysFleeData(Data)}
{$ENDIF}

function TActionAlwaysFlee.CanStop: Boolean;
begin
  Exit(true);
end;

procedure TActionAlwaysFlee.Update(const SecondsPassed: Single);
begin
  //inherited: no inherited because no "on-target" TODO

  if not ParentMonster.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  LocalStats.IncStat(Data.ClassName);
  ParentMonster.Ai.AiFlee := true;
end;

{ TActionAlwaysFleeData --------------------------------------- }

function TActionAlwaysFleeData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Will flee if sees the character.', []),
    Classname, 1));
end;

function TActionAlwaysFleeData.Action: TActionClass;
begin
  Exit(TActionAlwaysFlee);
end;

initialization
  RegisterSimpleSerializableObject(TActionAlwaysFlee);
  RegisterSerializableData(TActionAlwaysFleeData);

end.

