{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  CastleUtils,
  GameSimpleSerializableObject, GameSerializableData, GameMarkAbstract,
  GameUnlockableEntry;

const
  TimeoutsCorrection = 0.2;

type
  EActionLoadNotImplemented = class(Exception);

type
  TActionAbstract = class abstract(TSimpleSerializableObject)
  protected
    procedure ActionFinished;
    procedure ResetParentToIdle;
  public
    Parent: TObject; // we can't properly typecast it due to cycle reference
    OnActionFinished: TSimpleNotifyEvent;
    function NoiseMultiplier: Single; virtual; abstract;
    function NoiseAddition: Single; virtual; abstract;
    function ProposedTimeSpeed: Single; virtual;
    function CanStop: Boolean; virtual;
    procedure Start; virtual;
    procedure Update(const SecondsPassed: Single); virtual;
    class function NewAction(const AParent: TObject): TActionAbstract; virtual;
  end;
  TActionClass = class of TActionAbstract;

  TActionAbstractData = class abstract(TSerializableData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    AttackRange: Single;
    SqrAttackRange: Single;
    WarmUpTime: Single;
    CoolDownTime: Single;
    MarkData: TMarkAbstractData; // TODO: move it to action-with-mark, not all actions need marks which counters proper validation
    TemporaryRequiresFullViewOfTarget: Boolean;
    function Description: TEntriesList; virtual;
    { Class of this action.
      Virtually we could leave that as serializable field
      But I cannot see any direct benefits of that
      and it can be a nuisance and prone to typos }
    function Action: TActionClass; virtual; abstract;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameActor;

type
  EActionWrongParent = class(Exception);

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$ENDIF}

procedure TActionAbstract.ActionFinished;
begin
  if Assigned(OnActionFinished) then
    OnActionFinished
  else
    ResetParentToIdle;
end;

procedure TActionAbstract.ResetParentToIdle;
begin
  ParentActor.ForceResetToIdle; // the current action can prevent from ResetToIdle therefore ForceResetToIdle to be able to actually finish the blocking action
end;

function TActionAbstract.ProposedTimeSpeed: Single;
begin
  Exit(1.0);
end;

function TActionAbstract.CanStop: Boolean;
begin
  Exit(true);
end;

procedure TActionAbstract.Start;
begin
end;

procedure TActionAbstract.Update(const SecondsPassed: Single);
begin
  // do nothing here
end;

class function TActionAbstract.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := Create;
  if not (AParent is TActor) then
    raise EActionWrongParent.Create('Cannot create action for non-actor');
  Result.Parent := AParent;
end;

{ TActionAbstractData ------------------------------- }

procedure TActionAbstractData.Validate;
begin
  // inherited - parent is abstract
  if AttackRange <= 0 then
    raise Exception.CreateFmt('ActionRange <= 0 in %s', [Self.ClassName]);
  if SqrAttackRange <= 0 then
    raise Exception.CreateFmt('SqrAttackRange <= 0 in %s', [Self.ClassName]);
  if WarmUpTime < 0 then
    raise Exception.CreateFmt('WarmUpTime < 0 in %s', [Self.ClassName]);
  if CoolDownTime < 0 then
    raise Exception.CreateFmt('CoolDownTime < 0 in %s', [Self.ClassName]);
  if Action = nil then
    raise Exception.CreateFmt('Action = nil in %s', [Self.ClassName]);
  { This can happen if the action data doesn't have a mark, like alarm // temporary TODO
  if MarkData = nil then
    raise Exception.CreateFmt('MarkData = nil in %s', [Self.ClassName]);
  }
end;

procedure TActionAbstractData.Read(const Element: TDOMElement);
begin
  AttackRange := Element.AttributeSingle('AttackRange');
  SqrAttackRange := Sqr(AttackRange);
  WarmUpTime := Element.AttributeSingle('WarmUpTime') - TimeoutsCorrection;
  if WarmUpTime < 0 then WarmUpTime := 0;
  CoolDownTime := Element.AttributeSingle('CoolDownTime') + TimeoutsCorrection;
  MarkData := TMarkAbstractData.ReadClass(Element.Child('Mark', false)) as TMarkAbstractData; // will return nil if no Mark is present
  TemporaryRequiresFullViewOfTarget := Element.AttributeBooleanDef('TemporaryRequiresFullViewOfTarget', false);
end;

function TActionAbstractData.Description: TEntriesList;
begin
  Result := TEntriesList.Create(false); // entries will be owned by TVinculopediaPageAbstract
end;

destructor TActionAbstractData.Destroy;
begin
  FreeAndNil(MarkData);
  inherited Destroy;
end;

end.

