{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ An actor with inventory, player control - player's character (PC)
  and some other features needed for a playable character
  (doesn't necessarily need to be under player's control,
  can make a full use of AI-only behavior) }
unit GamePlayerCharacter;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, Generics.Collections, DOM,
  CastleTimeUtils,
  GameSerializableObject,
  GameAiAbstract, GameActionMoveAbstract,
  GameActor, GamePlayerCharacterData, GameMapTypes,
  GamePlayerCharacterExperience, GameViewEndGame,
  GameInventory, GameApparelSlots, GameInventoryItem,
  GameBlueprint;

type
  { all heroes are supposed to have two meaningful body halves }
  TBodyHalf = (bhTop, bhBottom);

type
  { Actor with bells and whistles }
  TPlayerCharacter = class(TActor)
  public const
    { Some "default" value for PC's vision range
      it may be significantly different for a specific character,
      but some external (character-unrelated) procedures
      need to take this value into account (like map generation) }
    DefaultVisionRange = 24;
  strict private const
    { Delay between some notifications about this character
      for now: those that happen when the PC changes current 
      (hurting feet, walking nude)
      in seconds, randomized }
    NotificationDelay = 30;
  strict private
    FLastSoundTime: TTimerResult;
    FBarefootNotificationDelay: Single;
    FLastBarefootNotification: TTimerResult;
    FNudeNotificationDelay: Single;
    FLastNudeNotification: TTimerResult;
    FLastSoundDuration: Single;
    FSphericalCollisionWithMonstersSpeedMultiplier: Single;
    { if spherical float-collides with any monster
      returns normalized distance to the nearest monster
      or 1.0 if no collision occurs }
    function SphericalCollisionWithMonsters: Single;
  protected
    { Todo: do PCs even need this? }
    procedure Die; override;
  public
    function PassableTiles: TMoveArray; override;
  public
    Ai: TAiAbstract;
  public
    { Contains definitions of the character base stats and features }
    Blueprint: TBlueprint;
    { Contains items this character has + functions related to those,
      including getting special items effects such as enchantment }
    Inventory: TInventory;
    { Contains experience and skills of this character 
      recalculates parent's stats }
    Experience: TPlayerCharacterExperience;
    { How many days this character needs to recover
      before will be available for dungeon run
      note as the day updates AFTER the run, set this value to +1 }
    Exhausted: Integer;
    { Stats specific to PC }
    Stamina, MaxStamina: Single;
    Will, MaxWill: Single;
    Magic, MaxMagic: Single;
    procedure HealMaxWill(const AValue: Single);
    procedure HealMaxStamina(const AValue: Single);
    { Reset this character to max stats }
    procedure Reset; override;
    { Deal physical damage to random body half }
    procedure Hit(const Damage: Single); override;
    { Deal physical damage to a specific body half }
    procedure HitSpecific(const Damage: Single; const BodyHalf: TBodyHalf); // TODO: Damage kind
    { Reduce health, and reduce max health accordingly }
    procedure DegradeHealth(const Damage: Single);
    { Damage health directly and show log message }
    procedure HitHealth(const Damage: Single);
    procedure HitStamina(const Damage: Single);
    procedure DegradeStamina(const Damage: Single);
    procedure HitWill(const Damage: Single);
    procedure DegradeWill(const Damage: Single);
    { Damage currently held weapon after it hits target
      Todo: more flexible event on hit (e.g. for bondage items) }
    procedure DamageWeapon; override;
    { The PC changed the tile: it affects the stats 
      damages footwear, if none hurts Health 
      if nude hurts willpower }
    procedure HurtFeet(const Damage: Single); override;
    function CanAct: Boolean; override;
    function CanBeInteractedWith: Boolean; override;
    function Unsuspecting: Boolean; override;
    function Immobilized: Boolean; override;
  public
    { restore stats with time }
    procedure RegenerateStats(const SecondsPassed: Single);
    { restore max stats with time and testing quality}
    procedure RegenerateMaxStats(
      const SecondsPassed: Single; const Quality: Single);
    { Calculate willpower regeneration multiplier based on current state and clothes }
    function WillRegenCoefficient(const Quality: Single): Single;
  public
    function GetShapePose: String;
  public
    { Play some sounds as this character:
      makes sure that last sounds has finished
      so that vocals won't overlap }
    { Laughing/giggles }
    procedure PlayGiggleSound;
    { Doing something hard }
    procedure PlayGruntSound;
    { Screaming in pain }
    procedure PlayHurtSound;
    { Heavy breathing }
    procedure PlayBreathSound;
    { Surprised "ouch" }
    procedure PlaySurpriseSound;
  public
    function GetDamageMultiplier: Single;
    function GetSpeed: Single; override;
    function MoveAction: TActionMoveClass; override;
    function GetSpeedMultiplier: Single;
    function GetRollSpeed: Single; override;
    function GetRollCost: Single;
    function GetRollCostMultiplier: Single;
    { Total "noisiness" of the character's body and inventory }
    function GetNoise: Single; override;
    { how effectively can this character resist?
      for now affects only few monsters chance to equip restraints -
      the lower the value, the lower resistance;
      at zero character cannot resist at all
      Note: Immobilized is treated separately (as often it shows a different log) }
    function ResistMultiplier: Single;
    { Real visible range of this character 
      includes all modifiers }
    function VisibleRange: Single;
    { Actor's data typecasted }
    function PlayerCharacterData: TPlayerCharacterData; inline;
  public
    { If this character is a prisoner 
      Todo: have it as inventor result - incapacitated, fully restrained}
    IsCaptured: Boolean;
    { Currently at the dungeon depth }
    AtMap: Byte;
    { Some centralized checks if the character is ready to go into the dungeon }
    function IsAvailableForDungeon: Boolean;
  public
    //procedure RespawnInRandomPlaceNaked;
    { Temporary: properly mark this character as captured 
      Todo: better run end conditions }
    procedure GetCaptured(const CaptureKind: TEndGameKind);
    { Same as GetCaptured, but doesn't call anything except this character functions
      For internal use - e.g. to recover from crash }
    procedure GetCapturedSafe;
    procedure Teleport(const ToX, ToY: Int16); override;
    { (try to) teleport to unexplored map area
      (trying to avoid aggressive monsters around)
      if fails: just teleport to a random spot }
    procedure TeleportToUnknown(const MinTeleportDistanceFraction: Single);
    procedure UpdateVisible; override;
    procedure Update(const SecondsPassed: Single); override;
  public const Signature = 'playerchar'; // deprecated 
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    constructor Create(const NotLoading: Boolean = true); override;
    destructor Destroy; override;
  end;
  TPlayerCharactersList = specialize TObjectList<TPlayerCharacter>;

implementation
uses
  CastleXmlUtils,
  GameRandom, GameSounds, GameTranslation, GameMap, GameLog, GameColors,
  GameMonster,
  TempData,
  GameAiPlayerSettlement,
  GameEnchantmentSpeedMultiplier,
  GameEnchantmentDamageMultiplier, GameEnchantmentNoiseMultiplier,
  GameEnchantmentRollCostMultiplier, GameEnchantmentResistMultiplier,
  GameEnchantmentNudeTouchWillpowerDamageMultiplier,
  GameEnchantmentWalkNudeWillpowerDamageMultiplier,
  GameActionIdle, GameActionPlayerRest, GameActionPlayerUnconscious,
  GameActionPlayerStunnedAbstract, GameActionKnockback, GameActionPlayerHeld,
  GameActionMoveWithoutPathfinding,
  GameViewGame, GameParticle, GameDifficultyLevel, GameBlueprintsDatabase;

type
  EDamageCalculationError = class(Exception);

procedure TPlayerCharacter.Reset;
begin
  Experience.Recalculate;
  inherited;
  MaxStamina := PlayerCharacterData.MaxStamina;
  Stamina := MaxStamina;
  MaxWill := PlayerCharacterData.MaxWill;
  Will := MaxWill;
  MaxMagic := PlayerCharacterData.MaxMagic;
  Magic := MaxMagic;
  //Exhausted := 0;
end;

procedure TPlayerCharacter.Hit(const Damage: Single);
begin
  //inherited Hit(Damage); ---- different logic here
  HitSpecific(Damage, TBodyHalf(Rnd.Random(Ord(High(TBodyHalf)) + 1)));
end;

{$PUSH} // PUSH-POP work only on a whole method, they don't work locally for some reason
procedure TPlayerCharacter.HitSpecific(const Damage: Single; const BodyHalf: TBodyHalf);
var
  RemainingDamage: Single;
  AbsorbedDamage: Single;
  DamageAbsorbers: TInventoryItemsList;
  WillDamage: Single;
  Item: TInventoryItem;
  E: TApparelSlot;
begin
  RemainingDamage := Damage;

  DamageAbsorbers := TInventoryItemsList.Create(false);
  {$WARN 6018 OFF}
  case BodyHalf of
    bhTop: begin
         for E in Blueprint.TopSlots do
           if (Inventory.Equipped[E] <> nil) and not DamageAbsorbers.Contains(Inventory.Equipped[E]) and (Inventory.Equipped[E].ItemData.Protection > 0) then
             DamageAbsorbers.Add(Inventory.Equipped[E]);
      end;
    bhBottom: begin
         for E in Blueprint.BottomSlots do
           if (Inventory.Equipped[E] <> nil) and not DamageAbsorbers.Contains(Inventory.Equipped[E]) and (Inventory.Equipped[E].ItemData.Protection > 0) then
             DamageAbsorbers.Add(Inventory.Equipped[E]);
      end;
    else
      raise EDamageCalculationError.CreateFmt('unexpected BodyHalf %s', [BodyHalf]);
  end;

  for Item in DamageAbsorbers do
    begin
      AbsorbedDamage := RemainingDamage * Item.ItemData.Protection;
      RemainingDamage -= AbsorbedDamage;
      if (Self = ViewGame.CurrentCharacter) and (AbsorbedDamage > 0.05) then
        ShowLog(GetTranslation('ArmorAbsorbsDamageLog'), [Item.Data.DisplayName, AbsorbedDamage], ColorLogAbsorbDamage);
      Inventory.DamageItem(Item, AbsorbedDamage);
    end;

  FreeAndNil(DamageAbsorbers);

  DegradeHealth(RemainingDamage);
  if Self = ViewGame.CurrentCharacter then
  begin
    if Abs(RemainingDamage - Damage) < 0.1 then
      ShowLog(GetTranslation('PlayerReceivedUnabsorbedDamageLog'), [FData.DisplayName, RemainingDamage], ColorLogUnabsorbedDamage)
    else
      ShowLog(GetTranslation('PlayerReceivedAbsorbedDamageLog'), [FData.DisplayName, RemainingDamage, Round(100 * RemainingDamage / Damage)], ColorLogAbsorbedDamage);
  end;

  if Health <= 0 then
  begin
    Die;
    Exit;
  end;

  WillDamage := Difficulty.LowHealthWillpowerDamage * RemainingDamage / MaxHealth;

  //Inventory.UpdateCoveredStatus; No need to, if an item was broken, then it was already force-recalculated in UnequipAndReturn
  if (Inventory.TopCovered = 0) and (BodyHalf = bhTop) then
  begin
    if Inventory.FindEffectMultiplier(TEnchantmentNudeTouchWillpowerDamageMultiplier) > 0.5 then
      ShowLog(GetTranslation('PlayerReceivedNakedTouchTop'), [FData.DisplayName], ColorLogNaked);
    WillDamage += Difficulty.HitNakedWillpowerDamage * Inventory.FindEffectMultiplier(TEnchantmentNudeTouchWillpowerDamageMultiplier);
  end;
  if (Inventory.BottomCovered = 0) and (BodyHalf = bhBottom) then
  begin
    if Inventory.FindEffectMultiplier(TEnchantmentNudeTouchWillpowerDamageMultiplier) > 0.5 then
      ShowLog(GetTranslation('PlayerReceivedNakedTouchBottom'), [FData.DisplayName], ColorLogNaked);
    WillDamage += Difficulty.HitNakedWillpowerDamage * Inventory.FindEffectMultiplier(TEnchantmentNudeTouchWillpowerDamageMultiplier);
  end;
  HitWill(WillDamage);

  ViewGame.WakeUp(true, true);
  NewParticle(CenterX, CenterY, Round(Damage).ToString, ColorParticlePlayerHurt);
end;
{$POP}

procedure TPlayerCharacter.DegradeHealth(const Damage: Single);
begin
  if Damage >= 0 then
  begin
    if Damage / Health > 0.25 then
      PlayHurtSound;
    Health -= Damage;
    MaxHealth -= Damage * Difficulty.StatsDegradationSpeed;
  end else
    ShowError('Cannot degrade health by negative value %.1n', [Damage]);
end;

procedure TPlayerCharacter.HitHealth(const Damage: Single);
begin
  if (Damage > MaxHealth / 100.0) and (Damage > 0.5) then
    ShowLog('%s lost %d health', [FData.DisplayName, Round(Damage)], ColorLogUnabsorbedDamage);
  DegradeHealth(Damage);
  NewParticle(CenterX, CenterY, Round(Damage).ToString, ColorParticlePlayerHurt);
end;

procedure TPlayerCharacter.HitStamina(const Damage: Single);
var
  PreviousStamina: Single;
begin
  if (Damage > MaxStamina / 100.0) and (Damage > 0.5) then
    ShowLog('%s lost %d stamina', [FData.DisplayName, Round(Damage)], ColorLogStaminaDamage);
  PreviousStamina := Stamina;
  DegradeStamina(Damage);
  if (PreviousStamina > 0) and (Stamina < 0) then
    ShowLog('%s is exhausted and can barely stand on her feet', [FData.DisplayName, Damage], ColorLogStaminaExhausted);
end;

procedure TPlayerCharacter.DegradeStamina(const Damage: Single);
begin
  if Damage >= 0 then
  begin
    Stamina -= Damage;
    MaxStamina -= Damage * Difficulty.StatsDegradationSpeed;
  end else
    ShowError('Cannot degrade stamina by negative value %.1n', [Damage]);
end;

procedure TPlayerCharacter.HitWill(const Damage: Single);
begin
  if (Damage > MaxWill / 100.0) and (Damage > 0.5) then
    ShowLog('%s lost %d willpower', [FData.DisplayName, Round(Damage)], ColorLogWillDamage);
  DegradeWill(Damage);
end;

procedure TPlayerCharacter.DegradeWill(const Damage: Single);
begin
  if Damage >= 0 then
  begin
    Will -= Damage;
    MaxWill -= Damage * Difficulty.StatsDegradationSpeed;
  end else
    ShowError('Cannot degrade willpower by negative value %.1n', [Damage]);
end;

procedure TPlayerCharacter.DamageWeapon;
begin
  inherited DamageWeapon;
  Inventory.DamageWeapon;
end;

procedure TPlayerCharacter.HurtFeet(const Damage: Single);
var
  WillDamageMultiplier: Single;
begin
  if Map.CurrentDepth = 0 then Exit; // no hurting feet in settlement
  inherited;

  // walking barefoot
  if Inventory.HurtFeet(Damage) then
  begin
    if FLastBarefootNotification.ElapsedTime > FBarefootNotificationDelay then
    begin
      if FLastBarefootNotification.ElapsedTime < FBarefootNotificationDelay + 2 then // avoid showing the log instantly
        ShowLog('%s stepped on a sharp stone hurting her foot', [Data.DisplayName], ColorLogUnabsorbedDamage);
      FBarefootNotificationDelay := Sqrt(Rnd.Random) * NotificationDelay;
      FLastBarefootNotification := Timer;
    end;
    if Health > Blueprint.WalkFeetDamageMultiplier * Damage / Difficulty.StatsDegradationSpeed + 1 then
      DegradeHealth(Blueprint.WalkFeetDamageMultiplier * Damage / Difficulty.StatsDegradationSpeed)
    else
    if Health > 1 then
      DegradeHealth(Health - 1);
  end;

  // walking around nude
  if Inventory.PartiallyOrFullyNude then
  begin
    if (FLastNudeNotification.ElapsedTime > FNudeNotificationDelay) then
    begin
      if FLastNudeNotification.ElapsedTime < FNudeNotificationDelay + 2 then // avoid showing the log instantly
      begin
        if Inventory.FindEffectMultiplier(TEnchantmentWalkNudeWillpowerDamageMultiplier) <= 0.5 then
          case Rnd.Random(5) of
            0: ShowLog('%s feels oddly confident about how she looks', [Data.DisplayName], ColorLogUnabsorbedDamage);
            1: ShowLog('Surprised %s feels even a bit excited walking like that', [Data.DisplayName], ColorLogUnabsorbedDamage);
            2: ShowLog('%s wonders why she was avoiding naked walks before', [Data.DisplayName], ColorLogUnabsorbedDamage);
            3: ShowLog('%s can''t help but enjoy air flowing around her sensitive bits', [Data.DisplayName], ColorLogUnabsorbedDamage);
            else ShowLog('%s definitely wants to try something like that again', [Data.DisplayName], ColorLogUnabsorbedDamage);
          end
        else
        if Inventory.Nude then
          case Rnd.Random(4) of
            0: ShowLog('%s has an eerie feeling as if she''s being watched', [Data.DisplayName], ColorLogUnabsorbedDamage);
            1: ShowLog('%s feels awkward walking around... like that', [Data.DisplayName], ColorLogUnabsorbedDamage);
            2: ShowLog('%s mumbles to herself that there is nobody here to see her like that', [Data.DisplayName], ColorLogUnabsorbedDamage);
            else ShowLog('%s feels really uneasy walking around completely naked', [Data.DisplayName], ColorLogUnabsorbedDamage);
          end
        else
        if (Inventory.TopCovered = 0) then
          case Rnd.Random(3) of
            0: ShowLog('%s can''t help but notice her breasts jiggle with every step', [Data.DisplayName], ColorLogUnabsorbedDamage);
            1: ShowLog('%s is unable to stop thinking she''s barechested', [Data.DisplayName], ColorLogUnabsorbedDamage);
            else ShowLog('%s has a weird sensation on her exposed nipples', [Data.DisplayName], ColorLogUnabsorbedDamage);
          end
        else
        if (Inventory.BottomCovered = 0) then
          case Rnd.Random(4) of
            0: ShowLog('%s senses like her exposed crotch is literally asking for trouble', [Data.DisplayName], ColorLogUnabsorbedDamage);
            1: ShowLog('%s feels unnaturally warm down there', [Data.DisplayName], ColorLogUnabsorbedDamage);
            2: ShowLog('%s senses as cold dungeon air tingles her sensitive bits', [Data.DisplayName], ColorLogUnabsorbedDamage);
            else ShowLog('%s is very uncomfortable walking with her bottom exposed', [Data.DisplayName], ColorLogUnabsorbedDamage);
          end
      end;
      FNudeNotificationDelay := Sqrt(Rnd.Random) * NotificationDelay;
      FLastNudeNotification := Timer;
    end;
    if Inventory.Nude then
      WillDamageMultiplier := 0.5
    else
    if (Inventory.TopCovered = 0) then
      WillDamageMultiplier := 0.2
    else
    if (Inventory.BottomCovered = 0) then
      WillDamageMultiplier := 0.3;

    WillDamageMultiplier *= Inventory.FindEffectMultiplier(TEnchantmentWalkNudeWillpowerDamageMultiplier);

    if Will > WillDamageMultiplier * Damage / Difficulty.StatsDegradationSpeed + 1 then
      DegradeWill(WillDamageMultiplier * Damage / Difficulty.StatsDegradationSpeed)
    else
    if Will > 1 then
      DegradeWill(Will - 1);
  end;
end;

function TPlayerCharacter.GetDamageMultiplier: Single;
begin
  Result := Inventory.FindEffectMultiplier(TEnchantmentDamageMultiplier);
end;

function TPlayerCharacter.GetSpeed: Single;
begin
  Result := (inherited GetSpeed) * GetSpeedMultiplier * FSphericalCollisionWithMonstersSpeedMultiplier;
end;

function TPlayerCharacter.GetSpeedMultiplier: Single;
begin
  Result := Inventory.FindEffectMultiplier(TEnchantmentSpeedMultiplier);
end;

function TPlayerCharacter.GetRollSpeed: Single;
begin
  Result := (inherited GetRollSpeed) * FSphericalCollisionWithMonstersSpeedMultiplier;
end;

function TPlayerCharacter.GetRollCostMultiplier: Single;
begin
  Result := Inventory.FindEffectMultiplier(TEnchantmentRollCostMultiplier);
end;

function TPlayerCharacter.GetNoise: Single;
begin
  Result := (Blueprint.BodyNoise + Inventory.TotalIdleNoise * Inventory.FindEffectMultiplier(TEnchantmentNoiseMultiplier)) * CurrentAction.NoiseMultiplier + CurrentAction.NoiseAddition;
end;

function TPlayerCharacter.ResistMultiplier: Single;
begin
  Result := Stamina / PlayerCharacterData.MaxStamina * Inventory.FindEffectMultiplier(TEnchantmentResistMultiplier);
end;

function TPlayerCharacter.GetRollCost: Single;
begin
  Result := PlayerCharacterData.RollStaminaCost * GetRollCostMultiplier;
end;

function TPlayerCharacter.CanAct: Boolean;
begin
  Result := not IsCaptured and inherited and (Will > 0);
end;

function TPlayerCharacter.CanBeInteractedWith: Boolean;
begin
  //Result := inherited CanBeInteractedWith;
  Result := true; // always? TODO
end;

function TPlayerCharacter.Unsuspecting: Boolean;
begin
  Exit((CurrentAction is TActionPlayerRest) or (CurrentAction is TActionPlayerUnconscious));
end;

function TPlayerCharacter.Immobilized: Boolean;
begin
  Exit((CurrentAction is TActionPlayerStunnedAbstract) or (CurrentAction is TActionKnockback) or (CurrentAction is TActionPlayerHeld));
end;

procedure TPlayerCharacter.PlayGiggleSound;
begin
  if FLastSoundTime.ElapsedTime > FLastSoundDuration then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_giggle');
  end;
end;

procedure TPlayerCharacter.PlayGruntSound;
begin
  if FLastSoundTime.ElapsedTime > FLastSoundDuration then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_grunt');
  end;
end;

procedure TPlayerCharacter.PlayHurtSound;
begin
  if FLastSoundTime.ElapsedTime > FLastSoundDuration then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_hurt');
  end;
end;

procedure TPlayerCharacter.PlayBreathSound;
begin
  if FLastSoundTime.ElapsedTime > FLastSoundDuration then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_breath');
  end;
end;

procedure TPlayerCharacter.PlaySurpriseSound;
begin
  if FLastSoundTime.ElapsedTime > FLastSoundDuration then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_surprise');
  end;
end;

procedure TPlayerCharacter.Die;
begin
  // leave it here for now? let the monsters properly check if target needs to be attacked
  // ShowLog(GetTranslation('PlayerBlacksOutLog'), [FData.DisplayName], ColorLogDefeat);
end;

function TPlayerCharacter.MoveAction: TActionMoveClass;
begin
  if Inventory.Blindfolded then
    Result := TActionMoveWithoutPathfinding
  else
    Result := inherited MoveAction;
end;

function TPlayerCharacter.PassableTiles: TMoveArray;
begin
  if Self = ViewGame.CurrentCharacter then
  begin
    if Map.PassablePlayerTiles = nil then
    begin
      ShowError('Map.PassablePlayerTiles = nil, generating new ones', []);
      Map.ScheduleUpdateVisible;
      Map.UpdateVisible;
    end;
    Exit(Map.PassablePlayerTiles);
  end else
    Result := inherited;
end;

procedure TPlayerCharacter.HealMaxWill(const AValue: Single);
begin
  if MaxWill + AValue < PlayerCharacterData.MaxWill then
    MaxWill := MaxWill + AValue
  else
    MaxWill := PlayerCharacterData.MaxWill;
end;

procedure TPlayerCharacter.HealMaxStamina(const AValue: Single);
begin
  if MaxStamina + AValue < PlayerCharacterData.MaxStamina then
    MaxStamina := MaxStamina + AValue
  else
    MaxStamina := PlayerCharacterData.MaxStamina;
end;

function TPlayerCharacter.SphericalCollisionWithMonsters: Single;
var
  M: TMonster;
  D: Single;
begin
  Result := 1.0;
  for M in Map.MonstersList do
    if M.CanAct and M.Aggressive then
    begin
      D := M.SphericalDistanceNormalizedSqr(Self);
      if D < Result then
        Result := D;
    end;
end;

procedure TPlayerCharacter.UpdateVisible;
begin
  inherited; // parent is empty
  //Map.LookAround(LastTileX + PredSize div 2, LastTileY + PredSize div 2);
  Map.ScheduleUpdateVisible;
end;

procedure TPlayerCharacter.Teleport(const ToX, ToY: Int16);
begin
  inherited;
  Map.PassablePlayerTiles := nil; // TODO: Temporary? Optimize?
  if not IsCaptured then
    UpdateVisible;
  if Self = ViewGame.CurrentCharacter then
    ViewGame.StopShakers;
end;

procedure TPlayerCharacter.TeleportToUnknown(const MinTeleportDistanceFraction: Single);
const
  MaxRetryCountInvisible = Integer(10000);
  MaxRetryCountMonsters = Integer(3000);
var
  AX, AY: Int16;
  RetryCount: Integer;
  DistanceSeed: TCoordList;
  DistanceMap: TDistanceMapArray;
  MaxDistance: TDistanceQuant;
begin
  DistanceSeed := TCoordList.Create;
  // TODO: cycle all active players
  AddSeed(DistanceSeed);
  DistanceSeed.Add(IntCoord(Map.ExitX, Map.ExitY));
  DistanceMap := Map.DistanceMap(PredSize, DistanceSeed);
  MaxDistance := Map.MaxDistance(DistanceMap);
  FreeAndNil(DistanceSeed);

  RetryCount := 0;
  repeat
    repeat
      AX := Rnd.Random(Map.SizeX);
      AY := Rnd.Random(Map.SizeY);
    until (Map.PassableTiles[PredSize][AX + Map.SizeX * AY]) and (Sqr(AX - Map.ExitX) + Sqr(AY - Map.ExitY) > Sqr(16)) and (DistanceMap[AX + Map.SizeX * AY] > MaxDistance * MinTeleportDistanceFraction);
    Inc(RetryCount);
  until ((Map.Visible[AX + Map.SizeX * AY] = 0) or (RetryCount > MaxRetryCountInvisible)) and (Map.NoMonstersNearby(AX, AY, Size) or (RetryCount > MaxRetryCountMonsters));
  DistanceMap := nil;
  Teleport(AX, AY);
  ViewGame.InternalMapInterface.TeleportTo(AX, AY);
end;

function TPlayerCharacter.PlayerCharacterData: TPlayerCharacterData; inline;
begin
  Exit({$IFDEF SafeActorTypecast}FData as TPlayerCharacterData{$ELSE}TPlayerCharacterData(FData){$ENDIF});
end;

function TPlayerCharacter.IsAvailableForDungeon: Boolean;
begin
  Exit(not IsCaptured and (Exhausted <= 0) and (AtMap = 0)); //AtMap is redundant? Exhausted should only matter in settlement
end;

procedure TPlayerCharacter.GetCaptured(const CaptureKind: TEndGameKind);

  procedure MoveCapturedCharacterIfNecessary;
  var
    NewDungeonLevel: Byte;
  begin
    NewDungeonLevel := AtMap;
    // note: the current character is not yet marked as "captured" so condition below is ">=", not ">"
    while (Map.CapturedCharacterOnLevel(NewDungeonLevel) >= Difficulty.MaxPrisonersAtLevel(NewDungeonLevel)) and (NewDungeonLevel > 0) do
      Dec(NewDungeonLevel);
    if (NewDungeonLevel > 0) and (NewDungeonLevel <> AtMap) then
    begin
      AtMap := NewDungeonLevel;
      ShowLog('To avoid logistic inconveniences the prisoner was moved to dungeon level %d', [ViewGame.CurrentCharacter.AtMap], ColorLogCaptured);
    end;
    // NOTE: the player may still accumulate multiple captured character on early levels. This issue might need to be addressed better to avoid new players being overwhelmed before understanding how the game works. TODO
  end;

begin
  ShowLog('%s was captured at dungeon level %d', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.AtMap], ColorLogCaptured);
  MoveCapturedCharacterIfNecessary;

  IsCaptured := true;
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
  Inventory.UnequipAndDisintegrateEverything;
  ViewGame.ScheduleEndGame;
  ViewEndGame.EndGameKind := CaptureKind;
end;

procedure TPlayerCharacter.GetCapturedSafe;
begin
  IsCaptured := true;
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
  Inventory.UnequipAndDisintegrateEverything;
end;

function TPlayerCharacter.WillRegenCoefficient(const Quality: Single): Single;
begin
  // sleeping peaks Will regen at 100%
  Result := 1.0;
  if Inventory.TopCovered = 0 then
    Result := Result / 1.5;
  if Inventory.BottomCovered = 0 then
    Result := Result / 3.5;

  // Bonus for sleeping quality
  if Result < Quality - 1.0 then
  begin
    Result := Quality - 1.0;
    if Result > 1.0 then
      Result := 1.0;
  end;
end;

function TPlayerCharacter.GetShapePose: String;
begin
  {$Warning 'Implement shape-pose properly'}
  Exit('default:default');
end;

procedure TPlayerCharacter.RegenerateMaxStats(const SecondsPassed: Single; const Quality: Single);
var
  Efficiency: Single;
begin
  Efficiency := SecondsPassed * Quality;

  { I'm not sure about MaxStat < 0 part,
    of course we can do
    (MaxStat < 0) and (Stat > MaxStat * 1.01)
    but this doesn't look like a good solution either
    for now it's causing problems only with stamina
    but in future capture logic it will need to be addressed properly }

  if (Health > MaxHealth * 0.99) or (MaxHealth < 0) then
  begin
    MaxHealth += Efficiency * PlayerCharacterData.MaxHealthRegen;
    if MaxHealth > PlayerCharacterData.MaxHealth then
      MaxHealth := PlayerCharacterData.MaxHealth;
  end;

  if (Stamina > MaxStamina * 0.99) or (MaxStamina < 0) then
  begin
    MaxStamina += Efficiency * PlayerCharacterData.MaxStaminaRegen;
    if MaxStamina > PlayerCharacterData.MaxStamina then
      MaxStamina := PlayerCharacterData.MaxStamina;
  end;

  if (Will > MaxWill * 0.99) or (MaxWill < 0) then
  begin
    MaxWill += Efficiency * WillRegenCoefficient(Quality) * PlayerCharacterData.MaxWillRegen;
    if MaxWill > PlayerCharacterData.MaxWill then
      MaxWill := PlayerCharacterData.MaxWill;
  end;
end;

procedure TPlayerCharacter.RegenerateStats(const SecondsPassed: Single);
begin
  Health += SecondsPassed * PlayerCharacterData.HealthRegen;
  if Health > MaxHealth then
    Health := MaxHealth;

  Stamina += SecondsPassed * PlayerCharacterData.StaminaRegen;
  if Stamina > MaxStamina then
    Stamina := MaxStamina;

  Will += SecondsPassed * WillRegenCoefficient(0) * PlayerCharacterData.WillRegen;
  if Will > MaxWill then
    Will := MaxWill;
end;

procedure TPlayerCharacter.Update(const SecondsPassed: Single);
var
  FSphericalCollisionWithMonsters: Single;
begin
  // cache collision with monsters
  FSphericalCollisionWithMonsters := SphericalCollisionWithMonsters;
  if FSphericalCollisionWithMonsters < 1.0 then
    FSphericalCollisionWithMonstersSpeedMultiplier := 0.1 + 0.9 * Sqrt(Sqrt(FSphericalCollisionWithMonsters))
  else
    FSphericalCollisionWithMonstersSpeedMultiplier := 1.0;

  // WARNING: Temporary.
  { In a normal flow the player character should be forced into unconscious/panic state
    and get captured only after restrained properly and all characters are defeated.
    +So in a team the chars can be rescued }
  if (Will <= 0) and not IsCaptured then
  begin
    ShowLog(GetTranslation('PlayerMentalBreakdown'), [FData.DisplayName], ColorLogDefeat);
    GetCaptured(egMental);
    Exit;
  end;
  if (Health <= 0) and not IsCaptured then
  begin
    ShowLog(GetTranslation('PlayerBlacksOutLog'), [FData.DisplayName], ColorLogDefeat);
    GetCaptured(egHealth);
    Exit;
  end;

  if not CanAct then
    Exit;

  RegenerateStats(SecondsPassed);
  RegenerateMaxStats(SecondsPassed, 1.0);

  if ViewGame.CurrentCharacter <> Self then
    Ai.Update(SecondsPassed);

  inherited;

  Inventory.Update(SecondsPassed);
end;

function TPlayerCharacter.VisibleRange: Single;
begin
   Exit(24.0 + Inventory.VisionBonus);
end;

procedure TPlayerCharacter.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('TEMPORARY-NAME', FData.DisplayName);
  Element.AttributeSet('Blueprint', Blueprint.Id);
  Element.AttributeSet('IsCaptured', IsCaptured);
  Element.AttributeSet('AtMap', AtMap);
  Element.AttributeSet('Vigor', Stamina);
  Element.AttributeSet('MaxVigor', MaxStamina);
  Element.AttributeSet('Psyche', Will);
  Element.AttributeSet('MaxPsyche', MaxWill);
  Element.AttributeSet('Enigma', Magic);
  Element.AttributeSet('MaxEnigma', MaxMagic);
  Element.AttributeSet('Exhausted', Exhausted);
  Inventory.Save(Element); // Element.CreateChild(TInventory.ClassName)
  Experience.Save(Element.CreateChild(TPlayerCharacterExperience.Signature));
end;

procedure TPlayerCharacter.Load(const Element: TDOMElement);
begin
  inherited;
  Data := TempPlayerCharacter; // TEMPORARY TODO
  Blueprint := BlueprintsDictionary[Element.AttributeStringDef('Blueprint', 'vulpine-female')];
  Data.DisplayName := Element.AttributeString('TEMPORARY-NAME');
  IsCaptured := Element.AttributeBoolean('IsCaptured');
  AtMap := Element.AttributeInteger('AtMap');
  Stamina := Element.AttributeSingle('Vigor');
  MaxStamina := Element.AttributeSingle('MaxVigor');
  Will := Element.AttributeSingle('Psyche');
  MaxWill := Element.AttributeSingle('MaxPsyche');
  Magic := Element.AttributeSingle('Enigma');
  MaxMagic := Element.AttributeSingle('MaxEnigma');
  Exhausted := Element.AttributeIntegerDef('Exhausted', 0); // todo: not def
  Inventory.Load(Element); //Element.Child(TInventory.ClassName, true)
  Experience.Load(Element.Child(TPlayerCharacterExperience.Signature, true));
  SetSize(FData.Size);
  FLastSoundDuration := 0;
end;

constructor TPlayerCharacter.Create(const NotLoading: Boolean = true);
begin
  inherited;
  Experience := TPlayerCharacterExperience.Create;
  Experience.Parent := Self;
  Inventory := TInventory.Create;
  Inventory.Parent := Self;
  Inventory.CreatePaperDoll;
  IsCaptured := false;
  AtMap := 0;
  FLastSoundDuration := 0;
  Ai := TAiPlayerSettlement.Create;
  Ai.Parent := Self;
  Ai.Data := TAiPlayerSettlementData.Create;
  Blueprint := BlueprintsList[Rnd.Random(BlueprintsList.Count)];
end;

destructor TPlayerCharacter.Destroy;
begin
  FreeAndNil(Inventory);
  FreeAndNil(Experience);
  FreeAndNil(FData);
  FreeAndNil(Ai.Data); // TODO: Wait, why?
  FreeAndNil(Ai);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TPlayerCharacter);
end.

