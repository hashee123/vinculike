{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameCharacterScreen;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleControls,
  GameApparelSlots;

type
  TCharacterScreen = class(TComponent) // CastleClassUtils -> TCastleComponent
  strict private
    ParentDesign: TCastleDesign;
    VerticalGroupBodyFeatures: TCastleVerticalGroup;
    procedure ClickReturn(Sender: TObject);
    procedure FillInValues;
  public
    function Exists: Boolean;
  public
    procedure SetWidth(const NewWidth: Single);
    procedure Show;
    procedure Hide;
    procedure Parse(const AParentDesign: TCastleDesign);
    procedure UpdateCaptions;
  end;

implementation
uses
  CastleComponentSerialize,
  CastleUiControls, CastleColors,
  GameFonts, GameSounds,
  GameViewGame, GameUiUtils,
  GameItemsDatabase,
  GameBodypartEdit,
  GameRandom;

{----------------------------------------------------------------------------------------}

procedure TCharacterScreen.Parse(const AParentDesign: TCastleDesign);
begin
  ParentDesign := AParentDesign;
  ParentDesign.Exists := false;

  VerticalGroupBodyFeatures := ParentDesign.DesignedComponent('VerticalGroupBodyFeatures') as TCastleVerticalGroup;

  (ParentDesign.DesignedComponent('ButtonReturn') as TCastleButton).OnClick := @ClickReturn;
  (ParentDesign.DesignedComponent('ButtonReturn') as TCastleButton).CustomFont := FontSoniano90;
  (ParentDesign.DesignedComponent('ButtonBody') as TCastleButton).OnClick := nil; // TODO
  (ParentDesign.DesignedComponent('ButtonBody') as TCastleButton).CustomFont := FontSoniano90;


  Hide;
end;

procedure TCharacterScreen.UpdateCaptions;
var
  I: Integer;
begin
  for I := 0 to Pred(VerticalGroupBodyFeatures.ControlsCount) do
    if VerticalGroupBodyFeatures.Controls[I] is TBodypartEditAbstract then // otherwise exception?
      TBodypartEditAbstract(VerticalGroupBodyFeatures.Controls[I]).UpdateValues;
end;

procedure TCharacterScreen.ClickReturn(Sender: TObject);
begin
  Sound('character_screen_hide');
  Hide;
end;

procedure TCharacterScreen.FillInValues;
var
  BodypartEdit: TBodypartEdit;
  BodypartColor: TBodypartColor;
  BodypartBoolean: TBodypartBoolean;
  A: TApparelSlot;
  C: String;
begin
  VerticalGroupBodyFeatures.ClearAndFreeControls;

  for A in ViewGame.CurrentCharacter.Blueprint.BodySlots do
  begin
    BodypartEdit := TBodypartEdit.Create(VerticalGroupBodyFeatures);
    BodypartEdit.ApparelSlot := A;
    BodypartEdit.ParentScreen := Self;
    VerticalGroupBodyFeatures.InsertFront(BodypartEdit);
  end;

  for C in ViewGame.CurrentCharacter.Blueprint.ColorTags.Keys do
  begin
    BodypartColor := TBodypartColor.Create(VerticalGroupBodyFeatures);
    BodypartColor.ColorTag := C;
    BodypartColor.ParentScreen := Self;
    VerticalGroupBodyFeatures.InsertFront(BodypartColor);
  end;

  BodypartBoolean := TBodypartBoolean.Create(VerticalGroupBodyFeatures);
  BodypartBoolean.Caption := 'Censored : ';
  BodypartBoolean.Value := @ViewGame.CurrentCharacter.Inventory.Censored;
  BodypartBoolean.ParentScreen := Self;
  VerticalGroupBodyFeatures.InsertFront(BodypartBoolean);

  BodypartBoolean := TBodypartBoolean.Create(VerticalGroupBodyFeatures);
  BodypartBoolean.Caption := 'Censored in screenshots : ';
  BodypartBoolean.Value := @ViewGame.CurrentCharacter.Inventory.CensoredInScreenshots;
  BodypartBoolean.ParentScreen := Self;
  VerticalGroupBodyFeatures.InsertFront(BodypartBoolean);

  UpdateCaptions;
end;

function TCharacterScreen.Exists: Boolean;
begin
  Exit(ParentDesign.Exists);
end;

procedure TCharacterScreen.SetWidth(const NewWidth: Single);
begin
  (ParentDesign.DesignedComponent('Content') as TCastleUserInterface).Width := NewWidth;
  //(ParentDesign.DesignedComponent('ScrollViewBodyFeatures') as TCastleUserInterface).Width := NewWidth;
end;

procedure TCharacterScreen.Show;
begin
  FillInValues;
  ParentDesign.Exists := true;
  ViewGame.PaperDoll.Nude := true;
  ViewGame.InvalidateInventory(nil);
end;

procedure TCharacterScreen.Hide;
begin
  ParentDesign.Exists := false;
  // note: paper doll is created before character screen
  ViewGame.PaperDoll.Nude := false;
  ViewGame.InvalidateInventory(nil);
end;

end.

