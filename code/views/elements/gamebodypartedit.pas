{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameBodypartEdit;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleControls, CastleUiControls, CastleMobileButton,
  GameApparelSlots;

type
  TBodypartEditAbstract = class abstract(TCastleUserInterface)
    protected const
      BodypartEditHeight = Single(60);
    protected
      HorizontalGroup: TCastleHorizontalGroup;
      CaptionLabel: TCastleLabel;
      procedure SetupButton(const AButton: TCastleMobileButton);
    public
      ParentScreen: TObject;
      procedure UpdateValues; virtual; abstract;
      constructor Create(AOwner: TComponent); override;
    end;

type
  TBodypartEdit = class(TBodypartEditAbstract)
  strict private
    ButtonPrevious, ButtonNext, ButtonRandom: TCastleMobileButton;
  public
    ApparelSlot: TApparelSlot;
    procedure ClickRandom(Sender: TObject);
    procedure ClickNext(Sender: TObject);
    procedure ClickPrevious(Sender: TObject);
    procedure UpdateValues; override;
    constructor Create(AOwner: TComponent); override;
  end;

type
  TBodypartColor = class(TBodypartEditAbstract)
  strict private
    ButtonRandom: TCastleMobileButton;
    procedure ClickRandom(Sender: TObject);
   public
    ColorTag: String;
    procedure UpdateValues; override;
    constructor Create(AOwner: TComponent); override;
  end;

type
  TBodypartBoolean = class(TBodypartEditAbstract)
  strict private
    ButtonToggle: TCastleMobileButton;
    procedure ClickToggle(Sender: TObject);
  public
    Caption: String;
    Value: ^Boolean;
    procedure UpdateValues; override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation
uses
  CastleComponentSerialize,
  CastleColors, CastleRectangles,
  GameFonts, GameSounds, GameColors,
  GameViewGame,
  GameBodyPart, GameItemsDatabase, GameItemDataAbstract, GameThemedButton,
  GameCharacterScreen,
  GameRandom;

procedure TBodypartEditAbstract.SetupButton(const AButton: TCastleMobileButton);
begin
  AButton.SetTheme('inventory_button_light');
  AButton.CustomFont := FontBender40;
  AButton.CustomTextColorUse := true;
  AButton.CustomTextColor := ColorDefault;
  AButton.EnableParentDragging := true;
  AButton.Height := BodypartEditHeight;
end;

constructor TBodypartEditAbstract.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  //Width := (ParentScreen as TCharacterScreen).Width; // not sure
  Width := 1000;
  Height := BodypartEditHeight;

  HorizontalGroup := TCastleHorizontalGroup.Create(Self);
  HorizontalGroup.Alignment := vpMiddle;
  InsertFront(HorizontalGroup);

  CaptionLabel := TCastleLabel.Create(Self);
  CaptionLabel.CustomFont := FontBender40;
  CaptionLabel.Color := ColorDefault;
  CaptionLabel.AutoSize := false;
  CaptionLabel.Width := 400;
  CaptionLabel.Height := BodypartEditHeight;
  CaptionLabel.Alignment := hpRight;
  HorizontalGroup.InsertFront(CaptionLabel);
end;

{ TBodypartEdit ---------------------------------------------------- }

procedure TBodypartEdit.ClickRandom(Sender: TObject);
var
  PreviousBodypart: TItemDataAbstract;
  BodyPartsList: TItemsDataAbstractList;
  PreviousIndex, BodyPartIndex: SizeInt;
begin
  Sound('menu_button');
  PreviousBodypart := ViewGame.CurrentCharacter.Inventory.Apparel[ApparelSlot].Data;
  ViewGame.CurrentCharacter.Inventory.DestroyEquippedItemSafe(ApparelSlot);
  BodyPartsList := GetItemsForSlot(ApparelSlot);
  if BodyPartsList.Count = 0 then
    raise Exception.CreateFmt('No items for slot %s', [ApparelSlotToStr(ApparelSlot)]);
  PreviousIndex := BodyPartsList.IndexOf(PreviousBodypart);
  repeat
    BodyPartIndex := Rnd.Random(BodyPartsList.Count);
  until (BodyPartsList.Count = 1) or (PreviousIndex <> BodyPartIndex);
  ViewGame.CurrentCharacter.Inventory.EquipApparel(TBodyPart.NewBodyPart(BodyPartsList[BodyPartIndex]));
  FreeAndNil(BodyPartsList);
  (ParentScreen as TCharacterScreen).UpdateCaptions;
end;

procedure TBodypartEdit.ClickNext(Sender: TObject);
var
  PreviousBodypart: TItemDataAbstract;
  BodyPartsList: TItemsDataAbstractList;
  BodyPartIndex: SizeInt;
begin
  Sound('menu_button');
  PreviousBodypart := ViewGame.CurrentCharacter.Inventory.Apparel[ApparelSlot].Data;
  ViewGame.CurrentCharacter.Inventory.DestroyEquippedItemSafe(ApparelSlot);
  BodyPartsList := GetItemsForSlot(ApparelSlot);
  if BodyPartsList.Count = 0 then
    raise Exception.CreateFmt('No items for slot %s', [ApparelSlotToStr(ApparelSlot)]);
  BodyPartIndex := BodyPartsList.IndexOf(PreviousBodypart);
  Inc(BodyPartIndex);
  if BodyPartIndex >= BodyPartsList.Count then
    BodyPartIndex := 0;
  ViewGame.CurrentCharacter.Inventory.EquipApparel(TBodyPart.NewBodyPart(BodyPartsList[BodyPartIndex]));
  FreeAndNil(BodyPartsList);
  (ParentScreen as TCharacterScreen).UpdateCaptions;
end;

procedure TBodypartEdit.ClickPrevious(Sender: TObject);
var
  PreviousBodypart: TItemDataAbstract;
  BodyPartsList: TItemsDataAbstractList;
  BodyPartIndex: SizeInt;
begin
  Sound('menu_button');
  PreviousBodypart := ViewGame.CurrentCharacter.Inventory.Apparel[ApparelSlot].Data;
  ViewGame.CurrentCharacter.Inventory.DestroyEquippedItemSafe(ApparelSlot);
  BodyPartsList := GetItemsForSlot(ApparelSlot);
  if BodyPartsList.Count = 0 then
    raise Exception.CreateFmt('No items for slot %s', [ApparelSlotToStr(ApparelSlot)]);
  BodyPartIndex := BodyPartsList.IndexOf(PreviousBodypart);
  Dec(BodyPartIndex);
  if BodyPartIndex < 0 then
    BodyPartIndex := Pred(BodyPartsList.Count);
  ViewGame.CurrentCharacter.Inventory.EquipApparel(TBodyPart.NewBodyPart(BodyPartsList[BodyPartIndex]));
  FreeAndNil(BodyPartsList);
  (ParentScreen as TCharacterScreen).UpdateCaptions;
end;

procedure TBodypartEdit.UpdateValues;
begin
  CaptionLabel.Caption := ViewGame.CurrentCharacter.Blueprint.BodySlotToHumanReadableString(ApparelSlot) + ' : ';
  ButtonRandom.Caption := ViewGame.CurrentCharacter.Inventory.Apparel[ApparelSlot].Data.DisplayName;
end;

constructor TBodypartEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ButtonPrevious := TCastleMobileButton.Create(Self);
  SetupButton(ButtonPrevious);
  ButtonPrevious.OnClick := @ClickPrevious;
  ButtonPrevious.Width := BodypartEditHeight;
  ButtonPrevious.Caption := '<<';
  HorizontalGroup.InsertFront(ButtonPrevious);

  ButtonRandom := TCastleMobileButton.Create(Self);
  SetupButton(ButtonRandom);
  ButtonRandom.OnClick := @ClickRandom;
  ButtonRandom.Width := 400;
  HorizontalGroup.InsertFront(ButtonRandom);

  ButtonNext := TCastleMobileButton.Create(Self);
  SetupButton(ButtonNext);
  ButtonNext.OnClick := @ClickNext;
  ButtonNext.Width := BodypartEditHeight;
  ButtonNext.Caption := '>>';
  HorizontalGroup.InsertFront(ButtonNext);
end;

{ TBodypartColor ---------------------------------------------------- }

procedure TBodypartColor.ClickRandom(Sender: TObject);
var
  OldColor: String;
begin
  Sound('menu_button');
  OldColor := ColorToHex(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag]);
  repeat
    ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag] := ViewGame.CurrentCharacter.Blueprint.ColorTags[ColorTag][Rnd.Random(ViewGame.CurrentCharacter.Blueprint.ColorTags[ColorTag].Count)];
  until (ViewGame.CurrentCharacter.Blueprint.ColorTags[ColorTag].Count = 1) or (OldColor <> ColorToHex(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag]));
  ViewGame.InvalidateInventory(nil);
  (ParentScreen as TCharacterScreen).UpdateCaptions;
end;

procedure TBodypartColor.UpdateValues;
begin
  CaptionLabel.Caption := ColorTag + ' : ';
  ButtonRandom.Caption := ColorToHex(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag]);
end;

constructor TBodypartColor.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ButtonRandom := TCastleMobileButton.Create(Self);
  SetupButton(ButtonRandom);
  ButtonRandom.OnClick := @ClickRandom;
  ButtonRandom.Width := 200;
  HorizontalGroup.InsertFront(ButtonRandom);
end;

{ TBodypartBoolean ---------------------------------------------------- }

procedure TBodypartBoolean.ClickToggle(Sender: TObject);
begin
  Sound('menu_button');
  Value^ := not Value^;
  ViewGame.InvalidateInventory(nil);
  (ParentScreen as TCharacterScreen).UpdateCaptions;
end;

procedure TBodypartBoolean.UpdateValues;

  function OnOff(const AValue: Boolean): String; // todo make StringUtils or similar
  begin
    if AValue then
      Exit('ON')
    else
      Exit('OFF');
  end;

begin
  CaptionLabel.Caption := Caption;
  ButtonToggle.Caption := OnOff(Value^);
end;

constructor TBodypartBoolean.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ButtonToggle := TCastleMobileButton.Create(Self);
  SetupButton(ButtonToggle);
  ButtonToggle.OnClick := @ClickToggle;
  ButtonToggle.Width := 100;
  HorizontalGroup.InsertFront(ButtonToggle);

  CaptionLabel.Width := 500;
end;

end.

