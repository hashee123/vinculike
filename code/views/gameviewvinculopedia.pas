{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ An in-game encyclopedia, containing info on monsters, items, objects, lore, etc. }
unit GameViewVinculopedia;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleControls, CastleUiControls, CastleKeysMouse,
  GameVinculopedia,
  GameVinculopediaPageAbstract, GameVinculopediaTab,
  GameViewAbstract;

type
  { core UI of vinculopedia }
  TViewVinculopedia = class(TViewAbstract)
  strict private
    Group1920x1080: TCastleUserInterface;
    CurrentTab: TVinculopediaTab;
    CurrentPage: TVinculopediaPageAbstract;
    VerticalGroupTopics: TCastleVerticalGroup;
    VerticalGroupPage: TCastleVerticalGroup;
    ScrollViewTopics: TCastleScrollView;
    ScrollViewPage: TCastleScrollView;
    HorizontalGroupTabs: TCastleHorizontalGroup;
    procedure ClickReturn(Sender: TObject);
    procedure ClickTab(Sender: TObject);
    procedure ClickTopic(Sender: TObject);
    { Fill in UI according to the situation }
    procedure FillInTabs;
    procedure FillInTopics;
    procedure FillInPage;
  public
    {$IFDEF Mobile}
    FirstFrame: Boolean;
    procedure BeforeRender; override;
    {$ENDIF}
    procedure Start; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
    constructor Create(AOwner: TComponent); override;
  end;

var
  ViewVinculopedia: TViewVinculopedia;

implementation
uses
  GameFonts, GameSounds, GameScreenEffect, GameUiUtils, GameColors,
  GameVinculopediaTopicButton, GameVinculopediaTabButton;

constructor TViewVinculopedia.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewvinculopedia.castle-user-interface';
  DesignPreload := false;
end;

procedure TViewVinculopedia.Start;
begin
  inherited;
  InterceptInput := true;

  TScreenEffect.Create(FreeAtStop).Inject(Self);

  Group1920x1080 := DesignedComponent('Group1920x1080') as TCastleUserInterface;

  (DesignedComponent('ButtonReturn') as TCastleButton).OnClick := @ClickReturn;
  (DesignedComponent('ButtonReturn') as TCastleButton).CustomFont := FontSoniano90;

  VerticalGroupTopics := DesignedComponent('VerticalGroupTopics') as TCastleVerticalGroup;
  VerticalGroupPage := DesignedComponent('VerticalGroupPage') as TCastleVerticalGroup;
  HorizontalGroupTabs := DesignedComponent('HorizontalGroupTabs') as TCastleHorizontalGroup;

  ScrollViewPage := DesignedComponent('ScrollViewPage') as TCastleScrollView;
  ScrollViewTopics := DesignedComponent('ScrollViewTopics') as TCastleScrollView;

  CurrentTab := nil; // to autoselect first tab
  FillInTabs;
  //FillInTopics; // both will happen automatically in the above
  //FillInPage;
  {$IFDEF Mobile}
  FirstFrame := true;
  {$ENDIF}
end;

{$IFDEF Mobile}
procedure TViewVinculopedia.BeforeRender;
begin
  inherited BeforeRender;
  if FirstFrame then
  begin
    CurrentTab := Vinculopedia.Tabs[0];
    FillInTabs;
    FirstFrame := false;
  end;
end;
{$ENDIF}

function TViewVinculopedia.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Event.IsKey(keyEscape) then
    ClickReturn(Self);

  if Event.IsKey(keyF5) or Event.IsKey(keyF11) then
    Result := false; // let window.press handle
end;

procedure TViewVinculopedia.ClickReturn(Sender: TObject);
begin
  Sound('menu_back');
  Container.PopView(Self);
end;

procedure TViewVinculopedia.ClickTab(Sender: TObject);
begin
  if not (Sender is TVinculopediaTabButton) then
    raise Exception.Create('not Sender is TVinculopediaTabButton');
  Sound('vinculopedia_tab');
  CurrentTab := TVinculopediaTabButton(Sender).Tab;
  FillInTabs; // optimize;
end;

procedure TViewVinculopedia.ClickTopic(Sender: TObject);
begin
  if not (Sender is TVinculopediaTopicButton) then
    raise Exception.Create('not Sender is TVinculopediaTopicButton');
  Sound('vinculopedia_topic');
  CurrentPage := TVinculopediaTopicButton(Sender).Page;
  FillInTopics; // todo: optimize
end;

procedure TViewVinculopedia.FillInTabs;
const
  WindowMargin = 30;
var
  B: TVinculopediaTabButton;
  Tab: TVinculopediaTab;
begin
  HorizontalGroupTabs.ClearAndFreeControls;
  for Tab in Vinculopedia.Tabs do
  begin
    B := TVinculopediaTabButton.Create(HorizontalGroupTabs);
    B.Tab := Tab;
    if CurrentTab = nil then
      CurrentTab := Tab;
    B.Selected := CurrentTab = B.Tab;
    B.Setup;
    B.Width := ((Group1920x1080.EffectiveWidth - 2 * WindowMargin + HorizontalGroupTabs.Spacing) / Vinculopedia.Tabs.Count - HorizontalGroupTabs.Spacing);
    B.Caption := Tab.Caption;
    B.Enabled := true;
    if not B.Selected then
      B.OnClick := @ClickTab;
    HorizontalGroupTabs.InsertFront(B);
  end;
  CurrentPage := nil;
  ScrollViewTopics.Scroll := 0;
  FillInTopics;
end;

procedure TViewVinculopedia.FillInTopics;
var
  I: Integer;
  B: TVinculopediaTopicButton;
begin
  VerticalGroupTopics.ClearAndFreeControls;
  for I := 0 to Pred(Vinculopedia.Pages.Count) do
    if Vinculopedia.Pages[I].Tab = CurrentTab.Id then
    begin
      B := TVinculopediaTopicButton.Create(VerticalGroupTopics);
      B.Page := Vinculopedia.Pages[I];
      B.Selected := CurrentPage = B.Page;
      B.Setup;
      if Vinculopedia.Pages[I].IsUnlocked then
      begin
        if CurrentPage = nil then
        begin
          CurrentPage := Vinculopedia.Pages[I];
          B.Selected := true; // Workaround: because for some reason B.Setup after this block hides Caption, TODO: proper fix
          B.Setup;
        end;
        B.Caption := Vinculopedia.Pages[I].Caption;
        B.CustomTextColor := Vinculopedia.Pages[I].TopicTextColor;
        if Vinculopedia.Pages[I].Image <> nil then
        begin
          B.Image.DrawableImage := Vinculopedia.Pages[I].Image;
          B.Image.OwnsDrawableImage := false;
          B.ImageScale := (B.Height - 20) / B.Image.DrawableImage.Height;
        end;
        B.Enabled := true;
        if not B.Selected then
          B.OnClick := @ClickTopic;
      end else
      begin
        B.Caption := '???????';
        B.CustomTextColor := ColorVinculopediaLocked;
        B.Enabled := false;
      end;
      VerticalGroupTopics.InsertFront(B);
    end;
  ScrollViewPage.Scroll := 0;
  FillInPage;
end;

procedure TViewVinculopedia.FillInPage;
begin
  VerticalGroupPage.ClearAndFreeControls;
  if CurrentPage <> nil then
    CurrentPage.InsertEntry(VerticalGroupPage);
end;

end.

