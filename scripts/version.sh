#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

#!/bin/bash
script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$script_path"

version=$(cat version_base.inc)$(git rev-list --count HEAD)
echo \'$version\' >../code/generated/version.inc
echo \<version value=\"$version\" \/\> >../code/generated/version.xml
echo $version
