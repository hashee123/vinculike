#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export PROJECT_PATH=GIT/vinculike
export CASTLE_ENGINE_PATH=/home/castle_game_engine

./home/${PROJECT_PATH}/scripts/dockerstart.sh

(cd /home/${PROJECT_PATH}/; ./scripts/purgecore.sh; rm -f *.dll)

echo 'Building Android:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --target=android --mode=release --package-format=android-apk --verbose >/home/${PROJECT_PATH}/build_android.log
echo 'Building Debian64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=linux --cpu=x86_64 --mode=release --package-format=deb --verbose >/home/${PROJECT_PATH}/build_deb.log
echo 'Building Linux64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=linux --cpu=x86_64 --mode=release --verbose >/home/${PROJECT_PATH}/build_linux.log
#echo 'Building Win32:'
#castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=win32 --cpu=i386 --mode=release --verbose >/home/${PROJECT_PATH}/build_win32.log
echo 'Building Win64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=win64 --cpu=x86_64 --mode=release --verbose >/home/${PROJECT_PATH}/build_win64.log

(cd /home/${PROJECT_PATH}/; ./scripts/purgecore.sh; rm -f *.dll)
echo 'Done.'

# After all builds and operations on the container finished:
# docker container stop cge
