#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# This is a script to batch-build this game using Castle Game Engine Docker image
# see https://castle-engine.io/docker for details
# Note that Docker images contain a Debian operation system and all the necessary tools and therefore are large
# Currently it has size of approximately 10 Gb.
# Make sure you have this space on your system drive, moving Docker images to a different drive is possible but complicated

# update Docker image by
# docker pull kambi/castle-engine-cloud-builds-tools:cge-none-fpc331
# (without CGE, use a local copy in $CASTLE_ENGINE_PATH)

# First start the Docker image by
# docker run -d -i --name=cge -v <<Local working folder which will become "home">>:/home/ kambi/castle-engine-cloud-builds-tools:cge-none-fpc331 bash
# then
# docker container exec cge sh /home/<<project folder related to Local working folder>>/dockerbuild.sh

# The folders below are system-specific, you'll need to adjust them for your setup
export PROJECT_PATH=GIT/vinculike
export CASTLE_ENGINE_PATH=/home/castle_game_engine

export FPCLAZARUS_VERSION=3.3.1
export PATH=/usr/local/fpclazarus/${FPCLAZARUS_VERSION}/fpc/bin/:${PATH}
FPCLAZARUS_REAL_VERSION=`fpc -iV`
echo 'Real FPC version:' ${FPCLAZARUS_REAL_VERSION}
export FPCDIR=/usr/local/fpclazarus/${FPCLAZARUS_VERSION}/fpc/lib/fpc/${FPCLAZARUS_REAL_VERSION}/

export HOME=/home/
echo 'Building CGE build tool:'
cd "$CASTLE_ENGINE_PATH"
rm -f ./tools/build-tool/castle-engine
./tools/build-tool/castle-engine_compile.sh
cp tools/build-tool/castle-engine /usr/local/bin/
castle-engine --version
