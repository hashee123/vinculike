---------------5542
WIPs:
* Body blueprints: way to define species and special body features (WIP)
* Shapepose: a way to define shape and pose of the actor
* Very WIP: character editor; very WIP UI: clicking on a specific bodypart or color will change it to something random if possible; During customization the character is automatically in nude mode

Body customizations and poses:

* New way of handling items/bodyparts images: each can have multiple associated images
* 28 noses variants (all that we'll ever need, I guess :))
* Support for automatic colorization, colorization by color tag
* Random color variants: 7 base colors, 7 belly colors, Hands/feet nails colors - 33 variants, Tattoo colors - 3 variants, 72 hair colors

Bigger features:

* More dynamic combat (Player character and monsters land hits quicker)
* Automatic time acceleration for longer tasks (repairing, disarming traps, equipping, etc) in case the character is not in danger
* Rebalance resting: now it is possible to fully restore all stats while resting. Resting starts slower, but is not capped at top.
* Rework of the inventory UI
* New kind of enemy movement: ghost (phase through walls)
* New shooting behavior: shoot at random angles (used by Serial Shooter and Panty Thief)
* New enemy type: Ecto Ghost - mostly useless against a dressed target, but will disarm and spit glue over a fully nude one. Spawns at lvl.17, patrols at lvl.20.
* 2 new restraints: Ecto Glue stick to hands (prevents doing anything complex until removed) or eyes (limits vision but not as much as a blindfold).
* New item: Glasses of Escape : reduces effort needed to escape restraints by 50%
* New enemy type: Panty Thief - Extremely efficient at stealing underwear, becomes absolutely useless if target is wearing overwear though. Quite capable against nude targets. Projectiles can also pick up underwear from the ground. Spawns at lvl.16, patrols at lvl.19.
* New enemy type: Flying Underwear (3 variants). When stolen by Panty Thief the underwear becomes self-aware and flees previous owner. It'll take some skill to catch it.
* New enemy type: Tickle Thief, similar to regular tickling enemies, but steals items instead of dropping them to the ground, preventing easy reequipping, and has a higher chance to equip restraints. Spawns at level 18, patrols at level 23.
* New enemy type: Dandelion. An average danger shooter, shoots in all directions around it, doing a minor damage per shot but releasing multiple bullets. Spawns at level 12, patrols at level 19.
* 2 New enemy types: Tickling Mess that spawns Small Ticklers. The latter are very weak variants of tickling slimes, however have a very high chance to equip bondage mittens.
* 2 New enemy types: Slime Mould that spawns Small Slimes. What they lack in strength small slimes make up in numbers.
* New enemy type: Veins Binder. Very similar to Binder Slime and Binder Aspheroid, but uses "Living Ropes" to tie up the character.
* 6 new restraints: Living Ropes. Very similar to regular ropes, but slowly regenerate durability with time (2 new enchantments).

Smaller features:

* Significantly reduce noise of overwear (by 20%-40%)
* Chastity underwear is less noisy (2.5 -> 1.7) and higher protection (3% -> 8%)
* Compress images (significantly reduce RAM and VRAM usage, increase render speed)
* Render paperdoll on GPU (significantly increase paperdoll rendering speed, support for colorization)
* Tickling aspheroid now spawns at level 22, patrols at level 24
* 2 mew map kinds: Checkers3x3 and Checkers4x4
* Option to censor only in screenshots
* Option to censor per-character (cannot be edited in the UI yet)
* Patrols go to a small area, not perfectly to the same spot on the map
* Monster go to investigate points near the target, not perfectly at the location of the noise
* Wisps are now using ghost-movement
* New map kind: ThickWalls
* New enchantment: StruggleAgainstRestraintsMultiplier (not used yet)
* Twinkles are now "rare" monsters (can't be the only monster kind on the map)
* ~~New lore entry in Vinculopedia~~ (WIP)
* Larger "go to map" buttons in Debug UI; Add "Restart Current Map" at the top of the list
* Significantly increase Sleeping Blindfold resting bonus: 2x -> 7x
* Vacuum stealer shoots first shot faster (prevent too easy kill even unarmed)
* FootwearDamageMultiplier = 0.1 at max difficulty, 0 at easy
* Increase vacuum stealer trap suspension duration 1.0 -> 1.5 seconds
* Better order of player characters rendering: first captured, then free, then current
* Stop spawning "ghost_cloth" and "jack_o_lantern"
* Significantly reduce binder enemies clothes damage.
* Toggle inventory mode button
* Increase pincher top naked vision 13->19
* Improve some combat logs
* Reduce Amulet of Protection noise 1.1 -> 0.1 and increase damage absorption 0.5 -> 0.8
* Antigrav Guard starts attacking much faster (1.7 -> 1.0 seconds delay) but cools down longer (4.8 -> 5.5)
* Some fixes in Vinculopedia articles
* Improve map sprites for torso, spirals
* Proper scrollbars theme

Bugfixes:

* "Garbage Collector" for safer management of memory - potentially should prevent some crashes (but in not too tested state may actually cause some ;)) and add a bit more "freedom" in what can be done in actions (e.g. action now can safely change action of the Parent)
* Bugfix: diagonal movement now always two-directional (finally!)
* Bugfix: fix some crashes when trying to report errors
* Bugfix: blindfolds properly marked as "UnequipDisintegrates"
* Bugfix: Traps no longer create "danger" state
* Bugfix: workaround occasional crash after zero g suspension22
* Bugfix: potential crash when attacking enemies blindfolded
* Bugfix: occasionally character failed to attack monster when blindfolded
* Bugfix: memory leak in case binder slime failed to apply restraints
* Bugfix: safer objects disposal, in fact hypothetically can crash more often - but will now be more specific about reasons, so those can be fixed easier
* Bugfix: Roleplayer always wakes up the target
* Bugfix: Roleplayer now will properly show a log message and give XP when kills itself by critical failure roll
* Bugfix: show "attack" description for projectile Zero-G
* Bugfix: empty patrol waves no longer possible
* Workaround: sometimes unable to attack 2x2 or smaller enemy when it stands too close to a wall
* Bugfix: avoid taking several screenshots if several views are active
* Bugfix: vacuum cleaner revert to simpler behavior, will not steal all items regardless of what was equipped
* Bugfix: steal to chest attack now steals to random chests, not always to the same one
* Bugfix: binder enemies now also consider hands bound as condition to fully immobilize the character.
* Bugfix: missing scrollbar when inventory is in compact mode
* Bugfix: crash when unequipping item and it was stolen/broken by a monster
* Fix "Barrel" object ordering in the middle of traps in vinculopedia

* Cancel CI/CD, unfortunately they've stopped working due to FPC bug which is fixed in newer version not available for CI/CD easily

# 0.55.5074
* Fix rare crash with vacuum cleaner new behavior
* Lower vision penalty for ghost cloth (-16 -> -12), slightly higher noise damping (70% -> 75%)

# 0.55.5070
* 3 new tilesets
* Change a letter in mimics' names
* Significant improvement of vacuum cleaners behavior
* Fix nude top spirals incorrectly marked as covering top
* Fix metal chest mimic name

# 0.55.5056
* Add vinculopedia entries for Jack'o'Lantern and Ghost Cloth
* Weapon renders on top of glasses (slightly breaks Sage Scarf, but fixes Ghost Cloth)
* Increase ghost cloth stealthiness (40% -> 70%)
* Spirals decrease resting quality, not increase it

# 0.55.5049
* New monster: Roleplayer. Deals 6d6 damage, rolls 1d20 for accuracy (1 - critical failure, 2-4 - miss, 5-19 - hit, 20 - critical success)
* New monster: Vacuum Stealer. Spawned from Stealer Trap, picks up items on the ground and uses ranged attacks.
* New trap: Stealer Trap - unequips and scatters around all character's items and sends a few Vacuum Stealers to pick those up.
* New items set (6 items) - spirals. Significantly increase damage but are almost incompatible with most other items.
* New item (temporary?) - Ghost Cloth, specifically for Halloween
* New item (temporary?) - Jack'o'Lantern, specifically for Halloween
* 2 new map patterns: Boxes11x11 (loose and regular) and Basic 4x4 (tight and cramped)
* Timeout before going to the next map is reduced to 3 seconds (was 4s)
* Return chance to avoid explored areas for curious monsters 30% -> 50%
* Straiter patrols sooner (28 -> 24) serial shooter too (24 -> 21)
* Reduce Sage Scarf max durability 120 -> 88
* Increase quarreler damage (roughly +40%)
* Reduce neckerchief durability from 24 -> 14
* Reduce rope durability, roughly 2x
* Catsuits spawn on deeper levels (tight 9->15; loose 8->9)
* Neckerchiefs increase inventory noise by 30%
* More readable "for all" symbol on the collar
* Fix occasional crash after the heroine was defeated
* Fix rare crash after resetting the game
* Fix "Cobalt" tileset shimmering on Desktop
* Fix TEnchantmentDamageBonus calculation when character was not nude
* Fix crash when struggling against restraints and those get destroyed by external event

# 0.54.4964
* New monster: Quarreler
* New restraint item: Neckerchief variants (9 colors)
* Reduce medical rack and its components durability (roughly by 50%)
* Significantly reduce durability of nude wristbands/anklets (roughly by 50%)
* Add missing particle effects to wisp, straiter, vibration attacks
* Reduce Sage Scarf noise 3.1 -> 1.3
* Safer inventory buttons update (hopefully workaround some crashes)
* Noise multiplier enchantment now is applied only to equipped items, "additions" are ignored
* Better description of noise multiplier enchantment

# 0.53.4925
* On higher difficulty levels the deeper dungeon levels can have more than one prisoner (1-5)
* Fix Whipper not waking up player character on attack
* Fix forgotten map never remembered if game is saved and then loaded while the player character is disoriented
* Fix locked sleeping bag not having stamina requirement to wake up
* Fix a few log messages
* Fix footwear never wearing off completely (now have a chance of breaking if max durability is below 10)
* Fix bondage footwear wearing off and dropping to the ground even if it should have disintegrated
* Fix potential problem with Medicus unequipping restraints in sleep
* Fix one gluebeetle capture story not fitting on screen sometimes
* Fix tickling slime mentioning "exhausted" character while the character may be simply unlucky :)
* Fix prisoners on one level can be max+1 instead of max
* Teleportation and Disrobing trap will now not steal footwear
* Vacuum cleaner will not steal footwear if some body slots were equipped, otherwise will steal weapon, otherwise - everything that can be stolen
* Reduce catsuits durability (especially of locked ones)
* Reduce gluebeetle's glue durability (approximately by 25%)
* Reduce straitjacket durability (by 60%)
* Reduce curious AI chance of prioritizing unexplored areas (50% -> 30%)
* Options: New Run will also resort prisoners on different map levels to cut their numbers on the same level

# 0.53.4898
* New monster: Whipper (spawns at lvl.11 -> patrols at lvl.14)
* Avoid placing the new prisoner on the same level if there are already captured characters here, new prisoner will be moved to earlier levels instead
* Fix stealth attacks sometimes stuck in place + a few more bugs, mainly with blindfolds
* Fix freeze when Wrapper attacked armed player
* Shoes no longer disintegrate when wear off, just drop on the ground broken
* Curious monsters now prioritize unexplored areas
* Significantly nerf Will'o'wisp monster
* More "particles notifications" for some effects applied to the player character
* Debug UI: visualize current character's navigation grid, make colliders not as opaque

# 0.52.4871
* 5 new items: Sleeping Bag, Locked Sleeping Bag, 3 variants of Mummy Wrappings (top, bottom and head)
* New monster: Wrapper (wraps into wrappings, starts spawning at lvl.14, patrols at lvl.17)
* Increase White Slime/Aspheroid attack cooldown
* Reduce catsuits spawn frequencey 0.7 -> 0.4
* Some internal improvements (legs are now separate from the torso, 5 new (yet unused) enchantments, etc.)

# 0.51.4832
* Nerf gluescarab: can't change action, lower homing, slower warmup
* Fix gluescarab/gluebeetle timeout calculation
* Manacler doesn't attack in sleep if character sleeps nude
* Workaround for ramming monsters (manacler, ram, gluebeetle) sometimes failing to attack in sleep
* Fix potential crash in CatchAndHold action
* Reduce stun shooter damage to clothes 3->1

# 0.51.4825
* 2 new items: Catsuits - in several forms: skin-tight catsuit, tight catsuit half unzipped, tight catsuit fully unzipped, tight catsuit zipper jammed, loose catsuit, loose catsuit broken, loose catsuit glued
* 1 new item: a matching pair of boots for catsuit - Strappy Boots
* 1 easter egg item ;)
* Vibrapawn has 20% chance of breaking the catsuit
* Living suit can repair catsuits, but has a 20% chance to lock them into a restraint item
* Full Suit is no longer a normal item and doesn't spawn on the map - now only used as a restraint
* Better log summary when character loses top/bottom or both
* Reduce Straiter health 60 -> 32.8 and increase XP reward
* Improvements to Medicus behavior: will now never force the target into captured state, but will either leave alone when the stats are low (if the target wasn't resisting) or will leave unconscious when the stats go below zero (if the target was resisting)
* Reduce web shooter damage to clothes 3 -> 1
* Fix tickling slime treating feet restraints as clothes
* Tickling slime plays sound when equipping/repairing bondage mittens in sleep
* Fix Pincher log message when repairing blindfold in sleep
* Fix Hooks traps and Bug shooters breaking unrepairable items makign them completely unusable
* Fix white border on some glue images
* Show stealth circles only when stealithing (enemies haven't noticed yet)
* When returning to the settlement a random character will be selected

# 0.51.4733
* Improve homing projectiles behavior
* Fix rare freeze in Dream Catcher behavior
* Rebalance Straiter: sees dressed target better, much slower, doesn't remove underwear
* Fix incorrect log message when Holder tries to catch character but she is already held
* Slightly slower Holder attack speed
* Slower Vibrapawn attack speed
* Fix gluebeetle undressing heroine in sleep marked as "forced"
* Slightly reduce Tickling Aspheroid health, a tiny bit increase Tickling Guard health
* Fix Pincer & Pincher map images
* Better armlet map sprite
* Rename Splittop into Velvet Top

# 0.51.4707
* New monster: Vibrapawn
* 2 new items: Top and Bottom Vibrators
* 3 new items: Armlets
* Items can affect wearer continuously
* Items can "throw marks" at the wearer
* Items can play looping sound
* Increase Trapper visibility
* Dream catcher now offers some healing if the heroine is wearing Blindfold of Lethargy
* Increase damage while wearing straitjacket from 1 to 3
* Reduce Straiter stamina damage
* Fix log when Tickle Slime strips target after catching by bondage item
* Fix collar slot not properly assigned to upper body
* Even better fix when attacking a group of monsters and there are non-aggressive among them (like Vacuum Cleaner or Trapper)

# 0.50.4638
* New trap: Sandpaper Trap (damages worn items)
* Better fix for misclicks when attacking a group of monsters and there's a chest/vacuum cleaner around
* Chance to resist straitjacket if not immobilized
* A few additional beneficial enchantments for Amulet of Freedom
* Fix a few vinculopedia entries

# 0.50.4622
* New monster: Straiter (spawns at lvl.19 -> patrols at lvl.28)
* New item: Straitjacket (extremely dangerous restraint)
* Dream Catcher can very rarely spawn a Tickling Slime (4%) or Straiter (1%)
* Improve render order - now chests and traps will always render under the monsters and will also not steal clicks from those
* Some blindfolds also prevent from waking up with low stamina
* Unique sounds for Dream Catcher, Serial Shooter and Antigrav Guard

# 0.49.4593
* Hotfix for shooters behavior
* Nerf Serial Shooter significantly
* Projectiles now fly only for a predefined range and disappear

# 0.49.4585
* New monster: Antigrav Guard
* New monster: Serial Shooter
* New monster: Holder
* New monster: Dream Catcher
* New item: Blindfold of Lethargy (new enchantment: prevents from waking up)
* New item: Amulet of Stripper
* Monsters can now shoot multiple projectiles in a burst
* Monsters now will try to surround the heroine
* Better Sleeping Blindfold image
* Improve Amulet of Nudist enchantments
* New attack mark for high-damage monsters
* Pincher won't steal glasses in sleep, but will just unequip those
* Pincher will treat any blindfold as acceptable and wont unequip/steal it
* Blindfolds increase damage from embarrassment (like getting undressed or touched naked)
* Always heal characters in the settlement
* Workaround a bug when monsters failed to attack the target and became confused under some circumstances
* Reduce Wisp health from 5 down to 3.4 - so that it can be killed in one hit even by a moderately restrained heroine
* Fix some enchantments showing incorrect strength values
* Fix rare crash when showing logs during closing the game
* Fix UI bug with "Unconscious" action
* Fix bugs in wisps behavior when attacking restrained target
* Contributing guidelines and compilation instructions

# 0.48.4473
* New item: Amulet of Nudist (protects against disrobing and other nudity-related damage)
* New item: Amulet of Rogue (increases stealth damage)
* New item: Kukri Knife (low damage but high stealth damage)
* 5 new possible enchantments (used by the items above :))
* More diverse notifications when walking nude (including when it doesn't hurt)
* Gluebeetle now retreats further with stolen clothes
* Rename "Special Effects" to "VFX" in Options
* Rebalance webbing projectiles a bit (twice lower durability, but thrice higher stacking)

# 0.48.4450
* Fix bug when character private parts covered status was not always calculated correctly
* Fix bug when Twinkle considered target dressed after stripping it naked in sleep
* Revert fix for "rendering sage scarf over weapon" - needs a different fix, which is in process
* Slightly nerf Medicus - reduce attack range

# 0.48.4440
* Better handling of monsters unequipping restraints (like Medicus or Gluebeetle)
* Bigger click margin for buttons
* Hooks Trap and Disrobing Trap no longer do heavy damage to nude targets, just drain stamina a bit
* Slightly increase vision radius when blindfolded (to avoid it messing with navigation)
* Fix several crashes when some objects tried to teleport outside the map
* Fix bug when Sage Scarf was rendering over weapon
* More consistent handling of speed when player is blocked by monsters
* Significantly reduce durability of Pot Mittens, Cloth and Leather Blindfolds
* Blindfold of sleeping slightly disorients the Player character
* Rename "Vegebeasts" vinculopedia tab to "Monsters" (shorter :D - I'll soon need a new tab and it won't fit otherwise)

# 0.48.4397
* New item: Blindfold of Sleeping adds +200% to resting quality
* Other blindfolds also add around +50% to resting quality
* Blindfolds reduce chance of resisting some attacks (by Living Suit, Twinkle and Tickling Slimes)
* Pincher treats any blindfold as a valid blindfold
* Fix music in EndGame screen (again :))
* Highlighting only currently selected character

# 0.48.4378
* Difficulty levels! The game is mostly tested on Extreme difficulty by me, but now (much) easier options are available
* After rescuing characters are exhausted and are not available for the next run for this day
* Short stories in log when characters enter the settlement (just a poorly done placeholder for now)
* Show "how many hits does this monster take to kill"
* Vacuum Cleaner and Mimic now also stop chasing the player character after some time
* Significantly reduce the number of guards at prisoners
* Increased disrobing willpower damage, but reduced stat degradation from it
* Reduce damage to willpower for walking naked by 50%
* Fix EndGame screen behavior - it was showing wrong character's name also showing wrong log message
* Fix bug when walking barefoot or nude could incapacitate and capture the character
* Fix character suffering willpower damage after blacking out
* Fix gluebeetle's projectiles behavior when multiple hit the player at once
* Fix gluebeetle interaction with sleeping target
* Fix some actions being performed even on pause (stealth attacks, wisps blinking, sometimes wardrobe malfunction happened without any movement, etc.)
* Fix grabber interaction with sleeping target
* Fix bug when sometimes the capture reason is logged several times
* Softer sound for stealing items in sleep
* Add missing sounds to some monsters attacks under some rare conditions
* Debug UI: Menu to go to a specific map level (replaces Next Map, Restart Map and Return Home)

# 0.47.4297
* Split gluebeetles into (weaker) Gluebeetle (starts spawning at lvl.13) and (stronger) Gluescarab (starts spawning at lvl.18)
* A bit less strict checks on inventory operations

# 0.47.4292
* Minion monsters (only wisps for now) can despawn after some time: wisps now live 10-20 seconds. Fixes issue when in a bad luck those can overwhelm the player character completely.
* Workaround issue when inventory buttons were not always in ready state when pressed (and game crashed, now it will just show an error message - it's not a proper fix yet)
* Significantly reduce disrobing damage to willpower (to counter current penalties for walking around nude)
* New map type: Circles 12x12
* 2 new tilesets
* Fix wisps sometimes being "unsuspecting" even when actively attacking player character
* Fix inability to change action on an item (e.g. equip the item while in process of repairing it)
* Randomize notifications delays, avoid showing them simultaneously
* Thief now will play attack sound even when steals items in sleep
* Small fixes to vinculopedia
* Small fixes to UI buttons placement
* Fix binder slime still reinforcing knots if the character was sleeping
* Upgrade Castle Engine: fix several potential issues on some ATI Radeon drivers for Windows.

# 0.47.4262
* Hotfix captured characters walking around the map.

# 0.47.4260
* Characters now walk randomly around the settlement and chat (or at least try to)
* New music for the settlement in case there are 7 or more character in there
* New music for "captured" scene
* Walking in the settlement doesn't hurt feet
* On a bigger scale: player's characters now also have AI - to do much more in future
* Increase stun chance for stun shooter 50% -> 60%
* Tickling aspheroid/guard have a higher chance to equip bondage mittens

# 0.46.4218
* On mobile devices tile size is 11, on desktop platforms - 32
* Automatically rebuild map tileset if tile size changed
* Deterministic tileset generation
* Binder slime will no longer use advantage over sleeping target
* Improvements to vinculopedia entries

# 0.46.4209
* Walking around nude damages willpower (0.3 multiplier if only top nude; 0.6 multiplier if only bottom nude)
* Being nude affects willpower regeneration less (25% less for nude top and 30% less for nude bottom)
* Dropping items now properly checks for walls (won't drop items behind the wall)
* Play hurt scream if damage > 25% of remaining health
* Rework movement and roll logic (fix or workaround multiple issues)
* Movement without pathfinding (automatically triggers when blindfolded)
* Visualize visible range for monsters depending on their sensory organs, not just for player character
* New trap kind: Traps Spawner
* Fix bug with traps overlapping
* Fix bug with possible incorrect characters actions or coordinates when changing map
* Fix bug when two actors tried to interact with the same item (e.g. player character and vacuum cleaner)
* Fix bug when immobilized actor could be reset to idle by some attacks
* Fix crash when monsters could spawn outside of the map area
* Fix sometimes absorbed damage shown as 0.0 (if too low)
* Fix crash when starting a new game
* Fix potential crash when loading "rescue" action
* Fix not saving map extrema in save game
* Debug UI: Spawn a trap
* Option to have on-screen clock
* Toggle FPS label is now in Options, not Debug UI
* Projectile hurt now shows damage in vinculopedia
* More general treatment of projectiles targets
* Grabber will no longer attack in sleep dealing double damage
* Better collision detection for projectiles
* Better screenshots filename
* Better log for map level when loading the game

# 0.45.4100
* Fix crash when clothes were broken by monster damage
* Vinculopedia: tip about white slime/aspheroid

# 0.45.4097
* Vinculopedia page: Tips & Tricks
* Vinculopedia page: Feedback
* Fixes to old vinculopedia pages + 2 missing pages for new footwear
* Fix updating inventory: equipping/unequipping blindfolds now works immediately
* Optimize updating inventory
* Fix trap can spawn too close to entrance
* Faster gluebeetle attack

# 0.45.4064
* Save/Load monsters AI states
* Big refactor for monsters behavior/marks - now they properly load their relevant data
* Remove "monsters stats" from monster report on the map
* Show information on monsters spawn/patrol level and frequency in Vinculopedia
* Show more information on some monsters actions in Vinculopedia
* Reduce gluebeetle danger level 4 -> 3
* Reduce Sage Scarf spawn frequency 100% -> 40%

# 0.45.4021
* Fix for Gluebeetle, Ram and manacler behavior

# 0.45.4011
* New footwear: High Sandals and Ballet Flats
* Reduce footwear damage absorption (roughly 2x)
* Increased Sage Scarf damage absorption 5% -> 10%
* Shadow Thief puts chests in extrema, not just at random

# 0.45.3996
* Debug UI: Add a specific item
* Debug UI: spawn specific monster
* Fix incorrect logs for Shadow Thief
* Shadow Thief will not create chests if there are still chests on the map
* Linked manacles no longer force to drop weapon (but prevent from picking anything up)
* Medicus and Archimedicus have some small nude vision bonus
* Medicus and Archimedicus do less damage (makes viable to use them for healing)
* Nerf pincer a bit (+60 ms to evade the attack)
* Fix cape rendering over dress on map sprite
* Fix collar rendering over sage scarf
* Improve all scrollbars performance
* Upgrade CGE: Use dedicated GPU if available
* Upgrade CGE: non-ASCII bug with FreeType library is fixed now

# 0.45.3970
* New monster: Shadow Thief with its assets and behavior (spawns at lvl.11 -> patrols at lvl.14)
* New item: Linked Manacles (restraint)
* Add Vinculopedia description for Gluebeetle action
* Add particles for Manacler attack
* Grabber now will not pull shoulder slot
* Slightly higher diversity of monsters kinds on level (7 -> 12)

# 0.45.3941
* New monster: Gluebeetle with its assets and behavior
* New items set: Honey Glue (restraints, 8 variants)
* New end-game: Glued (3 variants)
* Reduce hearing bonus of all-hearing glasses to 20 (was 40)
* Add X-Ray vision to Sage Scarf
* Small fix to amulet sprite
* Increase tickling aspheroid danger level (will spawn in smaller quantities)
* Tried to improve rolling monsters behavior - sometimes trying to roll/shoot through walls

# 0.44.3909
* New item: Amulet of Protection (absorbs 50% done to the chest)
* New item: Sage Scarf (blindfolds player but doubles damage and adds other buffs, especially if the character is naked)
* Less monsters kinds per dungeon level (more often "narrowly specialized" levels)
* Add missing vinculopedia entries for new monsters and items
* Fix wording in some vinculopedia entries
* Fix bug when monster equipped a restraint item, but player was in the process of equipping/unequipping a normal item, and the restraint would get unequipped
* Nerf pincer a bit
* Pincers now have curious AI
* Set proper depth for glasses spawning

# 0.43.3879
* 3 new items: X-Ray Glasses, All-Hearing Glasses and Glasses of Sharp Sight
* 2 new bondage items: Cloth Blindfold, Leather Blindfold
* 2 new monsters: Pincer (spawns at lvl.8), Pincher (spawns at lvl.19)
* Make 2 variants of Manacler: Rusty Manacler (now spawns at lvl.10) and Shiny Manacler (spawns at lvl.21)
* Fix map blinking blank for a frame in some situations
* Trapper is now significantly faster

# 0.43.3814
* Hotfix for roll action
* Vinculopedia page about Debug UI

# 0.43.3806
* New monster: Manacler with its assets and behavior (spawns at lvl.8 patrols at lvl.22, curious and rare)
* New items set: Wristbands and Anklets (restraints)
* New trap: Zero g Trap with its assets and behavior
* New items: Slant Top and Slant Skirt
* Rework some traps images: Alarm, Teleport and Straps
* Map sprites for most items that were missing those (ropes, medical rack, web, stardust, collar, amulets, etc)
* Fix resting balance
* Fix several minor bugs in action roll and derivatives
* Fix Ram behavior bug that forced it to forget target after attacking
* Fix potential crash in Ram behavior
* Fix bug with rendering multi-slot items
* Fix calculating of main slot of the multi-slot item
* Fix handling transparency of items when drawing paperdoll or sprites
* Fix enchantments for stardust
* A bit less monsters near the entrance

# 0.42.3690
* Walking barefoot hurts, otherwise footwear takes damage
* Some fixes to Vinculopedia articles
* Remove "small hammer" duplicate entry
* Significantly increase idle timeouts (monsters are less active now)
* Rope harness on feet also increases roll stamina cost, not only reduces speed
* Tickling sleep attack is a bit more aggressive now: amulets and other secondary items won't protect against getting feet mittens

# 0.42.3640
* Vinculopedia with unlockable pages (replaces "How to Play" and offers much more)
* Stats system for unlocking vinculopedia pages/entries and achievements
* Rename Vital, Vigor, Psyche -> Health, Stamina, Willpower
* Better AI handling of idle movements
* Better (rollover) buttons in inventory and characters selection
* Fix unconscious character not treated as helpless
* Binders, Living Suits, Twinkles, Tickling and Bug shooters can take advantage of immobilized (stunned) target
* Monsters are more frequently on soft-guard than on guard (will not always return to the guard point)
* Fix traps firing at imprisoned characters
* Fix compact inventory button size
* Fix memory leak in AI of some monsters
* Fix trapper has zero damage
* Fix trapper AI behavior
* Trapper has 100% chance to put a trap (was 50%)
* Trapper properly checks collisions with other traps or player characters
* Reduce trapper noise
* Thief slime doesn't wake up character if there is nothing to steal
* More consistent display of logs in case of small changes of player character's stats
* A bit longer shot delay for Stun Shooter and reduce its health
* Fix stun shooter's mark not marked as independent
* Hooks Trap, Disrobing Trap and Teleport Trap/Vacuum Cleaner do not remove amulets
* Workaround some monsters (grabber, tickling, ram, bug shooter, etc) sometimes not waking up the character properly after a sleep attack
* Amulet of berserk works if health is <50% (was <33%)
* Fix feet mittens reducing damage instead of speed
* Debug UI: Add newbie
* Debug UI: Visualize extrema points (dungeon generation logic elements)

# 0.41.3366
* Set trapper spawn frequency to 10%

# 0.41.3362
* Refactor: Action now can have unique data
* Refactor: Marks now can have unique data
* Refactor: Monsters now can have unique AIs
* New monster: Trapper: leaves traps around the dungeon (14 -> 26) [Test of new AI behavior :D]
* New monster: Stun Shooter: fires homing projectiles that can stun or drain stamina on nude target (12 -> 24)
* Support for homing projectiles
* Projectiles do not disappear with monster's death/change of action
* Reduce memory usage by images
* Monsters can stop patrolling at a specific depth (no more too weak enemies on high depths)
* Avoid redundant checks in data loading sequence (tiny optimization)
* Remove unused LCL library dependency
* A bit faster Living Suit attack: 1.1 -> 1.0 seconds delay
* Reduce Full Suit protection 98% -> 92%
* Amulet of Freedom reduces stamina cost for dash roll 30% -> 50%
* Fix log for movement speed penalty

# 0.40.3247
* Better positioning of prisoners on the map
* Fix equipping amulet of bells using wrong color in log
* Fix bug curious monsters didn't check entrance
* Fix occasional crashes/freezes after overthief teleporting away
* Fix sometimes prisoners put on top of chest
* Exclude nearest area to the entrance from generating chests
* Better formulate enchantments requirements

# 0.40.3222
* Amulet of the Wild requires player also be unarmed (some bondage items also count as unarmed)
* New: Amulet of Berserk: +30% damage, +10% speed, -40% roll stamina cost when health below 33%
* Grabber no longer pulls amulets (and breaking them in one hit)
* Increase amulet damage absorption 1% -> 3% + a bit noisier
* New: Amulet of Bells (restraint), very noisy, can be equipped by tickling slime/aspheroid
* Fix bug that enchantments were counted incorrectly for multi-slot items
* Web increases durability non-linearly (+10% for every new layer)

# 0.40.3203
* Fix inconsistent bondage items behavior (especially those that were "mostly beneficial"), now they have manually assigned debuffs
* Increase noise when rescuing character 24->48
* Pathfinding optimization
* Stacking items enchantments
* More generic handling of enchantment requirements, show specific logs about those
* Increase Amulet of Muffle effect 25%->50%
* Add Amulet of Freedom (reduces dash roll cost by 30% when bare-chested)
* Control spawn frequency of items and monsters (enchanted amulets now spawn 4 times less frequent than regular items, rare monsters spawn 5-10 times less than regular ones)

# 0.39.3173
* Fix occasional error handling Medicus attack

# 0.39.3169
* Split "censored" layer into 3: nipples (2 variants), nethers (3 variants) and pubic hair (4+1 variants)
* Some clothes can hide other layers
* Add invisible tag to traps (they won't show AI state now)
* Grabber can pull amulets
* Fix crash in Debug UI when asking for a new random body
* Fix bug chests deleting extrema points used by AI
* Fix extremas not properly handled by AI at the beginning of the game
* Fix crash when attacking with an item in shoulders slot, but not in top overwear slot
* Fix rare crash when generating mimics

# 0.39.3146
* Enchanted items support (WIP)
* 4 enchanted amulets: one increases damage, second increases speed, third reduces clothes noise and fourth adds 7 damage if character is nude
* New entrance/exit generation algorithm
* 2 new items: Patches Top and Patch Bottom (underwear)
* Add barrel container (and can never be a mimic)
* Split items and bodyparts handling
* Fix shoulder restraints not applying any negative effects (now reduce damage by 5%)
* Fix Ram and Wisp attack log message
* Fix when character teleporting camera animating to new position
* Fix grabber image being too big
* Fix censored map sprite
* Fix log message when undergoing undressing during voluntary check-up at Medicus
* Fix sound not being played when Medicus reinforces restraints
* Fix only currently selected character able to hear monsters
* Fix sometimes invisible monsters breaking long-press/right-click behavior
* Fix player character continuing attacking monster after it teleported away
* Use gloves map sprite image for cloth knuckles
* Reduce Overthief damage
* Reduce Medicus/Archimedicus damage multipliers
* Some "invisible" internal fixes and refactors

# 0.38.3039
* 8 new sprites for items and hair
* Reduce sound volume for fast shooter

# 0.38.3032
* Fix player character unable to wake when wisps attack
* Fix player able to cancel stun or knockback by roll
* Wisps play attack sounds also on disrobing/disarming
* Wisps can damage psyche/willpower only on nude stunned target
* Wisps can disarm only stunned target
* Displayed items durability cannot be zero in compact inventory
* Significantly (roughly 2x) reduce wardrobe malfunction chance on all items
* Significantly reduce towel noise (roughly 4x)
* Workaround rams unable to attack sleeping target

# 0.38.3022
* Hopefully fixed Roll/StealthAttack bug
* Tinkered a bit with movement logic, I don't think I've improved it too much, but hopefully a bit better now

# 0.38.3015
* 2 new monster: Will'o'wisp - spawns wisps (with their assets and behavior logic); lvl.23 - patrol at lvl.25
* 3 new items: underwear set (Double Stripes, Stripe Bottom) and weapon (Small Talon)
* Double disorientation from teleportation
* Nerf living suit: slower attack (0.9->1.1) lower nude vigor damage (5x->2x)
* Undressing bottom is more embarrassing than top (0.12/0.17 was 0.13/0.13)
* Fix bug when stats didn't regenerate if max value gone below zero
* Fix coordinates calculation after teleportation
* Fix symmetric bells on bondage/feet mittens
* Much more prominent visual difference between Ram and Siege Ram

# 0.37.2968
* Better inventory images for most bondage items

# 0.36.2962
* Switch between Full and Compact inventory layout by clicking on character's avatar
* New enemies: Ram and Siege Ram with a new mechanics (ram) and assets
* Generic bondage item icon (to be changed into specific bondage items later)
* Batched rendering of map items (big optimization)
* Return tileset to 11x11 size, it's too laggy on mobile, still waiting for optimization or other changes
* Fix bug with inventory buttons updating sequence
* Fix equip slots summary when showing message about struggling to exhaustion
* Fix typo "Restar This Map"
* Change item slot opacity 100% -> 20%

# 0.36.2918
* Better default zoom
* Shake map on some events
* Change tileset resolution from 11x11 -> 32x32 (temporarily?)
* Ability to blur tileset
* Blur instead of darken some floor tiles
* Colorize 2 monochrome tilesets (to better discern between visible/not-visible)
* Add 4 tilesets
* Fix occasional crash during tileset generation

# 0.36.2888
* Fix stealth attack on running away targets (e.g. Twinkle or Alarmist)
* Fix some items not having transparency in holes in map images
* Weapon always gets damaged by 2, not random 1-3
* Increase red aspheroid health 21 -> 22.5
* Increase Living Suit mark opacity 19% -> 67%

# 0.36.2868
* Rework stats regeneration mechanics a bit:
* Nude top or bottom now doesn't prevent from regenerating max psyche(willpower), only slows it down
* Nude top affects psyche(willpower) regeneration speed much less than nude bottom
* Resting "quality" slowly increases with time
* Separate "resting" and "unconscious" state
* Fix censored status not updating in-game after changing it in options
* Double Twinkle's vigor(stamina) damage when interacting with sleeping dressed target
* Log Twinkle damage when interacting with target sleeping in restraints that cover nudity

# 0.36.2855
* Fix Vacuum Cleaner not becoming aggressive after attacked
* Fix sometimes the rescued character had restraints on

# 0.36.2851
* Fix occasional crash with Alarmist action
* Improve log colors for Living Suit, Binder Slimes and Straps Trap
* Better version label position
* Better monster aggression calculation, use it to improve quality of some actions
* Improve player teleportation and disrobing clothes teleportation
* Improve monsters resetting to idle after teleportation
* Debug UI: Teleport

# 0.36.2837
* Hotfix a rendering bug on Android target

# 0.36.2834
* Rare monsters: Some monster types (thief, overthief, alarmist) will be considered "rare"
* Rare monsters cannot be the only kind of monsters on the level
* Rare monsters numbers are 3 times less than that of regular monsters of other kinds
* Rare monsters can occasionally spawn in patrols in small quantities
* Rare monsters cannot spawn in traps spawns
* Soft Guard: Will return to the guard position until has a direct vision of Player
* 25% of monsters will soft-guard (50% guard), which reduces the quantity of initially active monsters 50% -> 25%
* Alarmist raises alarm with a delay (to enable quick kill to silence it)
* Some additional sprites + small fixes to map sprites for Player Character
* Fix sometimes incorrect scroll position in ground items list
* Fix traps and mimics not resetting to idle with other monsters
* Fix behavior for 2x2 shooting monsters
* Fix some issues with Inventory Buttons (but not all of them yet)
* Debug UI: Drop all items
* Debug UI: Restart current map
* Debug UI: Return Home without capture
* Debug UI: Visualize colliders (navgrid)
* Debug UI: Visualize players & monsters origin points on navgrid
* Larger "Close" button in Debug UI
* Note about Debug UI in README

# 0.35.2793
* Improve shooters behavior. Now they won't shoot into the wall (at least less often :D)
* Improve Bug Shooter teleportation logic. Will more likely pick a favorable position for attack
* Dummy temporary placeholder player character sprite dressup

# 0.35.2776
* Hotfix for occasional crashes when using debug UI

# 0.35.2774
* 2 new monsters: Alarmist and Living Suit (with own action and assets)

# 0.34.2747
* Improvements to specialized dungeon levels feature
* Change texture base tile size to 11x11
* Rework handling of player paperdoll
* Automatically colorize player's map sprite
* Less calls to redraw the inventory - optimization
* Rework Inventory buttons handling (no visible changes, just better code :D)
* Fix too many monsters spawning at upper dungeon levels
* Fix unable to long-press/right-click objects while character is standing still but busy (repairing, equipping, etc.)
* Fix not all monsters were affected by Alarm Trap
* Disable zoom in/out buttons during resting
* Fix broken zoom buttons behavior when disabled
* Fix scroll in ground items resetting to top every frame in some situations
* Fix incorrect "giggle" sound when Medicus interacting with character's neck
* Fix struggle unequip bondage item displaying wrong information on bodyparts freed if item covered multiple of those
* Show logs on stats loss when attacked by Medicus
* Better zoom margins on different device resolution
* Darker floor in Cobalt map (traps were hard to see on mobile display)
* No special treatment for shoulders and neck inventory slots, now they are always shown
* Icon for neck (collar) slot
* Do not show FPS in caption in release builds
* If player character wakes up seeing monsters then game will be set to pause
* Fix grabber not pulling items equipped in shoulders slot
* Fix chests and traps teleporting away if player character was teleported near them.
* Warning to avoid installing DEB package and standalone of different versions
* Reduce Twinkle healing strength by half if the target was not nude
* Increase overall Medicus damage when curing target
* Archimedicus causes significantly higher damage than Medicus
* Significantly increase protection (90% -> 98%) and reduce noisiness (2.0 -> 0.5) of Full Suit
* Most monsters who had naked vision bonus will be more likely to see nude bottom than top (exception: Tickling opponents do the inverse)
* Metal mimic is tougher but slower than wooden one
* Reduce persistent damage caused by Hooks and Disrobing traps on a naked target
* Bug shooter stutters nude target, not only when bugs crawl under bondage items
* Dash roll can be executed as long as Stamina > 0 (was > Dash roll stamina cost, which was not always intuitive as this value can be changed by bondage items)
* Web layers from Web Shooter now add, not multiply (sometimes could easily end up in durability of > 5k, practically permanent)

# 0.33.2678-experimental
* EXPERIMENTAL: Limit quantity of monster types on the level
* Spawn a limited amount of monster kinds on the level
* Only a limited amount of monster kinds can patrol the level
* Monsters quantity (in spawns and patrols) is determined by their danger level
* Load/Save patrol list into savegame
* New chest variations: 4 different images
* New mimic variations: 2 weak (wooden) and 2 strong (metal)
* New map kind: Twins12x12
* Improve tilesets: make floors darker so that traps are visible
* New map tilesets: 3 kinds (Vault, Honey and Brown)

# 0.32.2646
* Increase webbing durability
* Higher shooting rate for web guards
* Fix Twinkle interaction with sleeping dressed target
* Fix potential freeze on Twinkle interaction with sleeping nude target

# 0.32.2642
* Reduce chastity items noise by 30%
* Move bug shooter to lvl. 21 -> 19, Archimedicus, Black and White aspheroids - one level deeper.

# 0.32.2637
* New monster: Bug Guard

# 0.32.2628
* New monster: Bug Shooter (start lvl.21, patrol lvl.25)
* Move Heavy Shooter to lvl.14->12, Web Shooter lvl.16->15
* Fix positioning bug with teleporting opponents
* Better attack sound for mimic

# 0.31.2612
* New monster: Mimic! New action: Push player.
* Fix player able to pathfind into forgotten area
* Allow long/right-clicks only during pause or player character idle
* Fix handling projectiles at low FPS
* Fix sorting of apparel slots in the inventory
* Added a subsystem for handling player character sprite (no visible change yet)

# 0.30.2581
* New method of loading and processing map tilesets, add 9 for diversity
* Increase Blue Aspheroid health 12->13
* Better sound for repairing/unequipping gloves

# 0.29.2549
* Invert bottom over/under slots
* Higher vision bonus for omnigrabber
* Better slow-down when colliding with monsters
* Shake player when almost-immobilized
* Display vigor lost when almost-immobilized
* Improve tickling slime interacting with sleeping target log texts
* Better reference to body part(s) when equipping bondage items

# 0.29.2539
* New item: Claw Gloves
* Twinkle doesn't equip all slots with stardust - but random amount, half on average
* Vigor > 0 can save from getting tied up
* Straps Trap cannot capture player character
* Properly render inventory slots order
* New image for cloak/sleeves inventory slot
* Fix sound for blue guard
* More descriptive guards names
* Better spikes knuckles map icon
* Disable Linux build in CI (to save at least some CI minutes)

# 0.29.2521
* New items: Full Suit and Boxing Gloves
* Internal rework of Damage/Protection handling

# 0.29.2509
* Fix potential crash during map generation
* 5% faster Overthief attack
* Better run-away AI (still flawed)

# 0.29.2503
* New monster: Omnigrabber
* New monster: Overthief
* Rename Thief Slime to "Thief"
* Fix potential crash when there are no slots to apply web
* Minor internal improvements

# 0.28.2493
* Hopefully workaround a series of bugs caused by improper action update, especially on low FPS, not a proper fix yet, but now it shouldn't crash or behave completely erratically
* Fix marks sometimes thrown in wrong spots
* Fix teleportation sometimes teleporting too close
* Fix disrobing trap sometimes throwing clothes too close
* Web shooters start spawn 17->16 level; patrol 26->18
* Significantly higher vision bonus for Web shooters
* Sounds for regular shooters
* Increase medicus noisiness 11.5 -> 15

# 0.27.2485
* Shooters' delay is now approximately half-splitted between warm-up and cool-down
* Default select target closer to the Player, not closer to click location
* Fix rendering of the Exit on forgotten territory
* Fix rendering of the Exit if partially visible
* Fix selecting click margin in Options
* Danger Sensor ignores traps, doesn't detect monsters behind the wall
* Minor grammar fixes
* Small optimizations

# 0.27.2470
* Web projectiles
* Web shooter, Web guard and regular Shooting guard with their assets
* Add shooters some visibility bonus
* Warm-up instead of cool-down shooting

# 0.26.2457
* Projectiles
* Shooting enemies: Fast Shooter, Red Shooter and Heavy Shooter
* Increase traps opacity from 3% to 4%

# 0.25.2450
* Hooks Trap - trap that breaks all clothes on character
* Each trap has individual image

# 0.25.2442
* Fix inventory UI not always updating properly

# 0.25.2438
* Continuous Integration / Continuous Delivery (nightly builds)
* New items icons
* Refactor translation API and logic - game text is read from an XML file
* Refactor Inventory API
* Some fixes to log strings
* Spawner trap and patrols monsters counts increase not so quickly with depth
* Properly report and handle missing translation strings
* Better handling of bondage items reinforcement
* More interesting XP calculation for rescuing (based on dungeon depth and rescue level)
* Rebalance XP a bit
* Reduce tickling aspheriod damage (5 -> 4), increase attack speed for tickling aspheroid and guard by 4% and 3%
* Fix alpha bleeding on "exit" tile
* Fix broken monsters names in the log
* Fix Twinkle incorrect willpower healing calculation when interacting with dressed sleeping target
* Twinkle no longer dies when interacting with sleeping dressed target
* More consistent Twinkle vigor damage (not "punish" player for having high vigor)
* Randomize damage of bondage items on hands by attacking (0.25-0.75)
* Small optimization to initialization sequence
* Add sound to spawner trap
* Rename "Archmedicus" to "Archimedicus"

# 0.24.2349
* Items can use unique map images
* Add temporary map images for all items
* 4 seconds timeout before going down to the next level
* Vacuum cleaner spawns additionally to monster patrol, not instead of
* Better grunting/breathing sounds when struggling with restraints
* Higher vision bonuses for aspheroids
* Reduce twinkle's healing bonus from 10% to 1%
* Move better items deeper into the dungeon
* Log about danger, do not just hide "camp" button
* Fix Exit rendered if it's in "forgotten" visible area
* More consistent characters set handling when loading fonts
* Add "For All" symbol to the in-game fonts and use it in game :)

# 0.23.2314
* Anesthetic trap
* Better tickling slime bondage mittens probability dependence on vigor
* Fix a typo
* Reduce pot mittens durability to 128 (was 250)

# 0.23.2300
* Fix 2 bugs with Twinkle interacting with sleeping dressed target

# 0.23.2297
* Fix playing "easy" dungeon music on levels that are actually hard
* Fix sometimes broken movement destination for monsters when attacking the character
* Fix error when medicus inspects heroine's neck
* Fix medicus unequipping bondage items during voluntary checkup
* Upgrade Castle Game Engine version
* Log how many characters still remain imprisoned on the level after one has been rescued
* Log which bodypart voluntarily equipped bondage items constrain

# 0.23.2281
* New map variant (rare and dangerous)
* Rework bondage mittens equip chance for Tickling Slime/Aspheroid/Guard (much lower on high stamina; twice higher on low stamina) and make it scale with enemy rank
* Fix bug with Binder Slime not being able to tie up neck slot
* Fix incorrect willpower healing calculation when Twinkle interacted with nude sleeping target
* Fix presentation of large numbers in UI
* Fix Grabber pulling and breaking collar
* Collar is "indestructible" (for now), special handling of indestructible items
* Improve wording in collar description
* Add "∀" symbol to collar graphics
* Monster tag "invisible" - do not display AI state for such monsters (for now only Thief Slime)

# 0.23.2269
* New apparel slot: neck
* New item: metal collar (and temporary mechanics around it)
* Rescues get a free unremovable collar :)
* If heroine was not resisting, medicus will not restrain her
* Fix no log shown if medicus finds healthy heroine sleeping
* Patrolling curious monsters spread on the map as soon as possible
* Fix bondage feet mittens sounds

# 0.23.2254
* More diverse interactions for tickling slime:
* - reinforce restrictive hands bondage instead of applying new
* - reinforce feet mittens instead of applying new pair
* - more diverse interaction with bondage items
* Medicus will first restrain hands and only then retreat if health is ok
* Reduce spiked knuckles durability

# 0.23.2247
* Improve curious AI to investigate deadends more systematically + avoid investigating entrance region too early

# 0.23.2242
* New map type: Cage 6x6 (rare, but dangerous)
* Allow narrower map sizes on higher levels
* Allow small niches in the wall
* Fix danger meter counting chests as danger
* Fix danger meter counting enemies in line of sight but far away
* Fix traps not resetting action when disarmed
* Fix Twinkle not resetting action when interacting with sleeping dressed target
* Fix inconsistent treatment of items spawn rarity
* Fix stealth meter not updating properly on action change
* Fix unable to remove medical rack
* Fix broken damage absorption of medical rack

# 0.23.2228
* 1 new body tattoo: fountain
* Stealth meter - black when safe, yellow when unsuspecting monster in sight, red when enemies are attacking
* Optimize paperdoll rendering

# 0.23.2219
* Set game to Pause when there are monsters nearby after loading the game
* Fix multiple bugs related to pause / resting UIs
* Fix twinkle attack in sleep behavior in case the player character is already exhausted
* Fix vacuum cleaner waking up heroine even if she is exhausted
* Add missing sounds to some buttons in pause menu

# 0.23.2203
* Fix one-piece underwear rendering on top of bottom overwear
* Remove range check when disarming traps
* Safer loading of bodyparts
* Small optimization

# 0.23.2196
* Player character customization (currently only randomization)
* Multiple assets for customizations: (1 body, 1 censored, 1 hands overlay, 1 tail, 19 tattoos, 2 ears, 3 eyebrows, 16 eyes (3 shapes), 21 hair (4 shapes), 3 mouths, 3 noses,
* Autogenerate paperdoll image
* Use random body part if body part failed to load
* Fix thief slime stealing from "more covered" body part instead of "less covered"

# 0.22.2150
* New character face
* Crop sounds, convert to WAV, 44 kHz + normalize volume
* Workaround roll/stealth attack behaving poorly at low FPS
* Partially fix Grabber attack during sleep not causing more damage

# 0.22.2128
* Customizable log length (default : 12)
* Fix handling of end-game screen in some marginal cases
* Fix unable to rescue characters
* Nerf Medicus opponent significantly; with new Archmedicus spawning later and being as dangerous as the original one
* Increase Pot Mittens damage from 12.7 -> 17.1 to make them a reasonable weapon

# 0.22.2116
* Map visibility is now fading, hiding monsters that are not directly visible
* Monsters can emit sounds (some monsters are noisier than others, some are extremely stealthy) and marked on map even if not directly visible
* Previous visibility is forgotten after teleporting but can be remembered if discovered a known area
* New item: Metal Fists Mittens (Pot Mittens) - can be found in the dungeon and voluntarily equipped. Those are weak weapons, but are better than some lowest kinds - but are restraining hands, preventing from any other actions.
* Bondage items can have own damage value, set very low damage for bondage mittens and wrists bars
* New characters sounds handling system
* Reduce monsters spawns per level by 8%
* Thief slime prioritizes stealing clothes that cover something, and runs away if uncovered
* Vacuum cleaner can also disarm target if she's naked
* Fix a few typos

# 0.21.2069
* Change monsters speed a bit (first of all, nerf Medicus speed and boost most aspheroids and guards)
* Fix Tickling and Medicus actions not stripping the target forcefully
* Fix typo in one of Medicus logs
* Show total XP for all killed monsters
* Reduce XP for vacuum cleaner

# 0.21.2063
* New opponent: Medicus - applies very restrictive bondage items and restores max vital, hurting other stats - with own game mechanics and assets
* Another failed attempt at fixing chests spawned too close to the exit
* Fix experience not always given if multiple enemies were hit simultaneously and some of them killed
* Fix hit point when attacking multiple enemies
* Fix Tickling Slime not moving unequipped items further away from Player when stripping naked in sleep

# 0.20.2031
* New opponent: Grabber with appropriate mechanics and assets. Starts spawning at depth 7 and starts patrolling at depth 13.
* Player attack now hits all monsters near the targeted one
* Deep sleep mechanics: Rest for 60 in-game seconds (a few seconds realtime) to enter deep sleep and heal faster.
* Fix chests considered in danger detection algorithm
* Fix crash when thief slime stole item while asleep
* Fix potential crash when the Player character is completely tied up
* Fix spelling
* Fix bondage items damage absorption (now it's more often "zero")
* Fix UI not updated when Vacuum Cleaner grabs items from under the Player character
* Fix chests sometimes spawning at exit
* Change sound when pulled from surprise to grunt
* Reduce collision range for Vacuum Cleaner to wake up Player character
* Sound for opening chest
* Binding slime will reinforce only ropes, not other bondage items
* Increase tickling vigor/stamina damage
* Reduce chastity items noise from 5 to 3.5
* Show logs for degrading psyche/mental

# 0.19.1973
* 3 new items: Cloth Knuckles, Brass Knuckles and Spiked Knuckles
* Reduce vacuum cleaner health 128 -> 61
* Remove generic vision bonus from Twinkle, it's ok if it doesn't see naked character
* Debug option to drain 100 vigor from character

# 0.19.1963
* Change map size progression. Quickly get to 7000 tiles and then advance slowly (trying to keep one dungeon level length in 5-15 minutes)
* Fix map proportions randomization
* Rebalance monsters spawns with depth
* More control over spawns and patrols
* Guards always spawn of different kind and never more than 4
* Have at least one of each kind of monsters generate on the dungeon map
* Different kinds of monster vision (can see naked/dressed heroine better under some circumstances) - now used by Cyan, Yellow, Thief slimes, Twinkle and guards
* Monsters are not too active at the beginning of the map
* Generate twice fewer traps
* Change patrols delay from 180 to 150 seconds to accommodate for smaller maps
* Increase twinkle's health, so it can't be killed by a bare-handed stealth attack
* More diverse reactions to twinkle's attacks
* More consistent vigor drains after interacting with twinkle in sleep
* Cannot wake up if sleeping exhausted (e.g. after interacting with twinkle or after a nice tickling session) taking damage will force to wake up
* Fleeing behavior significantly improved (for thief and twinkle)
* Thief slime never spawned in spawner trap or patrol
* Add item noise to many actions, weapons make noise on attacks (now fighting with golden bells on is very, very loud)
* Reduce bondage mittens noisiness (it was almost a certain gameover)
* Reduce player's attack cooldown (it's a broken mechanic for now, but needs a larger change to fix)
* Gray slime - generic very weak opponent for upper levels
* Play surprise sound when cyan slime equips bondage mittens or drags heroine closer
* Play sound when stardust is applied in sleep
* Show logs for vigor/psyche damage
* Reduce tickling damage to psyche
* Lower Tickling Slime damage
* Rename yellow slime to Binder Slime
* Fix item durability displayed "0" sometimes

# 0.18.1907
* Twinkle will also interact with heroine sleeping naked
* Make Twinkle a bit faster
* Cyan slime has also a chance to equip bondage mittens on vigor > 0 character
* Fix Alarm Trap alerting Vacuum Cleaner
* Fix incorrect zoom calculation when determining click accuracy

# 0.18.1901
* Twinkle monster with several "Punish Clothes" mechanics and all necessary assets
* Stardust "clothes" - 7 items
* Cyan slime will also try to equip bondage mittens on exhausted heroine + rebalance vigor damage
* Fix cyan slime sometimes can accidentally drag player character into a blocked tile
* Better calculation of and configurable click margin in Options
* Special log message for tickle-disarming
* Fix chests blocking movement
* Better empty chest log
* Increase vacuum cleaner timeouts, it was too quick
* Wake up heroine on collision with vacuum cleaner
* Increase spiked club damage
* Fix delay between grunts when struggling against bondage
* More limited guards spawns
* Do not increase monsters damage with depth - just increase static damage values
* Silently stealing items doesn't count as forced disrobing
* Character won't even start struggling if exhausted
* Exit warns about patrol spawining in around 10 seconds
* Fix stealth attack can be canceled by clicking
* Some smaller fixes and improvements

# 0.17.1846
* Tickling action and cyan slime that makes a good use of it, all necessary assets
* Implement Curious AI for cyan slimes
* Disrobing and stealing items doesn't wake up heroine
* 2 new items: Bondage Mittens and Bondage Feet Mittens
* Bondage Mittens prevent character from performing most of the tasks until those are removed
* Cyan slime can equip bondage mittens on player character in sleep or if holding by another bondage item
* Detained characters are now protected by Guards (Black, Red, Cyan and Yellow)
* Vacuum Cleaner monster with own vacuum cleaner AI that collects items scattered around the map
* 80% of items are generated in chests, mostly in deadends and nooks
* Hitting chest doesn't degrade the weapon
* Increase distance for monsters spawn even further from entrance (still doesn't seem enough)
* Rebalancing of bondage items
* Increase chance of equipping new bondage items over reinforcing old ones
* Update tutorial for new monsters and mechanics (3 new pages), proof-reading
* Improve log readability
* Reduce mental damage from stripping
* Apply mental damage only if attacker hits uncovered private parts + better log message
* Fix killing already dead monsters by debug command
* Fix stealth attack not provoking monsters to fight back
* Fix saving of uninitialized complex actions
* Fix thief slime stops running away to investigate noise
* Fix characters list size in Pause menu
* Other small fixes and improvements

# 0.16.1731
* 7 new items: Belt Top, Belt Bottom, Spiked Club, Footwraps, Miniskirt, Sleeves, Old Cape (unique item given only to newbies)
* Again rebalance enemies initial spawn distance (hopefully this time did it right)

# 0.16.1708
* Reduce volume of breath noises
* Reduce enemy alert range
* Increase distance from monsters to the entry point

# 0.16.1702
* Add some funny noises when character loses clothes, gets tied up or struggles with ropes (11 surprised, 5 heavy breathing, 12 grunting)
* Fix enemies timeouting from chasing the player too quickly and not alerting other enemies while passing by them (this might be a bit unbalanced now)

# 0.16.1691
* New character skin (for now cannot be changed or customized, just replaced the previous one)
* Fix alpha bleeding
* New map pattern 13x13
* Rescue action is very noisy now
* Highlight items that are likely to break
* Mark active character in Pause menu
* Fixes to log messages

# 0.15.1671
* Captured characters are now located on the dungeon level and need to be rescued (to free and rescue: long-press or right-click them)
* Sounds handling for rescuing process
* Updated tutorial page
* Show log on captured characters at current dungeon level
* Cut some corners on 6x6 and 8x8 map patterns
* New 11x11 map pattern
* Teleportation and disrobing traps now teleport character or stuff into a remote place, preferably unexplored
* Highlight items that are likely disintegrate
* Better info in characters list
* Debug option to hit character for 10 and 100 damage
* More order with actions logs (E.g. disarm trap now shows log when disarm actually starts, not long before)
* Logs for resting
* New attack mark for aspheroids
* Some small fixes and optimizations

# 0.14.1625
* Entrance is generated only on map corners, to avoid player being surrounded in the beginning
* Reduce target map free area a bit
* Fix bug with movement and large update times (especially during resting)
* Better wall sliding during action roll (still not perfect fix, but works better than before, yet the character still gets stuck sometimes)

# 0.14.1613
* New logic of objects positioning in the dungeon (Warning: Experimental!)
* Rework and optimize monsters/items generation
* Player can now start at random point in the dungeon
* Increase monsters damage with depth (Temporary / Experimental)
* Increase white slime attack time (now it's absolutely deadly, maybe a bad idea)
* Double slime/traps ropes strength (maybe a bad idea)

# 0.13.1592
* When Player character is attacked in sleep the game wakes up in active pause mode
* Buttons to toggle Party and Menu in Pause mode
* Drag map in active pause to pan around
* Support for clicks and long/right-clicks during active pause
* Rubberbanding of map center to Player character or dragging coordinates

# 0.12.1581
* Click accuracy now depends on zoom level
* If multiple objects are clicked within the accuracy radius, click will pick the one clicked most accurately
* Fix bug with items sometimes calculating their position wrong
* Fix/improve log messages for disrobing and teleporting traps

# 0.12.1573
* Properly reset monsters state after teleporting
* Increase disarm trap range (fix sometimes accidentally triggering trap instead of disarming it)
* Invalidate UI when resizing window
* A few more minor fixes

# 0.12.1566
* Now if character is too far away from the trap - no need to accurately move closer, she will carefully come closer herself (accidentally stepping into traps was very annoying on mobile :))
* Fix issue with some tops not properly reacted to by bondage attacks
* Fix problems with unresponsive to long press inventory buttons
* Workaround possible crash when loading game if monster immediately goes to investigate player's position

# 0.12.1560
* Fix UI in case there are too many items on the ground
* Fix longclick behavior on inventory buttons
* Fix bug with broken current character action loading
* Safer loading of actions
* Improve parameters of two-slot underwear (2 items)

# 0.12.1547
* Add 4 monsters types:
* White slime - very high damage but very low health
* White aspheroid - extremely high damage but low health
* Black slime - low damage but a huge pool of health
* Black aspheroid - enormous amount of health
* Add 3 clothes item:
* Chasity bra and Chasity belt: bondage items that properly cover private parts but offer almost no protection in contrast to Corsette that did exactly the opposite
* High boots - better version of boots
* Add 4 high-level weapons:
* Shortsword, Longsword, Broadsword, Axe
* Set default volume to 50%
* Fix possible crash when spawning enemy patrols
* Fix possible crash when spawner trap spawns enemies
* Fix white slime animation
* Fix potential crash when monster is teleported or loaded outside of the map
* Fix & improve FreeDesktop comment

# 0.12.1513
* Splash screen
* Add 2 music tracks (one for upper dungeon levels, one for deeper)
* Minor fixes

# 0.12.1508
* Double stats degradation rate
* Disrobing trap doesn't scatter items, but teleports them in one pile
* More accurate item durability display (x/y, not 50%)
* Monsters patrols go to random point
* Move FPS label to center
* Debug UI button to kill all monsters and traps on the map
* Fix debug UI closing when going to next map
* Fix possible crash when resting

# 0.12.1495
* Fix bug with alarm trap (caused monsters to save incorrectly)
* Safer saving/loading game
* Option to end current run (in case game crashes at start)
* Add debug options to capture current character

# 0.12.1463
* Fix film grain effect on Android devices and make it a bit more intense.

# 0.12.1458
* Screen effects - Film grain.
* Toggle Screen effects in Options.
* Change LimitFPS value. Default to 60.
* Scrollable Options screen.
* Don't show decimal fraction of FPS.
* Fix font for label "Vibration".

# 0.12.1436
* Debug UI (cheat mode :)): show FPS, show map, spawn monsters, go to next dungeon level, reset character (heal 100%), add random items, level-up.
* Limit FPS to 60 (instead of 100, will soon be adjustable in Options)
* Do not clear background (optimization)
* Save game on quit on Desktop
* Better calculation of music track type for the dungeon map
* Fix calculation of noise when manipulating items
* Fix Audionautix and MaxStack attribution
* Some other small fixes/improvements/WIPs

# 0.11.1402
* 12 new dungeon music tracks (for easier levels)
* Improve readme

# 0.11.1385
* Monsters now have "spawn levels" - stronger monsters spawn deeper in the abandoned mine.
* 5 high-level enemies (aspheroids) that spawn after 10, 12, 14, 16 and 18 levels.
* XP from struggling against bondage is now proportional to restraints difficulty.
* Update some tutorial entries.
* Small fixes and improvements.

# 0.11.1366
* New stealth mechanics! - now different items and actions have significantly different noise levels.
* Visualize current noise levels and detection range for player character.
* Visualization of enemy AI state - idle, cautious, attacking
* Tutorial on new stealth mechanics
* Stealth attack on short click too, it was very hard to perform on touch devices
* Alarm trap - notifies monsters of the current position
* Update tutorial on traps
* Spawner trap's monsters now chase player for longer time
* Nerf spawner trap a bit
* Make particles size aware of UI scale (they were too small on high-DPI devices)
* Increase glass bottle damage

# 0.10.1328
* Visual feedback on most combat events, such as dealing or taking damage, getting tied up or items stolen
* 4 more capture stories (1 for defeat by Psyche, 3 for defeat by Vitality)
* Spawner trap - spawns multiple monsters when triggered
* Teleport trap no longer disarms player
* Bonus per level reduced: Vitality: 10 -> 5; Vigor 15 -> 10; Psyche 5 -> 5
* Reduce amount of generated loot 75% -> 58% (50% was too little; 75% too many)
* Reduce time to disable trap 3s -> 1.5s
* Reduce time to repair item 5s -> 4s
* Increase rapid unscheduled disrobing damage 10% -> 18% (25% was too much)
* If item is broken, repair it when trying to equip
* Fix asymmetric colliders bug, return proper margin for picking up items
* Fixed inconsistent punctuation in logs
* Fixes to How to Play
* Fix bug that caused traps to hurt performance
* WIP - reworking of actions and marks and some other non-user-visible changes :)

# 0.10.1286
* Long-press/right-click to repair items on the ground.
* New item repairing formula: the more repaired, the less durability is wasted.
* Experience for repairing items depends on how much was actually repaired
* Do not clear visible after teleporting
* Reduce psyche damage from rapid unscheduled disrobing 25%(44%) -> 10%(19%)
* WIP - reworking of actions and marks and some other non-user-visible changes :)

# 0.10.1262
* Add 1 article of clothes: Heart Dress - high protection, but low durability.
* Keep weapon after teleporting (it was way too punishing)
* Fix teleport trap sometimes teleporting near monsters or into another trap.
* Fix character appearing undressed on the first frame of the game.
* Fix UI not updating when thief slime drops stolen items.
* Fix restraints penalty not applied to actual amount of vigor used for dash roll.
* Reduce XP from disarming trap from 10 to 5.
* Make traps a tiny bit harder again. With disarm trap skill they're too easy again.

# 0.10.1246
* Stealth attack: while undetected, long-press or right-click to perform a stealth attack on monster, dealing massive damage
* Disarm traps: stand close enough and long-press or right-click the trap
* Nerf traps a bit.

# 0.10.1231
* Add 6 more items: Velvets, Splittop, Waisttowel, Bathwear, Spartan, Corsette (bondage item)
* Volume for ropes sprites
* Rework how bondage items durability is calculated (was 1 durability = 1 vigor; now 1 durability = 5 vigor to break)
* Items rarity - some items now spawn only on deeper dungeon levels
* As now there are more trash items now and more ways to lose them - generate 25% more items overall
* More logs; logs about multipliers due to restraints; fixes to some logs and colors
* Add experience for fixing items
* A few other small fixes/improvements

# 0.10.1184
* Proper hotfix for spawning traps instead of monsters (they were just freezing instead in the previous fix)
* A few minor fixes.

# 0.10.1181
* Hotfix: Don't spawn traps as monsters patrols

# 0.10.1179
* Traps! Triggered once by player character stepping on them (don't!). With bells and whistles.
* Spike trap: Hurts player character for 30 damage
* Straps trap: Ties the player character up
* Teleport trap: Teleports the character to a random area leaving all equipped items behind (bondage items are fine though)
* Disrobing trap: Undresses the character completely and scatters clothes around the dungeon
* Update tutorial on the topic
* Fix some issues with marks and monsters rendering
* Game over sound
* Fix reinforced bondage weaker than it should be.
* Slower Psyche regeneration
* Resting speed is now slower, as regeneration speed now allows for that.
* Fix Thief Slime returning to guard the guard post after fleeing.
* 2 new ropework-related stories for end game screen.

# 0.9.1146
* Make Thief Slime a tiny bit more dangerous
* Improve UI and logs color scheme, fix colors consistency
* Resting doubles regeneration speed
* All Rapid Unscheduled Disrobing (RUD) events (item breaking, wardrobe malfunctions, stealing items) are now treated properly, show proper logs and apply appropriate psyche damage
* Disrobing damages fraction of psyche, not fixed value (it was absolutely unfair against newbies to the point it was safer to go naked, than to get undressed in combat)

# 0.9.1126
* Long click/tap or right click on the map will log information about items or monsters there
* If the ropes are just applied they're much weaker, and become dramatically stronger if reinforced
* Minor improvements

# 0.9.1109
* Small rebalance of thief slime: prioritizes clothes to steal, stealing weapon only when nothing more is left
* A bit better run away AI
* Don't drop items on exit, where Player can't pick them up
* Fix tutorial pages for monsters
* Rebalance monsters a tiny bit
* Make clothes damage by yellow slime consistent and non-hardcoded

# 0.9.1099
* Proof reading of in-game and out-game texts and tutorial image
* Fix tied up hands playing wrong attack sound
* Implement actions and marks data - important internal system
* Fix bug when in some cases the story about ropes was displayed even if the heroine wasn't completely tied up
* Fix bug when sometimes monsters reinforcing knots had very little effect
* Minimalistic stealth system - the less items equipped, the small the distance when the monster notice player character
* New enemy: Thief Slime who will try to steal items from player character and run away. Kill it to recover stolen items.
* And overall subsystem so that monsters now can have loot items dropped on death and properly saved in savegame.
* Manually set XP reward for monsters, automatically calculated values were bad.

# 0.8.1050
* Walking/dashing through enemies now 67% slower (kinda monsters blocking path and preventing from easily speedrunning the dungeon level)
* Reduce penalties for bondage slots a bit:
* Chest and waist now have only 5% penalty instead of 10%
* Player can dash roll always, instead vigor cost increases by 10%, 20% and 30% correspondingly if waist, legs and feet are tied up
* Logs now show which body part was tied up or freed
* Enemies can now (significantly) tighten already attached restraints on the Player character instead of adding new ones
* Breaking out of bounds now grants experience
* Remove random chance from escaping bondage
* Fix wardrobe malfunction chance depending on FPS
* Degraded stats are now displayed in red to properly show current maximum of the stats

# 0.7.1029
* Bondage system:
* New monster - Yellow slime, that can severely damage player character's clothes and tie her up
* Tying up top parts reduces player's damage (up to 31% penalty), tying up bottom parts - speed (up to 31% penalty)
* Bound feet and legs also prevent from using dodge roll
* Rope ties need a lot of vigor to struggle out
* Yellow slime can reinforce bounds, making them much stronger
* Special marks for Yellow slime, can appear behind the player character
* Resting system:
* Player can now pass time quickly to recover stats. Especially useful when struggling against bondage.
* Add 3 pages of tutorial for new systems
* Wardrobe malfunction: Some items can now... just slip off during action.
* Fix bug with resetting enemy spawn timer on save/load.
* Add 2 new inventory items: crosstie top, bottom
* clothes use less inventory slots when possible
* Unrepairable items (broken bottle for now)
* Mouse wheel zooms in/out
* Tons of small improvements and bugfixes
* WIP - refactor and save/load of actions (this feature is still unstable, you may see some errors when loading the game, they should not cause any damage though)

# 0.6.892
* How to Play section in Main Menu and Pause Menu explaining basics of the game
* One more clothing item (One-split)
* Minor UI fixes
* Update credits
* Workaround 2 potential bugs
* Special sound for starting the game.

# 0.5.864
* Add 2 music tracks
* Avoid "experience to next level = 0" glitch
* A bit harder lower dungeon levels (more monsters spawns)

# 0.5.856
* Safer loading of obsolete save games (will try to avoid errors and force to start a new game only if something went really badly wrong)
* Pause menu doesn't block interaction with the game
* Add 1 item (weapon: poker)
* Multiple small fixes, workarounds and updates

# 0.5.839
* Rebalance items and psyche regen.
* Multiple bugfixes, including a savegame breaking bug with navigation grid error.

# 0.5.829
* New dungeon generation algorithm: Stamp. I still don't like it, but at least it's now clear where the Player character can go. 4 stamps: Town, 6x6, 8x8, 10x10.
* Crop map borders if they are not used (tiny optimization)
* Fix render bug of large maps on Android.
* Some items rebalance.
* Stats gains with level-up rebalance.
* Multiple small fixes and improvements.

# 0.4.802
* Capture and rescue mechanics. For now very primitively: if characters runs out of Vitality or Psyche she gets captured. If some next character successfully exits the dungeon level on which she was captured - she is rescued and is returned to the settlement, where she is available for choosing through Pause menu.
* Draw all player characters on the map
* Temporary capture story.
* UI for selecting characters.
* Multiple characters support.
* Store references to every object in game.

# 0.3.778
* Levels require twice as much experience
* Autosave every 30 seconds to avoid losing progress in case of a crash (which is still possible in Prototype)
* Fix installation issue on Android 12 (huge thanks to Michalis Kamburelis, author of the Castle Game Engine, for fixing it and to Deathcomes18 for reporting the issue and testing the fix!)

# 0.3.773
* Basic progression system (monsters give XP, level-up grants +10 Vitality, +10 Vigor and +10 Psyche)
* Save game version detection (to detect incompatible savegames and warn that it won't be able to load)

# 0.2.760
* Randomize monsters patrols better
* Better monsters visibility detection
* Monsters stop chasing player character after defeated her

# 0.2.751
* Save system. Not perfect yet, but mostly working already.
* Dummy end-game state.
* Add 2 items: bikini top/bottom
* Other small fixes and improvements

# 0.1.726
* Add 1 clothing item (poncho)
* Add 2 weapon items (broken bottle and small knife)
* Add 1 new monster type - purple slime, ranged attack
* Make blue slime larger and slower
* Do not trigger level exit unless Player steps on the stairs
* All monsters attacks have a bit longer reach, but are a bit slower
* Fix several bugs and crashes
* Working on saving the game (still unstable, disabled for now).

# 0.1.687
* Fix vibration on Android

# 0.1.683
* Hotfix for freeze on Android (temporarily disable vibration)
* Randomization for UI shaker

# 0.1.675
* First prototype release
